/**
 * *** NOTE ON IMPORTING FROM ANGULAR AND NGUNIVERSAL IN THIS FILE ***
 *
 * If your application uses third-party dependencies, you'll need to
 * either use Webpack or the Angular CLI's `bundleDependencies` feature
 * in order to adequately package them for use on the server without a
 * node_modules directory.
 *
 * However, due to the nature of the CLI's `bundleDependencies`, importing
 * Angular in this file will create a different instance of Angular than
 * the version in the compiled application code. This leads to unavoidable
 * conflicts. Therefore, please do not explicitly import from @angular or
 * @nguniversal in this file. You can export any needed resources
 * from your application's main.server.ts file, as seen below with the
 * import for `ngExpressEngine`.
 */

import 'zone.js/dist/zone-node';

import * as express from 'express';
import {join} from 'path';
import { Response } from 'express';
import { RESPONSE } from '@nguniversal/express-engine/tokens';

/**
 *
 *
 * Setting windows globals for Server-Side (node App)
 *
 *
 */
// const Window = require('window');
//
// const window        = new Window();
//
// const { document }  = new Window();
//
// global['window']    = window;
// global['document']  = document;

// const domino            = require('domino');
// const window            = domino.createWindow('<h1>Hello world</h1>', 'http://example.com');
// const document          = window.document;
// global['window']        = window;
// global['document']      = document;
// // global['DOMTokenList']  = window.DOMTokenList;
// // global['Node']          = window.Node;
// // global['Text']          = window.Text;
// // global['HTMLElement']   = window.HTMLElement;
// // global['navigator']     = window.navigator;
//
// Object.defineProperty(window.document.body.style, 'transform', {
//   value: () => {
//     return {
//       enumerable: true,
//       configurable: true,
//     };
//   },
// });
// global['CSS']   = null;
// // global['XMLHttpRequest'] = require('xmlhttprequest').XMLHttpRequest;
// global['Prism'] = null;
//
// const fakeStorage: Storage = {
//   length: 0,
//   clear: () => {
//   },
//   getItem: (_key: string) => null,
//   key: (_index: number) => null,
//   removeItem: (_key: string) => {
//   },
//   setItem: (_key: string, _data: string) => {
//   }
// };
//
// (global as any)['localStorage'] = fakeStorage;

/***********  End of Globals ******************/


var compression = require('compression');

// Express server
const app = express();

const PORT = process.env.PORT || 4000;
// const PORT = process.env.PORT || 80;
// const PORT = process.env.PORT || 80;
const DIST_FOLDER = join(process.cwd(), 'dist/browser');

// * NOTE :: leave this as require() since this file is built Dynamically from webpack
const {AppServerModuleNgFactory, LAZY_MODULE_MAP, ngExpressEngine, provideModuleMap} = require('./dist/server/main');

// Our Universal express-engine (found @ https://github.com/angular/universal/tree/master/modules/express-engine)
app.engine('html', ngExpressEngine({
  bootstrap: AppServerModuleNgFactory,
  providers: [
    provideModuleMap(LAZY_MODULE_MAP)
  ]
}));

app.set('view engine', 'html');
app.set('views', DIST_FOLDER);

// For comperession
app.use(compression());

// Example Express Rest API endpoints
// app.get('/api/**', (req, res) => { });
// Serve static files from /browser
app.get('*.*', express.static(DIST_FOLDER, {
  maxAge: '1y'
}));

// // All regular routes use the Universal engine
// app.get('*', (req, res) => {
//   res.render('index', { req, res });
//
//   // res.render('index', {req, res},
//   //   (error, html) => {
//   //     if(error)
//   //       // error handle
//   //     if (!res.headersSent) {
//   //       res.send(html);
//   //     }
//   //   });
// });

app.get('*', async (req, res) => {
  res.render('index.html', {req, res, providers: [
      {
        provide: RESPONSE,
        useValue: res,
      },
    ]}, (error, html) => {
    if (error) {
      console.log(`Error generating html for req ${req.url}`, error);
      return (req as any).next(error);
    }
    res.send(html);
    if (!error) {
      if (res.statusCode === 200) {
        //toCache(req.url, html);
      }
    }
  });
});

// Start up the Node server
app.listen(PORT, () => {
  console.log(`Running Node Express server listening on http://localhost:${PORT}`);
});

// Optimization to preload all the JavaScript modules. We don't want to server push or preload them
// too soon, as that negatively impacts the Google PageSpeed Insights score (which is odd, but true).
// Instead, we start to load them once this file loads.
// let paths = window.preloadJavaScriptPaths || [],
//     body = document.querySelector('body'),
//     element;
// paths.forEach(path => {
//     console.log("path: ",path);
//     element = document.createElement('link');
//     element.setAttribute('rel', 'preload');
//     element.setAttribute('as', 'script');
//     element.setAttribute('crossorigin', 'anonymous');
//     element.setAttribute('href', path);
//     body.appendChild(element);
// });

let t,e,r,o=window.preloadJavaScriptPaths||[],
  i=document.querySelector("body");
  console.log("t,e,r,o: ",t,e,r,o);
  for(e=0;e<o.length;e++) {
    r=o[e],
    t=document.createElement("link"),
    t.setAttribute("rel","preload"),
    t.setAttribute("as","script"),
    t.setAttribute("crossorigin","anonymous"),
    t.setAttribute("href",r),
    i.appendChild(t);

    console.log("t,e,r,o: ",t,e,r,o);
  }

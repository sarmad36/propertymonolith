// $(".remove-this a").on("click",function() {
// 	$(this).parent("span").parent("div").addClass("af-none");
// });
 $(document).ready(function() {
    $("body").tooltip({ selector: '[data-toggle=tooltip]' });
});

// //
// window.dataLayer = window.dataLayer || [];
// function gtag(){dataLayer.push(arguments);}
// gtag('js', new Date());
//
// gtag('config', 'UA-132356604-9');
//
//
// function set_ProfessionalService_Schema() {
//     var script = document.createElement('script');
//     script.type = 'application/ld+json';
//     script.text = JSON.stringify({
// 				"@context": "http://schema.org",
// 				"@type": "ProfessionalService",
// 				"name": "Gharbaar",
// 				"address": {
// 				"@type": "PostalAddress",
// 				"streetAddress":  "16th Floor Tower B, Centaurus Mall, F-8, Islamabad",
// 				"addressLocality": "Islamabad",
// 				"addressRegion": "Punjab",
// 				"postalCode": "44220"
// 				},
// 				"sameAs" : [
// 					"https://www.instagram.com/gharbaar.official/",
// 					"https://www.facebook.com/gharbaarOfficial/?modal=admin_todo_tour",
// 					"https://twitter.com/Gharbaar_92",
// 					"https://www.youtube.com/c/gharbaarofficial"
// 				 ],
// 						"aggregateRating": {
// 				"@type": "AggregateRating",
// 				"ratingValue": "4.8",
// 				"ratingCount": "146",
// 				"bestRating": "5",
// 				"worstRating": "1"
// 				},
// 				"logo": "https://gharbaar.com/assets/site/images/logo_ghar.svg",
// 				"image": "https://gharbaar.com/assets/site/images/banner.jpg",
// 				"email": "info@gharbaar.com",
// 				"telePhone": " 051-8746136",
// 				"url": "https://gharbaar.com"
// 			});
//     document.querySelector('body').appendChild(script);
// }
//
// // Call Function to set Scheme
// set_ProfessionalService_Schema();
//
// function set_FAQPage_Schema() {
// 	var script = document.createElement('script');
// 	script.type = 'application/ld+json';
// 	script.text = JSON.stringify(
// 		{"@context":"https://schema.org","@type":"FAQPage","mainEntity":[{"@type":"Question","name":"What is Blue World City and where is it located?","acceptedAnswer":{"@type":"Answer","text":"Blue World City is a mega housing project planned and developed under a Chinese development authority. It is located at Chakri Road in Rawalpindi."}},{"@type":"Question","name":"Does Blue World City have any payment installment plans? If yes then what are they?","acceptedAnswer":{"@type":"Answer","text":"Blue World City offers easy to follow and pay monthly payment installment plans that are designed to assist the people for easy investment. The installment plans are available in 3 and 4 years payment plans."}},{"@type":"Question","name":"What are the development charges? Are they included in the price?","acceptedAnswer":{"@type":"Answer","text":"Development charges are separate and they are not included in installments."}},{"@type":"Question","name":" If I book a plot, when will I get its complete possession?","acceptedAnswer":{"@type":"Answer","text":"Since the construction is in process, the possession is not available at this time, except in the phase 1 of the society. The possession is expected to be given in 3 to 4 years, after the complete project’s development."}}]}
// 	);
// 	document.querySelector('body').appendChild(script);
// }
//
// // Call Function to set Scheme
// set_FAQPage_Schema();

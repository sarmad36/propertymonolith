import { NgModule } from '@angular/core';
import { CommonModule, } from '@angular/common';
import { BrowserModule  } from '@angular/platform-browser';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { AuthGuard } from './services/_helpers';

import { SiteLayoutComponent } from './Layouts/site-layout/site-layout.component';
import { UserDashboardLayoutComponent } from './Layouts/user-dashboard-layout/user-dashboard-layout.component';
// import { UserLayoutComponent } from './Layouts/user-layout/user-layout.component';
// import { AuthLayoutComponent } from './layouts/auth-layout/auth-layout.component';
// import { DashboardComponent } from './UserPanel/pages/dashboard/dashboard.component';

import { PageNotFoundComponent } from './Site/pages/page-not-found/page-not-found.component';

// Project Pages
import { BlueWorldCityComponent } from './Site/pages/blue-world-city/blue-world-city.component';
// import { SkyparkoneComponent } from './Site/pages/skyparkone/skyparkone.component';
// import { HotOfferComponent } from './Site/pages/hot-offer/hot-offer.component';
// import { OverseasBlockComponent } from './Site/pages/overseas-block/overseas-block.component';
// import { PaymentPlanComponent } from './Site/pages/payment-plan/payment-plan.component';
// import { CapitalsmartcityComponent } from './Site/pages/capitalsmartcity/capitalsmartcity.component';

const routes: Routes = [
  // {
  //   path: '',
  //   // redirectTo: 'login',
  //   redirectTo: '',
  //   pathMatch: 'full',
  // },

  {
    path: 'dashboard',
    component: UserDashboardLayoutComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: '',
        // loadChildren: './layouts/user-layout/user-layout.module#UserLayoutModule'
        loadChildren: () => import('./layouts/user-dashboard-layout/user-dashboard-layout.module').then(m => m.UserDashboardLayoutModule)
      }
    ]
  },

  // {
  //   path: 'blue-world-city',
  //   // loadChildren: () => import('./Site/pages/blue-world-city/blue-world-city.module').then(m => m.BlueWorldCityModule)
  //   loadChildren: () => import('./Site/pages/blue-world-city/blue-world-city.module').then(m => m.BlueWorldCityModule)
  //   // component: BlueWorldCityComponent,
  //   // // canActivate: [AuthGuard],
  //   // children: [
  //   //   {
  //   //     path: '',
  //   //     loadChildren: () => import('./Site/pages/blue-world-city/blue-world-city.module').then(m => m.BlueWorldCityModule)
  //   //   }
  //   // ]
  // },

  { path: 'blue-world-city',                                  component: BlueWorldCityComponent },
  { path: 'blue-world-city/success',                          component: BlueWorldCityComponent },

  // { path: 'blue-world-city/hot-offer',                        component: HotOfferComponent },
  // // { path: 'blueworldcity/hot_offer',                          redirectTo: 'blue-world-city/hot-offer' },
  // { path: 'blue-world-city/overseas-block',                   component: OverseasBlockComponent},
  { path: 'blue_world_city/overseas_block',                   redirectTo: 'blue-world-city/overseas-block' },
  { path: 'blueworldcity/overseas_block',                     redirectTo: 'blue-world-city/overseas-block' },
  { path: 'blueworldcity',                                    redirectTo: 'blue-world-city' },
  { path: 'blue_world_city',                                  redirectTo: 'blue-world-city' },
  // { path: 'skyparkone',                                       component: SkyparkoneComponent },
  // { path: 'skyparkone/payment-plan',                          component: PaymentPlanComponent },
  //
  // { path: 'capital-smart-city',                               component: CapitalsmartcityComponent },
  // { path: 'capital-smart-city/success',                       component: CapitalsmartcityComponent },

  {
    path: '',
    component: SiteLayoutComponent,
    // canActivate: [AuthGuard],
    children: [
      {
        path: '',
        loadChildren: () => import('./Layouts/site-layout/site-layout.module').then(m => m.SiteLayoutModule)
      }
    ]
  },

  { path: '404',                                              component: PageNotFoundComponent },
  { path: '**',                                               component: PageNotFoundComponent },
  // { path: '**',                                               redirectTo: '404' },

  {
    path: 'login',
    // redirectTo: 'login',
    redirectTo: '',
    pathMatch: 'full',
  },

  // {
  //   path: '**',
  //   // redirectTo: 'login'
  //   redirectTo: ''
  // }
];

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    RouterModule.forRoot(routes,{
      useHash: false,
      // preloadingStrategy: PreloadAllModules,
      initialNavigation: 'enabled'
    })
  ],
  exports: [
  ],
})
export class AppRoutingModule { }

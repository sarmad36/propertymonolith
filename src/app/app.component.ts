import { Component, ApplicationRef } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  public isProjectPage      : any;
  public showDashboard_Nav  : any = false;
  public map_togfooter      : any = false;

  // Subscriptions
  public routerSubscription : any;

  constructor(
    public router : Router,
  ){
      this.routerSubscription = this.router.events.subscribe((val) => {
        if(val instanceof NavigationEnd) {
          if(val.url.indexOf('blue-world-city') !== -1)
            this.isProjectPage = true;
          else
            this.isProjectPage = false;

          //
          if(val.url.indexOf('dashboard') !== -1)
            this.showDashboard_Nav = true;
          else
            this.showDashboard_Nav = false;

          if((val.url.indexOf('sale') === 1 || val.url.indexOf('rent') === 1 || (val.url.indexOf('find-pro') === 1 && val.url !== '/find-pro')) && (window.innerWidth > 768))
          {
            this.map_togfooter = true;
          }
          else{
            this.map_togfooter = false;
          }
   			}
	    });
  }

  ngOnDestroy() {
    if (typeof this.routerSubscription != 'undefined') {
      //prevent memory leak when component destroyed
       this.routerSubscription.unsubscribe();
    }
  }
}

import { Renderer2, Component, OnInit, Inject, ElementRef  } from '@angular/core';
import { AppSettings } from '../../../services/_services';
declare var window;

@Component({
  selector: 'app-g-maps',
  templateUrl: './g-maps.component.html',
  styleUrls: ['./g-maps.component.css']
})
export class GMapsComponent implements OnInit {

  constructor() {
      if(typeof $ != "undefined" && !$('#gmaps_script')[0]) {
        // Inject Maps Api's Script
        $('body').append('<script id="gmaps_script" async defer src="https://maps.googleapis.com/maps/api/js?key='+AppSettings.GOOGLE_MAPS_KEY+'&libraries=geometry,places"></script>');
      }
  }

  ngOnInit() {
  }


  ngAfterViewInit(): void {
    //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
    //Add 'implements AfterViewInit' to the class.
  }

  ngOnDestroy() {
    // // Remove Tawk-to
    // if(typeof window != "undefined") {
    //   if(typeof window.google != "undefined") {
    //       delete window.google;
    //   }
    //
    //   if(typeof $ != "undefined") {
    //     $('#gmaps_script').remove();
    //   }
    // }
  }

}

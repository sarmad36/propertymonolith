import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { FooterComponent } from './footer/footer.component';
import { HeaderComponent } from './header/header.component';
import { TawkToComponent } from './tawk-to/tawk-to.component';


@NgModule({
  imports: [
    CommonModule,
    RouterModule,
  ],
  declarations: [
    FooterComponent,
    HeaderComponent,
    TawkToComponent
  ],
  exports: [
    FooterComponent,
    HeaderComponent,
    TawkToComponent
  ]
})
export class SiteIncludesModule { }

import { Component, OnInit } from '@angular/core';
import { AppSettings, HelperService } from '../../../services/_services';
import { Router, ActivatedRoute, NavigationEnd } from "@angular/router";

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {
  public buildingImageFooter : any = true;
  public addIndex : any = true;
  public togglefoot : any = false;
  public paddingFooter : any = false;
  public btntext : any = '<i class="fa fa-info-circle"></i>  About, Contact, & More';
  constructor(
    private helperService: HelperService,
    private router: Router,
  ) {
    this.router.events.subscribe((val) => {
      if(val instanceof NavigationEnd) {
        if(val.url == "/find-pro" ){
            this.addIndex = false;
        }
        else if(val.url == "/blue-world-city"){
          this.paddingFooter = true;
        }
        else if(val.url == "/capital-smart-city"){
          this.paddingFooter = true;
        }
        else if(val.url == "/skyparkone"){
          this.paddingFooter = true;
        }
        else if(val.url.indexOf("/agent") != -1) {
          this.paddingFooter = true;
        }
        else if(val.url.indexOf("/interior-designer") != -1) {
          this.paddingFooter = true;
        }
        else if(val.url.indexOf("/architect") != -1) {
          this.paddingFooter = true;
        }
        else if(val.url.indexOf("/contractors-builder") != -1) {
          this.paddingFooter = true;
        }
        else if(val.url.indexOf("/3d-animator") != -1) {
          this.paddingFooter = true;
        }
        else if(val.url.indexOf("/home-insurance") != -1) {
          this.paddingFooter = true;
        }
        else if(val.url.indexOf("/home-automation") != -1) {
          this.paddingFooter = true;
        }
        else if(val.url.indexOf("/home-security") != -1) {
          this.paddingFooter = true;
        }
        else if(val.url.indexOf("/property") != -1) {
          this.paddingFooter = true;
        }
         else {
           this.addIndex = true;
        }

        window.scrollTo(0, 0)
      }
    });
  }

  ngOnInit() {
  }
  ngAfterViewInit() {
    //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
    //Add 'implements AfterViewInit' to the class.
    if (typeof document != "undefined") {
      var imgDefer = document.getElementsByTagName('img');
      for (var i=0; i<imgDefer.length; i++) {
        if(imgDefer[i].getAttribute('data-src')) {
          imgDefer[i].setAttribute('src',imgDefer[i].getAttribute('data-src'));
        }
      }
    }
  }

  investRoute() {
    let routeLink =  this.helperService.investRoute(AppSettings.cities[0]);
    return routeLink;
  }

  togfoot() {
    if(this.togglefoot){
      this.togglefoot=false;
      this.btntext='<i class="fa fa-info-circle"></i>  About, Contact, & More';
    }
    else{
      this.togglefoot=true;
      this.btntext='<i class="fa fa-close"></i>  Close';
    }
  }

}

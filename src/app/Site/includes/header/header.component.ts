import { Component, OnInit, Injectable } from '@angular/core';
import { AuthenticationService, UserService, SEOService, AppSettings, HelperService, PurposeService, FavoriteService, HideService } from '../../../services/_services';
import { Location } from '@angular/common';

// Home Page
import { HomeComponent } from '../../pages/home/home.component';
// Social Login
import { AuthService } from "angularx-social-login";

// Bootsteap Components
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';

// Modal Components
import { LoginComponent } from '../../entry-components/login/login.component';
import { SiginUpOptsComponent } from '../../entry-components/sigin-up-opts/sigin-up-opts.component';
import { Router, ActivatedRoute, NavigationEnd } from "@angular/router";
// import { SiginUpComponent } from '../../entry-components/sigin-up/sigin-up.component';

// Sweet Alerts
import Swal from 'sweetalert2';
declare var $;

@Injectable({
  providedIn: 'root',
})

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  public modalRef           : BsModalRef;
  public isUserLoggedIn     : any = false;
  public loading            : any = false;
  public toggleSide         : any = false;
  public sideNav            : any = false;
  public user :any = {
    'first_name'            : '',
    'last_name'             : '',
    'email'                 : '',
    'password'              : '',
    'phone_number'          : '',
    'dob'                   : null,
  }
  public searchByID         : any = "";
  public activeRoute        : any = "";
  public searchingLoad      : any = false;
  public atHomePage         : any = true;
  public atAddPage          : any = false;
  public authSub            : any;
  public socail_AuthSub     : any;
  public propuseSub         : any;
  public Purpose            : any;
  public favoriteSub        : any;
  public favoritePropCount  : any = 0;
  public hiddenSub          : any;
  public hiddenPropCount    : any = 0;

  // Subscriptions
  public routerSubscription   : any;
  public ImagesUrl            :any = AppSettings.IMG_ENDPOINT;

  constructor(
    private modalService: BsModalService,
    private authService: AuthService,
    private userService: UserService,
    private authenticationService: AuthenticationService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private seoService: SEOService,
    private homePage: HomeComponent,
    private helperService: HelperService,
    private purposeService: PurposeService,
    private _location: Location,
    private favoriteService : FavoriteService,
    private hideService     : HideService,
  ) {
    this.routeCheck();
  }

  routeCheck(){
    // Check for Dashboard to set Navbar Css Class
    this.routerSubscription = this.router.events.subscribe((val) => {
      if(val instanceof NavigationEnd) {
        // console.log("*** Header val: ",val);

        this.activeRoute = val.url;

        // if (typeof window != "undefined") {
        //   if (window.innerWidth < 1300 && window.innerWidth > 991) {
        //       $("html").css("zoom","0.8");
        //   }
        // }
        if(val.url == '/') {
            this.atHomePage = true;
            this.atAddPage = false;
            this.setSEO_Tags('Buy, Rent, Sell, Homes & Real Estate Properties In Pakistan', 'Gharbaar.com, the leading real estate resource for property buyers, sellers, landlords, tenants and real estate agents in Pakistan. Real-time residential, commercial, and investment listing updates. Gharbaar.com is the easy-to-use online portal that takes the hassle and uncertainty out of buying, selling, renting, or owning.', false);
        }
        else if(val.url == '/add') {
            this.atAddPage = true;
            this.atHomePage = false;

            this.setSEO_Tags('Add new property on Gharbaar.com', 'Add new property on Gharbaar.com')
        }
        else if(val.url == '/find-pro') {
            this.atHomePage = true;
            this.atAddPage = false;
            this.setSEO_Tags('Search Agents, Developers, Architects in Pakistan', 'Gharbaar.com is easy-to-use online portal that takes the hassle and uncertainty out of b uying, selling, renting, or owning. Real-time residential, commercial, and investment listing updates.', false);
        }
        else if(val.url == '/blueworldcity') {
            this.atHomePage = false;
            this.atAddPage = false;
            this.setSEO_Tags('Blue World City Islamabad | Gharbaar.com', "All about Blue World City Islamabad. Project Details, NOC, Location, Map, Prices and more at Pakistan's best property portal | Gharbaar.com", false);
        }
        else if(val.url == '/blue-world-city') {
            this.atHomePage = false;
            this.atAddPage = false;
            // this.setSEO_Tags('Blue World City Islamabad | Plots For Sale | Gharbaar.com', "All about Blue World City Islamabad. Project Details, NOC, Location, Map, Prices and more at Pakistan's best property portal | Gharbaar.com", false);

        }
        else if(val.url == '/capital-smart-city') {
            this.atHomePage = false;
            this.atAddPage = false;
            // this.setSEO_Tags('Capital Smart City Islamabad | Plots For Sale | Gharbaar.com', "All about Capital Smart City. Project Details, NOC, Location, Map, Prices and more at Pakistan's #1 home buying resource. Gharbaar.com", false);

        }
        else if(val.url == '/blue-world-city/overseas-block') {
            this.atHomePage = false;
            this.atAddPage = false;
            // this.setSEO_Tags('Blue World City Islamabad Overseas Block | Gharbaar.com', "All about Blue World City Islamabad Overseas Block | Gharbaar.com", false);
        }
        else if(val.url == '/blue-world-city/hot-offer') {
            this.atHomePage = false;
            this.atAddPage = false;
            this.setSEO_Tags('Blue World City Islamabad Hot Offer | Gharbaar.com', "Blue World City Islamabad Hot Offer. | Gharbaar.com", true);
        }
        else if(val.url == '/skyparkone') {
            this.atHomePage = false;
            this.atAddPage = true;
            this.setSEO_Tags('Skypark One Gulberg Islamabad Development Project | Gharbaar.com', "Exclusive luxury living in Gulberg Greens | Elegant residential and commercial units | Early Bird Pricing and more at Islamabad's best property portal Gharbaar.com", false);
        }
        else if(val.url == '/skyparkone/payment-plan'){
          this.atHomePage = false;
          this.atAddPage = true;
          this.setSEO_Tags('Eight pricing plans | Eight housing and commercial levels | Best Pricing ', 'A wide selection of available units with a full range of pricing. Select your unit today for Early Bird Pricing.', false);
        }
        else if(val.url == '/about-us') {
            this.atHomePage = false;
            this.atAddPage = false;
            this.setSEO_Tags('Pakistan’s Real Estate Property Portal | Buy & Sell Property', "Find the best property in Pakistan with a complete range of properties for sale, houses for rent in commercial areas and developed house schemes.", false);
        }
        else if(val.url == '/agent-registration') {
            this.atHomePage = false;
            this.atAddPage = false;
            this.setSEO_Tags('Agent Registration Form | Contact Us | Gharbaar', "Fill out this agent registration form with your accurate information in all the fields to make sure that you are properly registered.", false);
        }
        else if(val.url == '/blog') {
            this.atHomePage = false;
            this.atAddPage = false;
            this.setSEO_Tags('Gharbaar Blogs | Property, Construction, Real Estate Trends', "Get updated information related to different properties and construction sites in Pakistan along with life style and home decor tips at Gharbaar blog.", false);
        }
        else if(val.url == '/academy') {
            this.atHomePage = false;
            this.atAddPage = false;
            this.setSEO_Tags('Real Estate Academy Courses & Training | Gharbaar', "Here are some academy courses to help you understand and gather all the tools, knowledge and information your need related to real estate.", false);
        }
        else if(val.url == '/manage-my-property') {
            this.atHomePage = false;
            this.atAddPage = false;
            this.setSEO_Tags('Manage My Property | Gharbaar', "Manage My Property | Gharbaar", true);
        }
        else if(val.url == '/contact') {
            this.atHomePage = false;
            this.atAddPage = false;
            this.setSEO_Tags('Contact Us | Gharbaar – Real Estate Property Portal', "Gharbaar is providing a wide range of property in Pakistan including buying and selling of houses and commercial plots. Contact us now.", false);
        }
        else if(val.url == '/wanted') {
            this.atHomePage = false;
            this.atAddPage = false;
            this.setSEO_Tags('Wanted Properties in Islamabad and Rawalpindi | Gharbaar', "Wanted Properties in Islamabad and Rawalpindi | Gharbaar", true);
        }
        else if(val.url == '/short-term-rentals') {
            this.atHomePage = false;
            this.atAddPage = false;
            this.setSEO_Tags('Properties for rent for short time period in Islamabad and Rawalpindi | Gharbaar', "Properties for rent for short time period in Islamabad and Rawalpindi | Gharbaar", true);
        }
        else if(val.url == '/searchlisting-newDesign') {
            this.atHomePage = false;
            this.atAddPage = false;
            this.setSEO_Tags('Design | Gharbaar', "Design | Gharbaar", true);
        }
        else if(val.url == '**') {
            this.atHomePage = false;
            this.atAddPage = false;
            this.setSEO_Tags('Page Not Found | Gharbaar', "Contact Us | Page Not Found", true);
        }
         else {
            this.atHomePage = false;
            this.atAddPage = false;
            this.seoService.setNoIndex(false);
        }
        if (typeof window != "undefined") {
            window.scrollTo(0, 0)
        }
      }
    });
  }

  ngOnInit() {

    // Subscribe To User Local Login
    if(typeof this.authenticationService.get_currentUserValue() != "undefined") {
      this.authSub = this.authenticationService.currentUserS$.subscribe(user => {
        if (user) {
            this.user = user.user;
            this.isUserLoggedIn = true;
        } else {
            // Move user to homepage
            let self = this;
            setTimeout(function() {
              self.isUserLoggedIn = false;
              if (self.activatedRoute.snapshot['_routerState'].url.indexOf('dashboard') !== -1) {
                  // console.log("LoggedOff dashboard")
                  self.router.navigate(['/']);
              }
            }, 500);
        }
      });

      //  Subscribe to User Socail Login
      if (typeof this.authService != 'undefined') {
        this.socail_AuthSub = this.authService.authState.subscribe((user) => {
          if (user !== null) {
            // Set User
            this.user = {
              'first_name'            : user.firstName,
              'last_name'             : user.lastName,
              'email'                 : user.email,
            };
          }
        });
      }
    }

    // Subscribe to Purpose Change
    this.getPurpose();

    // Subscribe to Property Favorites
    this.getFavoriteProps_Count();

    // Subscribe to Property Favorites
    this.getHiddenProps_Count();
  }

  ngOnDestroy() {
    if (typeof this.authSub != 'undefined') {
      //prevent memory leak when component destroyed
      this.authSub.unsubscribe();
    }

    if (typeof this.socail_AuthSub != 'undefined') {
      //prevent memory leak when component destroyed
      this.socail_AuthSub.unsubscribe();
    }

    if (typeof this.propuseSub != 'undefined') {
      //prevent memory leak when component destroyed
      this.propuseSub.unsubscribe();
    }

    if (typeof this.favoriteSub != 'undefined') {
      //prevent memory leak when component destroyed
      this.favoriteSub.unsubscribe();
    }

    if (typeof this.hiddenSub != 'undefined') {
      //prevent memory leak when component destroyed
      this.hiddenSub.unsubscribe();
    }

    if (typeof this.routerSubscription != 'undefined') {
      //prevent memory leak when component destroyed
       this.routerSubscription.unsubscribe();
    }
  }

  setPurposeRoute(){
    // let routeLink =  this.helperService.investRoute(AppSettings.cities[0]);
    if(this.Purpose != '_I_O')
      return this.helperService.getLocURL({}, AppSettings.cities[1], true, this.Purpose, Object.keys(AppSettings.searchTypes)[0], "");
    else
      return this.helperService.getLocURL({}, AppSettings.cities[1], true, 'sale', Object.keys(AppSettings.searchTypes)[0], "");
  }

  investRoute() {
    let routeLink =  this.helperService.investRoute({"id": 0, "name" : "All"});
    return routeLink;
  }

  searchListingByID() {
    // Show Loader
    this.searchingLoad = true;
    const url = AppSettings.API_ENDPOINT + "property/" + this.searchByID;
    this.helperService.httpGetRequests(url).then(property => {
        // Hide loader
        this.searchingLoad = false;

        // Reset Search
        this.searchByID = "";

        // Open Property Detail Page
        let url = this.helperService.propertyRoute(property);
        this.router.navigate([url]);

    }).catch(error => {
        console.error("error: ",error);

        // Hide loader
        this.searchingLoad = false;

        if(error.status == 404)
          Swal.fire('Error', '404 Property not found', 'error');
    })
  }

  setSEO_Tags(title: string, desc: string, setNoIndex = false) {
      let self = this;
      setTimeout(function() {
        // Set Basic Meta Tags
        self.seoService.updateTitle(title);
        self.seoService.updateDescription(desc);

        // Set Og Meta tags
        self.seoService.updateOgTitle(title);
        self.seoService.updateOgDesc(desc);
        self.seoService.updateOgUrl();

        // Set Twitter Meta Tags
        self.seoService.updateTwitterTitle(title);
        self.seoService.updateTwitterDesc(desc);

        // Set Canonical Tag
        self.seoService.updateCanonicalUrl();

        if(setNoIndex)
            self.seoService.setNoIndex(setNoIndex);
      }, 500);
    }

  getPurpose() {
    this.propuseSub = this.purposeService.purposeS$.subscribe((purpose) => {
      this.Purpose = purpose;
    });
  }

  setPurpose(purpose) {
    this.homePage.setPurpose(purpose);
  }

  getFavoriteProps_Count() {
    this.favoriteSub = this.favoriteService.favoritesS$.subscribe((properties) => {
      this.favoritePropCount = this.favoriteService.getfavorites_Count();
    });
  }

  getHiddenProps_Count() {
    this.hiddenSub = this.hideService.hiddenS$.subscribe((hidden) => {
      this.hiddenPropCount = this.hideService.getHiddenProps_Count();
    });
  }

  open_SignUpOpts_Modal() {
    const initialState = {};
    this.modalRef = this.modalService.show(SiginUpOptsComponent, {initialState});
  }

  open_Login_Modal() {
    const initialState = {};
    this.modalRef = this.modalService.show(LoginComponent, {initialState});
    // this.modalRef.content.closeBtnName = 'Close';
  }

  signOut(): void {
    this.authService.signOut();
  }

  logout() {
    // Log user out from Social Login
    if (this.authService['_user'] != null)
      this.signOut();

    // Show Loader
    this.loading = true;

    this.authenticationService.logout()
       .then(resp => {
            // console.log("resp: ",resp);
            if(resp) {
              // Set User to empty
              this.user           = {
                                      'first_name'            : '',
                                      'last_name'             : '',
                                      'email'                 : '',
                                      'password'              : '',
                                      'phone_number'          : '',
                                      'dob'                   : null,
                                    };
              this.isUserLoggedIn = false;

              // console.log("this.user: ",this.user);
            }

            // Hide Loader
            this.loading = false;
        },
        error => {
            // alert(JSON.stringify(error));
            console.log("error: ",error);

            // Hide Loader
            this.loading = false;
        });
  }

  toggleSidebar() {
    this.toggleSide = !this.toggleSide;
    this.sideNav = !this.sideNav;
    $('body').toggleClass("overflow-hidden");
  }
  closeSideNav(){
    this.toggleSide = false;
    this.sideNav = false;
    $('body').removeClass("overflow-hidden");
  }
}

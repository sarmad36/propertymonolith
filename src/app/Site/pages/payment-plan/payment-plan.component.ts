import { Component, OnInit } from '@angular/core';

declare var $;

@Component({
  selector: 'app-payment-plan',
  templateUrl: './payment-plan.component.html',
  styleUrls: ['./payment-plan.component.css']
})
export class PaymentPlanComponent implements OnInit {

  public monthsText :any = "0";
  public daysText   :any = "0";
  public hoursText  :any = "0";
  public minsText   :any = "0";
  public secText    :any = "0";

  constructor() { }

  ngOnInit() {
  }
}

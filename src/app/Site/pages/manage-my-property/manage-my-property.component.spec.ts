import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageMyPropertyComponent } from './manage-my-property.component';

describe('ManageMyPropertyComponent', () => {
  let component: ManageMyPropertyComponent;
  let fixture: ComponentFixture<ManageMyPropertyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageMyPropertyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageMyPropertyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

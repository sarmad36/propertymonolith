import { Component, OnInit } from '@angular/core';
import { AppSettings, HelperService, SEOService, PurposeService, FavoriteService, HideService, SaveSearchService, EmailBucketService } from '../../../services/_services';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-manage-my-property',
  templateUrl: './manage-my-property.component.html',
  styleUrls: ['./manage-my-property.component.css']
})
export class ManageMyPropertyComponent implements OnInit {

  public ajaxloader               : any = false;

  public subscriptionFooter             : any = { email: ''};
  public subscriptionFooterForm         : FormGroup;
  public footSubmitted                  : any = false;

  constructor(
    private helperService         : HelperService,
    private formBuilder           : FormBuilder,
  ) { }

  ngOnInit() {

    this.subscriptionFooterForm = this.formBuilder.group({
      email    : [null, [Validators.required, Validators.email]],
    });
  }

  get g() { return this.subscriptionFooterForm.controls; }
  AddSubscribeFoot(){
    this.footSubmitted        = true;

    // Stop here if form is invalid
    if (this.subscriptionFooterForm.invalid) {
     return;
    }

    this.ajaxloader = true;
    let url  = AppSettings.API_ENDPOINT + 'subscribers/add';
    this.helperService.httpPostRequests(url, this.subscriptionFooter)
      .then(resp => {
        //console.log("resp: ",resp);
        this.ajaxloader = false;
        if (typeof resp != "undefined" && resp != null && typeof resp.error === "undefined") {
          this.subscriptionFooter = { email: ''};

          Swal.fire('Request Sent', 'Your message has been sent successfully. We will get back to you shortly.', 'success');

          // Set Submission false
          this.footSubmitted = false;
        } else {
            console.error(resp);
        }
      })
      .catch(error => {
        console.log("error: ",error);
        this.ajaxloader = false;
        Swal.fire('Newsletter Subscription', "You have already subscribed to the newsletter" , 'warning');
        // Set Submission false
        this.footSubmitted = false;
      });
  }

}

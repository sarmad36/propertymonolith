import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Location } from '@angular/common';
import { Router, ActivatedRoute, NavigationEnd } from "@angular/router";
import { AppSettings, HelperService, SEOService, PurposeService, FavoriteService, HideService, SaveSearchService, EmailBucketService } from '../../../services/_services';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';

import Swal from 'sweetalert2';

declare var $;

@Component({
  selector: 'app-blogs-detail',
  templateUrl: './blogs-detail.component.html',
  styleUrls: ['./blogs-detail.component.css']
})
export class BlogsDetailComponent implements OnInit {
  public toggleToc : any = false;
  public toggleComment : any = false;


  public blogID                   : any;
  public blogTitle                : any;
  public BlogsResp                : any = [];
  public BlogsPopArticles         : any = [];
  public quickLinks               : any = [];
  public contentLoaded            : any = false;
  public TableOfContents          : any = [];
  public TableOfContents1         : any = [];

  public subscription             : any = { email: ''};
  public subscriptionForm         : FormGroup;
  public submitted                : any = false;
  public ajaxloader               : any = false;

  public subscriptionlike : any = { email: ''};
  public subscriptionFormLike         : FormGroup;
  public submittedlike                : any = false;


  public comment              : any = { blog_id: '', parent_id: '', name: '' , email: '' , message: ''};
  public commentForm          : FormGroup;
  public commentAdd           : any = false;

  public commentData      :any = [];

  public ImagesUrl            :any = AppSettings.IMG_ENDPOINT;

  public articleHelpful       : any = true;
  public feedReview           : any = { blog_id: '', feedback_type : ''};

  public shareUrl       = '';
  public shareIImageUrl = '';
  // Subscriptions
  public routerSubscription : any;
  public activeRoute        : any = "";

  public innerComment       : any = false;
  public  outerComment      : any = true;

  public subscriptionFooter             : any = { email: ''};
  public subscriptionFooterForm         : FormGroup;
  public footSubmitted                  : any = false;

  public likeSubsc            : any = false;



  constructor(
    private helperService         : HelperService,
    private formBuilder           : FormBuilder,
    private activatedRoute        : ActivatedRoute,
    private seoService            : SEOService,
    private _location             : Location,
    private cdr                   : ChangeDetectorRef,
  ) {
      this.routerSubscription = this.activatedRoute.params.subscribe(routeParams => {
         console.log("routeParams: ",routeParams);
        // this.blogID = this.helperService.render_blog_URL(routeParams.slug)['id'];
        this.blogTitle = routeParams;
        console.log("this.blogTitle ", this.blogTitle );
        this.getBlogDetails();
      });

  }

  ngOnInit() {
    this.subscriptionForm = this.formBuilder.group({
      email    : [null, [Validators.required, Validators.email]],
    });
    this.subscriptionFormLike = this.formBuilder.group({
      email    : [null, [Validators.required, Validators.email]],
    });
    this.subscriptionFooterForm = this.formBuilder.group({
      email    : [null, [Validators.required, Validators.email]],
    });
    this.commentForm = this.formBuilder.group({
      name     : [null, Validators.required],
      email    : [null, [Validators.required, Validators.email]],
      message  : [null, Validators.required],
    });
  }
  ngOnDestroy() {
    if (typeof this.routerSubscription != 'undefined') {
      //prevent memory leak when component destroyed
       this.routerSubscription.unsubscribe();
    }
  }

  getBlogDetails() {
    // Show Content Loader
    this.contentLoaded = false;

    let url = AppSettings.API_ENDPOINT + 'blogs/get-by-slug/' + this.blogTitle.slug;
    this.helperService.httpGetRequests(url).then(resp => {
      if (typeof resp != "undefined") {
        this.BlogsResp = resp.blog;

        // Add Responsive div for tables
        this.cdr.detectChanges();
        if (typeof $ != "undefined") {
            $( $('body').find('table') ).wrap( "<div class='table-responsive'></div>" );
        }

        this.BlogsPopArticles = resp.popular_articles;

        this.blogID = this.BlogsResp.id;
        this.quickLinks = resp.links;

        this.getTOC();

        // Set SEO Tags
        this.setSEO_Tags();
        this.getAllComments();
        // console.log("this.BlogsResp" , this.BlogsResp);
        // console.log("this.BlogsPopArticles" , this.BlogsPopArticles);
      }

      // Hide Content Loader
      this.contentLoaded = true;
    }).catch(error => {
        console.error("error: ",error);

        // Hide Content Loader
        this.contentLoaded = false;
    });
  }

  openInnerReply(comIndex){
    if(typeof this.commentData[comIndex].showForm == 'undefined')
      this.commentData[comIndex].showForm = false;

    this.commentData[comIndex].showForm = true;
    this.outerComment  = false;
    // this.innerComment  = true;
  }
  closeInnerReply(comIndex) {
    if(typeof this.commentData[comIndex].showForm == 'undefined')
      this.commentData[comIndex].showForm = false;

    this.commentData[comIndex].showForm = false;
    this.outerComment  = true;
    // this.innerComment  = false;
  }

  get a() { return this.commentForm.controls; }
  addComment(parentId){
    this.commentAdd        = true;

    // Stop here if form is invalid
    if (this.commentForm.invalid) {
     return;
    }
    this.ajaxloader = true;

    this.comment.blog_id = this.blogID;
    this.comment.parent_id = parentId;

    let url  = AppSettings.API_ENDPOINT + 'blog-comments/add';

    this.helperService.httpPostRequests(url, this.comment)
      .then(resp => {
        this.comment = { blog_id: this.blogID , parent_id: this.comment.parent_id , name: '' , email: '' , message: ''};
        //console.log("resp: ",resp);
        this.ajaxloader = false;

        if (typeof resp != "undefined" && resp != null && typeof resp.error === "undefined") {

          Swal.fire('Request Sent', 'Your message has been sent successfully.', 'success');
          this.toggleComment = true;
          // Set Submission false
          this.commentAdd = false;
          this.getAllComments();
          this.innerComment=false;
          this.outerComment=true;
        } else {
            console.error(resp);
        }
      })
      .catch(error => {
        console.log("error: ",error);
        this.ajaxloader = false;
        // Set Submission false
        this.commentAdd = false;
      });
  }

  getAllComments(){
    const url = AppSettings.API_ENDPOINT + 'blog-comments/get';

    let data = {blog_id: this.blogID};
    this.helperService.httpPostRequests(url, data).then(resp => {
      if (typeof resp != "undefined") {
        this.commentData = resp;
        // console.log("this.commentData" , this.commentData);
      }
    }).catch(error => {
        console.error("error: ",error);
    });
  }

  scrollSec(id){
    var headings = $("#" + id).offset();
    window.scrollTo({ top: headings.top - 0, behavior: 'smooth'});
  }

  getTOC() {
    // Reset table
    this.TableOfContents = [];
    let self = this;
    setTimeout(function() {
      var headings = $('.innerBodySingleBlog').children().find('h2, h3, h4');

      for (let i = 0; i < headings.length; i++) {
          let id = 'TOC_' + i;

          // Set Heddings
          let data = { name: $(headings[i]).text(), id: id, type: headings[i].nodeName };
          $(headings[i]).html($(headings[i]).text());

          self.TableOfContents.push(data);
          $(headings[i]).attr("id", id);
      }
    }, 100);
  }

  get f() { return this.subscriptionForm.controls; }
  AddSubscribe() {
    this.submitted        = true;

    // Stop here if form is invalid
    if (this.subscriptionForm.invalid) {
     return;
    }

    this.ajaxloader = true;
    let url  = AppSettings.API_ENDPOINT + 'subscribers/add';
    this.helperService.httpPostRequests(url, this.subscription)
      .then(resp => {
        //console.log("resp: ",resp);
        this.ajaxloader = false;
        if (typeof resp != "undefined" && resp != null && typeof resp.error === "undefined") {
          this.subscription = { email: ''};

          Swal.fire('Request Sent', 'Your message has been sent successfully. We will get back to you shortly.', 'success');

          // Set Submission false
          this.submitted = false;
        } else {
            console.error(resp);
        }
      })
      .catch(error => {
        console.log("error: ",error);
        this.ajaxloader = false;
        // Set Submission false
        this.submitted = false;
      });
  }

  get g() { return this.subscriptionForm.controls; }
  AddSubscribeFoot(){
    this.footSubmitted        = true;

    // Stop here if form is invalid
    if (this.subscriptionFooterForm.invalid) {
     return;
    }

    this.ajaxloader = true;
    let url  = AppSettings.API_ENDPOINT + 'subscribers/add';
    this.helperService.httpPostRequests(url, this.subscriptionFooter)
      .then(resp => {
        //console.log("resp: ",resp);
        this.ajaxloader = false;
        if (typeof resp != "undefined" && resp != null && typeof resp.error === "undefined") {
          this.subscriptionFooter = { email: ''};

          Swal.fire('Request Sent', 'Your message has been sent successfully. We will get back to you shortly.', 'success');

          // Set Submission false
          this.footSubmitted = false;
        } else {
            console.error(resp);
        }
      })
      .catch(error => {
        console.log("error: ",error);
        this.ajaxloader = false;
        // Set Submission false
        this.footSubmitted = false;
      });
  }

  get l() { return this.subscriptionFormLike.controls; }
  AddSubscribelike() {
    this.submittedlike        = true;

    // Stop here if form is invalid
    if (this.subscriptionFormLike.invalid) {
     return;
    }

    this.ajaxloader = true;
    let url  = AppSettings.API_ENDPOINT + 'subscribers/add';
    this.helperService.httpPostRequests(url, this.subscriptionlike)
      .then(resp => {
        //console.log("resp: ",resp);
        this.ajaxloader = false;
        if (typeof resp != "undefined" && resp != null && typeof resp.error === "undefined") {
          this.subscriptionlike = { email: ''};

          Swal.fire('Request Sent', 'Your message has been sent successfully. We will get back to you shortly.', 'success');

          // Set Submission false
          this.submittedlike = false;
        } else {
            console.error(resp);
        }
      })
      .catch(error => {
        console.log("error: ",error);
        this.ajaxloader = false;
        Swal.fire('Newsletter Subscription', "You have already subscribed to the newsletter" , 'warning');

        // Set Submission false
        this.submittedlike = false;
      });
  }

  toggleTableContent(){
    this.toggleToc = !this.toggleToc;
  }
  commentToggle(param){

    if (param == "close") {
      this.toggleComment = false;
      var subscriber = $("#" + 'commentBox').offset();
      window.scrollTo({ top: subscriber.top - 550, behavior: 'smooth'});
    }
    else if(param == "open"){
      this.toggleComment = true;
    }
    else{

    }
  }
  commentScroll(){
    this.toggleComment = true;
    var x = $("#" + 'commentSec').offset();
    window.scrollTo({ top: x.top - 30, behavior: 'smooth'});
  }
  feedback(param){

    if (param == "negative") {
        this.feedReview = { blog_id: this.blogID, feedback_type : 'negative'};
        this.articleHelpful = false ;
    }
    else{
        this.feedReview = { blog_id: this.blogID, feedback_type : 'positive'};
        this.likeSubsc = true ;
    }

    const url  = AppSettings.API_ENDPOINT + 'blogs/feedback';
    this.helperService.httpPostRequests(url, this.feedReview)
      .then(resp => {
        if (typeof resp != "undefined" && resp != null && typeof resp.error === "undefined") {

        } else {
            console.error(resp);
        }
      })
      .catch(error => {
        console.log("error: ",error);
        this.ajaxloader = false;
        Swal.fire('Newsletter Subscription', "You have already subscribed to the newsletter" , 'warning');
        // Set Submission false
        this.submitted = false;
      });
  }

  setSEO_Tags() {

    let image = this.ImagesUrl + 'uploads/bloggers/' + this.BlogsResp.blogger.image;
    let self  = this;

      // Set Basic Meta Tags
      self.seoService.updateTitle(this.BlogsResp.meta_title);
      self.seoService.updateDescription(this.BlogsResp.meta_description);

      // Set Og Meta tags
      self.seoService.updateOgTitle(this.BlogsResp.meta_title);
      self.seoService.updateOgDesc(this.BlogsResp.meta_description);
      self.seoService.updateOgUrl();

      // Set Twitter Meta Tags
      self.seoService.updateTwitterTitle(this.BlogsResp.meta_title);
      self.seoService.updateTwitterDesc(this.BlogsResp.meta_description);

      // Set Canonical Tag
      self.seoService.updateCanonicalUrl();

      // Set Socail Params
        this.shareUrl       = this._location['_platformLocation'].href;
        // console.log("shareUrl", this.shareUrl);
        this.shareIImageUrl = image;
  }

  getBlogUrl(blog){
    return this.helperService.get_blog_URL(blog);
  }
  getAuthorUrl(blogger){
  return this.helperService.get_author_URL(blogger);
  }
  getCatUrl(category){
  return this.helperService.get_category_URL(category);
  }
  setLocUrl(location){
    if (location.is_city != "1") {
        return this.helperService.getLocURL(location, location, false, location.purpose, location.type, location.sub_type);
    } else {
        return this.helperService.getLocURL({}, location, true, location.purpose, location.type, location.sub_type);
    }
  }


}

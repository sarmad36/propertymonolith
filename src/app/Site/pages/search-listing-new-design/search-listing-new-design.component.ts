import { Component, OnInit, ViewChild ,ElementRef, HostListener  } from '@angular/core';
import { Location } from '@angular/common';
import { Router, ActivatedRoute } from "@angular/router";
import { AuthenticationService, AppSettings, HelperService, SEOService, PurposeService, FavoriteService, HideService, SaveSearchService, EmailBucketService } from '../../../services/_services';
import { FormControl } from '@angular/forms';
import { Subject } from 'rxjs';

// Bootsteap Components
import { BsModalRef, BsModalService, ModalOptions } from 'ngx-bootstrap/modal';

// Modal Components
import { LoginComponent } from '../../entry-components/login/login.component';
import { ContactDetailsComponent } from '../../entry-components/contact-details/contact-details.component';
import { InquiryModalComponent } from '../../entry-components/inquiry-modal/inquiry-modal.component';
import { OwlCarousel } from 'ngx-owl-carousel';

declare var $;

@Component({
  selector: 'app-search-listing-new-design',
  templateUrl: './search-listing-new-design.component.html',
  styleUrls: ['./search-listing-new-design.component.css']
})
export class SearchListingNewDesignComponent implements OnInit {
  @ViewChild('section_af', {static: false}) section_af: ElementRef;
  @ViewChild('hide_sticky', {static: false}) hide_sticky: ElementRef;
  @ViewChild('propertyType', { static: false })  propertyType: ElementRef;
  @ViewChild('section_map', {static: false}) section_map: ElementRef;
  @ViewChild('hide_map', {static: false}) hide_map: ElementRef;
  @ViewChild('propMap', {static: false}) mapElement: ElementRef;
  @ViewChild('owlElement', {static: false}) owlElement: OwlCarousel;


  public gharbaarLoader       : any = true;
  // Map Objects
  public listingMap       : any;
  public mapBounds        : any;
  public simpleMapUrl     : any = '';
  public showSimpleMap    : any = false;
  public infowindo        : any;
  public listingMarker    : any;
  public Location_LatLng  : any = [];
  public markerlatitudes  : any;
  public markersArray     : any = [];
  public infoWindowProperty : any = [];

  // stickfilter Params
  public stickfilter    :any = false;
  public addPad              = false;

  // Search Params
  public purposes        :any = AppSettings.purpose;
  public cities          :any = AppSettings.cities;
  public areas           :any = [];
  public searchTypes     :any = [];
  public searchSubTypes  :any = AppSettings.searchTypes;
  public GOOGLE_MAPS_KEY :any = "";
  public labelPrice      :any = "";

  public agencies           :any = [];

  // Params
  public SelectedCity   = AppSettings.cities[0];
  public searchBeds     = AppSettings.searchBeds;
  public SelectedArea   = [];
  public SelectedType   = "House";

  // Ng-Slect Params
  public areasBuffer  = [];
  public bufferSize   = 10;
  public numberOfItemsFromEndBeforeFetchingMore = 5;
  public areasLoading = false;
  public areaQuery    = "";
  public areasError   = false;

  // Https Params
  protected ngUnsubscribe: Subject<void> = new Subject<void>();
  public httpLoading     = false;
  public corsHeaders     : any;

  public areasSelect_F = new FormControl('');


  public showFeatures :any = false;
  public searchParms  :any = {
    type     : "",
    sub_type : "all",
    city: { id: null, name: "" },
    area : [],
    agency : [],
    // area: {
    //         city_id   : null,
    //         id        : null,
    //         name      : "",
    //         p_loc     : [],
    //         parent_id : ""
    //       },
    purpose      : "sale",
    min_price    : 0,
    max_price    : 0,
    min_area     : 0,
    max_area     : 0,
    area_unit    : 'marla',
    bedrooms     : 0,
    bathrooms    : 0,
    sort_by      : "package",
    sort_type    : "DESC",
    current_page : 1
  };

  public viewAllLocURL        :any = "";
  public modalChangeUnit      :any = 'marla';
  public parentLocs           :any = [];
  public Types_Prop_Count     :any = {};
  public subLocs              :any = [];
  public sortedSubLocs        :any = [];
  public locs_sorted          :any = [];
  public ListingsData         :any = [];
  public Listings_Available   :any = false;
  public pagination           :any = [];
  public locsByCity           :any = false;
  public loadingBreadCrumbs   :any = false;
  public toggleloadless       :any = true;
  public searchPrices         :any = AppSettings.searchPrices;
  public searchUnits          :any = AppSettings.searchUnits;
  public searchAreas          :any = AppSettings.searchAreas;
  public propPackages         :any = AppSettings.propPackages;
  public SortBy               :any = "package-DESC";
  public ImagesUrl            :any = AppSettings.IMG_ENDPOINT;
  public hideLocs             :any = false;
  public hideEmailBucket      :any = true;
  public modalRef             :BsModalRef;
  public innerSideFilter      :any = false;
  public overFlow             :any = false;

  // Subscriptions
  public routerSubscription : any;

  public setbedValue     : any = 0;
  public setnumbertype   : any;
  public filterNotBed    : any = true;

  public setBathValue    : any = 0;
  public setBathtype     : any;
  public filterNotBath   : any = true;

  public maptoggler      : any = true;
  public mapViewMob      : any = true;

  public stickmap        : any = false;
  public footeroffSet    : any = 0;
  public openLoc        : any = false;
  public authSubs       : any;
  public autoHeightLoc  : any = false;
  public propertyRouteInfo  : any;
  // public propertyImages      : any = [];
  // public mySlideImages     = [];
  // public mySlideOptions    = {items: 1, dots: false, nav: false, thumbs:false};
  constructor(
    private authenticationService : AuthenticationService,
    private router                : Router,
    private activatedRoute        : ActivatedRoute,
    private helperService         : HelperService,
    private seoService            : SEOService,
    private purposeService        : PurposeService,
    private _location             : Location,
    private favoriteService       : FavoriteService,
    private hideService           : HideService,
    private saveSearchService     : SaveSearchService,
    private modalService          : BsModalService,
    private emailBucketService    : EmailBucketService,
    private elementRef            : ElementRef,
  ) {
    // Get Globals from AppSettings
    this.searchTypes     = Object.keys(AppSettings.searchTypes);
    // this.searchSubTypes  = AppSettings.searchTypes;
    // this.searchPrices    = AppSettings.searchPrices;
    // this.searchAreas     = AppSettings.searchAreas;
    // this.searchUnits     = AppSettings.searchUnits;
    // this.propPackages    = AppSettings.propPackages;
    this.GOOGLE_MAPS_KEY = AppSettings.GOOGLE_MAPS_KEY;

    // console.log("this.activatedRoute: ",this.activatedRoute);
    this.routerSubscription = this.activatedRoute.params.subscribe(routeParams => {
      let data = routeParams;

      // Get filter params from local storage
      if (typeof this.helperService.getSearchParms() != "undefined" && this.helperService.getSearchParms() != null) {
        if (typeof this.helperService.getSearchParms().agency != "undefined")
        this.searchParms.agency = this.helperService.getSearchParms().agency;

        if (typeof this.helperService.getSearchParms().minPrice != "undefined")
        this.searchParms.min_price = this.helperService.getSearchParms().minPrice;

        if (typeof this.helperService.getSearchParms().maxPrice != "undefined")
        this.searchParms.max_price = this.helperService.getSearchParms().maxPrice;

        if (typeof this.helperService.getSearchParms().minArea != "undefined")
        this.searchParms.min_area = this.helperService.getSearchParms().minArea;

        if (typeof this.helperService.getSearchParms().maxArea != "undefined")
        this.searchParms.max_area = this.helperService.getSearchParms().maxArea;

        if (typeof this.helperService.getSearchParms().area_unit != "undefined") {
          this.searchParms.area_unit = this.helperService.getSearchParms().area_unit;
          if(this.searchAreas[this.searchParms.area_unit] == 'undefined') {
            this.searchParms.area_unit = 'marla';
          }

          this.modalChangeUnit       = this.searchParms.area_unit;
        }

        if (typeof this.helperService.getSearchParms().bedrooms != "undefined")
        this.searchParms.bedrooms = this.helperService.getSearchParms().bedrooms;
      }

      // Set Search Type
      this.searchParms.type     = data.type.split("-")[0];

      if(typeof data.type.split("-")[1] != "undefined")
      this.searchParms.sub_type = data.type.split("-")[1].replace("_", " ");
      else
      this.searchParms.sub_type = 'all';

      // Set Filter SEO Tags
      let filterSEO = [];

      // Get Filter if isset
      if(typeof data.filter != "undefined") {
        filterSEO = data.filter.split("-");

        console.log("filterSEO: ", filterSEO);
        this.searchParms.min_area  = parseInt(data.filter.split("-")[0]);
        this.searchParms.max_area  = parseInt(data.filter.split("-")[0]);
        this.searchParms.area_unit = data.filter.split("-")[1];
        this.modalChangeUnit       = this.searchParms.area_unit;
      }

      // Save Current Route
      this.saveSearchService.updateSearch_URL(this.activatedRoute['_routerState'].snapshot.url);

      // Set Search Purpose
      this.searchParms.purpose = this.activatedRoute.snapshot.url[0].path;

      // Reset View Loc URL Var
      this.viewAllLocURL = '';

      if (typeof data.slug != "undefined") {
        let slug = this.helperService.renderLocURL(data.slug);

        if(parseInt(slug.searchBy) == 1) {   // Search by City
          this.searchParms.city.id   = parseInt(slug.id);
          this.searchParms.city.name = slug.name;
          this.searchParms.area      = [];
          this.parentLocs            = [];

          this.getSubLocationsByCity(this.searchParms.city.id);
          this.locsByCity = true;

          // Get Search Areas
          this.getSearchAreas();

          // Get Property Listing
          this.getListings();

          let self = this;
          setTimeout(function() {
            console.log("filterSEO: ", filterSEO);

            // Set SEO Tags
            self.setSEO_Tags("City", filterSEO);
          }, 500);

          // Get View All Locs URL
          self.setViewAllLocURL();

        } else if(parseInt(slug.searchBy) == 2) { // Search by Area
          this.getParentLocations(parseInt(slug.id)).then(resp => {
            // Get Search Areas
            this.getSearchAreas();

            // Get Property Listing
            this.getListings();

            // Set SEO Tags
            console.log("filterSEO: ", filterSEO);
            this.setSEO_Tags("Area", filterSEO);
          })
        }
      }
    });
    if (typeof window != "undefined") {
      if (window.innerWidth < 768){
        this.mapViewMob = false;
        this.maptoggler = false;
      }
    }
  }

  @HostListener('window:scroll', ['$event']) track(event) {
    // console.log("window.pageYOffset", window.pageYOffset);
    // console.log("this.hide_map.nativeElement.offsetTop", this.hide_map.nativeElement.offsetTop);
    if (typeof window != "undefined") {

      if (window.innerWidth < 1280 && window.innerWidth > 991){
        this.footeroffSet = 0;
      }
      else{
        this.footeroffSet = 0;
      }

      if(window.pageYOffset >= this.section_map.nativeElement.offsetTop + 10 && window.pageYOffset < this.hide_map.nativeElement.offsetTop - this.footeroffSet) {
        this.stickmap = true;
        // console.log("if this.stickmap" ,this.stickmap)
      } else{
        this.stickmap = false;
        // console.log("else this.stickmap" ,this.stickmap)
      }
      if(window.pageYOffset >= 152) {
        this.stickfilter = true;
        this.addPad = true;
      } else if (window.pageYOffset < 152){
        this.stickfilter = false;
        this.addPad = false;
      }
    }

  }



  ngOnInit() {
    // Bind jQuery for Dropdwons etc
    this.bindJQueryFuncs();

  }

  bindJQueryFuncs() {
    if (typeof $ != "undefined") {
      $('.filterBtns .dropdown > button').on('click', function (event) {
        $('.filterBtns .dropdown').removeClass('open');
        $(this).parent().toggleClass('open');
      });
      $('body').on('click', function (e) {
        if (!$('.filterBtns .dropdown').is(e.target)
        && $('.filterBtns .dropdown').has(e.target).length === 0
        && $('.open').has(e.target).length === 0
      ) {
        $('.filterBtns .dropdown').removeClass('open');
      }
    });
  }
}

ngAfterViewInit() {
  // Toggle the side navigation
  $("#sidebarToggle, #sidebarToggleTop").on('click', function(e) {
    console.log("here");
    $("body").toggleClass("sidebar-toggled");
    $(".sidebar").toggleClass("toggled");
    if ($(".sidebar").hasClass("toggled")) {
      $('.sidebar .collapse').collapse('hide');
    };
  });

  // Close any open menu accordions when window is resized below 768px
  $(window).resize(function() {
    if ($(window).width() < 768) {
      $('.sidebar .collapse').collapse('hide');
    };
  });

  // Prevent the content wrapper from scrolling when the fixed side navigation hovered over
  $('body.fixed-nav .sidebar').on('mousewheel DOMMouseScroll wheel', function(e) {
    if ($(window).width() > 768) {
      var e0 = e.originalEvent,
      delta = e0.wheelDelta || -e0.detail;
      this.scrollTop += (delta < 0 ? 1 : -1) * 30;
      e.preventDefault();
    }
  });

  // Scroll to top button appear
  $(document).on('scroll', function() {
    var scrollDistance = $(this).scrollTop();
    if (scrollDistance > 100) {
      $('.scroll-to-top').fadeIn();
    } else {
      $('.scroll-to-top').fadeOut();
    }
  });

  // Smooth scrolling using jQuery easing
  $(document).on('click', 'a.scroll-to-top', function(e) {
    var $anchor = $(this);
    $('html, body').stop().animate({
      scrollTop: ($($anchor.attr('href')).offset().top)
    }, 1000, 'easeInOutExpo');
    e.preventDefault();
  });
}


ngOnDestroy() {
  if (typeof this.routerSubscription != 'undefined') {
    //prevent memory leak when component destroyed
    this.routerSubscription.unsubscribe();
  }
}

// setOwlCaro() {
//   this.mySlideImages     = [];
//
//   if (this.propertyImages.length == 1) {
//       // Set Owl Images
//       this.mySlideImages.push(this.ImagesUrl + 'uploads/property-images/' + this.propertyImages[0]);
//
//        this.owlElement.reInit();
//   } else {
//       for (let i = 0; i < this.propertyImages.length; i++) {
//         // Set Owl Images
//         this.mySlideImages.push(this.ImagesUrl + 'uploads/property-images/' + this.propertyImages[i]);
//       }
//       this.owlElement.reInit();
//   }
//
// }

setPropertyType(type) {
  // this.searchParms.type     = type;
  // this.searchParms.sub_type = "all";
  if(!this.loadingBreadCrumbs) {
    let routeLink = '';
    if (typeof this.searchParms.area.id != "undefined" && this.searchParms.area.id != null) {
      routeLink = this.helperService.getLocURL(this.searchParms.area, this.searchParms.city, false, this.searchParms.purpose, type, "all");
    } else {
      routeLink = this.helperService.getLocURL({}, this.searchParms.city, true, this.searchParms.purpose, type, "all");
    }
    return routeLink;
  }

  // // this.search();
  // this.setLocRoute();
}

setPropertySubType(type) {
  // this.searchParms.sub_type = type;

  if(!this.loadingBreadCrumbs) {
    let routeLink = '';
    if (typeof this.searchParms.area.id != "undefined" && this.searchParms.area.id != null) {
      routeLink = this.helperService.getLocURL(this.searchParms.area, this.searchParms.city, false, this.searchParms.purpose, this.searchParms.type, type);
    } else {
      routeLink = this.helperService.getLocURL({}, this.searchParms.city, true, this.searchParms.purpose, this.searchParms.type, type);
    }
    return routeLink;
  }

  // // this.search();
  // this.setLocRoute();
}

getFilterURL(filter) {
  if(!this.loadingBreadCrumbs) {
    let routeLink = '';
    if (typeof this.searchParms.area.id != "undefined" && this.searchParms.area.id != null) {
      routeLink = this.helperService.getLocURL_for_Filter(this.searchParms.area, this.searchParms.city, false, filter, this.searchParms.purpose, this.searchParms.type, this.searchParms.sub_type);
    } else {
      routeLink = this.helperService.getLocURL_for_Filter({}, this.searchParms.city, true, filter, this.searchParms.purpose, this.searchParms.type, this.searchParms.sub_type);
    }
    return routeLink;
  }
}

resetPropType() {
  this.setPropertyType(this.searchTypes[0]);
  this.searchParms.type = this.searchTypes[0];
  this.searchParms.sub_type = "";
  // this.search();
  this.setLocRoute();
}
resetSearch() {
  this.searchParms.area       =  [];
  this.searchParms.agency     = [];
  this.searchParms.min_price  = 0;
  this.searchParms.max_price  = 0;
  this.searchParms.min_area   = 0;
  this.searchParms.max_area   = 0;
  this.searchParms.area_unit  = 'marla';
  this.searchParms.bedrooms   = 0;
  this.searchParms.bathrooms  = 0;
  console.log("hi")
  this.setLocRoute();
}
setPrice(type, price) {
  if(type == 'min') {
    this.searchParms.min_price = price;
  } else if(type == 'max') {
    this.searchParms.max_price = price;
  }
}

setMinPrices(price) {
  if (this.searchParms.max_price != 0) {
    if(price < this.searchParms.max_price) {
      return true;
    } else {
      return false;
    }
  } else {
    return true;
  }
}

resetPrice() {
  this.searchParms.min_price = 0;
  this.searchParms.max_price = 0;

  // this.search();
  this.setLocRoute();
  if (typeof $ != "undefined") {
    $("#mobileFiterModal").modal("hide");
  }
}

setArea(type, area) {
  if(type == 'min') {
    this.searchParms.min_area = area;
  } else if(type == 'max') {
    this.searchParms.max_area = area;
  }
}

setMinAreas(area) {
  if (this.searchParms.max_area != 0) {
    if(area < this.searchParms.max_area) {
      return true;
    } else {
      return false;
    }
  } else {
    return true;
  }
}

resetArea() {
  this.searchParms.min_area = 0;
  this.searchParms.max_area = 0;
  if (typeof $ != "undefined") {
    $("#mobileFiterModal").modal("hide");
  }
  // this.search();
  this.setLocRoute();
}

UpdateUnit() {
  this.searchParms.area_unit = this.modalChangeUnit;

  this.resetArea();
}

setSEO_Tags(args: string, filterSEO: any[]) {
  // Set Title & Metatag for property
  if (args == "City") {
    let title = '';
    if(filterSEO.length > 0) {
      if(this.searchParms.sub_type == '' || this.searchParms.sub_type == 'all')
      title = filterSEO[0] + ' ' + filterSEO[1].toUpperCase() + ' ' + this.searchParms.type + ' for '+ this.searchParms.purpose +' in '+ this.searchParms.city.name +' | Gharbaar.com';
      else
      title = filterSEO[0] + ' ' + filterSEO[1].toUpperCase() + ' ' + this.searchParms.sub_type + ' for '+ this.searchParms.purpose +' in '+ this.searchParms.city.name +' | Gharbaar.com';
    } else {
      if(this.searchParms.sub_type == '' || this.searchParms.sub_type == 'all')
      title = this.searchParms.type + ' for '+ this.searchParms.purpose +' in '+ this.searchParms.city.name +' | Gharbaar.com';
      else
      title = this.searchParms.sub_type + ' for '+ this.searchParms.purpose +' in '+ this.searchParms.city.name +' | Gharbaar.com';
    }

    // Set Description
    // let desc  = 'Find properties for '+ this.searchParms.purpose +' in '+this.searchParms.city.name +' on Gharbaar.com. Get complete details of properties and available amenities. Pakistan\'s No.1 Real Estate Portal. The most up-to-date listing information.'
    let desc  = '';
    if(filterSEO.length > 0) {
      if(this.searchParms.sub_type == '' || this.searchParms.sub_type == 'all')
      desc = 'Find ' + filterSEO[0] + ' ' + filterSEO[1].toUpperCase() + ' ' + this.searchParms.type + ' for '+ this.searchParms.purpose +' in '+ this.searchParms.city.name +' on Gharbaar.com. Get complete details of properties and available amenities. Pakistan\'s No.1 Real Estate Portal. The most up-to-date listing information.';
      else
      desc = 'Find ' + filterSEO[0] + ' ' + filterSEO[1].toUpperCase() + ' ' + this.searchParms.sub_type + ' for '+ this.searchParms.purpose +' in '+ this.searchParms.city.name +' on Gharbaar.com. Get complete details of properties and available amenities. Pakistan\'s No.1 Real Estate Portal. The most up-to-date listing information.';
    } else {

      // Without SEO Filter
      if(this.searchParms.sub_type == '' || this.searchParms.sub_type == 'all')
      desc = 'Find ' + this.searchParms.type + ' for '+ this.searchParms.purpose +' in '+ this.searchParms.city.name +' on Gharbaar.com. Get complete details of properties and available amenities. Pakistan\'s No.1 Real Estate Portal. The most up-to-date listing information.';
      else
      desc = 'Find ' + this.searchParms.sub_type + ' for '+ this.searchParms.purpose +' in '+ this.searchParms.city.name +' on Gharbaar.com. Get complete details of properties and available amenities. Pakistan\'s No.1 Real Estate Portal. The most up-to-date listing information.';
    }

    // Set Basic Meta Tags
    this.seoService.updateTitle(title);
    this.seoService.updateDescription(desc);

    // Set Og Meta tags
    this.seoService.updateOgTitle(title);
    this.seoService.updateOgDesc(desc);
    this.seoService.updateOgUrl();

    // Set Twitter Meta Tags
    this.seoService.updateTwitterTitle(title);
    this.seoService.updateTwitterDesc(desc);

    // Set Canonical Tag
    this.seoService.updateCanonicalUrl();

  } else {
    let title = '';
    if(filterSEO.length > 0) {
      if(this.searchParms.sub_type == '' || this.searchParms.sub_type == 'all')
      title = filterSEO[0] + ' ' + filterSEO[1].toUpperCase() + ' ' + this.searchParms.type + ' for '+ this.searchParms.purpose +' in '+ this.searchParms.area.name +', '+ this.searchParms.city.name +' | Gharbaar.com';
      else
      title = filterSEO[0] + ' ' + filterSEO[1].toUpperCase() + ' ' + this.searchParms.sub_type + ' for '+ this.searchParms.purpose +' in '+ this.searchParms.area.name +', '+ this.searchParms.city.name +' | Gharbaar.com';
    } else {
      if(this.searchParms.sub_type == '' || this.searchParms.sub_type == 'all')
      title = this.searchParms.type + ' for '+ this.searchParms.purpose +' in '+ this.searchParms.area.name +', '+ this.searchParms.city.name +' | Gharbaar.com';
      else
      title = this.searchParms.sub_type + ' for '+ this.searchParms.purpose +' in '+ this.searchParms.area.name +', '+ this.searchParms.city.name +' | Gharbaar.com';
    }

    // Set Description
    // let desc  = 'Find properties for '+ this.searchParms.purpose +' in '+this.searchParms.city.name +' on Gharbaar.com. Get complete details of properties and available amenities. Pakistan\'s No.1 Real Estate Portal. The most up-to-date listing information.'
    let desc  = '';
    if(filterSEO.length > 0) {
      if(this.searchParms.sub_type == '' || this.searchParms.sub_type == 'all')
      desc = 'Find ' + filterSEO[0] + ' ' + filterSEO[1].toUpperCase() + ' ' + this.searchParms.type + ' for '+ this.searchParms.purpose +' in '+ this.searchParms.area.name +', '+ this.searchParms.city.name +' on Gharbaar.com. Get complete details of properties and available amenities. Pakistan\'s No.1 Real Estate Portal. The most up-to-date listing information.';
      else
      desc = 'Find ' + filterSEO[0] + ' ' + filterSEO[1].toUpperCase() + ' ' + this.searchParms.sub_type + ' for '+ this.searchParms.purpose +' in '+ this.searchParms.area.name +', '+ this.searchParms.city.name +' on Gharbaar.com. Get complete details of properties and available amenities. Pakistan\'s No.1 Real Estate Portal. The most up-to-date listing information.';
    } else {

      // Without SEO Filter
      if(this.searchParms.sub_type == '' || this.searchParms.sub_type == 'all')
      desc = 'Find ' + this.searchParms.type + ' for '+ this.searchParms.purpose +' in '+ this.searchParms.area.name +', '+ this.searchParms.city.name +' on Gharbaar.com. Get complete details of properties and available amenities. Pakistan\'s No.1 Real Estate Portal. The most up-to-date listing information.';
      else
      desc = 'Find ' + this.searchParms.sub_type + ' for '+ this.searchParms.purpose +' in '+ this.searchParms.area.name +', '+ this.searchParms.city.name +' on Gharbaar.com. Get complete details of properties and available amenities. Pakistan\'s No.1 Real Estate Portal. The most up-to-date listing information.';
    }

    // Set Basic Meta Tags
    this.seoService.updateTitle(title);
    this.seoService.updateDescription(desc);

    // Set Og Meta tags
    this.seoService.updateOgTitle(title);
    this.seoService.updateOgDesc(desc);
    this.seoService.updateOgUrl();

    // Set Twitter Meta Tags
    this.seoService.updateTwitterTitle(title);
    this.seoService.updateTwitterDesc(desc);

    // Set Canonical Tag
    this.seoService.updateCanonicalUrl();
  }
}

getSearchAreas() {
  // Reset Area Selects
  this.areas        = [];
  this.areasBuffer  = [];

  const url  = AppSettings.API_ENDPOINT + 'locations/get-all-locations?city_id=' + this.searchParms.city.id;
  // console.log("url: ",url);
  this.helperService.httpGetRequests(url).then(resp => {
    if(typeof resp != "undefined") {
      // console.log("resp: ",resp);

      // Set Areas
      this.areas       = resp.locations;
      this.areasBuffer = this.areas.slice(0, this.bufferSize);

      // Hide Area Loader
      this.areasLoading = false;
    }
  });
}
getSearchAgencies(event: any){
  // Reset Area Selects
  this.agencies        = [];
  // this.agenciesBuffer  = [];

  const url  = AppSettings.API_ENDPOINT + 'get-agency-name';
  let data  = { city_id: this.searchParms.city.id, search: event.term };
  // let data  = { search: event.term };

  this.helperService.httpPostRequests(url, data).then(resp => {
    if(typeof resp != "undefined") {
      console.log("resp: ",resp);

      // Set Agency
      this.agencies       = resp;
    }
  });
}

selectCity(city) {
  // Reset Area Selects
  this.searchParms.city = city;
  // console.log("this.searchParms.city :",  this.searchParms.city)
  this.areas        = [];
  this.areasBuffer  = [];
  this.searchParms.area = {
    city_id   : null,
    id        : null,
    name      : "",
    p_loc     : [],
    parent_id : ""
  };

  // let selected = event.target.selectedOptions[0];
  // this.searchParms.city = { id: selected.value, name: selected.label}

  // Update Search
  // this.search();
  this.setLocRoute();
}

selectArea() {
  // console.log("this.SelectedArea:", this.SelectedArea);
  // console.log("this.searchParms.area:", this.searchParms.area);
  if(this.searchParms.area == null)
  this.searchParms.area = {
    city_id   : null,
    id        : null,
    name      : "",
    p_loc     : [],
    parent_id : ""
  };

  // console.log("this.searchParms: ",this.searchParms)
  // Update Search
  // this.search();
  this.setLocRoute();
}

InvertPurpose() {
  if (this.searchParms.purpose == 'sale') {
    // this.searchParms.purpose = 'rent';
    return this.helperService.getLocURL(this.searchParms.area, this.searchParms.city, this.locsByCity, 'rent', this.searchParms.type, this.searchParms.sub_type);
  } else {
    // this.searchParms.purpose = 'sale';
    return this.helperService.getLocURL(this.searchParms.area, this.searchParms.city, this.locsByCity, 'sale', this.searchParms.type, this.searchParms.sub_type);
  }

  this.setPurpose();
}

updatePurpose(purpose) {
  this.searchParms.purpose = purpose;
  this.setPurpose();
}

setPurpose() {
  this.purposeService.updatePurpose(this.searchParms.purpose);

  // this.search();
  this.setLocRoute();
}

setViewAllLocURL() {
  let purpose = 1; // 1 is for purpose 'sale'
  if(this.searchParms.purpose == 'rent')
  purpose = 2;

  let searchSeq = this.helperService.getSearchTypes_Seq();
  let typeSubType_ID = searchSeq[this.searchParms.type].id;
  if(this.searchParms.sub_type != 'all' && this.searchParms.sub_type != "")
  typeSubType_ID = searchSeq[this.searchParms.sub_type].id;

  this.viewAllLocURL = '/locations/' + this.searchParms.city.name.replace(/ /g, "_") + '-' + this.searchParms.city.id + '-' + purpose + '-' + typeSubType_ID;
}

getLocURL(location, byCity) {
  return this.helperService.getLocURL(location, this.searchParms.city, byCity, this.searchParms.purpose, this.searchParms.type, this.searchParms.sub_type);
}

setLocRoute() {
  let routeLink = '';

  if (typeof this.searchParms.area.id != "undefined" && this.searchParms.area.id != null) {
    routeLink = this.helperService.getLocURL(this.searchParms.area, this.searchParms.city, false, this.searchParms.purpose, this.searchParms.type, this.searchParms.sub_type);

    // Set Params locally
    this.setParams_locally(this.searchParms.area);
  } else {
    routeLink = this.helperService.getLocURL({}, this.searchParms.city, true, this.searchParms.purpose, this.searchParms.type, this.searchParms.sub_type);

    // Set Params locally
    this.setParams_locally([]);
  }

  // Navigate to new Route
  this.router.navigateByUrl('/', {skipLocationChange: true}).then(()=>
  this.router.navigate([routeLink]));

  if (typeof $ != "undefined") {
    $("#mobileFiterModal").modal("hide");
  }
}

getParentLocations(id): Promise<any> {
  // Show BreadCrumb Loader
  this.loadingBreadCrumbs = true;

  const url = AppSettings.API_ENDPOINT + "locations/get-parent-locations-by-sublocation?sub_loc=" + id;
  return this.helperService.httpGetRequests(url).then(resp => {
    // Hide BreadCrumb Loader
    this.loadingBreadCrumbs = false;

    if (typeof resp.p_loc != "undefined") {
      let searchedArea = resp.p_loc.slice()[0];

      // if(typeof this.searchParms.area.p_loc != "undefined") {
      // }
      this.parentLocs  = [...resp.p_loc.slice().reverse()];


      this.searchParms.area = {
        city_id   : searchedArea.city_id,
        id        : searchedArea.id,
        name      : searchedArea.name,
        p_loc     : this.parentLocs,
        parent_id : searchedArea.parent_id,
        slug      : searchedArea.slug
      }
      // Set City param
      this.searchParms.city = { id: searchedArea.city_id, name: resp.city };

      // Remove the Last parent as its the same as searched area
      this.parentLocs.pop();

      // Get Searched Area Locations
      this.getSubLocationsByArea(searchedArea.id);
      this.locsByCity = false;

      return true;
    }
  })
}

getSubLocationsByArea(id) {
  const url  = AppSettings.API_ENDPOINT + 'locations/get-sub-locations-details?p_id=' + id  + "&type=" + this.searchParms.type + "&sub_type=" + this.searchParms.sub_type + "&purpose=" + this.searchParms.purpose;
  this.helperService.httpGetRequests(url).then(resp => {
    if (typeof resp.locations != "undefined") {
      this.subLocs        = resp.locations;
      this.subLocs.sort((a,b) => a.counts - b.counts);
      this.subLocs.reverse();

      this.sortedSubLocs  = [...resp.locations];
      this.sortedSubLocs.sort((a,b) => a.name.localeCompare(b.name));
    }

    // Set Prop Counts
    if(typeof resp.types != "undefined")
    this.Types_Prop_Count = resp.types;
  });
}

getSubLocationsByCity(id) {
  // Show BreadCrumb Loader
  this.loadingBreadCrumbs = true;

  const url  = AppSettings.API_ENDPOINT + 'locations/get-parent-locations-details?city_id=' + id + "&type=" + this.searchParms.type + "&sub_type=" + this.searchParms.sub_type + "&purpose=" + this.searchParms.purpose;
  this.helperService.httpGetRequests(url).then(resp => {
    // Hide BreadCrumb Loader
    this.loadingBreadCrumbs = false;

    if (typeof resp.locations != "undefined") {
      this.subLocs     = resp.locations;
      this.subLocs.sort((a,b) => a.counts - b.counts);
      this.subLocs.reverse();

      this.sortedSubLocs  = [...resp.locations];
      this.sortedSubLocs.sort((a,b) => a.name.localeCompare(b.name));
    }

    // Set Prop Counts
    if(typeof resp.types != "undefined")
    this.Types_Prop_Count = resp.types;
  });
}

toggleSort() {
  this.locs_sorted = !this.locs_sorted;
}

toggleLocsDiv() {
  this.hideLocs = !this.hideLocs;
}

SortListing() {
  console.log("this.SortBy: ",this.SortBy.split('-'));
  this.searchParms.sort_by   = this.SortBy.split('-')[0];
  this.searchParms.sort_type = this.SortBy.split('-')[1];

  // Sort Listing
  this.getListings();
}

getPageList(page, prev, next) {
  if (prev) {
    this.searchParms.current_page = page - 1;
    if(this.searchParms.current_page < 0)
    this.searchParms.current_page = 0;
  } else if (next) {
    this.searchParms.current_page = page + 1;
    if(this.searchParms.current_page > this.ListingsData.last_page)
    this.searchParms.current_page = this.searchParms.current_page - 1;
  } else {
    this.searchParms.current_page = page;
  }

  this.getListings();
}

getListings() {
  const url  = AppSettings.API_ENDPOINT + 'search';

  let agent_id = '';
  if(this.searchParms.agency)
  agent_id = this.searchParms.agency.user_id;

  let data = {
    purpose      : this.searchParms.purpose,
    type         : this.searchParms.type,
    sub_type     : this.searchParms.sub_type,
    city_id      : this.searchParms.city.id,
    location_id  : this.searchParms.area.id,
    min_price    : this.searchParms.min_price,
    max_price    : this.searchParms.max_price,
    min_area     : this.searchParms.min_area,
    max_area     : this.searchParms.max_area,
    area_unit    : this.searchParms.area_unit,
    bedrooms     : this.searchParms.bedrooms,
    bathrooms    : this.searchParms.bathrooms,
    sort_by      : this.searchParms.sort_by,
    sort_type    : this.searchParms.sort_type,
    current_page : this.searchParms.current_page,
    agent_id     : agent_id
  }
  // console.log("**********data: ",data);
  this.helperService.httpPostRequests(url, data).then(resp => {
    if (typeof resp != "undefined") {
      this.ListingsData = resp;
      this.gharbaarLoader = false;
      // console.log("this.ListingsData",this.ListingsData)

      // Initialize Map
      let self = this;
      setTimeout(function() {
        self.initMap();
      }, 300);

      // Save search data
      this.saveSearchService.updateSearch(resp.data);

      if (this.ListingsData.data.length > 0) {
        this.Listings_Available = true;
      } else {
        this.Listings_Available = false;
      }

      this.pagination = [];
      if (this.ListingsData.last_page > 1) {
        for (let i = 1; i <= this.ListingsData.last_page; i++) {
          this.pagination.push(i);
        }
      }
    }
  }).catch(error => {
    console.error("error: ",error);
  });
}

checkPropFavrt(id) {
  return this.favoriteService.getFavoritePropByIndex(id);
}

togglefavoriteProp(property) {
  property.id = property.p_id;
  if(this.checkPropFavrt(property.id))
  this.favoriteService.removefavoriteProp(property.id);
  else
  this.favoriteService.addfavoriteProp(property);
}

checkPropHidden(id) {
  return this.hideService.getHiddenPropByIndex(id);
}

toggleHiddenProp(property) {
  property.id = property.p_id;
  if(this.checkPropHidden(property.id))
  this.hideService.removeHiddenProp(property.id);
  else
  this.hideService.addHiddenProp(property);
}

propertyRoute(property) {
  return this.helperService.propertyRoute(property);
}

propertyURL(property) {
  return this._location['_platformLocation'].protocol + '//' + this._location['_platformLocation'].hostname + this.helperService.propertyRoute(property);
}
replaceString(s) {
  return s.toLowerCase().replace(/ /g, '-').replace(/_/g, '-').replace(/[/\\*]/g, "-").replace(/---/g, '-');
}
agencyProfileRoute(agency) {
  // console.log("agency",agency)
  return '/agent/' + this.replaceString(agency.agency_city) + '_' + this.replaceString(agency.agency_name) + '-' + agency.user_id;
}
open_ContactDetails_Modal(property) {
  const initialState = { property: property };
  const config: ModalOptions = { class: 'modal-sm phone-number', initialState };
  this.modalRef = this.modalService.show(ContactDetailsComponent, config);
}

open_Inquiry_Modal(property) {
  const initialState = { property: property };
  const config: ModalOptions = { class: 'modal-md af-signup inquiry', initialState };
  this.modalRef = this.modalService.show(InquiryModalComponent, config);
}

checkBucketPropsByID(propID) {
  return this.emailBucketService.checkBucketPropsByID(propID);
}

toggleEmailBasket(property) {
  if (this.emailBucketService.checkBucketPropsByID(property.p_id)) {   // If already exisiting then remove it
    this.emailBucketService.removeBucketPropByID(property.p_id);
  } else {                                                                // Else add to Email Basket
    this.emailBucketService.addBucketProps(property);
  }
}

convertToInt(val) {
  return parseInt(val);
}

expandFeatures(property, index) {
  console.log("property: ",property);
  console.log("index: ",index);

  if (typeof this.ListingsData.data[index].expand == "undefined") {
    this.ListingsData.data[index].expand = false;
  }

  this.ListingsData.data[index].expand = !this.ListingsData.data[index].expand;
  // property.expand
}

setParams_locally(loc) {
  let data = {
    city       : this.searchParms.city,
    area       : loc,
    agency     : this.searchParms.agency,
    type       : this.searchParms.type,
    sub_type   : this.searchParms.sub_type,
    purpose    : this.searchParms.purpose,
    minPrice   : this.searchParms.min_price,
    maxPrice   : this.searchParms.max_price,
    minArea    : this.searchParms.min_area,
    maxArea    : this.searchParms.max_area,
    area_unit  : this.searchParms.area_unit,
    bedrooms   : this.searchParms.bedrooms,
    bathrooms  : this.searchParms.bathrooms,
  }

  // Set Search Params
  this.helperService.setSearchParms(data);
}

navigateRoute(url, params) {
  this.router.navigateByUrl('/', {skipLocationChange: true}).then(()=>
  this.router.navigate([url, params]));
}

toggleFeatures() {
  this.showFeatures = !this.showFeatures;
}

toggleEmailBucket(){
  this.hideEmailBucket = !this.hideEmailBucket ;
}

openSideInner(){
  this.innerSideFilter = !this.innerSideFilter ;
  $("body").toggleClass("overflow-hidden");

  if (typeof window != "undefined") {
    if (window.innerWidth < 768){
      window.scrollTo(0, 0);
    }
  }
}

addOverflow(){
  this.overFlow = !this.overFlow;
}

//////////////////////////////////////////
/********* Areas Ng-Select Fns *********/
////////////////////////////////////////
onScrollToEnd_Areas() {
  this.fetchMore_Areas();
}

onScroll_Areas({ end }) {
  if (this.areasLoading || this.areas.length <= this.areasBuffer.length) {
    return;
  }

  if (end + this.numberOfItemsFromEndBeforeFetchingMore >= this.areasBuffer.length) {
    this.fetchMore_Areas();
  }
}

private fetchMore_Areas() {
  const len = this.areasBuffer.length;
  const more = this.areas.slice(len, this.bufferSize + len);
  this.areasLoading = true;
  // using timeout here to simulate backend API delay
  setTimeout(() => {
    this.areasLoading = false;
    this.areasBuffer  = this.areasBuffer.concat(more);
  }, 200)
}
setNumbers(args,type) {
  if (type == "beds") {
    this.searchParms.bedrooms = this.searchParms.bedrooms + args;
    if (this.searchParms.bedrooms < 0) {
      this.searchParms.bedrooms = 0;
    }
    this.getListings();
  }
  else if (type == "baths") {
    this.searchParms.bathrooms = this.searchParms.bathrooms + args;
    if (this.searchParms.bathrooms < 0) {
      this.searchParms.bathrooms = 0;
    }
    this.getListings();
  }
  else{}
}
toggleMap(){
  this.maptoggler = !this.maptoggler;
  this.mapViewMob = !this.mapViewMob;
}
toggleLocations(toggle){
  this.openLoc = !this.openLoc;
  if (toggle == 'open') {
    $("body").addClass("overflow-hidden")
  }
  else{
    $("body").removeClass("overflow-hidden")
  }
}

initMap() {
  if (typeof google != "undefined") {
    // Hide Simple Map
    this.showSimpleMap = false;

    // Initialize Map if not already
    if(!this.listingMap) {
      // let position = new google.maps.LatLng(this.ListingsData.data[i].latitude, this.ListingsData.data[i].longitude);
      const mapProperties = {
        // center    : position,
        clickableIcons : false,
        zoom           : 15,
        mapTypeId      : google.maps.MapTypeId.ROADMAP,
        panControlOptions: {
          position: google.maps.ControlPosition.RIGHT_TOP
        },
        streetViewControlOptions: {
          position: google.maps.ControlPosition.RIGHT_TOP
        },
        zoomControlOptions: {
          position: google.maps.ControlPosition.RIGHT_TOP
        },
        styles : (AppSettings.mapStyles as any)
      };

      this.listingMap  = new google.maps.Map(this.mapElement.nativeElement, mapProperties);

      this.mapBounds   = new google.maps.LatLngBounds();

      // Initialize Info Window
      this.infowindo = new google.maps.InfoWindow();
    }

    // Add Listing Markers
    this.addMarkers();
  }
}

addMarkers() {
  // Empty Markers Array
  this.markersArray = [];

  for (let i = 0; i < this.ListingsData.data.length; i++) {
    if(this.ListingsData.data[i].latitude != null && this.ListingsData.data[i].longitude != null) {
      let position = new google.maps.LatLng(this.ListingsData.data[i].latitude, this.ListingsData.data[i].longitude);
      let price    = this.ListingsData.data[i].price + '';
      this.labelPrice = price;

      this.setMarker(position, this.ListingsData.data[i]);

      // let url =  "assets/site/images/gharbaarPin.svg";
      // let marker = new google.maps.Marker({
      //     map       : this.listingMap,
      //     position  : position,
      //     icon      : url
      // });
      //
      // this.markersArray.push(marker);
      //
      // // Extend Bounds
      // this.mapBounds.extend(position);

      this.listingMap.setCenter(position);
      // this.markersArray.push(marker);
    }
  }

  // Set Bounds or Show Simple map incase no markers are available
  // if(this.markersArray.length > 0)
  //   this.listingMap.fitBounds(this.mapBounds);
  // else
  //   this.setMapWithoutMarkers();
  if(this.markersArray.length > 1)
  this.listingMap.fitBounds(this.mapBounds);
  else if(this.markersArray.length < 1)
  this.setMapWithoutMarkers();
  // this.showSimpleMapFn();

}

setMarker(position, property) {
  let self = this;

  function HTMLMarker(lat, lng) {
    this.lat = lat;
    this.lng = lng;
    this.pos = new google.maps.LatLng(lat,lng);
  }

  HTMLMarker.prototype          = new google.maps.OverlayView();
  HTMLMarker.prototype.onRemove = function(){}

  //init your html element here
  HTMLMarker.prototype.onAdd = function() {
    // console.log("**** property: ",property);
    this.div = document.getElementById("mapsLabel-" + property.p_id);
    // this.div.className = "htmlMarker";
    this.div.style.position = 'absolute';
    // this.div.innerHTML = '<span>'+label+'</span>';
    var panes = this.getPanes();
    panes.overlayImage.appendChild(this.div);
  }

  HTMLMarker.prototype.draw = function() {
    var overlayProjection  = this.getProjection();
    var position           = overlayProjection.fromLatLngToDivPixel(this.pos);
    var panes              = this.getPanes();
    this.div.style.left    = position.x + 'px';
    this.div.style.top     = position.y + 'px';

    let pos = this.pos;
    let div = this.div;
    google.maps.event.addDomListener(this.div, "click", function(event) {
      // // google.maps.event.trigger(self, "click");
      // // console.log("event: ", event);
      // console.log("property: ", property);
      // console.log("pos: ", pos);
      // console.log("self.listingMap: ", self.listingMap);
      //  console.log("self.infowindo: ", self.infowindo);
      self.infoWindowProperty = property;
      self.propertyRouteInfo = self.helperService.propertyRoute(property);
      $('#infowindow-content').removeClass('af-none');

      $('.mapBox').removeClass('hideTri');

      self.infowindo.setContent(document.getElementById('infowindow-content'));
      self.infowindo.setPosition(pos);
      self.infowindo.open(self.listingMap);
    });
  }

  //to use it
  let marker;
  marker = new HTMLMarker(position.lat(), position.lng());
  marker.setMap(this.listingMap);

  // Extend Bounds
  this.mapBounds.extend(marker.pos);

  // Add to Markers Array
  this.markersArray.push(marker);
}

adjustMap(property_id, bool){
  if (bool) {
    $("#mapsLabel-" + property_id).addClass("hovered");
  }
  else{
    $("#mapsLabel-" + property_id).removeClass("hovered");
  }
}
adjustList(property_id, bool){
  if (bool) {
    $("#listLabel-" + property_id).addClass("hovered");
  }
  else{
    $("#listLabel-" + property_id).removeClass("hovered");
  }
}

setMapWithoutMarkers() {
  let address;
  if(this.locsByCity)
  address = this.searchParms.city.name;
  else
  address = this.searchParms.area.name + ', ' + this.searchParms.city.name;

  // console.log("********** address: ",address);
  let self     = this;
  let geocoder = new google.maps.Geocoder();
  geocoder.geocode( { 'address': address}, function(results, status) {
    if (status.toString() == "OK") {
      self.Location_LatLng  = new google.maps.LatLng(results[0].geometry.location.lat(), results[0].geometry.location.lng());
      self.listingMap.setCenter(self.Location_LatLng);
    } else {
      // alert('Geocode was not successful for the following reason: ' + status);
      console.error('Geocode was not successful for the following reason: ' + status);
    }
  });
}

closeInfoWindow(){
  $('#infowindow-content').addClass('af-none');
  $('.mapBox').addClass('hideTri');
}
showSimpleMapFn() {
  // Show Simple Map if map has no markers
  this.showSimpleMap = true;

  let address;
  if(this.locsByCity)
  address = this.searchParms.city.name;
  else
  address = this.searchParms.area.name + ' ' + this.searchParms.city.name;

  this.simpleMapUrl = 'https://www.google.com/maps/embed/v1/place?q='+ address +'&key=' + AppSettings.GOOGLE_MAPS_KEY;
}
open_Login_Modal() {
  this.modalService.show(LoginComponent, {});
}
saveProperty(is_liked,properties) {
  let user = this.authenticationService.get_currentUserValue();
  if (user) {
    properties.is_liked = is_liked;
    let data  = { property_id: properties.p_id };
    const url = AppSettings.API_ENDPOINT + 'auth/like';

    this.helperService.httpPostRequests(url, data).then(resp => {
      // console.log("resp: ",resp);
    }).catch(error => {
      console.error("error: ",error);
    });
  } else {
    this.open_Login_Modal(); // Open Login Pop-up
  }
}
autoHeight(){
  this.autoHeightLoc = !this.autoHeightLoc;
}
closeFilterview(){
  console.log('hi')
  $("#mobileFiterModal").modal("hide");
}
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchListingNewDesignComponent } from './search-listing-new-design.component';

describe('SearchListingNewDesignComponent', () => {
  let component: SearchListingNewDesignComponent;
  let fixture: ComponentFixture<SearchListingNewDesignComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchListingNewDesignComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchListingNewDesignComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

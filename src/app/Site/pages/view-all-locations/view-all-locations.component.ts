import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Router, ActivatedRoute } from "@angular/router";
import { AppSettings, HelperService, SEOService, PurposeService, FavoriteService, HideService, SaveSearchService } from '../../../services/_services';

declare var $;

@Component({
  selector: 'app-view-all-locations',
  templateUrl: './view-all-locations.component.html',
  styleUrls: ['./view-all-locations.component.css']
})
export class ViewAllLocationsComponent implements OnInit {

  public city             : any = { id: '', name : ''};
  public locations        : any = [];
  public sorted_locations : any = [];
  public locs_sorted      : any = false;
  public purpose          : any = "";
  public type             : any = "";
  public sub_type         : any = "all";

  constructor(
    private router                : Router,
    private activatedRoute        : ActivatedRoute,
    private helperService         : HelperService,
    private seoService            : SEOService,
    private _location             : Location,
  ) {
  }

  ngOnInit() {

    // this.router.navigate(['/']);
    // console.log("this.activatedRoute: ",this.activatedRoute);
    this.activatedRoute.params.subscribe(routeParams => {
      let data = routeParams.city.split('-');

      if(data.length == 4) {
          this.city.name = data[0];
          this.city.id   = parseInt(data[1]);
          if(parseInt(data[2]) == 1)
            this.purpose = 'sale';
          else
            this.purpose = 'rent';

          let seacrhTypes = this.helperService.getSearchTypes_Seq();

          for (let key in seacrhTypes) {
              if(seacrhTypes[key].id == parseInt(data[3])) {
                if(seacrhTypes[key].type == 'type')
                  this.type = key;
                else {
                  this.type     = seacrhTypes[key].parent;
                  this.sub_type = key;
                }
                break;
              }
          }

          // Get Locations
          this.getSubLocationsByCity();

          // Set SEO
          this.setSEO_Tags("City");
      } else {
          this.NotFound404();
      }
    });

  }

  NotFound404() {
    this.router.navigate(['/']);
  }

  getSubLocationsByCity() {
    const url  = AppSettings.API_ENDPOINT + 'locations/get-parent-locations-details?city_id=' + this.city.id + "&type=" + this.type + "&sub_type=" + this.sub_type + "&purpose=" + this.purpose;
    this.helperService.httpGetRequests(url).then(resp => {
      if (typeof resp.locations != "undefined") {
        this.locations        = resp.locations;
        this.locations.sort((a,b) => a.counts - b.counts);
        this.locations.reverse();
        // this.locations = resp.locations;

        this.sorted_locations = [...resp.locations];
        this.sorted_locations.sort((a,b) => a.name.localeCompare(b.name));
      }
    });
  }

  toggleSort() {
    console.log("this.locs_sorted: ",this.locs_sorted);
    this.locs_sorted = !this.locs_sorted;
  }

  getLocURL(location, byCity) {
    return this.helperService.getLocURL(location, this.city, byCity, this.purpose, this.type, this.sub_type);
  }

  setSEO_Tags(args: string) {
    let title = "";
    if(this.sub_type == '' || this.sub_type == 'all')
      title = 'Locations of '+ this.type +' for '+ this.purpose +' in '+ this.city.name +' - Gharbaar.com';
    else
      title = 'Locations of '+ this.sub_type +' for '+ this.purpose +' in '+ this.city.name +' - Gharbaar.com';

    let desc = "";
    if(this.sub_type == '' || this.sub_type == 'all')
      desc  = 'List of '+ this.type +' for '+ this.purpose +' in '+ this.city.name +' - Gharbaar.com';
    else
      desc  = 'List of '+ this.sub_type +' for '+ this.purpose +' in '+ this.city.name +' - Gharbaar.com';

    // Set Title & Metatag for property
    if (args == "City") {
        // Set Basic Meta Tags
        this.seoService.updateTitle(title);
        this.seoService.updateDescription(desc);

        // Set Og Meta tags
        this.seoService.updateOgTitle(title);
        this.seoService.updateOgDesc(desc);
        this.seoService.updateOgUrl();

        // Set Twitter Meta Tags
        this.seoService.updateTwitterTitle(title);
        this.seoService.updateTwitterDesc(desc);

        // Set Canonical Tag
        this.seoService.updateCanonicalUrl();

    } else {
        // Set Basic Meta Tags
        this.seoService.updateTitle(title);
        this.seoService.updateDescription(desc);

        // Set Og Meta tags
        this.seoService.updateOgTitle(title);
        this.seoService.updateOgDesc(desc);
        this.seoService.updateOgUrl();

        // Set Twitter Meta Tags
        this.seoService.updateTwitterTitle(title);
        this.seoService.updateTwitterDesc(desc);

        // Set Canonical Tag
        this.seoService.updateCanonicalUrl();
    }
  }

  navigateRoute(url, params) {
    this.router.navigateByUrl('/', {skipLocationChange: true}).then(()=>
      this.router.navigate([url, params]));
  }

}

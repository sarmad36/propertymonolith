import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewAllLocationsComponent } from './view-all-locations.component';

describe('ViewAllLocationsComponent', () => {
  let component: ViewAllLocationsComponent;
  let fixture: ComponentFixture<ViewAllLocationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewAllLocationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewAllLocationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

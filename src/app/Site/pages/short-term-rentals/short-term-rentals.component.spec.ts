import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShortTermRentalsComponent } from './short-term-rentals.component';

describe('ShortTermRentalsComponent', () => {
  let component: ShortTermRentalsComponent;
  let fixture: ComponentFixture<ShortTermRentalsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShortTermRentalsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShortTermRentalsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

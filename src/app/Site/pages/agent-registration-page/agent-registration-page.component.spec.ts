import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgentRegistrationPageComponent } from './agent-registration-page.component';

describe('AgentRegistrationPageComponent', () => {
  let component: AgentRegistrationPageComponent;
  let fixture: ComponentFixture<AgentRegistrationPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgentRegistrationPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgentRegistrationPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

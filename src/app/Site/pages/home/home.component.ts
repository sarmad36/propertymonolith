import { Component, OnInit, AfterContentInit, ViewChild, Injectable } from '@angular/core';
import { Router } from "@angular/router";
import { HttpClient } from '@angular/common/http';
import { AppSettings, HelperService, PurposeService, LastSaveSearchService } from '../../../services/_services';
import { FormControl } from '@angular/forms';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { OwlCarousel } from 'ngx-owl-carousel';
// Bootsteap Components
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';

declare var navigator;

@Injectable({
  providedIn: 'root',
})

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, AfterContentInit {
  // @ViewChild('areasSelect', {static: false}) public areasSelect: any;

  public ImagesUrl :any = AppSettings.IMG_ENDPOINT;
  public bsModalRef: BsModalRef;

  // City and Areas List
  public cities          :any = AppSettings.cities;
  public areas           :any = [];

  public searchTypes     :any = [];
  public searchSubTypes  :any = [];

  // Params
  public SelectedCity   = AppSettings.cities[1];
  public SelectedArea   = [];
  public SelectedType    = "";
  public SelectedSubType = "";
  public Purpose        = "";
  public PurposeMob     = "";
  public searchPrices   = [];
  public minPrice       = 0;
  public maxPrice       = 0;
  public searchUnits    = [];
  public searchAreas    = {};
  public tempUnit       = "marla";
  public selectedUnit   = "marla";
  public minArea        = 0;
  public maxArea        = 0;
  public searchBeds     = AppSettings.searchBeds;
  public bedrooms       = 0;
  public SelectedProject :any;

  // Ng-Slect Params
  public areasBuffer  = [];
  public bufferSize   = 10;
  public numberOfItemsFromEndBeforeFetchingMore = 5;
  public areasLoading = false;
  public areaQuery    = "";
  public areasError   = false;

  // Https Params
  protected ngUnsubscribe: Subject<void> = new Subject<void>();
  public httpLoading     = false;
  public corsHeaders     : any;

  public areasSelect_F = new FormControl('');

  // Subscriptions
  public propuseSub :any;

  // Investment Projects Params
  public projects     : any = [];
  public projectsLocs : any = AppSettings.projectsLocs;

  // Last Search memeber
  public lastSearchSub  :any;
  public lastSearchData : any = [];
  public  popularLocations  :any;
  public  recentBlogs  :any;
  public homeData      :any;
  public platinumData  :any;
  public featuredAgency  :any;

  public platinumSlideAgencies = [];
  public activeCenter :any;
  public platinumAgenciesOptions = {mouseDrag:false,loop:true,rewind:true, dots: false, nav: true, thumbs:false,center:true,margin:25,stagePadding:25,
                                          responsive:{
                                              0:{
                                                  items:1
                                              },
                                              767:{
                                                  items:2
                                              },
                                              1000:{
                                                  items:3
                                              }
                                          }
                                      };

  public featuresArray1 = [];
  public featuresArray2 = [];
  public featuresArray3 = [];
  public featureAgenciesOptions = { mouseDrag:false,loop:true, dots: false, nav: true, thumbs:false,center:true,margin:0,stagePadding:45,
                                          responsive:{
                                              0:{
                                                  items:2,
                                              },
                                              767:{
                                                  items:5,
                                              },
                                              1000:{
                                                  items:10,
                                              }
                                          }
                                      };
  public featureSlideAgencies  =  [];
  public homePageBlogsItems;
  public homePageBlogs = { mouseDrag:true,loop:true, dots: false, nav: true, thumbs:false,center:true,margin:0,stagePadding:0,
                                          responsive:{
                                              0:{
                                                  items:1
                                              },
                                              767:{
                                                  items:1
                                              },
                                              1000:{
                                                  items:1
                                              }
                                          }
                                      };

  constructor(
    private modalService              : BsModalService,
    private http                      : HttpClient,
    private router                    : Router,
    private helperService             : HelperService,
    private purposeService            : PurposeService,
    private LastSaveSearchService     : LastSaveSearchService,
  ) {
    // Get Globals from AppSettings
    this.searchTypes     = Object.keys(AppSettings.searchTypes);
    this.searchSubTypes  = AppSettings.searchTypes;
    this.searchPrices    = AppSettings.searchPrices;
    this.searchAreas     = AppSettings.searchAreas;
    this.searchUnits     = AppSettings.searchUnits;

    // Set default selected type
    this.SelectedType    = this.searchTypes[0];

  }

  public openModal(template) {
    this.bsModalRef = this.modalService.show(template);
  }
  ngOnInit() {

    // Subscribe to Purpose 'Sale' or 'Rent'
    this.getPurpose(); // Resets everything

    // // Get Locations on Init
    // this.selectCity();

    if(typeof $ != "undefined") {
      // Dropdown close prevention
      $('body').on('click', function(event) {
        $('.status1.bootstrap-select.open').removeClass('open');
      });
      $('#myTabs').on('click', '.nav-tabs a', function() {
        $(this).closest('.dropdown').addClass('dontClose');
      })

      $('#myDropDown').on('hide.bs.dropdown', function(e) {
        if ( $(this).hasClass('dontClose') ) {
          e.preventDefault();
        }
        $(this).removeClass('dontClose');
      });

      // Prevent dropdown close on Price selection
      $('#price_groups').on('click', 'li', function() {
        $('#priceDiv').addClass('dontClose');
      })

      $('#priceDiv').on('hide.bs.dropdown', function(e) {
        if ( $(this).hasClass('dontClose') ) {
          e.preventDefault();
        }
        $(this).removeClass('dontClose');
      });

      // Prevent dropdown close on Area selection
      $('#area_groups').on('click', 'li', function() {
        $('#prparea').addClass('dontClose');
      })

      $('#prparea').on('hide.bs.dropdown', function(e) {
        if ( $(this).hasClass('dontClose') ) {
          e.preventDefault();
        }
        $(this).removeClass('dontClose');
      });
    }
  }

  ngAfterViewInit(){
    if(typeof $ != "undefined") {


      $(document).ready(function() {
          ($('.banner-carousel-new .owl-carousel') as any).owlCarousel({
          loop:true,
          margin:0,
          nav:false,
          dots:false,
          autoplay:true,
          autoplaySpeed: 3000,
          mouseDrag:false,
          animateOut: 'fadeOut',
          animateIn: 'fadeIn',
          responsive:{
              0:{
                  items:1
              },
              600:{
                  items:1
              },
              1000:{
                  items:1
              }
          }
        });
      });
    }
  }

  ngAfterContentInit() {
    this.getPopularLocations();
    //Called after ngOnInit when the component's or directive's content has been initialized.
    //Add 'implements AfterContentInit' to the class.
    this.deferImages();

    // Initialize Map
    let self = this;
    setTimeout(function() {
      self.getAdress();
    }, 1500);

    this.getLastSearchData();
  }

  ngOnDestroy() {
    if (typeof this.propuseSub != 'undefined') {
      //prevent memory leak when component destroyed
      this.propuseSub.unsubscribe();
    }
  }
  deferImages(){
    if (typeof document != "undefined") {
      var imgDefer = document.getElementsByTagName('img');
      for (var i=0; i<imgDefer.length; i++) {
        if(imgDefer[i].getAttribute('data-src')) {
          imgDefer[i].setAttribute('src',imgDefer[i].getAttribute('data-src'));
        }
      }
      var picDefer = document.getElementsByTagName('source');
      for (var i=0; i<picDefer.length; i++) {
        if(picDefer[i].getAttribute('data-src')) {
          picDefer[i].setAttribute('srcset',picDefer[i].getAttribute('data-src'));
        }
      }
    }
  }
  getPurpose() {
    this.propuseSub = this.purposeService.purposeS$.subscribe((purpose) => {
      // Reset Search and then update purpose
      this.resetSearch();

      this.Purpose = purpose;
    });
  }

  setPurpose(purpose: any) {
    this.purposeService.updatePurpose(purpose);
    this.PurposeMob = purpose;
  }

  removeSidebarFilter(){
    this.PurposeMob = '';
  }

  selectCity() {
    // Reset Area Selects
    this.areas        = [];
    this.areasBuffer  = [];
    // this.areasSelect.active = [];
    this.SelectedArea = [];
    this.projects     = [];

    if (this.SelectedCity.name != "") {
      let index = this.cities.findIndex(x => x.name === this.SelectedCity.name);
      this.SelectedCity = {"id": this.cities[index].id, "name": this.cities[index].name};
      this.projects     = AppSettings.projects[this.SelectedCity.id];
      // Show Area Loader
      // this.areasLoading = true;
      /////////////Get - locations /////////////
      let url  = AppSettings.API_ENDPOINT + 'locations/get-all-locations?city_id=' + this.SelectedCity.id;
      this.helperService.httpGetRequests(url).then(resp => {
        if(typeof resp.locations != "undefined") {
          // Set Areas
          this.areas       = resp.locations;
          this.areasBuffer = this.areas.slice(0, this.bufferSize);

          // Hide Area Loader
          this.areasLoading = false;
        }
      }).catch(error => {
        console.error("error: ",error);
      });


      // url  = AppSettings.API_ENDPOINT + 'projects/get-projects';
      // this.helperService.httpGetRequests(url).then(resp => {
      //   //Set Projects
      //   this.projects       = resp;
      //
      //   // Hide Area Loader
      //   this.areasLoading = false;
      //
      // }).catch(error => {
      //   console.error("error: ",error);
      // });

    }
  }

  changingBeds() {
    // console.log("this.bedrooms:", this.bedrooms);
  }

  selectArea() {
    if(this.SelectedArea == null)
    this.SelectedArea = [];
  }

  setPropertyType(type) {
    this.SelectedType    = type;
    this.SelectedSubType = "";
  }

  setPropertySubType(type) {
    this.SelectedSubType = type;
  }

  resetSearch() {
    this.SelectedCity = AppSettings.cities[1];
    this.selectCity(); // Reset Areas according to city
    this.SelectedType    = this.searchTypes[0];
    this.SelectedSubType = "";
    this.resetPrice();
    this.resetArea();
    this.resetUnit();
    this.bedrooms = 0;
  }

  UpdateUnit() {
    this.resetArea();
    this.selectedUnit = this.tempUnit;
  }

  resetUnit() {
    this.selectedUnit = "marla";
    this.tempUnit     = "marla";
  }

  checkPrice(type) {
    // console.log(type);
    // if(type == 'min' && this.minPrice > 5000000000) {
    //   alert('Min prize is too high');
    // }
    // else if(type == 'min' && (this.maxPrice > 0 && this.minPrice > this.maxPrice)) {
    //     alert('Min prize can\'t be greater than max');
    // } else if(type == 'max' && this.maxPrice > 5000000000) {
    //     alert('Max prize is too high');
    // }
  }

  setPrice(type, price) {
    if(type == 'min') {
      this.minPrice = price;
    } else if(type == 'max') {
      this.maxPrice = price;
    }
  }

  setMinPrices(price) {
    if (this.maxPrice != 0) {
      if(price < this.maxPrice) {
        return true;
      } else {
        return false;
      }
    } else {
      return true;
    }
  }

  resetPrice() {
    this.minPrice = 0;
    this.maxPrice = 0;
  }

  setArea(type, area) {
    if(type == 'min') {
      this.minArea = area;
    } else if(type == 'max') {
      this.maxArea = area;
    }
  }

  setMinAreas(area) {
    if (this.maxArea != 0) {
      if(area < this.maxArea) {
        return true;
      } else {
        return false;
      }
    } else {
      return true;
    }
  }

  resetArea() {
    this.minArea = 0;
    this.maxArea = 0;
  }

  setLocRoute() {
    // Set Params locally
    this.setParams_locally();

    if (typeof this.SelectedArea['id'] != "undefined") {
        return this.helperService.getLocURL(this.SelectedArea, this.SelectedCity, false, this.Purpose, this.SelectedType, this.SelectedSubType);
    } else {
        return this.helperService.getLocURL({}, this.SelectedCity, true, this.Purpose, this.SelectedType, this.SelectedSubType);
    }
  }


  investRoute() {
    // Set Params locally
    this.setParams_locally();

    // let routeLink =  '/projects/' + this.SelectedCity.name.replace(/ /g, "_") + "-" + this.SelectedCity.id + "-1"; // 1 in params specify its a City
    let routeLink =  this.helperService.investRoute(this.SelectedCity);
    return routeLink;
    // console.log("routeLink home",routeLink)
  }
  setPurposeRoute(purpose){
    return this.helperService.getLocURL({}, AppSettings.cities[1], true, purpose, Object.keys(AppSettings.searchTypes)[0], "");
  }

  setParams_locally() {
    let data = {
      city         : this.SelectedCity,
      area         : this.SelectedArea,
      type         : this.SelectedType,
      sub_type     : this.SelectedSubType,
      purpose      : this.Purpose,
      minPrice     : this.minPrice,
      maxPrice     : this.maxPrice,
      minArea      : this.minArea,
      maxArea      : this.maxArea,
      area_unit    : this.selectedUnit,
      bedrooms     : this.bedrooms
    }

    // Set Search Params
    this.helperService.setSearchParms(data);
  }

  setAgencyParams_locally() {
    let data = {
      city         : this.SelectedCity,
      // area         : this.SelectedArea,
      project      : this.SelectedProject
    }

    // Set Search Params
    this.helperService.setInvestSearchParms(data);
  }

  //////////////////////////////////////////
  /********* Areas Ng-Select Fns *********/
  ////////////////////////////////////////
  onScrollToEnd_Areas() {
    this.fetchMore_Areas();
  }

  onScroll_Areas({ end }) {
    if (this.areasLoading || this.areas.length <= this.areasBuffer.length) {
      return;
    }

    if (end + this.numberOfItemsFromEndBeforeFetchingMore >= this.areasBuffer.length) {
      this.fetchMore_Areas();
    }
  }

  private fetchMore_Areas() {
    const len = this.areasBuffer.length;
    const more = this.areas.slice(len, this.bufferSize + len);
    this.areasLoading = true;
    // using timeout here to simulate backend API delay
    setTimeout(() => {
      this.areasLoading = false;
      this.areasBuffer  = this.areasBuffer.concat(more);
    }, 200)
  }

  getAdress() {
    let self = this;
    if(navigator) {
      navigator.geolocation.getCurrentPosition(resp => {
        let pos = {lng: resp.coords.longitude, lat: resp.coords.latitude};
        var geocoder = new google.maps.Geocoder;
        geocoder.geocode({'location': pos}, function(results, status) {
          if (status === 'OK') {
            for (let i = 0; i < results.length; i++) {
                if(results[i].types.indexOf("administrative_area_level_3") != -1) {
                  if(results[i].address_components[0].long_name != self.SelectedCity.name) { // Check if city isnt already selected
                    if(results[i].address_components[0].long_name == 'Islamabad') {
                        self.SelectedCity = self.cities[0];
                        self.selectCity();
                        break;
                    } else {
                        self.SelectedCity = self.cities[1];
                        self.selectCity();
                        break;
                    }
                  }
                  else {
                    break;
                  }
                }
            }
          }
        });
      },
      err => {
      });
    }
  }
  getLastSearchData() {
    this.lastSearchSub = this.LastSaveSearchService.searchesS$ .subscribe(resp => {
      this.lastSearchData = this.LastSaveSearchService.getlastSearch_array();
      // console.log("getlastSearch_array", this.lastSearchData)
    });
  }
  setLastSearchData() {
    let route      = '';

    let type_sub_type = this.SelectedType;
    if(this.SelectedSubType != "all" && this.SelectedSubType != "")
      type_sub_type = this.SelectedSubType;

    let searchName = '';
    if (typeof this.SelectedArea['id'] != "undefined") {
        route      = this.helperService.getLocURL(this.SelectedArea, this.SelectedCity, false, this.Purpose, this.SelectedType, this.SelectedSubType);
        searchName = type_sub_type + ' for ' + this.Purpose + ' in ' + this.SelectedArea['name'];
    } else {
        route      = this.helperService.getLocURL({}, this.SelectedCity, true, this.Purpose, this.SelectedType, this.SelectedSubType);
        searchName = type_sub_type + ' for ' + this.Purpose + ' in ' + this.SelectedCity['name'];
    }

    // Set Search Data
    let data = {
      searchName   : searchName,
      type         : 'All ' + type_sub_type,
      route        : route
    }

    this.LastSaveSearchService.updateSearch(data);
  }

  getPopularLocations() {
    let url = AppSettings.API_ENDPOINT + 'get-home-data';
    this.helperService.httpGetRequests(url).then(resp => {
      if (typeof resp != "undefined") {
        this.popularLocations = resp.popular_locations;
        this.recentBlogs = resp.recent_blogs;
        this.homeData = resp;
        this.platinumData   =  resp.platinum_agency;
        this.featuredAgency = resp.featured_agency;

        this.featuresArray1 = [...resp.featured_agency];
        this.featuresArray2 = [...resp.featured_agency];
        this.featuresArray3 = [...resp.featured_agency];

        let length_1;
        let length_2;
        let length_3;
        if(resp.featured_agency.length / 3 < 11) {
            length_1 = 11;
            length_2 = 11;
            length_3 = resp.featured_agency.length - 22;
        } else {
            length_1 = resp.featured_agency.length / 3;
            length_2 = resp.featured_agency.length / 3;
            length_3 = resp.featured_agency.length - length_1 - length_2;
        }

        // console.log("length_1 : ",length_1)
        // console.log("length_2 : ",length_2)
        // console.log("length_3 : ",length_3)

        this.featuresArray1 = this.featuresArray1.slice(0, length_1);
        this.featuresArray2 = this.featuresArray2.slice(length_1, length_1 + length_2);
        this.featuresArray3 = this.featuresArray3.slice(length_1 + length_2, length_1 + length_2 + length_3);
        // console.log("this.featuresArray1 : ",this.featuresArray1)
        // console.log("this.featuresArray2 : ",this.featuresArray2)
        // console.log("this.featuresArray3 : ",this.featuresArray3)

        if (this.platinumData ==  '' ) {
          this.owlReinit();
        }
        if (this.featuredAgency ==  '' ) {
          this.owlReinit();
        }

        // console.log("response",resp);
      }
    }).catch(error => {
        console.error("error: ",error);
    });
  }

  owlReinit(){
    if(typeof $ != "undefined") {
      ($('.platinumAgenciesCarousel .owl-carousel')as any).owlCarousel({
          loop:true,
          margin:40,
          nav:true,
          dots:false,
          center:true,
          responsive:{
              0:{
                  items:1
              },
              600:{
                  items:2
              },
              1000:{
                  items:3
              }
          }
      });
      ($('.featuredAgenciesCaro .owl-carousel')as any).owlCarousel({
          loop:true,
          margin:10,
          nav:true,
          dots:false,
          center:true,
          responsive:{
              0:{
                  items:1
              },
              600:{
                  items:1
              },
              1000:{
                  items:1
              }
          }
      });
    }
  }
  setLocUrl(location){
    if (location.is_city != "1") {
        return this.helperService.getLocURL(location, location, false, location.purpose, location.type, location.sub_type);
    } else {
        return this.helperService.getLocURL({}, location, true, location.purpose, location.type, location.sub_type);
    }
  }
  replaceString(s) {
    return s.toLowerCase().replace(/ /g, '-').replace(/_/g, '-').replace(/[/\\*]/g, "-").replace(/---/g, '-');
  }
  setProProfileRoute(platinumAgency){
    // console.log("platinumAgency: ",platinumAgency)
      return '/agent/' + this.replaceString(platinumAgency.city) + '-' + this.replaceString(platinumAgency.name) + '-' + platinumAgency.user_id;
  }

  randIndex1(index) {
    console.log("***** index: ",index)
    index = index + 1;
    console.log("***** index: ",index)
    return index;
  }

  randIndex2(index) {
    return index + 2;
  }
}

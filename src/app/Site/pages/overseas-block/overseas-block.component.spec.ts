import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OverseasBlockComponent } from './overseas-block.component';

describe('OverseasBlockComponent', () => {
  let component: OverseasBlockComponent;
  let fixture: ComponentFixture<OverseasBlockComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OverseasBlockComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OverseasBlockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

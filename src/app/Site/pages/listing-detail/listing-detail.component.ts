import { Component, OnInit, ViewChild, ElementRef, ChangeDetectorRef } from '@angular/core';
import { Location } from '@angular/common';
import { Router, Route, ActivatedRoute } from "@angular/router";
import { AuthenticationService, AppSettings, HelperService, SEOService, FavoriteService, HideService, SaveSearchService } from '../../../services/_services';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { OwlCarousel } from 'ngx-owl-carousel';

// Bootsteap Components
import { BsModalRef, BsModalService, ModalOptions } from 'ngx-bootstrap/modal';

// Modal Components
import { LoginComponent } from '../../entry-components/login/login.component';
import { ContactDetailsComponent } from '../../entry-components/contact-details/contact-details.component';

// Sweet Alerts
import Swal from 'sweetalert2';

import {} from 'googlemaps';


// declare var google;
declare var jQuery;
declare var $;

@Component({
  selector: 'app-listing-detail',
  templateUrl: './listing-detail.component.html',
  styleUrls: ['./listing-detail.component.css']
})
export class ListingDetailComponent implements OnInit {
  @ViewChild('propMap', {static: false}) mapElement: ElementRef;
  @ViewChild('owlElement', {static: false}) owlElement: OwlCarousel;
  @ViewChild('owlthumb', {static: false}) owlthumb: OwlCarousel;

  // Google Map Params
  public googleMapsObj    : any = { maps: {} };
  public map              : any;
  public commuteMap       : any;
  public geocoder         : any;
  public Location_LatLng  : any;
  public markersArray     : any = [];

  // Commute Params
  public TravelMode          : any = 'DRIVING';
  public autoComplete        : any;
  public directionsDisplay   : any;
  public directionsService   : any;
  public distanceService     : any;
  public orign_Pos           : any;
  public destination_Pos     : any;
  public destination_Marker  : any;
  public destination_Address : any;
  public infowindo           : any;
  public infowindoContent    : any;

  // Nearby Params
  public Nearby             : any = { Mosques: [], Edu_Insitutes: [], Hospitals: [], Restaurants: [], Parks: [] };
  public authSubs           : any;
  public routerSubscription : any;

  public objectKeys : any = Object.keys;
  public propertyID : any;
  public LocationID : any;
  public hideForm   : any = false;
  public property   : any = {
                              feature : null,
                              user    : {
                                          first_name: '',
                                          last_name: ''
                                        },
                            };
  public parentLocs          : any;
  public showPropertyDetails : any = false;
  public showSocialSharing   : any = false;
  public socialSharingEmail  : any = "";
  public loadingProp         : any = false;
  public ImagesUrl           : any = AppSettings.IMG_ENDPOINT;
  public propertyImages      : any = [];
  public propertyDetail      : any = "";
  public gmapURl             : any = "";
  public propertyCall        : any = false;
  public propPackages        : any = {};

  // Params for Locally Favorite, Hide and Report
  public locallyFavorited     : any = false;
  public locallyHidden        : any = false;
  public reportedProp         : any = false;

  // Request Info Form
  public lead      : any = { name: '', email: '', phone: ''};
  public LeadForm  : FormGroup;
  public submitted : any = false;

  // Previouly Searched Properties
  public propSearches : any = {};
  public showPrevNext : any = false;
  public PrevURL      : any;
  public SearchURL    : any;
  public NextURL      : any;
  public modalRef     : BsModalRef;

  // Social Shore Params
  public shareUrl       = '';
  public shareIImageUrl = '';

  // Owl Carousel
  public mySlideImages     = [];
  public myCarouselImages  = [];
  public mySlideOptions    = {items: 1, dots: false, nav: true, thumbs:false};
  public myCarouselOptions  = {items: 8, dots: false, nav: false, thumbs:false};

  public whatsapLead             : any = {property_id: '', type: 'whatsapp'};


  // side form open close

  public openForm : any = false;
  public leadForm : any = false;


  constructor(
    private authenticationService : AuthenticationService,
    private activatedRoute        : ActivatedRoute,
    private helperService         : HelperService,
    private seoService            : SEOService,
    private router                : Router,
    private formBuilder           : FormBuilder,
    private modalService          : BsModalService,
    private _location             : Location,
    private favoriteService       : FavoriteService,
    private hideService           : HideService,
    private saveSearchService     : SaveSearchService,
    private cdr                   : ChangeDetectorRef
  ) {
      // Get Globals from AppSettings
      this.propPackages = AppSettings.propPackages;
  }

  ngOnInit() {
    // Set Maps
    if (typeof google != "undefined") {
      this.googleMapsObj = google;
      this.map      = this.googleMapsObj.maps.Map;
      this.geocoder = this.googleMapsObj.maps.Geocoder();
    }

    // Set FormGroup
    this.LeadForm = this.formBuilder.group({
      name: [null, Validators.required],
      email: [null, [Validators.required, Validators.email]],
      phone: [null, Validators.required],
      message: [null, Validators.required]
    });

    this.routerSubscription = this.activatedRoute.params.subscribe(routeParams => {
  		// console.log("routeParams: ",routeParams);

      // let params = this.activatedRoute.snapshot.params;
      let slug = this.helperService.renderPropertyURL(routeParams.slug);

      // Set Property ID
      this.propertyID = parseInt(slug.id);

      // Get Property Details
      this.getPropertyDetails(this.propertyID);

      // Set Previous Next for Navigation
      this.setPrevNextSearch();
  	});

  }
  ngAfterViewInit() {

    $(document).ready(function() {
      var bigimage = $(".owl-main");
      var thumbs = $(".owl-custom-thumbnails");
      //var totalslides = 10;
      var syncedSecondary = true;

      bigimage
        .owlCarousel({
        items: 1,
        slideSpeed: 5000,
        nav: false,
        autoplay: false,
        dots: false,
        loop: false,
        responsiveRefreshRate: 200,
        // navText: [
        //   '<i class="fa fa-arrow-left" aria-hidden="true"></i>',
        //   '<i class="fa fa-arrow-right" aria-hidden="true"></i>'
        // ]
      })
        .on("changed.owl.carousel", syncPosition);

      thumbs
        .on("initialized.owl.carousel", function() {
        thumbs
          .find(".owl-item")
          .eq(0)
          .addClass("current");
      })
        .owlCarousel({
        dots: false,
        nav: false,
        // navText: [
        //   '<i class="fa fa-arrow-left" aria-hidden="true"></i>',
        //   '<i class="fa fa-arrow-right" aria-hidden="true"></i>'
        // ],
        smartSpeed: 200,
        slideSpeed: 500,
        slideBy: 4,
        responsiveRefreshRate: 100,
        responsive: {
          0: {
            items: 3
          },
          600: {
            items: 6
          },
          1000: {
            items: 12
          }
        }
      })
        .on("changed.owl.carousel", syncPosition2);

      function syncPosition(el) {
        //if loop is set to false, then you have to uncomment the next line
        var current = el.item.index;

        //to disable loop, comment this block
        // var count = el.item.count - 1;
        // var current = Math.round(el.item.index - el.item.count / 2 - 0.5);
        //
        // if (current < 0) {
        //   current = count;
        // }
        // if (current > count) {
        //   current = 0;
        // }
        //to this
        thumbs
          .find(".owl-item")
          .removeClass("current")
          .eq(current)
          .addClass("current");
        var onscreen = thumbs.find(".owl-item.active").length - 1;
        var start = thumbs
        .find(".owl-item.active")
        .first()
        .index();
        var end = thumbs
        .find(".owl-item.active")
        .last()
        .index();

        if (current > end) {
          thumbs.data("owl.carousel").to(current, 100, true);
        }
        if (current < start) {
          thumbs.data("owl.carousel").to(current - onscreen, 100, true);
        }
      }

      function syncPosition2(el) {
        if (syncedSecondary) {
          var number = el.item.index;
          bigimage.data("owl.carousel").to(number, 100, true);
        }
      }

      thumbs.on("click", ".owl-item", function(e) {
        e.preventDefault();
        var number = $(this).index();
        bigimage.data("owl.carousel").to(number, 300, true);
      });
    });

    this.authSubs = this.authenticationService.currentUserS$.subscribe(user => {
      if (user) {
          this.lead.name  = user.user.first_name + ' ' + user.user.last_name;
          this.lead.email = user.user.email;
          this.lead.phone = user.user.phone_number;

          this.cdr.detectChanges();

          if(this.propertyCall)
            this.getPropertyDetails(this.propertyID);  // Get details if user sesson changes. Check ensures that its not called on page load
      } else {
          this.property.is_liked = 0;

          // Reset Lead Form
          this.lead.name  = '';
          this.lead.email = '';
          this.lead.phone = '';
          // console.log("--- user: ", user);
      }
    });

  }
  ngOnDestroy() {
    console.log("Component Dismounted");

    if (typeof this.authSubs != 'undefined') {
      //prevent memory leak when component destroyed
      this.authSubs.unsubscribe();
    }

    if (typeof this.routerSubscription != 'undefined') {
      //prevent memory leak when component destroyed
       this.routerSubscription.unsubscribe();
    }
    // google.maps.event.removeListener(this.autoComplete);
    // google.maps.event.clearInstanceListeners(this.autoComplete);
    // this.autoComplete = null;
  }
  setPrevNextSearch() {
    // Get Previouly Searched Results for To and Fro Navigation
    this.propSearches = this.saveSearchService.getSearch();
    if (typeof this.propSearches != "undefined") {
      this.showPrevNext = Object.keys(this.propSearches).length > 0 && this.propSearches[this.propertyID];

      if(this.showPrevNext) {
        this.getPrevUrl();
        this.getSearchURL();
        this.getNextvUrl();
      }
    }
  }

  getPrevUrl() {
    this.PrevURL = this.saveSearchService.getPrevUrl(this.propertyID);
  }

  getSearchURL() {
    this.SearchURL = this.saveSearchService.getSearch_URL();
  }

  getNextvUrl() {
    this.NextURL = this.saveSearchService.getNextvUrl(this.propertyID);
  }

  // convenience getter for easy access to form fields
  get f() { return this.LeadForm.controls; }

  // Request Info Add Lead
  AddLead(leadData) {
    leadData.type         = 'request_info';
    leadData.property     = [];

    let prop = { user_id: this.property.user_id, property_id: this.property.id }
    leadData.property.push(prop);
    this.submitted        = true;

    // Stop here if form is invalid
    if (this.LeadForm.invalid) {
     console.log(this.LeadForm.controls);
     return;
    }

    let user = this.authenticationService.get_currentUserValue();
    if (user) {
      leadData['requested_by'] = user.user.id;
    }

    const url  = AppSettings.API_ENDPOINT + 'lead/add';
    console.log(leadData);
    this.helperService.httpPostRequests(url, leadData)
      .then(resp => {
        if (typeof resp != "undefined" && resp != null && typeof resp.error === "undefined" ) {
          this.lead = {};

          Swal.fire('Request Sent', 'Your message has been sent successfully. You will receive a reply directly at your email address.', 'success')

          // Set Submission false
          this.submitted = false;
        } else {
            console.error(resp);
        }
      })
      .catch(error => {
        console.log("error: ",error);

        // Set Submission false
        this.submitted = false;
      });
  }

  getPropertyDetails(id) {
    // Show Loader
    this.loadingProp = true;

    let user = this.authenticationService.get_currentUserValue();
    let data = {};
    if (user) {
      data['auth_id'] = user.user.id;
    }

    const url  = AppSettings.API_ENDPOINT + 'property/details/' + id;

    this.helperService.httpPostRequests(url, data).then(resp => {
      // Hide Loader
      this.loadingProp = false;

      if (typeof resp != "undefined" && resp != null && typeof resp.error === "undefined" ) {
        this.property    = resp;
        this.gmapURl     = 'https://www.google.com/maps/embed/v1/place?q='+ this.property.location_name +' '+ this.property.city +'&key=' + AppSettings.GOOGLE_MAPS_KEY;

        // Get Breadcrumbs
        this.getBreadCrumbs();

        // Hide/Show Enquiry form
        this.hideForm = this.property.status == 'pending' || this.property.status == 'expired' ? true : false;

        // Make links for local images
        if (this.property.user.image != null && this.property.user.image.indexOf("https://") == -1 && this.property.user.image.indexOf("http://") == -1) {
           this.property.user.image = AppSettings.IMG_ENDPOINT + 'uploads/users/thumbnails/' + this.property.user.image;
         }

        // Set Prop Desc
        if (this.property.description != null)
          this.propertyDetail = this.property.description;

        // Set Property call to true after initial call on page load
        this.propertyCall = true;

        // Set SEO Tags
        this.setSEO();

        // Check if property favorited or hidden locally
        this.getFavoritePropByIndex();
        this.getHiddenPropByIndex();

        // Initialize Map
        let self = this;
        setTimeout(function() {
          self.initMap();
        }, 300);
      }
    }).catch(error => {
      // Hide Loader
      this.loadingProp = false;

      console.error("error: ",error);

      if(error.status === 404)
        Swal.fire('Error', '404 Property not found', 'error');

      // Set Submission false
      this.submitted = false;
    });
  }

  setSEO() {
    // this._location['_platformLocation'].href
    // Set Prop Gallery
      let image = this._location['_platformLocation'].protocol + '//' + this._location['_platformLocation'].hostname + "/assets/site/images/default-image.jpg";

    if (this.property.images != "" && this.property.images) {
        this.propertyImages = this.property.images;
        image = this.ImagesUrl + 'uploads/property-images/' + this.propertyImages[0].name.split('.')[0] + '.jpg';
        // Set Owl Carousel
        this.setOwlCaro();
    }

    // Set Default Msg for Inquiry
    this.lead.message = 'I need some information regarding your property Gharbaar - ID-'+ this.property.id +'. Kindly get back to me at the earliest possible opportunity.';

    // Set Title & Metatag for property
    let title = this.property.title + ' in '+ this.property.location_name +', '+ this.property.city +' ID'+ this.property.id +' | Gharbaar.com';
    let desc  = "";

    if(this.property.description)
      desc = this.property.location_name + ' Property. '+ this.property.title +' - '+ this.property.description;
       // desc = this.property.location_name + ' Property. '+ this.property.title +' - '+ (this.property.description).substring(0, 149);
    else
       desc = this.property.location_name + ' Property. '+ this.property.title;

    // Set description lengt6h
    if(desc.length > 160)
      desc = desc.substring(0, 160);

    // Set Basic Meta Tags
      this.seoService.updateTitle(title);
      this.seoService.updateDescription(desc);

    // Set Og Meta tags
      this.seoService.updateOgTitle(title);
      this.seoService.updateOgDesc(desc);
      this.seoService.updateOgUrl();
      this.seoService.updateOgImage(image);

    // Set Twitter Meta Tags
      this.seoService.updateTwitterTitle(title);
      this.seoService.updateTwitterDesc(desc);

    // Set Canonical Tag
      this.seoService.updateCanonicalUrl();

    // Set Socail Params
      this.shareUrl       = this._location['_platformLocation'].href;
      this.shareIImageUrl = image;
  }

  getBreadCrumbs() {
    this.helperService.getParentLocations(this.property.location.id).then(resp => {
      this.parentLocs = resp.parentLocs;
    })
  }

  toggleSocialShare() {
    this.showSocialSharing = !this.showSocialSharing;
  }

  sendEmailLink() {
    console.log("socialSharingEmail: ",this.socialSharingEmail);
  }

  saveProperty(is_liked) {
    let user = this.authenticationService.get_currentUserValue();
    if (user) {
        this.property.is_liked = is_liked;
        let data  = { property_id: this.property.id };
        const url = AppSettings.API_ENDPOINT + 'auth/like';

        this.helperService.httpPostRequests(url, data).then(resp => {
          console.log("resp: ",resp);
        }).catch(error => {
          console.error("error: ",error);
        });
    } else {
        this.open_Login_Modal(); // Open Login Pop-up
    }
  }

  getFavoritePropByIndex() {
    this.locallyFavorited = this.favoriteService.getFavoritePropByIndex(this.property.id);
  }

  togglefavoriteProp() {
    if(this.locallyFavorited)
      this.favoriteService.removefavoriteProp(this.property.id);
    else
      this.favoriteService.addfavoriteProp(this.property);

    this.locallyFavorited = !this.locallyFavorited;
    // console.log("this.property: ",this.property);
    // this.favoriteService.addfavoriteProp(this.property);
  }

  getHiddenPropByIndex() {
    this.locallyHidden   = this.hideService.getHiddenPropByIndex(this.property.id);
  }

  toggleHiddenProp() {
    if(this.locallyHidden)
      this.hideService.removeHiddenProp(this.property.id);
    else
      this.hideService.addHiddenProp(this.property);

    this.locallyHidden = !this.locallyHidden;
  }

  reportProp() {
    this.reportedProp = true;

    // Add Listing to hidden aswell
    this.hideService.addHiddenProp(this.property);
    this.locallyHidden = true;

    // Remove from locally favorited
    this.favoriteService.removefavoriteProp(this.property.id);
    this.locallyFavorited = false;

    // Report property to backend
    let url  = AppSettings.API_ENDPOINT + 'property/report';
    let data = { property_id: this.propertyID };
    this.helperService.httpPostRequests(url, data).then(resp => {
      console.log("resp: ", resp);
    })
  }

  open_ContactDetails_Modal() {
    const initialState = { property: this.property };
    const config: ModalOptions = { class: 'modal-sm phone-number', initialState };
    this.modalRef = this.modalService.show(ContactDetailsComponent, config);
  }

  open_Login_Modal() {
    this.modalService.show(LoginComponent, {});
  }

  getLocURL(location, byCity) {
    let city = { id: this.property.city_id, name: this.property.city }
    return this.helperService.getLocURL(location, city, byCity);
  }

  agencyProfileRoute(agent) {
    return this.helperService.agencyProfileRoute(agent);
  }

  navigateRoute(url: any, params: any) {
    this.router.navigateByUrl('/', {skipLocationChange: true}).then(()=>
      this.router.navigate([url, params]));
  }

  convertToInt(val) {
    return parseInt(val);
  }

  togglePropertyDetails(arg) {
    this.showPropertyDetails = arg;
  }

  setOwlCaro() {
    this.mySlideImages     = [];
    this.myCarouselImages  = [];

    if (this.propertyImages.length == 1) {
        // Set Owl Images
        if (this.propertyImages[0].is_video == '1') {
        this.mySlideImages.push(this.ImagesUrl + 'uploads/property-images/' + this.propertyImages[0].name.split('.')[0] + '.jpg');
        this.myCarouselImages.push(this.ImagesUrl + 'uploads/property-images/' + this.propertyImages[0].name.split('.')[0] + '.jpg');
        }
        else{
          this.mySlideImages.push(this.ImagesUrl + 'uploads/property-images/' + this.propertyImages[0].name);
          this.myCarouselImages.push(this.ImagesUrl + 'uploads/property-images/' + this.propertyImages[0].name);
        }
         this.owlElement.reInit();
         this.owlthumb.reInit();
    } else {
        for (let i = 0; i < this.propertyImages.length; i++) {
          if (this.propertyImages[i].is_video == '1') {
          // Set Owl Images
          this.mySlideImages.push(this.ImagesUrl + 'uploads/property-images/' + this.propertyImages[i].name.split('.')[0] + '.jpg');
          this.myCarouselImages.push(this.ImagesUrl + 'uploads/property-images/' + this.propertyImages[i].name.split('.')[0] + '.jpg');
        }
        else{
          this.mySlideImages.push(this.ImagesUrl + 'uploads/property-images/' + this.propertyImages[i].name);
          this.myCarouselImages.push(this.ImagesUrl + 'uploads/property-images/' + this.propertyImages[i].name);
        }
        // this.owlElement.trigger('destroy.owl.carousel');
        // this.owlElement.trigger('initialize.owl.carousel');
        // Refresh
        this.owlElement.reInit();
        this.owlthumb.reInit();
      }
    }
  }

  initMap() {
    // // Get Current Location
    // const url = "https://www.googleapis.com/geolocation/v1/geolocate?key=" + AppSettings.GOOGLE_MAPS_KEY;
    // this.helperService.httpPostRequests(url, []).then(resp => {
    //   var currentLoc = new google.maps.LatLng(resp.location.lat, resp.location.lng);
    // })
    if (typeof google != "undefined") {
      this.Location_LatLng = new google.maps.LatLng(this.property.location.latitude, this.property.location.longitude);
      // this.Location_LatLng = new google.maps.LatLng(79.546465, 99.15451);

      const mapProperties = {
          center: this.Location_LatLng,
          zoom: 15,
          mapTypeId: google.maps.MapTypeId.ROADMAP
     };
     this.commuteMap = new google.maps.Map(this.mapElement.nativeElement, mapProperties);

     // Center the Map and Set Initial Place Marker
     this.commuteMap.setCenter(this.Location_LatLng);
     var marker = new google.maps.Marker({
         map      : this.commuteMap,
         position : this.Location_LatLng,
         label    : 'A'
     });

     this.markersArray.push(marker);

     // Get Nearby Places
     this.getNearby_locs('mosque');
     this.getNearby_locs('school');
     this.getNearby_locs('hospital');
     this.getNearby_locs('restaurant');
     this.getNearby_locs('park');

     // // Get Lat/Lng from Address
     // this.addressToLatLng();

     // Set Services GLobally
     this.directionsDisplay = new google.maps.DirectionsRenderer;
     this.directionsService = new google.maps.DirectionsService;
     this.distanceService   = new google.maps.DistanceMatrixService;
     this.directionsDisplay.setMap(this.commuteMap);

     // Set InfoWindow
     this.infowindo        = new google.maps.InfoWindow();
     this.infowindoContent = document.getElementById('infowindow-content');
     this.infowindo.setContent(this.infowindoContent);

     // Init AutoComplete
     this.setAutocompete();
    }
  }

  // addressToLatLng() {
  //   var address = this.property.location_name + ', ' + this.property.city;
  //   let self = this;
  //
  //   this.geocoder.geocode( { 'address': address}, function(results, status) {
  //     if (status.toString() == "OK") {
  //       self.Location_LatLng  = results[0].geometry.location;
  //
  //       self.commuteMap.setCenter(self.Location_LatLng);
  //       var marker = new google.maps.Marker({
  //           map      : self.commuteMap,
  //           position : self.Location_LatLng,
  //           label    : 'A'
  //       });
  //
  //       self.markersArray.push(marker);
  //
  //       // Get Nearby Places
  //       self.getNearby_locs('mosque');
  //       self.getNearby_locs('school');
  //       self.getNearby_locs('hospital');
  //       self.getNearby_locs('restaurant');
  //       self.getNearby_locs('park');
  //
  //     } else {
  //       // alert('Geocode was not successful for the following reason: ' + status);
  //       console.error('Geocode was not successful for the following reason: ' + status);
  //     }
  //   });
  // }


  getNearby_locs(type) {
    var request = {
       location: this.Location_LatLng,
       radius: 500,
       type: type,
     };

     let self    = this;
     var service = new google.maps.places.PlacesService(this.commuteMap);

     service.nearbySearch(request, function name(results, status) {
         switch(type) {
           case "mosque":
             self.Nearby.Mosques = results;
             break;
           case "school":
             self.Nearby.Edu_Insitutes = results;
             break;
           case "hospital":
             self.Nearby.Hospitals = results;
             break;
           case "restaurant":
             self.Nearby.Restaurants = results;
             break;
           case "park":
             self.Nearby.Parks = results;
             break;
           default:
             // text = "No value found";
         }
     });
  }

  setMarkers(type) {
    this.clearMapMarkers();
    let results = this.Nearby[type];
    this.createMarkers(results, type);
  }

  createMarkers(places, type) {
    var bounds = new google.maps.LatLngBounds();
    for (var i = 0, place; place = places[i]; i++) {

      let url;
      if (type == 'Edu_Insitutes') {
          url = "assets/site/images/edu-place.png";
      }
      else if (type == 'Hospitals') {
        url = "assets/site/images/host-place.png";
      }
      else if (type == 'Restaurants') {
        url = "assets/site/images/hotel-place.png";
      }
      else if (type == 'Parks') {
        url = "assets/site/images/park-pin.png";
      }
      else if (type == 'Mosques') {
        url = "assets/site/images/masjid.png";
      }
      else {
          url = place.icon;
      }
      var image = {
                    url: url,
                    size: new google.maps.Size(71, 71),
                    origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(17, 34),
                    scaledSize: new google.maps.Size(35, 35)
                  };

      var marker = new google.maps.Marker({
                                      map: this.commuteMap,
                                      icon: image,
                                      title: place.name,
                                      position: place.geometry.location
                                    });
      // Set Marker in Global Array
      this.markersArray.push(marker);

      bounds.extend(place.geometry.location);
    }

    // Set Bounds for Main Location Marker
    bounds.extend(this.Location_LatLng);

    this.commuteMap.fitBounds(bounds);
  }

  clearMapMarkers() {
    if(this.destination_Marker)
      this.destination_Marker.setMap(null);

    this.destination_Pos = null;

    // Clear Previous Directions
    this.directionsDisplay.set('directions', null);

    for (var i = 1; i < this.markersArray.length; i++ ) {
      this.markersArray[i].setMap(null);
    }
    this.markersArray.length = 1;

    this.commuteMap.setZoom(15);
    this.commuteMap.setCenter(this.markersArray[0].getPosition());
  }

  SetTravelMode(mode) {
    this.TravelMode = mode;

    // Clear Previous Directions
    this.directionsDisplay.set('directions', null);

    this.clearMapMarkers();
    this.getAutoComplete_Place();

    // // Update Travel Mode
    // this.calculateAndDisplayRoute();
  }

  setAutocompete() {
    var input        = <HTMLInputElement>document.getElementById('autocompleteSearch');
    this.autoComplete = new google.maps.places.Autocomplete(input);

    // Bind the map's bounds (viewport) property to the autocomplete object,
    // so that the autocomplete requests use the current map bounds for the
    // bounds option in the request.
    this.autoComplete.bindTo('bounds', this.commuteMap);

    // Set the data fields to return when the user selects a place.
    this.autoComplete.setFields(
        ['address_components', 'geometry', 'icon', 'name']);

    let self = this;
    this.autoComplete.addListener('place_changed', function() {
      self.getAutoComplete_Place();
    });
  }

  getAutoComplete_Place() {
    let self = this;

    self.clearMapMarkers();

    var place = self.autoComplete.getPlace();
    if (typeof place === "undefined" || !place.geometry) {
      return;
    }

    // If the place has a geometry, then present it on a map.
    if (place.geometry.viewport) {
      self.commuteMap.fitBounds(place.geometry.viewport);
    } else {
      self.commuteMap.setCenter(place.geometry.location);
      self.commuteMap.setZoom(15);  // Why 17? Because it looks good.
    }

    var marker = new google.maps.Marker({
      map      : self.commuteMap,
      position : place.geometry.location
    });

    // Set Marker in Global Array
    self.markersArray.push(marker);

    // Set Destination Pos
    self.destination_Pos = marker;

    var address = '';
    if (place.address_components) {
      address = [
        (place.address_components[0] && place.address_components[0].short_name || ''),
        // (place.address_components[1] && place.address_components[1].short_name || '')
        (place.address_components[2] && place.address_components[2].short_name || '')
      ].join(' ');
    }

    // Set Destination Address for Info Window
    self.destination_Address = address;

    self.calculateAndDisplayRoute();
  }

  diretcionTo(loc) {
    // Set Destination Address for Info Window
    this.destination_Address = loc.name + ' ' + loc.vicinity;

    this.TravelMode      = 'DRIVING';
    this.destination_Pos = { position: {lat: loc.geometry.location.lat(), lng: loc.geometry.location.lng()} };

    this.calculateAndDisplayRoute();
  }

  calculateAndDisplayRoute() {
    if (this.destination_Pos) {
      let self = this;

      this.directionsService.route({
        origin      : self.markersArray[0].position,
        destination : self.destination_Pos.position,
        travelMode  : google.maps.TravelMode[self.TravelMode]
      }, function(response, status) {
        console.log("response: ",response)
        if (status == 'OK') {
          self.directionsDisplay.setDirections(response);

          self.directionsDisplay.setOptions({
                        suppressMarkers: true,
                    });

          // Calculate Distance
          self.calculateDistance();
        } else {
          self.clearMapMarkers();
          // window.alert('Directions request failed due to ' + status);
        }
      });
    }
  }

  calculateDistance() {
    let self = this;

    this.distanceService.getDistanceMatrix({
      origins       : [self.markersArray[0].position],
      destinations  : [self.destination_Pos.position],
      travelMode    : google.maps.TravelMode[self.TravelMode],
      unitSystem    : google.maps.UnitSystem.METRIC,
      avoidHighways : false,
      avoidTolls    : false
    }, function(response, status) {
      if (status !== 'OK') {
        // alert('Error was: ' + status);
      } else {
        console.log("response: ",response);
        var originList       = response.originAddresses;
        var destinationList  = response.destinationAddresses;


        for (var i = 0; i < originList.length; i++) {
          var results = response.rows[i].elements;
          for (var j = 0; j < results.length; j++) {

            // Set Destination Marker
            if(self.destination_Marker)
              self.destination_Marker.setMap(null);

            self.makeMarker_Single(self.markersArray[0].position, 'A');
            self.destination_Marker = self.makeMarker_Single(self.destination_Pos.position, 'B');

            self.infowindo.close();
            self.infowindoContent.children['place-icon'].src            = "https://maps.gstatic.com/mapfiles/place_api/icons/geocode-71.png";
            self.infowindoContent.children['place-name'].textContent    = self.destination_Address;
            self.infowindoContent.children['place-address'].textContent = results[j].duration.text + ' ' + self.TravelMode + ' - ' + results[j].distance.text;

            self.infowindo.open(self.commuteMap, self.destination_Marker);
          }
        }
      }
    });
  }

  makeMarker_Single( position, label ) {
    let self = this;
    return new google.maps.Marker({
      position: position,
      map: self.commuteMap,
      // icon: icon,
      label: label,
    });
  }

  openWhatsapp() {
    if(typeof window != "undefined")
      window.open("https://api.whatsapp.com/send?phone="+ this.property.user.mobile_number +"&text=Hi%2C%20I%20would%20like%20to%20inquire%20about%20your%20property%20at%20Gharbaar.com.%20Please%20contact%20me%20back%20at%20your%20earliest.%20Thanks", "_blank");

  }
  whatsappLeadFunc(){
    let url  = AppSettings.API_ENDPOINT + 'update-stats';
    this.whatsapLead = {property_id: this.propertyID, type: 'whatsapp'};
    this.helperService.httpPostRequests(url, this.whatsapLead)
      .then(resp => {
        //console.log("resp: ",resp);
        // this.ajaxloader = false;
        if (typeof resp != "undefined" && resp != null && typeof resp.error === "undefined") {
          //Swal.fire('Request Sent', 'Your message has been sent successfully. We will get back to you shortly.', 'success');
          // Set Submission false
        } else {
            console.error(resp);
        }
      })
      .catch(error => {
        console.log("error: ",error);
        // this.ajaxloader = false;
        // Set Submission false
        this.submitted = false;
      });
  }

  openSideForm(){
    this.openForm = !this.openForm;
    this.leadForm = !this.leadForm;
    $('#contact_mobile').modal('hide');
    $('body').toggleClass("overflow-hidden");
    $('.main-content').toggleClass("overflow-hidden");
  }

}

import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { AppSettings, HelperService, SEOService} from '../../../services/_services';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {
  public countryCode         : any = '92';

  public contact             : any = { name: '', email: '', phone_number: '', message: ''};
  public contactForm         : FormGroup;
  public submitted           : any = false;
  public ajaxloader          : any = false;

  constructor(
    private formBuilder           : FormBuilder,
    private helperService         : HelperService,
  ) {

  }

  ngOnInit() {
    this.contactForm = this.formBuilder.group({
      name    : [null, [Validators.required]],
      phone    : [null, [Validators.required]],
      email    : [null, [Validators.required, Validators.email]],
      message    : [null, [Validators.required]],
    });
  }

  get f() { return this.contactForm.controls; }
  AddContact(){
    this.submitted        = true;

    // Stop here if form is invalid
    if (this.contactForm.invalid) {
     return;
    }

    this.ajaxloader = true;
    let url  = AppSettings.API_ENDPOINT + 'contact-us';
    this.helperService.httpPostRequests(url, this.contact)
      .then(resp => {
        //console.log("resp: ",resp);
        this.ajaxloader = false;
        if (typeof resp != "undefined" && resp != null && typeof resp.error === "undefined") {
          this.contact = { name: '', email: '', phone_number: '', message: ''};

          Swal.fire('Request Sent', 'Your message has been sent successfully. We will get back to you shortly.', 'success');

          // Set Submission false
          this.submitted = false;
        } else {
            console.error(resp);
        }
      })
      .catch(error => {
        console.log("error: ",error);
        this.ajaxloader = false;
        // Set Submission false
        this.submitted = false;
      });
  }

  onCountryChange(country: any) {
    this.countryCode = country.dialCode;
    console.log("this.countryCode", this.countryCode)
  }
  hasError($event){
    // console.log("hasError" ,$event);
    // this.hasErrorPhn = $event;
  }

  getNumber($event){
    console.log("getNumber" ,$event);
    // this.phnNumber = $event;
  }
  telInputObject(obj) {
  }
}

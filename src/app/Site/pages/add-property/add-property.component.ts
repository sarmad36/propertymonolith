import { Component, OnInit, ViewChild, TemplateRef, ElementRef, ChangeDetectorRef } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AlertService, AuthenticationService, AppSettings, UserService, HelperService } from '../../../services/_services';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { Router } from "@angular/router";
import { DragulaService } from 'ng2-dragula';

// Modal Components
import { SiginUpOptsComponent } from '../../entry-components/sigin-up-opts/sigin-up-opts.component';

// Social Login
// import { AuthService } from "angularx-social-login";

// Bootsteap Components
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';

import { HeaderComponent } from '../../includes/header/header.component';

import {} from 'googlemaps';

@Component({
  selector: 'app-add-property',
  templateUrl: './add-property.component.html',
  styleUrls: ['./add-property.component.css']
})
export class AddPropertyComponent implements OnInit {

  @ViewChild('propMap', {static: false}) mapElement: ElementRef;
  @ViewChild('openMap', {static: false}) openMap: ElementRef;
  // Google Map Params
  public googleMapsObj    : any = { maps: {} };
  public map              : any;
  public commuteMap       : any;
  public modalMap         : any;
  public geocoder         : any;
  public Location_LatLng  : any;
  public markersArray     : any = [];
  public listingMarker    : any;
  public ModalMarker      : any;
  public showMap          : any = false;
  public completeAdress   : any;
  public finalLatitude    : any;
  public finalLongitude   : any;

  // Commute Params
  public TravelMode          : any = 'DRIVING';
  public input               : any;
  public autocomplete        : any;
  public directionsDisplay   : any;
  public directionsService   : any;
  public distanceService     : any;
  public orign_Pos           : any;
  public destination_Pos     : any;
  public destination_Marker  : any;
  public destination_Address : any;
  public infowindo           : any;
  public infowindoContent    : any;


  public modalRef: BsModalRef;
  public isUserLoggedIn     : any = false;
  public loading            : any = false;
  public user :any = {
    'first_name'            : '',
    'last_name'             : '',
    'email'                 : '',
    'password'              : '',
    'phone_number'          : '',
    'dob'                   : null,
  }


  // Add Property ngModel
  public saveData : any = {
                            purpose            : "sale",
                            title              : null,
                            ip_address         : '',
                            type               : "",
                            sub_type           : null,
                            city               : "",
                            city_id            : null,
                            location_id        : "",
                            location_name      : "",
                            address            : "",
                            display_address    : false,
                            size               : "",
                            unit               : "",
                            price              : "",
                            price_text         : "",
                            description        : "",
                            property_feature   : {
                                                    year_built          : "",
                                                    bathrooms           : 0,
                                                    BathsText           : "No. of Bathrooms",
                                                    bedrooms            : 0,
                                                    BedsText            : "No. of Bedrooms",
                                                    parking_spaces      : 0,
                                                    ParkingText         : "Total Parking Space",
                                                    floors              : 0,
                                                    floorsText          : "No. of Floors",
                                                    down_payment        : "",
                                                    down_paymentText    : "",

                                                    security_deposit        : "",
                                                    security_depositText    : "",

                                                    maintenance_charges        : "",
                                                    maintenance_chargesText    : "",

                                                    central_heating     : false,
                                                    central_cooling     : false,
                                                    lift                : false,
                                                    public_parking      : false,
                                                    underground_parking : false,
                                                    internet            : false,
                                                    cctv_camera         : false,
                                                    backup_generator    : false,
                                                    electricity         : false,
                                                    gas                 : false,
                                                    maintenance         : false,
                                                    water               : false,
                                                    tv_lounge           : false,
                                                    dining_room         : false,
                                                    drawing_room        : false,
                                                    kitchen             : false,
                                                    store_room          : false,
                                                    lawn                : false,
                                                    swimming_pool       : false,
                                                    furnished           : false,
                                                    wifi                : false,
                                                    balcony             : false,
                                                    laundry_room        : false,
                                                    servant_quarter     : false,
                                                    dirty_kitchen       : false,
                                                    posession           : false,
                                                    corner              : false,
                                                    park_facing         : false,
                                                    boundary_wall       : false,
                                                    sewerage            : false,
                                                    north               : false,
                                                    north_east          : false,
                                                    south_west          : false,
                                                    east                : false,
                                                    north_west          : false,
                                                    south               : false,
                                                    south_east          : false,
                                                    west                : false,
                                                  },
                            images             : [],
                            contact            : [],
                          };
  // Contact Params
  public previewMedia : any;
  public propContacts : any = [ { num: '' } ];
  public propGallery  : any = [];
  public breadcrumbs  : any = [];
  public contact1     : any = "";
  public contact2     : any = "";
  public showContact2 : any = false;


  public additionalFeaturesSet : any = false;

  // FilePond Params
  @ViewChild('myPond', {static: false}) myPond: any;

  pondOptions = {
    class: 'my-filepond',
    multiple: true,
    // allowReorder: 'true',
    labelIdle: '<div class="portfolio-pond"><img src="assets/site/images/pondGallery.svg" class="upload-img" alt=""/><p style="margin-bottom:0px;margin-top:10px;color: #bcb8b8;">Drop image here or click to upload</p><p style="font-size:12px;color: #bcb8b8;">(max file size 5 mb)</p></div>',
    // acceptedFileTypes: 'image/jpeg, image/png, video/mp4',
    acceptedFileTypes: 'image/jpeg, image/png, video/mp4',
    imagePreviewTransparencyIndicator: 'grid',
    allowImagePreview: false,
    imagePreviewHeight: 150,
    maxFileSize: '5MB',
    imagePreviewMaxFileSize: '5MB',

    // stylePanelLayout: 'compact circle',
    server: {
        url: AppSettings.API_ENDPOINT,
        timeout: 16000,
        process: {
            url: './temp-images',
            method: 'POST',
            withCredentials: false,
            timeout: 16000,
            onload: (response) => {
              let image = JSON.parse(response).temp_image;

              let name  = image.name;
              let video = '';
              if(image.is_video) {
                video = image.name;
                name  = image.name.split('.')[0] + '.jpg';
              }

                // Add image to pro image array
                this.propGallery.push({ id: image.id, name: name, video: video, caption: image.original_name, url: AppSettings.TEMP_IMAGES + name, videoUrl: AppSettings.TEMP_IMAGES + video, isTemp : true, is_video: image.is_video  });
                return image.id;
            },
            onerror: (response) => {
                return response.key;
            },
            ondata: (formData) => {
                return formData;
            }
        },
        revert: (uniqueFileId, load, error) => {
            // Clear Image
            load();
        },
        restore : null,
        load    : null,
        fetch   : null
    }
  }

  public pondFiles      = [];
  public propertyImages = [];

  public showPondPicker     = false;

  // Custum Params
  public showCustomTitle = false;
  public showResdAddOn   = false;
  public showPlotAddOn   = false;
  public showCommAddOn   = false;
  public submitted       = false;
  public disableArea     = true;

  // Https Params
  protected ngUnsubscribe: Subject<void> = new Subject<void>();
  public httpLoading     = false;
  public SavingProperty  = false;
  public corsHeaders     : any;

  // City and Areas List
  public cities          :any = AppSettings.cities;
  public areas           :any = [];
  public searchTypes     :any = [];
  public searchSubTypes  :any = [];
  public SelectedArea_Main = [];

  public addForm : FormGroup;

  // Params
  public SelectedCity   : any;


  // Ng-Slect Params
  public areasBuffer  = [];
  public bufferSize   = 10;
  public numberOfItemsFromEndBeforeFetchingMore = 5;
  public areasLoading = false;
  public SelectedArea = null;
  public areaQuery    = "";
  public areasError   = false;

  // Select Areas Tiers 1 params
  public areasTier1         = [];
  public areasTier1_buffer  = [];
  public SelectedArea_Tier1 = [];
  public areasTier1Loading  = false;
  public areasTier1Error    = false;

  // Select Areas Tiers 2 params
  public showAreasTier2     = false;
  public areasTier2         = [];
  public SelectedArea_Tier2 = [];
  public areasTier2Loading  = false;
  public areasTier2Error    = false;

  // Select Areas Tiers 3 params
  public showAreasTier3     = false;
  public areasTier3         = [];
  public SelectedArea_Tier3 = [];
  public areasTier3Loading  = false;
  public areasTier3Error    = false;

  // Select Areas Tiers 4 params
  public showAreasTier4     = false;
  public areasTier4         = [];
  public SelectedArea_Tier4 = [];
  public areasTier4Loading  = false;
  public areasTier4Error    = false;


  public showSuccess        = false;
  public showFailure        = false;
  public showFailureMsg     = "";

  // Subscriptions
  public userSubscription   : any;

  public searchUnits        :any = [];
  constructor(
      private http           : HttpClient,
      private authenticationService: AuthenticationService,
      private fb             : FormBuilder,
      private header         : HeaderComponent,
      private router         : Router,
      private elementRef     : ElementRef,

      private modalService   : BsModalService,
      // private authService    : AuthService,
      private userService    : UserService,
      private helperService  : HelperService,
      private alertService   : AlertService,
      private cdr            : ChangeDetectorRef,
      private dragulaService : DragulaService,
  ) {
      // Set Cors Header with token if User Logged In
      this.userSubscription = this.authenticationService.currentUserS$.subscribe(user => {
        if (user) {
            var corsHeaders = {
                  headers: new HttpHeaders()
                    .set('Content-Type',  'application/json')
                    .set('Accept',  'application/json')
                    .set('Authorization',  `Bearer ${user.access_token}`)
                }

              // Set User
              this.user = user.user;
              this.isUserLoggedIn = true;

            // Submit Data After User Logged-In
            if (this.submitted)
              this.SaveData();

        } else {
          var corsHeaders = {
                headers: new HttpHeaders()
                  .set('Content-Type',  'application/json')
                  .set('Accept',  'application/json')
              }

            this.isUserLoggedIn = false;
        }
        this.corsHeaders = corsHeaders;
      });

      // Get Client IP
      this.saveData.ip_address = this.helperService.getIPAddress().then(resp => {
        this.saveData.ip_address = resp.ip;
      });

      // Get Globals from AppSettings
      this.searchTypes     = Object.keys(AppSettings.searchTypes);
      this.searchSubTypes  = AppSettings.searchTypes;
      this.searchUnits     = AppSettings.searchUnits;

      // Initialize Gallery Dragable images
      this.dragulaService.createGroup("HANDLES", {
        // removeOnSpill: true
        // moves: (el, container, handle) => {
        //   this.cdr.detectChanges();
        //   return true;
        // }
      });
  }

  ngOnInit() {

    // Set FormGroup
    this.addForm = this.fb.group({
            title: [null],
            type: [null, Validators.required],
            sub_type: [null, Validators.required],
            city: [null, Validators.required],
            area: [null],
            areasTier1: [null],
            areasTier2: [null],
            areasTier3: [null],
            areasTier4: [null],
            size: [null, Validators.required],
            unit: [null, Validators.required],
            price: [null, [Validators.required, Validators.max(999999999)]],
            contact1: [null, Validators.required],
        });

    // // Disable Area Select
    // this.addForm.controls.area.disable();
    // this.addForm.controls.areasTier1.disable();
  }

  ngAfterViewInit() {
    //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
    //Add 'implements AfterViewInit' to the class.
    // this.getCurrentLocation();

    // Initiate Number Regex Validation
    this.initNumberFilters();
    // Select your input element.
    var number = document.getElementById('price');

    // Listen for input event on numInput.
    number.onkeydown = function(e) {
        if(!((e.keyCode > 95 && e.keyCode < 106)
          || (e.keyCode > 47 && e.keyCode < 58)
          || e.keyCode == 8)) {
            return false;
        }
    }
  }

  ngOnDestroy() {
    if (typeof this.userSubscription != 'undefined') {
      //prevent memory leak when component destroyed
       this.userSubscription.unsubscribe();
    }

    // Destroy Dragulla service.
    if (typeof this.dragulaService != 'undefined') {
      this.dragulaService.destroy('HANDLES');
    }
  }

  initNumberFilters() {
    //******* Vlidators **********//
    function setInputFilter(textbox, inputFilter) {
      ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"].forEach(function(event) {
        textbox.addEventListener(event, function() {
          if (inputFilter(this.value)) {
            this.oldValue = this.value;
            this.oldSelectionStart = this.selectionStart;
            this.oldSelectionEnd = this.selectionEnd;
          } else if (this.hasOwnProperty("oldValue")) {
            this.value = this.oldValue;
            this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
          }
        });
      });
    }

    // Install input filters.
    setTimeout(function() {
      let inputs = document.getElementsByClassName("regxValidate");
      for(let i = 0; i < inputs.length; i++) {
        setInputFilter(inputs[i], function(value) {
          return /^[\d -]*$/.test(value);
        });
      }
    }, 500);
  }

  setPurpose(arg) {
    this.saveData.purpose = arg;
  }
  convertPrice(FOR = "DP") {
    if (FOR == "price") {
        let price = this.saveData.price;
        if (price > 1000) {
          this.saveData.price_text = price;
        }
    } else if (FOR == "SD") {
        let price = this.saveData.property_feature.security_deposit;
        if (price > 1000) {
            this.saveData.property_feature.security_depositText = price;
        }
    } else if (FOR == "MC") {
        let price = this.saveData.property_feature.maintenance_charges;
        if (price > 1000) {
            this.saveData.property_feature.maintenance_chargesText = price;
        }
    } else {
        let price = this.saveData.property_feature.down_payment;
        if (price > 1000) {
            this.saveData.property_feature.down_paymentText = price;
        }
    }
  }

  toggleTitle(toggleShow: boolean) {
    this.showCustomTitle = toggleShow;
    this.saveData.title  = null;
  }

  addRemoveContact(action, index = null) {
    if(action == 'add') {
        this.propContacts.push({ num: '' });

        // Re-Initiate Number Regex Validation
        this.initNumberFilters();
    } else {
        this.propContacts.splice(index, 1)
    }
  }

  resetPropertyFeatures() {
    this.saveData.property_feature = {
                            year_built          : "",
                            bathrooms           : 0,
                            BathsText           : "No. of Bathrooms",
                            bedrooms            : 0,
                            BedsText            : "No. of Bedrooms",
                            parking_spaces      : 0,
                            ParkingText         : "Total Parking Space",
                            floors              : 0,
                            floorsText          : "No. of Floors",
                            down_payment        : "",
                            down_paymentText    : "",
                            central_heating     : false,
                            central_cooling     : false,
                            lift                : false,
                            public_parking      : false,
                            underground_parking : false,
                            internet            : false,
                            cctv_camera         : false,
                            backup_generator    : false,
                            electricity         : false,
                            gas                 : false,
                            maintenance         : false,
                            water               : false,
                            tv_lounge           : false,
                            dining_room         : false,
                            drawing_room        : false,
                            kitchen             : false,
                            store_room          : false,
                            lawn                : false,
                            swimming_pool       : false,
                            furnished           : false,
                            wifi                : false,
                            balcony             : false,
                            laundry_room        : false,
                            servant_quarter     : false,
                            dirty_kitchen       : false,
                            posession           : false,
                            corner              : false,
                            park_facing         : false,
                            boundary_wall       : false,
                            sewerage            : false,
                            north               : false,
                            north_east          : false,
                            south_west          : false,
                            east                : false,
                            north_west          : false,
                            south               : false,
                            south_east          : false,
                            west                : false,
                          };
  }

  openAdditionalDetails(type) {
    // Reset Property Features on Type Change
    this.resetPropertyFeatures();

    this.showResdAddOn = false;
    this.showPlotAddOn = false;
    this.showCommAddOn = false;

    switch(type) {
      case "Homes":
        this.showResdAddOn = true;
        this.saveData.type = type;
        this.saveData.unit = "marla";
        break;
      case "Plots":
        this.showPlotAddOn = true;
        this.saveData.type = type;
        this.saveData.unit = "marla";
        break;
      case "Commercial":
        this.showCommAddOn = true;
        this.saveData.type = type;
        this.saveData.unit = "kanal";
        // this.saveData.unit = "feet";
        break;
      default:
        // text = "No value found";
    }

    this.saveData.sub_type = null;
  }

  selectCity() {
    // Reset Area Selects
    this.addForm.get('area').patchValue([]);
    this.addForm.get('areasTier1').patchValue([]);
    this.areas        = [];
    this.areasBuffer  = [];
    this.areasTier1   = [];
    this.SelectedArea = null;

    this.areasTier2  = [];
    this.SelectedArea_Tier2 = [];
    this.areasTier3  = [];
    this.SelectedArea_Tier3 = [];
    this.areasTier4  = [];
    this.SelectedArea_Tier4 = [];
    this.showAreasTier2 = false;
    this.showAreasTier3 = false;
    this.showAreasTier4 = false;

    if (this.SelectedCity) {
        this.saveData.city    = this.SelectedCity.name;
        this.saveData.city_id = this.SelectedCity.id;

        // // Enable Area Search Select
        // this.addForm.controls.area.enable();

        // Show Area Loader
        this.areasLoading = true;

        this.showMap = true;

        // Show Area Loader
        this.areasLoading = true;
        let url  = AppSettings.API_ENDPOINT + 'locations/get-all-locations?city_id=' + this.SelectedCity.id;
        this.helperService.httpGetRequests(url).then(resp => {
          if(typeof resp.locations != "undefined") {
            // Set Areas
            this.areas        = resp.locations;

            // Update Changes
            this.cdr.detectChanges();

            // Enable Area Tier1 Select
            this.addForm.controls.area.enable();

            // Hide Area Loader
            this.areasLoading = false;
          }
        }).catch(error => {
            console.error("error: ",error);

            // Hide Area Loader
            this.areasLoading = false;
        });
    } else {
        // // Disable Area Selects
        // this.addForm.controls.area.disable();
        // this.addForm.controls.areasTier1.disable();

        // Remove from main model
        this.saveData.city = "";
    }
  }

  searchAreas(event: any) {
    // This aborts all HTTP requests.
    this.ngUnsubscribe.next();

    // Show Area Loader
    this.areasLoading = true;

    // // This completes the subject properlly.
    // this.ngUnsubscribe.complete();

    const url  = AppSettings.API_ENDPOINT + 'locations/get-all-locations?city_id=' + this.SelectedCity.id + "&search=" + event.term;
    this.httpGetRequests_AreaSearch(url).then(resp => {
      if(typeof resp != "undefined") {

        // Set Areas
        this.areas       = resp.locations;
        this.areasBuffer = this.areas.slice(0, this.bufferSize);

        // Hide Area Loader
        this.areasLoading = false;
      }
    });
  }

  selectArea() {
    this.breadcrumbs = [];

    if (this.SelectedArea != null) {
      // Set mian selected area for al Tiers
      this.SelectedArea_Main = this.SelectedArea;

      // initiate Map
      this.initMap();

      // Show Map
      this.showMap = true;

      // Set Marker Position
      this.addressToLatLng(this.SelectedArea_Main['name'], this.SelectedCity.name);

      this.helperService.getParentLocations(this.SelectedArea['id']).then(resp => {
        if(resp) {
          console.log("resp: ",resp);

          this.breadcrumbs.push({ id: this.SelectedCity.id, name: this.SelectedCity.name });

          // Set Breadcrumbs
          if(resp.parentLocs.length > 0) {
            // Set Agency breadcrumbs for location
            // this.breadcrumbs.push({ id: this.SelectedCity.id, name: this.SelectedCity.name });
            this.breadcrumbs = [...this.breadcrumbs, ...resp.parentLocs];
          }

          this.breadcrumbs.push({ id: this.SelectedArea.id, name: this.SelectedArea.name });

          console.log("this.breadcrumbs: ",this.breadcrumbs);
        }
      });

      // // Disable Area Tier1 Select
      // this.addForm.controls.areasTier1.disable();

      // Remove Error Css
      if(this.submitted) {
          this.areasError = false;
          $(".af-area .ng-select-container").css("border-color", "");
      }
    } else {
      // // Enable Area Tier1 Select
      // this.addForm.controls.areasTier1.enable();

      // Clear mian selected area for al Tiers
      this.SelectedArea_Main = [];

      // Hide Map
      this.showMap = false;
    }
  }


  //////////////////////////////////////////
  /********* HTTP Requests Fns ***********/
  ////////////////////////////////////////
  httpGetRequests_AreaSearch(url): Promise<any> {
      // Set loader true
      this.httpLoading = true;

      return this.http.get(url, this.corsHeaders)
          .pipe( takeUntil(this.ngUnsubscribe) )
          .toPromise()
          .then( resp => {
              // Set loader false
              this.httpLoading = false;

              return resp;
          })
          .catch(error => {
              // Set loader false
              this.httpLoading = false;
          });
    }

  httpGetRequests(url): Promise<any> {
      // Set loader true
      this.httpLoading = true;

      return this.http.get(url, this.corsHeaders)
          .toPromise()
          .then( resp => {
              // Set loader false
              this.httpLoading = false;

              return resp;
          })
          .catch(error => {
              // Set loader false
              this.httpLoading = false;
          });
    }

    httpPostRequests(url): Promise<any> {
        // Set loader true
        this.httpLoading = true;

        return this.http.post(url, this.saveData, this.corsHeaders)
            .pipe( takeUntil(this.ngUnsubscribe) )
            .toPromise()
            .then( resp => {
                // Set loader false
                this.httpLoading = false;

                return resp;
            })
            .catch(error => {
                // Set loader false
                this.httpLoading = false;

                // Show Error Msg
                if(typeof error.error != "undefined") {
                    this.showFailureMsg = error.error.message;
                } else {
                    this.showFailureMsg = "Something went wrong. Please try again.";
                }
                this.showFailure    = true;

                // Hide loader
                this.SavingProperty = false;
            });
      }

    httpDeleteRequests(url): Promise<any> {
        // Set loader true
        this.httpLoading = true;

        return this.http.delete(url, this.corsHeaders)
            .pipe( takeUntil(this.ngUnsubscribe) )
            .toPromise()
            .then( resp => {
                // Set loader false
                this.httpLoading = false;

                return resp;
            })
            .catch(error => {
                // Set loader false
                this.httpLoading = false;
            });
      }

    //////////////////////////////////////////
    /********* Areas Ng-Select Fns *********/
    ////////////////////////////////////////
    onScrollToEnd_Areas() {
        this.fetchMore_Areas();
    }

    onScroll_Areas({ end }) {
        if (this.areasLoading || this.areas.length <= this.areasBuffer.length) {
            return;
        }

        if (end + this.numberOfItemsFromEndBeforeFetchingMore >= this.areasBuffer.length) {
            this.fetchMore_Areas();
        }
    }

    private fetchMore_Areas() {
        const len = this.areasBuffer.length;
        const more = this.areas.slice(len, this.bufferSize + len);
        this.areasLoading = true;
        // using timeout here to simulate backend API delay
        setTimeout(() => {
            this.areasLoading = false;
            this.areasBuffer  = this.areasBuffer.concat(more);
        }, 200)
    }

    //////////////////////////////////////////
    /********** FilePond Handler ***********/
    ////////////////////////////////////////
    pondHandleInit() {
    }

    pondHandleAddFile(event: any) {
      this.showPondPicker = true;
    }

    pondClicked() {
      this.myPond.browse();
    }

    pondRemoveImage(event) {
    }

    pondFileProcessed(event) {
      console.log("*** event: ",event);

      if(!event.error)
        this.myPond.removeFile(event.file.id);
    }

    setAsMainMedia(index) {
      console.log("index: ",index);
      [this.propGallery[0], this.propGallery[index]] = [this.propGallery[index], this.propGallery[0]];
    }

    removeFile(imageID, isTemp) {
      let url;
      if(isTemp)
        url  = AppSettings.API_ENDPOINT + 'temp-images/' + imageID;
      else
        url  = AppSettings.API_ENDPOINT + 'admin/delete-gallery-image-by-id/' + imageID;
      this.helperService.httpDeleteRequests(url);

      // Remove image id from property image array
      for (let i = 0; i < this.propGallery.length; i++) {
        if(this.propGallery[i].id == imageID) {
          this.propGallery.splice(i, 1);
        }
      }
    }

    //////////////////////////////////////////
    /******* Additional Features Fns *******/
    ////////////////////////////////////////
    num_of_Baths(args) {
      this.saveData.property_feature.bathrooms = this.saveData.property_feature.bathrooms + args;

      if (this.saveData.property_feature.bathrooms < 0) {
          this.saveData.property_feature.bathrooms = 0;
      }

      switch(this.saveData.property_feature.bathrooms) {
        case 0:
          this.saveData.property_feature.BathsText = "No. of Bathrooms";
          break;
        case 1:
          this.saveData.property_feature.BathsText = "1 Bathroom";
          break;
        default:
          this.saveData.property_feature.BathsText = this.saveData.property_feature.bathrooms + " Bathrooms";
          break;
      }
    }

    num_of_Beds(args) {
      this.saveData.property_feature.bedrooms = this.saveData.property_feature.bedrooms + args;

      if (this.saveData.property_feature.bedrooms < 0) {
          this.saveData.property_feature.bedrooms = 0;
      }

      switch(this.saveData.property_feature.bedrooms) {
        case 0:
          this.saveData.property_feature.BedsText = "No. of Bedrooms";
          break;
        case 1:
          this.saveData.property_feature.BedsText = "1 Bedroom";
          break;
        default:
          this.saveData.property_feature.BedsText = this.saveData.property_feature.bedrooms + " Bedrooms";
          break;
      }
    }

    num_of_parking(args) {
      this.saveData.property_feature.parking_spaces = this.saveData.property_feature.parking_spaces + args;

      if (this.saveData.property_feature.parking_spaces < 0) {
          this.saveData.property_feature.parking_spaces = 0;
      }

      switch(this.saveData.property_feature.parking_spaces) {
        case 0:
          this.saveData.property_feature.ParkingText = "Total Parking Space";
          break;
        case 1:
          this.saveData.property_feature.ParkingText = "1 Parking Space";
          break;
        default:
          this.saveData.property_feature.ParkingText = this.saveData.property_feature.parking_spaces + " Parking Spaces";
          break;
      }
    }

    num_of_floor(args) {
      this.saveData.property_feature.floors = this.saveData.property_feature.floors + args;

      if (this.saveData.property_feature.floors < 0) {
          this.saveData.property_feature.floors = 0;
      }

      switch(this.saveData.property_feature.floors) {
        case 0:
          this.saveData.property_feature.floorsText = "No. of Floors";
          break;
        case 1:
          this.saveData.property_feature.floorsText = "1 Floor";
          break;
        default:
          this.saveData.property_feature.floorsText = this.saveData.property_feature.floors + " Floors";
          break;
      }
    }


    //////////////////////////////////////////
    /************** Save Data **************/
    ////////////////////////////////////////

    // convenience getter for easy access to form fields
    get f() { return this.addForm.controls; }

    checkLocationErrors() {
      if (this.SelectedArea_Main.length == 0) {
        if (this.addForm.controls.area.enabled) {
            this.areasError = true;
            $(".af-area .ng-select-container").css("border-color", "#dc3545");
        } else if(this.showAreasTier4) {
            this.areasTier4Error = true;
            $(".areasTier4 .ng-select-container").css("border-color", "#dc3545");
        } else if(this.showAreasTier3) {
            this.areasTier3Error = true;
            $(".areasTier3 .ng-select-container").css("border-color", "#dc3545");
        } else if(this.showAreasTier2) {
            this.areasTier2Error = true;
            $(".areasTier2 .ng-select-container").css("border-color", "#dc3545");
        } else if(this.addForm.controls.areasTier1.enabled) {
            this.areasTier1Error = true;
            $(".areasTier1 .ng-select-container").css("border-color", "#dc3545");
        }

          return true;
      } else {
          return false;
      }
    }

    setPropertData() {
      // Set Location Details
      this.saveData.location_id   = this.SelectedArea_Main['id'];
      this.saveData.location_name = this.SelectedArea_Main['name'];
      this.saveData.latitude      = this.Location_LatLng.lat();
      this.saveData.longitude     = this.Location_LatLng.lng();

      // Set Contact Details
      this.saveData.contact = [];
      for (let i = 0; i < this.propContacts.length; i++) {
          if (this.propContacts[i].num != '') {
            this.saveData.contact.push(this.propContacts[i].num);
          }
      }

      // Set Property Images
      this.saveData.images = [];
      for (let i = 0; i < this.propGallery.length; i++) {
          if (this.propGallery[i].id != '') {
            // this.saveData.images.push(this.propGallery[i].id);
              this.saveData.images.push(this.propGallery[i]);
          }
      }

      if (this.saveData.images.length == 0) {
          this.saveData.images = this.map2Image();
      }

      if (this.saveData.property_feature.year_built == "" && this.saveData.property_feature.bathrooms == 0 && this.saveData.property_feature.bedrooms == 0 && this.saveData.property_feature.parking_spaces == 0 && this.saveData.property_feature.floors == 0 && this.saveData.property_feature.down_payment == ""
          && !this.saveData.property_feature.central_heating && !this.saveData.property_feature.central_cooling && !this.saveData.property_feature.lift && !this.saveData.property_feature.public_parking && !this.saveData.property_feature.underground_parking
          && !this.saveData.property_feature.internet && !this.saveData.property_feature.cctv_camera && !this.saveData.property_feature.backup_generator && !this.saveData.property_feature.electricity && !this.saveData.property_feature.gas && !this.saveData.property_feature.maintenance
          && !this.saveData.property_feature.water && !this.saveData.property_feature.tv_lounge && !this.saveData.property_feature.dining_room && !this.saveData.property_feature.drawing_room && !this.saveData.property_feature.kitchen && !this.saveData.property_feature.store_room
          && !this.saveData.property_feature.lawn && !this.saveData.property_feature.swimming_pool && !this.saveData.property_feature.furnished && !this.saveData.property_feature.wifi && !this.saveData.property_feature.balcony && !this.saveData.property_feature.laundry_room
          && !this.saveData.property_feature.servant_quarter && !this.saveData.property_feature.dirty_kitchen && !this.saveData.property_feature.posession && !this.saveData.property_feature.corner && !this.saveData.property_feature.park_facing && !this.saveData.property_feature.boundary_wall
          && !this.saveData.property_feature.sewerage && !this.saveData.property_feature.north && !this.saveData.property_feature.north_east && !this.saveData.property_feature.south_west && !this.saveData.property_feature.east && !this.saveData.property_feature.north_west
          && !this.saveData.property_feature.south && !this.saveData.property_feature.south_east && !this.saveData.property_feature.west )
          {
            // Remove Property Features Key
            delete this.saveData.property_feature;
          }
    }

    gotoTop() {
      let el = this.elementRef.nativeElement.querySelector('.scroll_on_error');
      el.scrollIntoView({behavior: "smooth", block: "start", inline: "nearest"});
    }

    SaveData() {

      // Hide previous failure messages
      this.hideFailureMsg();

      // Set Submission True
      this.submitted = true;
      let locErrors = this.checkLocationErrors();

      // Stop here if form is invalid and scroll to Top
      if (this.addForm.invalid || locErrors) {
          this.gotoTop();

          return;
      }

      // Set MAin Data if no Errors Occured
      this.setPropertData();


      // Check if user is LoggedIN
      let user = this.authenticationService.get_currentUserValue();
      if (!user) {
          this.open_SignUpOpts_Modal();
      } else {
        var corsHeaders = {
              headers: new HttpHeaders()
                .set('Content-Type',  'application/json')
                .set('Accept',  'application/json')
                .set('Authorization',  `Bearer ${user.access_token}`)
            }
        this.corsHeaders = corsHeaders;

        // Set Submission false
        this.submitted = false;

        // Show loader
        this.SavingProperty = true;

        const url  = AppSettings.API_ENDPOINT + 'auth/property';
        this.httpPostRequests(url).then(resp => {
          if (typeof resp != "undefined") {

            if (!resp.error) {
                this.showSuccess = true;

                let self = this;
                setTimeout(function(){
                  self.router.navigateByUrl('/', {skipLocationChange: true}).then(()=>
                    self.router.navigate(["/add"]));
                }, 1500);
            }

          } else {
          }

          // Hide loader
          this.SavingProperty = false;
        })
      }

      if(typeof this.saveData.property_feature == "undefined")
        this.resetPropertyFeatures();
    }

    hideFailureMsg() {
      this.showFailure = false;
    }

    open_SignUpOpts_Modal() {
      const initialState = {};
      this.modalRef = this.modalService.show(SiginUpOptsComponent, {initialState});
    }

    getCurrentLocation() {
      // // Get Current Location
      // const url = "https://www.googleapis.com/geolocation/v1/geolocate?key=" + AppSettings.GOOGLE_MAPS_KEY;
      // return this.helperService.httpPostRequests(url, []).then(resp => {
      //   this.Location_LatLng = new google.maps.LatLng(resp.location.lat, resp.location.lng);
      //
      //   return true;
      // });
      let self = this;
      // Try HTML5 geolocation.
      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(position => {
          self.setPropMarker(position.coords.latitude, position.coords.longitude);
        });
      }
    }

    initMap() {
      if (typeof google != "undefined") {
        const mapProperties = {
            center    : this.Location_LatLng,
            zoom      : 15,
            disableDefaultUI: true,
            suppressInfoWindows:true,
            mapTypeId : google.maps.MapTypeId.ROADMAP,
            clickableIcons: false,
            styles : (AppSettings.mapStyles as any)
       };

       this.commuteMap = new google.maps.Map(this.mapElement.nativeElement, mapProperties);
       // console.log("this.commuteMap",this.commuteMap);
       let url =  "/assets/site/images/gharbaarPin.svg";
       // var image = {
       //               url: url,
       //               size: new google.maps.Size(71, 71),
       //               origin: new google.maps.Point(0, 0),
       //               anchor: new google.maps.Point(17, 34),
       //               scaledSize: new google.maps.Size(35, 35)
       //             };
       this.listingMarker = new google.maps.Marker({
           map       : this.commuteMap,
           position  : this.Location_LatLng,
           icon      : url,
           draggable : false
       });

       // Get Lat/Lng from Address
       this.googleMapsObj = google;
       this.map           = this.googleMapsObj.maps.Map;
       this.geocoder      = new google.maps.Geocoder();

       //********** Company Address Autocomplete Listner ******//
        let self          = this;
        this.input        = document.getElementById('propertyAddress');
        this.autocomplete = new google.maps.places.Autocomplete(this.input);

        this.autocomplete.bindTo('bounds', this.commuteMap);
        this.autocomplete.addListener('place_changed', function() {
         self.listingMarker.setVisible(false);
         let place = self.autocomplete.getPlace();

         // Set Office Address by Place
         self.saveData.address = place.formatted_address;

         if (!place.geometry) {
           return;
         }

         if (place.geometry.viewport) {
             self.commuteMap.fitBounds(place.geometry.viewport);
         } else {
             self.commuteMap.setCenter(place.geometry.location);
             self.commuteMap.setZoom(15);
         }
         self.listingMarker.setPosition(place.geometry.location);
         self.listingMarker.setVisible(true);
         self.Location_LatLng = new google.maps.LatLng(place.geometry.location.lat(),place.geometry.location.lng());

         // Geocode
         self.geocoder.geocode({'latLng': self.Location_LatLng}, function(results, status) {
           if (status.toString() == "OK") {
               // Set Company Area nd city
               self.setModalMapLocation_By_Localities(results, false);
           } else {
               // alert('Geocode was not successful for the following reason: ' + status);
               console.error('Geocode was not successful for the following reason: ' + status);
           }
         });
       });
      }
    }

    addressToLatLng(area_name, city_name) {
      // var address = area_name + ', ' + city_name;
      this.saveData.address = area_name + ', ' + city_name;
      let self    = this;

      console.log("this.saveData.address",this.saveData.address);

      this.geocoder.geocode( { 'address': this.saveData.address}, function(results, status) {
        if (status.toString() == "OK") {
            self.setPropMarker(results[0].geometry.location.lat(), results[0].geometry.location.lng());
            // self.Location_LatLng  = results[0].geometry.location;
            //
            // self.commuteMap.setCenter(self.Location_LatLng);
            // self.listingMarker.setPosition(self.Location_LatLng);
        } else {
            // alert('Geocode was not successful for the following reason: ' + status);
            console.error('Geocode was not successful for the following reason: ' + status);
        }
      });
    }

    setModalMapLocation_By_Localities(results, updateAddress = true) {
      // Set Agency Address Params
      if(updateAddress)
        this.saveData.address  = results[0].formatted_address;

      this.finalLatitude         = results[0].geometry.location.lat();
      this.finalLongitude        = results[0].geometry.location.lng();

      // Show Area Loader
      this.areasLoading = true;

      this.helperService.getSublocalities(results).then(resp => {
        if(resp) {
          console.log("*** resp: ", resp);

          // Set City
          this.SelectedCity.name = resp.city.name;
          // this.agent.agency.city_id = resp.city.city_id;
          this.selectCity();

          // Set Area
          if(resp.p_loc.length > 0) {
            let p_locs = resp.p_loc;

            // Set Selected Area
            this.SelectedArea = { id: p_locs[0].id, name: p_locs[0].name };

            // Set Agency breadcrumbs for location
            this.breadcrumbs = [];
            this.breadcrumbs.push(this.SelectedCity);
            this.breadcrumbs = [...this.breadcrumbs, ...p_locs.slice().reverse()];

            // Update Changes
            this.cdr.detectChanges();
          }
        }

        // Hide Area Loader
        this.areasLoading = false;
      });
    }

    setPropMarker(lat, lng) {
      this.Location_LatLng  = new google.maps.LatLng(lat, lng);
      this.commuteMap.setCenter(this.Location_LatLng);
      this.listingMarker.setPosition(this.Location_LatLng);
    }

    initMapInModal() {
      let self = this;

      // Init New Map for Modal Else Recenter the already Existing
      if(!this.modalMap) {
        const mapProperties = {
              center    : this.Location_LatLng,
              zoom      : 15,
              mapTypeId : google.maps.MapTypeId.ROADMAP
        };

        this.modalMap  = new google.maps.Map(this.openMap.nativeElement, mapProperties);

        let url =  "assets/site/images/gharbaarPin.svg";
        // var image = {
        //               url        : url,
        //               size       : new google.maps.Size(71, 71),
        //               origin     : new google.maps.Point(0, 0),
        //               anchor     : new google.maps.Point(17, 34),
        //               scaledSize : new google.maps.Size(35, 35)
        //             };

        this.ModalMarker = new google.maps.Marker({
            map       : this.modalMap,
            position  : this.Location_LatLng,
            icon      : url,
            draggable : true
        });

        // Set InfoWindow
        this.infowindo        = new google.maps.InfoWindow();
        this.infowindoContent = document.getElementById('infowindow-content');
        this.infowindo.setContent(this.infowindoContent);

        // Add Listner for Map Marker
        google.maps.event.addListener(this.ModalMarker, 'dragend', function() {
        self.geocoder.geocode({'latLng': self.ModalMarker.getPosition()}, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            if (results[0]) {
              // $('#address').val(results[0].formatted_address);
              self.completeAdress = results[0].formatted_address;


              // For Storing Data
              self.saveData.address = results[0].formatted_address;

              // $('#latitude').val(marker.getPosition().lat());
              self.finalLatitude = self.ModalMarker.getPosition().lat();
              // console.log("self.finalLatitude: ",self.finalLatitude);
              // $('#longitude').val(marker.getPosition().lng());
              self.finalLongitude = self.ModalMarker.getPosition().lng();

              // Update Property Marker
              self.setPropMarker(self.ModalMarker.getPosition().lat(), self.ModalMarker.getPosition().lng())

              // console.log("self.finalLongitude: ",self.finalLongitude);
              self.infowindo.setContent(results[0].formatted_address);

              self.infowindo.open(self.modalMap, self.ModalMarker);
            }

            //*******  Get Sub localities  ********//
            self.setModalMapLocation_By_Localities(results);
          }
        });
      });
      }
      else {
        this.modalMap.setCenter(this.Location_LatLng);
        this.ModalMarker.setPosition(this.Location_LatLng);
      }
    }

    map2Image() {
      return `https://maps.googleapis.com/maps/api/staticmap?center=`+ this.Location_LatLng.lat() +`,`+ this.Location_LatLng.lng() +`&zoom=14&size=800x600&markers=anchor:anchor:32,10%7Cicon:https://gharbaar.com/assets/site/images/gharbaarPin.png%7C`+ this.Location_LatLng.lat() +`,`+ this.Location_LatLng.lng() +`&key=` + AppSettings.GOOGLE_MAPS_KEY;
    }

}

import { Component, OnInit, ChangeDetectorRef, ElementRef, ViewChild } from '@angular/core';
import { Router, Route, ActivatedRoute, NavigationEnd } from "@angular/router";
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AppSettings, SEOService, HelperService } from '../../../services/_services';
import { BsModalService, BsModalRef, ModalOptions } from 'ngx-bootstrap/modal';
import { ProContactDetailsComponent } from '../../entry-components/pro-contact-details/pro-contact-details.component';
import { OwlCarousel } from 'ngx-owl-carousel';

// Alert Plugin
import Swal from 'sweetalert2';

declare var $;

@Component({
  selector: 'app-find-pro-details',
  templateUrl: './find-pro-details.component.html',
  styleUrls: ['./find-pro-details.component.css']
})
export class FindProDetailsComponent implements OnInit {

  @ViewChild('propMap', {static: false}) mapElement: ElementRef;
  @ViewChild('owlElement', {static: false}) owlElement: OwlCarousel;
  @ViewChild('owlthumb', {static: false}) owlthumb: OwlCarousel;

  public modalRef      : BsModalRef;
  public lead         : any = { name: '', email: '', phone: '' , message: '', user_id: ''};
  public LeadForm     : FormGroup;
  public submitted    : any = false;

  public desktopbanner      : any = true;
  public hoverEffectOver    : any = false;

  public openForm           : any = false;
  public sideopened         : any = false;
  public leadForm           : any = false;

  public getProData       : any = false;

  // Agent and Agency Details
  public pro           : any;
  public ProCategories : any = AppSettings.ProCategoriesRoutes;
  public proID         : any;
  public proCategoryID : any;
  public proCategory   : any;
  public ImagesUrl      : any = AppSettings.IMG_ENDPOINT;

  public prosMap          : any;
  public mapBounds        : any;
  public simpleMapUrl     : any = '';
  public showSimpleMap    : any = false;
  public infowindo        : any;
  public markersArray     : any = [];
  public infoWindowProperty : any = [];

  // Subscribers
  public routerSubscription : any;

  // Owl Carousel
  public mySlideImages      = [];
  public myCarouselImages   = [];
  public mobileProBannerItems;
  public mySlideOptions     = {startPosition:0,items: 1, dots: false, nav: true, thumbs:false};
  public myCarouselOptions  = {items: 4, dots: false, nav: false, thumbs:false};
  public mobileProBanner    = {items: 1, dots: false, nav: true, thumbs:false};
  public showCarouselModal  : any = false;
  public simiProwhatsapp    : any;

  public owlNumberActive    : any;
  public totalNumberImages  : any;
  public owlOpen            : any;
  public owlChecker         : any = true;
  public BigowlChecker      : any = true;

  constructor(
    private elementRef            : ElementRef,
    private formBuilder           : FormBuilder,
    private seoService            : SEOService,
    private helperService         : HelperService,
    private router                : Router,
    private modalService          : BsModalService,
    private cdr                   : ChangeDetectorRef
  ) {
    // Subscribe to Route Cgange
    this.routerSubscription = this.router.events.subscribe((val) => {
      if(val instanceof NavigationEnd) {
        let slug = this.helperService.renderProProfileURL(val.url);
        console.log("**** slug: ",slug);

        // Set Agent ID
        this.proID = parseInt(slug.id);

        // Set Category ID
        this.proCategoryID = parseInt(slug.category);

        // Get Agent Details
        this.getAgentDetails();
      }
    });
   }

  ngOnInit() {
    // Define Form
    this.LeadForm = this.formBuilder.group({
      name: [null, Validators.required],
      email: [null, [Validators.required, Validators.email]],
      phone: [null, Validators.required],
      message: [null, Validators.required]
    });

  }

  ngAfterViewInit() {
    if (typeof window != "undefined") {
        if (window.innerWidth < 768){
          this.desktopbanner =false;
          this.cdr.detectChanges();
          this.mobOwlCarousal();
          this.deferParseImages();
        }
        else{
          this.deferParseImages();
        }
    }
  }

  ngOnDestroy() {
    if (typeof this.routerSubscription != 'undefined') {
      //prevent memory leak when component destroyed
       this.routerSubscription.unsubscribe();
    }
  }

  get f() { return this.LeadForm.controls; }
  // Request Info Add Lead
  AddLead() {
    // leadData.push(prop);
    this.submitted        = true;

    // Stop here if form is invalid
    if (this.LeadForm.invalid) {
     // console.log(this.LeadForm.controls);
     return;
    }
    // let user = this.authenticationService.get_currentUserValue();
    // if (user) {
    //   leadData = user.user.id;
    // }
    this.lead.user_id = this.proID;
    const url  = AppSettings.API_ENDPOINT + 'pro/lead/add';
    this.helperService.httpPostRequests(url, this.lead)
      .then(resp => {
        if (typeof resp != "undefined" && resp != null && typeof resp.error === "undefined" ) {
          this.lead = {};

          Swal.fire('Request Sent', 'Your message has been sent successfully. You will receive a reply directly at your email address.', 'success')

          // Set Submission false
          this.submitted = false;
        } else {
            console.error(resp);
        }
      })
      .catch(error => {
        console.log("error: ",error);

        // Set Submission false
        this.submitted = false;
      });
  }

  open_ContactDetails_Modal() {
    const initialState = { proID: this.pro.id, proName: this.pro.first_name + ' ' + this.pro.last_name };
    const config: ModalOptions = { class: 'modal-sm phone-number', initialState };
    this.modalRef = this.modalService.show(ProContactDetailsComponent, config);
  }

  openWhatsapp(){
    if(typeof window != "undefined")
    // console.log("this.agent.whatsapp_number",this.agent.whatsapp_number)
      window.open("https://api.whatsapp.com/send?phone="+ this.pro.whatsapp_number_code + this.pro.whatsapp_number +"&text=Hi%2C%20I%20would%20like%20to%20inquire%20about%20your%20professional%20at%20Gharbaar.com.%20Please%20contact%20me%20back%20at%20your%20earliest.%20Thanks", "_blank");
  }

  mobOwlCarousal(){
    if(typeof $ != "undefined") {
      ($(".projMobile div:not(.cloned) .fancybox")as any).fancybox({
           buttons: [
             "zoom",
             "share",
             "slideShow",
             "fullScreen",
             "download",
             "thumbs",
             "close"
           ],
           iframe : {
            preload : false
           }
       });
    }
  }

  deferParseImages(){
    var imgDefer = document.getElementsByTagName('img');
    for (var i=0; i<imgDefer.length; i++) {
      if(imgDefer[i].getAttribute('data-src')) {
        imgDefer[i].setAttribute('src',imgDefer[i].getAttribute('data-src'));
      }
    }
    var picDefer = document.getElementsByTagName('source');
    for (var i=0; i<picDefer.length; i++) {
      if(picDefer[i].getAttribute('data-src')) {
        picDefer[i].setAttribute('srcset',picDefer[i].getAttribute('data-src'));
      }
    }
  }

  hoverEffect(param){
    if(param == "in"){
      this.hoverEffectOver = true;
    }
    else{
      this.hoverEffectOver = false;
    }
  }

  openSideForm(){
    this.openForm = !this.openForm;
    this.sideopened = !this.sideopened;
    this.leadForm = !this.leadForm;
    $('body').toggleClass("overflow-hidden");
    // $('.main-content').toggleClass("overflow-hidden");
    window.scrollTo(0, 0);
    // $('#formName').focus();
  }

  getAgentDetails() {
    this.getProData = true;
    let url  = AppSettings.API_ENDPOINT + 'pro/get-details';
    let data = { id: this.proID, category: this.proCategoryID };

    this.helperService.httpPostRequests(url, data).then(resp => {
        this.pro = resp;
         console.log("this.agent",this.pro);
        if (!this.pro.id) {
          this.helperService.redirectTo404();
          return;
        }
        // For replacing the url according to ID change
        this.checkUrlResponse();

        let self = this;
        setTimeout(function() {
          self.initMap();
        }, 300);

        // Detect Changes
        this.cdr.detectChanges();
        this.setSEO();

    }).catch(error => {
        if(error.status === 404)
          this.helperService.redirectTo404();
          // this.router.navigate([AppSettings.notFoundUrl]);

        console.error("error: ",error);
    })
  }

  checkUrlResponse(){
    if (typeof window != "undefined") {
      if (this.pro) {
        let data = { id: this.pro.id, category: this.pro.company.category, city: this.pro.company.city, name: this.pro.company.name }
        let setUrl = this.helperService.proProfileRoute(data);
           window.history.replaceState({}, '', setUrl);
      }
    }
  }

  openProjectCarouselModal() {
    if (typeof $ != "undefined") {
      if(!this.showCarouselModal) {
          this.showCarouselModal = true;

          let self = this;
          setTimeout(function(){
             // self.deferParseImages();

            // Initiate Owl Carousel
            self.setOwlCaro();

            // Show CarouselModal
            $('#ProjectCarouselModal').modal('show');
          }, 100);
      } else {
          // Show CarouselModal
          $('#ProjectCarouselModal').modal('show');
      }
    }
  }

  setOwlCaro() {
    this.mySlideImages     = [];
    this.myCarouselImages  = [];


    for (let i = 0; i < this.pro.gallery.length; i++) {
      // Set Owl Images
      if (this.pro.gallery[i].is_video == '1') {
        this.mySlideImages.push(this.ImagesUrl + 'uploads/pro/gallery/' + this.pro.gallery[i].name.split('.')[0] + '.jpg');
        this.myCarouselImages.push(this.ImagesUrl + 'uploads/pro/gallery/thumbnails/' + this.pro.gallery[i].name.split('.')[0] + '.jpg');
      }
      else{
        this.mySlideImages.push(this.ImagesUrl + 'uploads/pro/gallery/' + this.pro.gallery[i].name);
        this.myCarouselImages.push(this.ImagesUrl + 'uploads/pro/gallery/thumbnails/' + this.pro.gallery[i].name);
      }

    }
    // this.owlElement.trigger('destroy.owl.carousel');
    // this.owlElement.trigger('initialize.owl.carousel');
    // Refresh
    // this.owlElement.reInit();
    // this.owlthumb.reInit();
    let self = this;
    if (typeof $ != "undefined") {
      $(document).ready(function() {
        var bigimage = $(".owl-main");
        var thumbs = $(".owl-custom-thumbnails");
        //var totalslides = 10;
        var syncedSecondary = true;

        bigimage
          .owlCarousel({
          items: 1,
          slideSpeed: 5000,
          nav: false,
          autoplay: false,
          dots: false,
          loop: false,
          responsiveRefreshRate: 200,
          // navText: [
          //   '<i class="fa fa-arrow-left" aria-hidden="true"></i>',
          //   '<i class="fa fa-arrow-right" aria-hidden="true"></i>'
          // ]
        })
          .on("changed.owl.carousel", syncPosition);

        thumbs
          .on("initialized.owl.carousel", function() {
          thumbs
            .find(".owl-item")
            .eq(0)
            .addClass("current");
        })
          .owlCarousel({
          dots: false,
          nav: false,
          // navText: [
          //   '<i class="fa fa-arrow-left" aria-hidden="true"></i>',
          //   '<i class="fa fa-arrow-right" aria-hidden="true"></i>'
          // ],
          smartSpeed: 200,
          slideSpeed: 500,
          slideBy: 4,
          responsiveRefreshRate: 100,
          responsive: {
            0: {
              items: 3
            },
            600: {
              items: 6
            },
            1000: {
              items: 12
            }
          }
        })
          .on("changed.owl.carousel", syncPosition2);

        function syncPosition(el) {
          //if loop is set to false, then you have to uncomment the next line
          var current;
          current = el.item.index
          //to disable loop, comment this block
          // var count = el.item.count - 1;
          // var current = Math.round(el.item.index - el.item.count / 2 - 0.5);
          //
          // if (current < 0) {
          //   current = count;
          // }
          // if (current > count) {
          //   current = 0;
          // }
          //to this
          thumbs
            .find(".owl-item")
            .removeClass("current")
            .eq(current)
            .addClass("current");
          var onscreen = thumbs.find(".owl-item.active").length - 1;
          var start = thumbs
          .find(".owl-item.active")
          .first()
          .index();
          var end = thumbs
          .find(".owl-item.active")
          .last()
          .index();
          // console.log("current",current)
          // console.log("thumbs.data(owl.carousel)",thumbs.data("owl.carousel"))
          if (current > end) {
            thumbs.data("owl.carousel").to(current, 100, true);
          }
          if (current < start) {
            thumbs.data("owl.carousel").to(current - onscreen, 100, true);
          }
        }

        function syncPosition2(el) {
          if (syncedSecondary) {
            var number;

            number = el.item.index;
            // console.log("number",number)
            // console.log("bigimage.data(owl.carousel)",bigimage.data("owl.carousel"))
            bigimage.data("owl.carousel").to(number, 100, true);
          }
        }

        thumbs.on("click", ".owl-item", function(e) {
          e.preventDefault();
          var number = $(this).index();
          bigimage.data("owl.carousel").to(number, 300, true);
        });
      });
    }
  }


  initMap() {
    if (typeof google != "undefined") {
      // Initialize Map if not already
      if(!this.prosMap) {
        let position = new google.maps.LatLng(this.pro.company.latitude, this.pro.company.longitude);
        const mapProperties = {
             center    : position,
             zoom      : 14,
             mapTypeId : google.maps.MapTypeId.ROADMAP
        };

        // console.log("this.mapElement: ",this.mapElement);
        this.prosMap    = new google.maps.Map(this.mapElement.nativeElement, mapProperties);
        this.mapBounds  = new google.maps.LatLngBounds();

        let url =  "assets/site/images/gharbaarPin.svg";
        var image = {
                      url: url,
                      size: new google.maps.Size(71, 71),
                      origin: new google.maps.Point(0, 0),
                      anchor: new google.maps.Point(17, 34),
                      scaledSize: new google.maps.Size(35, 35)
                    };

        let marker = new google.maps.Marker({
            map      : this.prosMap,
            position : position,
            icon: image,
            draggable: false
        });
      }
    }
  }

  setSEO() {

    // let image = this._location['_platformLocation'].protocol + '//' + this._location['_platformLocation'].hostname + "/assets/site/images/default-image.jpg";
    // if (this.agent.agency.logo) {
    //      image = this.ImagesUrl + 'uploads/agencies/thumbnails/' + this.agent.agency.logo;
    // }

    // Set Title & Metatag for property
    let title = this.pro.company.name + ' | Gharbaar.com';
    // let title =   'Professionals | Gharbaar.com';
    let desc  = this.pro.company.description + ' | Gharbaar.com';
    // let desc  = 'Professionals | Gharbaar.com';

    // Set description lengt6h
    if(desc.length > 160)
      desc = desc.substring(0, 160);

    // Set Basic Meta Tags
      this.seoService.updateTitle(title);
      this.seoService.updateDescription(desc);

    // Set Og Meta tags
      this.seoService.updateOgTitle(title);
      this.seoService.updateOgDesc(desc);
      this.seoService.updateOgUrl();
      // this.seoService.updateOgImage(image);

    // Set Twitter Meta Tags
      this.seoService.updateTwitterTitle(title);
      this.seoService.updateTwitterDesc(desc);

    // Set Canonical Tag
      this.seoService.updateCanonicalUrl();

      // Set Socail Params
        // this.shareUrl       = this._location['_platformLocation'].href;
        // // console.log("shareUrl",this.shareUrl)
        // this.shareIImageUrl = image;
  }

  proNumbers(index){
    // this.simiAgentNumber = { phone: this.agent.similar_agents[index].phone_number, whatsapp: this.agent.similar_agents[index].whatsapp_number, whatsappCode: this.agent.similar_agents[index].whatsapp_number_code};
    // console.log("index",index);
    // console.log("this.pro.similar_pros[index]",this.pro.similar_pros[index]);
    let proname;
    if (this.pro.similar_pros[index].first_name && this.pro.similar_pros[index].last_name) {
        proname = this.pro.similar_pros[index].first_name + ' ' + this.pro.similar_pros[index].last_name;
    }
    else{
      proname = this.pro.similar_pros[index].first_name;
    }
    const initialState = { proName: proname, phone_number: this.pro.similar_pros[index].phone_number, whatsapp_number: this.pro.similar_pros[index].whatsapp_number, whatsapp_number_code: this.pro.similar_pros[index].whatsapp_number_code, staff: false, simiAgencies:true};
    const config: ModalOptions = { class: 'modal-sm phone-number', initialState };
    this.modalRef = this.modalService.show(ProContactDetailsComponent, config);
  }
  proWhatsapp(index){
    this.simiProwhatsapp = { whatsapp: this.pro.similar_pros[index].whatsapp_number, whatsappCode: this.pro.similar_pros[index].whatsapp_number_code};
    if(typeof window != "undefined")
      window.open("https://api.whatsapp.com/send?phone="+ this.simiProwhatsapp.whatsappCode + this.simiProwhatsapp.whatsapp +"&text=Hi%2C%20I%20would%20like%20to%20inquire%20about%20your%20agency%20at%20Gharbaar.com.%20Please%20contact%20me%20back%20at%20your%20earliest.%20Thanks", "_blank");
  }
  setProProfileRoute(pro) {
    return this.helperService.proProfileRoute(pro);
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FindProDetailsComponent } from './find-pro-details.component';

describe('FindProDetailsComponent', () => {
  let component: FindProDetailsComponent;
  let fixture: ComponentFixture<FindProDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FindProDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FindProDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

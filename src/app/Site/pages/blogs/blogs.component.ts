import { Component, OnInit } from '@angular/core';
import { AppSettings, HelperService, SEOService, PurposeService, FavoriteService, HideService, SaveSearchService, EmailBucketService } from '../../../services/_services';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-blogs',
  templateUrl: './blogs.component.html',
  styleUrls: ['./blogs.component.css']
})
export class BlogsComponent implements OnInit {

  public ImagesUrl            :any = AppSettings.IMG_ENDPOINT;

  public BlogsResp         :any = [];
  public BlogsData         :any = [];
  public Blogs_Available   :any = false;
  public cat_id            :any = null;
  public categoryData      :any = [];

  public pagination        :any = [];
  public contentLoaded     :any = false;

  public subscription             : any = { email: ''};
  public subscriptionForm         : FormGroup;
  public submitted                : any = false;
  public ajaxloader               : any = false;

  public subscriptionFooter             : any = { email: ''};
  public subscriptionFooterForm         : FormGroup;
  public footSubmitted                  : any = false;

  constructor(
    private helperService         : HelperService,
    private formBuilder           : FormBuilder,
  ) {
    this.getAllBlogs(false);
    this.getAllCategories();
   }

  ngOnInit() {

    this.subscriptionForm = this.formBuilder.group({
      email    : [null, [Validators.required, Validators.email]],
    });
    this.subscriptionFooterForm = this.formBuilder.group({
      email    : [null, [Validators.required, Validators.email]],
    });
  }

  get f() { return this.subscriptionForm.controls; }
  AddSubscribe(){
    this.submitted        = true;

    // Stop here if form is invalid
    if (this.subscriptionForm.invalid) {
     return;
    }

    this.ajaxloader = true;
    let url  = AppSettings.API_ENDPOINT + 'subscribers/add';
    this.helperService.httpPostRequests(url, this.subscription)
      .then(resp => {
        //console.log("resp: ",resp);
        this.ajaxloader = false;
        if (typeof resp != "undefined" && resp != null && typeof resp.error === "undefined") {
          this.subscription = { email: ''};

          Swal.fire('Request Sent', 'Your message has been sent successfully. We will get back to you shortly.', 'success');

          // Set Submission false
          this.submitted = false;
        } else {
            console.error(resp);
        }
      })
      .catch(error => {
        console.log("error: ",error);
        this.ajaxloader = false;
        Swal.fire('Newsletter Subscription', "You have already subscribed to the newsletter" , 'warning');
        // Set Submission false
        this.submitted = false;
      });
  }

  get g() { return this.subscriptionFooterForm.controls; }
  AddSubscribeFoot(){
    this.footSubmitted        = true;

    // Stop here if form is invalid
    if (this.subscriptionFooterForm.invalid) {
     return;
    }

    this.ajaxloader = true;
    let url  = AppSettings.API_ENDPOINT + 'subscribers/add';
    this.helperService.httpPostRequests(url, this.subscriptionFooter)
      .then(resp => {
        //console.log("resp: ",resp);
        this.ajaxloader = false;
        if (typeof resp != "undefined" && resp != null && typeof resp.error === "undefined") {
          this.subscriptionFooter = { email: ''};

          Swal.fire('Request Sent', 'Your message has been sent successfully. We will get back to you shortly.', 'success');

          // Set Submission false
          this.footSubmitted = false;
        } else {
            console.error(resp);
        }
      })
      .catch(error => {
        console.log("error: ",error);
        this.ajaxloader = false;
        Swal.fire('Newsletter Subscription', "You have already subscribed to the newsletter" , 'warning');
        // Set Submission false
        this.footSubmitted = false;
      });
  }

  getByCategory(cat_id) {
    this.cat_id    = cat_id;
    // this.BlogsResp = [];
    // this.BlogsData = [];

    // Get Blogs
    this.getAllBlogs(false);
  }

  getAllBlogs(nextPage) {
    this.contentLoaded = false;
    let url;
    if(!nextPage) {
      url = AppSettings.API_ENDPOINT + 'blogs/get-all';
      this.BlogsResp = [];
      this.BlogsData = [];
    }
    else if (this.BlogsResp.last_page > this.BlogsResp.current_page) {
        let page = this.BlogsResp.current_page + 1;
        url = AppSettings.API_ENDPOINT + 'blogs/get-all?page=' + page;
    } else
        return false;

    let data = { cat_id : this.cat_id }

    this.helperService.httpPostRequests(url, data).then(resp => {
      if (typeof resp != "undefined") {
        this.BlogsResp = resp;
        this.BlogsData = [...this.BlogsData, ...resp.data];

        this.contentLoaded =true;
        // console.log("this.BlogsResp" , this.BlogsResp);
        // console.log("this.BlogsData" , this.BlogsData);
      }
    }).catch(error => {
        console.error("error: ",error);
        this.contentLoaded =false;
    });
  }

  getAllCategories() {
    const url = AppSettings.API_ENDPOINT + 'blogCategories/get';
    let data = {};
    this.helperService.httpPostRequests(url, data).then(resp => {
      if (typeof resp != "undefined") {
        this.categoryData = resp;
        // console.log("this.categoryData" , this.categoryData);
      }
    }).catch(error => {
        console.error("error: ",error);
    });
  }

  getBlogUrl(blog){
    return this.helperService.get_blog_URL(blog);
  }

}

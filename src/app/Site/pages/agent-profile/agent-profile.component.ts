import { Component, OnInit, ElementRef, ViewChild, HostListener, ChangeDetectorRef } from '@angular/core';
import { Location } from '@angular/common';
import { Router, Route, ActivatedRoute, NavigationEnd } from "@angular/router";
import { AuthenticationService, AppSettings, HelperService, SEOService, FavoriteService, HideService, SaveSearchService } from '../../../services/_services';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

// Bootstrap Components
import { BsModalService, BsModalRef, ModalOptions } from 'ngx-bootstrap/modal';
import { ProContactDetailsComponent } from '../../entry-components/pro-contact-details/pro-contact-details.component';
import { OwlCarousel } from 'ngx-owl-carousel';

// Alert Plugin
import Swal from 'sweetalert2';

@Component({
  selector: 'app-agent-profile',
  templateUrl: './agent-profile.component.html',
  styleUrls: ['./agent-profile.component.css']
})
export class AgentProfileComponent implements OnInit {

  // @ViewChild('section_af', {static: false}) section_af: ElementRef;
  // @ViewChild('propertiesId', {static: false}) propertiesId: ElementRef;
  // @ViewChild('officesId', {static: false}) officesId: ElementRef;
  // @ViewChild('staff_contentId', {static: false}) staff_contentId: ElementRef;
  // @ViewChild('overview', {static: false}) overview: ElementRef;
  // @ViewChild('hide_sticky', {static: false}) hide_sticky: ElementRef;
  @ViewChild('owlElement', {static: false}) owlElement: OwlCarousel;
  @ViewChild('propMap', {static: false}) mapElement: ElementRef;


  public stickNavbar    : any = false;
  public spiedTags      = ['SECTION'];
  public currentSection = 'overview';
  public addPad         = false;
  public ImagesUrl      : any = AppSettings.IMG_ENDPOINT;

  // Agent and Agency Details
  public agent         : any;
  public agentID       : any;
  public proCategoryID : any;
  public proCategory   : any;

  public agentContact  : any;
  public modalRef      : BsModalRef;

  public staffNumber   :any = {};
  public whatsappNumber:any = {};

  public simiAgentNumber   :any = {};
  public simiAgentwhatsapp :any = {};

  public ShowAgentNum     : any = false;

  // Request Info Form
  public lead         : any = { name: '', email: '', phone: '' , message: '', user_id: ''};
  public LeadForm     : FormGroup;
  public submitted    : any = false;
  public authSubs     : any;
  public getAgentData : any = false;

  // Social Shore Params
  public shareUrl       = '';
  public shareIImageUrl = '';

  // Map Objects
  public prosMap          : any;
  public mapBounds        : any;
  public simpleMapUrl     : any = '';
  public showSimpleMap    : any = false;
  public infowindo        : any;
  public listingMarker    : any;
  public Location_LatLng  : any = [];
  public markerlatitudes  : any;
  public markersArray     : any = [];
  public infoWindowProperty : any = [];
  public labelPrice      :any = "";

  // Subscribers
  public routerSubscription : any;
  public openForm           : any = false;
  public sideopened         : any = false;
  public leadForm           : any = false;

  public mySlideProperties = [];
  public mySlideOptions    = {  dots: false, nav: true, thumbs:false,stagePadding: 10,margin:20,
                                  responsive:{
                                      0:{
                                          items:1
                                      },
                                      650:{
                                          items:2
                                      },
                                      991:{
                                          items:3
                                      },
                                      1200:{
                                          items:4
                                      }
                                  }};

  constructor(
    private authenticationService : AuthenticationService,
    private elementRef            : ElementRef,
    private activatedRoute        : ActivatedRoute,
    private router                : Router,
    private helperService         : HelperService,
    private seoService            : SEOService,
    private _location             : Location,
    private modalService           : BsModalService,
    private formBuilder           : FormBuilder,
    private cdr                   : ChangeDetectorRef
  ) {
    // Subscribe to Route Cgange
    this.routerSubscription = this.router.events.subscribe((val) => {
      if(val instanceof NavigationEnd) {
        let slug = this.helperService.renderProProfileURL(val.url);
        // console.log("**** slug: ",slug);

        // Set Agent ID
        this.agentID = parseInt(slug.id);

        // Set Category ID
        this.proCategoryID = parseInt(slug.category);

        // this.proCategory = parseInt(slug.category);
        this.getAgentDetails();
      }
    });

  }

  // HostListener for nav scrolller and sticky
  // @HostListener('window:scroll', ['$event']) track(event) {
  //   if(window.pageYOffset >= this.section_af.nativeElement.offsetTop - 10 && window.pageYOffset < this.hide_sticky.nativeElement.offsetTop - 10) {
  //     this.stickNavbar = true;
  //   } else {
  //     this.stickNavbar = false;
  //   }
  //
  //   this.addPad = false;
  //
  //   if(window.pageYOffset >= this.overview.nativeElement.offsetTop - 50 && window.pageYOffset < this.officesId.nativeElement.offsetTop){
  //     this.currentSection = 'overview';
  //   }
  //   if(window.pageYOffset >= this.officesId.nativeElement.offsetTop - 50 && window.pageYOffset < this.staff_contentId.nativeElement.offsetTop) {
  //     this.currentSection = 'offices';
  //   }
  //    if(window.pageYOffset >= this.staff_contentId.nativeElement.offsetTop - 50 && window.pageYOffset < this.propertiesId.nativeElement.offsetTop) {
  //     this.currentSection = 'staff_content';
  //   }
  //    if(window.pageYOffset >= this.propertiesId.nativeElement.offsetTop - 50){
  //     this.currentSection = 'properties';
  //   }
  // }

  // onSectionChange(sectionId: string) {
  //   if (typeof sectionId == "undefined") {
  //     sectionId = 'overview';
  //   }
  //   this.currentSection = sectionId;
  // }
  //
  // scrollTo(section) {
  //   var x = $("#" + section).offset();
  //   window.scrollTo({ top: x.top - 60, behavior: 'smooth' });
  // }

  ngOnInit() {
    // Subscribe to Route Cgange
    // this.activatedRoute.params.subscribe(routeParams => {
    //   console.log("routeParams: ",routeParams)
    //   let slug = this.helperService.renderAgencyProfileURL(routeParams.slug);
    //   console.log("slug",slug)
    //   // Set Agent ID
    //   this.agentID = parseInt(slug.id);
    //   // this.proCategory = parseInt(slug.category);
    //   this.getAgentDetails();
    // });



    // Define Form
    this.LeadForm = this.formBuilder.group({
      name: [null, Validators.required],
      email: [null, [Validators.required, Validators.email]],
      phone: [null, Validators.required],
      message: [null, Validators.required]
    });
  }

  ngAfterViewInit() {
    this.authSubs = this.authenticationService.currentUserS$.subscribe(user => {
      if (user) {
          this.lead.name  = user.user.first_name + ' ' + user.user.last_name;
          this.lead.email = user.user.email;
          this.lead.phone = user.user.phone_number;

          this.cdr.detectChanges();
      } else {
          // Reset Lead Form
          this.lead.name  = '';
          this.lead.email = '';
          this.lead.phone = '';
          // console.log("--- user: ", user);
      }
    });

  }

  ngOnDestroy() {
    if (typeof this.routerSubscription != 'undefined') {
      //prevent memory leak when component destroyed
       this.routerSubscription.unsubscribe();
    }
  }
  deferParseImages(){
    if (typeof document != "undefined") {
      var imgDefer = document.getElementsByTagName('img');
      for (var i=0; i<imgDefer.length; i++) {
        if(imgDefer[i].getAttribute('data-src')) {
          imgDefer[i].setAttribute('src',imgDefer[i].getAttribute('data-src'));
        }
      }
      var picDefer = document.getElementsByTagName('source');
      for (var i=0; i<picDefer.length; i++) {
        if(picDefer[i].getAttribute('data-src')) {
          picDefer[i].setAttribute('srcset',picDefer[i].getAttribute('data-src'));
        }
      }
    }
  }
  checkUrlResponse(){
    if (typeof window != "undefined") {
      if (this.agent) {
        let setUrl = this.helperService.agencyProfileRoute(this.agent);
           window.history.replaceState({}, '', setUrl);
      }
    }
  }

  get f() { return this.LeadForm.controls; }

  // Request Info Add Lead
  AddLead() {
    // leadData.push(prop);
    this.submitted        = true;

    // Stop here if form is invalid
    if (this.LeadForm.invalid) {
     // console.log(this.LeadForm.controls);
     return;
    }
    // let user = this.authenticationService.get_currentUserValue();
    // if (user) {
    //   leadData = user.user.id;
    // }
    this.lead.user_id = this.agentID;
    const url  = AppSettings.API_ENDPOINT + 'pro/lead/add';
    this.helperService.httpPostRequests(url, this.lead)
      .then(resp => {
        if (typeof resp != "undefined" && resp != null && typeof resp.error === "undefined" ) {
          this.lead = {};

          Swal.fire('Request Sent', 'Your message has been sent successfully. You will receive a reply directly at your email address.', 'success')

          // Set Submission false
          this.submitted = false;
        } else {
            console.error(resp);
        }
      })
      .catch(error => {
        console.log("error: ",error);

        // Set Submission false
        this.submitted = false;
      });
  }

  getAgentDetails() {
    this.getAgentData = true;
    let url  = AppSettings.API_ENDPOINT + 'pro/get-details';
    let data = { id: this.agentID, category: this.proCategoryID };

    this.helperService.httpPostRequests(url, data).then(resp => {

        this.agent = resp;
         // console.log("this.agent",this.agent);
        if (!this.agent || !this.agent.id) {
          this.helperService.redirectTo404();
          return;
        }
        // For replacing the url according to ID change
        this.checkUrlResponse();
        this.deferParseImages();
        // Detect Changes
        this.cdr.detectChanges();

        // Initiate Maps
        if(this.agent.offices.length > 0) {
          let self = this;
          setTimeout(function() {
            self.initMap();
          }, 300);
        }

        this.setSEO();

        if(this.agent.property_types.length > 0) {
            let types = '';
            for (let i = 0; i < this.agent.property_types.length; i++) {
                types += this.agent.property_types[i].sub_type;

                let temp = this.agent.property_types.length - 1;
                if(i < temp)
                  types += ', ';
            }
            this.agent.property_types = types;
        } else {
            this.agent.property_types = '';
        }
        // console.log("this.agent: ",this.agent);
    }).catch(error => {
        if(error.status === 404)
          this.helperService.redirectTo404();
          // window.location.replace(AppSettings.notFoundUrl);
          // this.router.navigateByUrl(AppSettings.notFoundUrl, {skipLocationChange: false});
          // this.router.navigate([AppSettings.notFoundUrl]);

        console.error("error: ",error);
    })
  }
  setSEO() {
    // Set Default Msg for Inquiry
     this.lead.message = "I'd like to have more information about this agent.";
    let image = this._location['_platformLocation'].protocol + '//' + this._location['_platformLocation'].hostname + "/assets/site/images/default-image.jpg";
    if (this.agent.agency.logo) {
         image = this.ImagesUrl + 'uploads/agencies/thumbnails/' + this.agent.agency.logo;
    }

    // Set Title & Metatag for property
    let title = this.agent.agency.name + ' | Gharbaar.com';
    let desc  = this.agent.agency.description + ' | Gharbaar.com';

    // Set description lengt6h
    if(desc.length > 160)
      desc = desc.substring(0, 160);

    // Set Basic Meta Tags
      this.seoService.updateTitle(title);
      this.seoService.updateDescription(desc);

    // Set Og Meta tags
      this.seoService.updateOgTitle(title);
      this.seoService.updateOgDesc(desc);
      this.seoService.updateOgUrl();
      this.seoService.updateOgImage(image);

    // Set Twitter Meta Tags
      this.seoService.updateTwitterTitle(title);
      this.seoService.updateTwitterDesc(desc);

    // Set Canonical Tag
      this.seoService.updateCanonicalUrl();

      // Set Socail Params
        this.shareUrl       = this._location['_platformLocation'].href;
        // console.log("shareUrl",this.shareUrl)
        this.shareIImageUrl = image;
  }
  open_ContactDetails_Modal() {
    let full_name;
    if (this.agent.last_name) {
        full_name = this.agent.first_name + ' ' + this.agent.last_name ;
    }
    else{
      full_name = this.agent.first_name;
    }

    const initialState = { proID: this.agent.id, proName: full_name };
    const config: ModalOptions = { class: 'modal-sm phone-number', initialState };
    this.modalRef = this.modalService.show(ProContactDetailsComponent, config);
  }
  convertToInt(val) {
    return parseInt(val);
  }
  propertyRoute(property) {
    return this.helperService.propertyRoute(property);
  }
  showPhoneNum(){
    this.ShowAgentNum = true;
  }
  openWhatsapp(){
    if(typeof window != "undefined")
      window.open("https://api.whatsapp.com/send?phone="+ this.agent.whatsapp_number_code + this.agent.whatsapp_number +"&text=Hi%2C%20I%20would%20like%20to%20inquire%20about%20your%20agency%20at%20Gharbaar.com.%20Please%20contact%20me%20back%20at%20your%20earliest.%20Thanks", "_blank");
  }
  staffNumbers(index){
    // this.staffNumber = { phone: this.agent.staff[index].phone_number, whatsapp: this.agent.staff[index].whatsapp_number, whatsappCode: this.agent.staff[index].whatsapp_number_code};
    const initialState = { proName: this.agent.staff[index].name, phone_number: this.agent.staff[index].phone_number, whatsapp_number: this.agent.staff[index].whatsapp_number, whatsapp_number_code: this.agent.staff[index].whatsapp_number_code, staff: true, simiAgencies:false };
    const config: ModalOptions = { class: 'modal-sm phone-number', initialState };
    this.modalRef = this.modalService.show(ProContactDetailsComponent, config);
  }
  staffWhatsapp(index){
    this.whatsappNumber = { whatsapp: this.agent.staff[index].whatsapp_number, whatsappCode: this.agent.staff[index].whatsapp_number_code};
    if(typeof window != "undefined")
      window.open("https://api.whatsapp.com/send?phone="+ this.whatsappNumber.whatsappCode + this.whatsappNumber.whatsapp +"&text=Hi%2C%20I%20would%20like%20to%20inquire%20about%20your%20agency%20at%20Gharbaar.com.%20Please%20contact%20me%20back%20at%20your%20earliest.%20Thanks", "_blank");
  }
  agenciesNumbers(index){
    // this.simiAgentNumber = { phone: this.agent.similar_agents[index].phone_number, whatsapp: this.agent.similar_agents[index].whatsapp_number, whatsappCode: this.agent.similar_agents[index].whatsapp_number_code};
    const initialState = { proName: this.agent.similar_agents[index].name, phone_number: this.agent.similar_agents[index].phone_number, whatsapp_number: this.agent.similar_agents[index].whatsapp_number, whatsapp_number_code: this.agent.similar_agents[index].whatsapp_number_code, staff: false, simiAgencies:true };
    const config: ModalOptions = { class: 'modal-sm phone-number', initialState };
    this.modalRef = this.modalService.show(ProContactDetailsComponent, config);
  }
  agenciesWhatsapp(index){
    this.simiAgentwhatsapp = { whatsapp: this.agent.similar_agents[index].whatsapp_number, whatsappCode: this.agent.similar_agents[index].whatsapp_number_code};
    if(typeof window != "undefined")
      window.open("https://api.whatsapp.com/send?phone="+ this.simiAgentwhatsapp.whatsappCode + this.simiAgentwhatsapp.whatsapp +"&text=Hi%2C%20I%20would%20like%20to%20inquire%20about%20your%20agency%20at%20Gharbaar.com.%20Please%20contact%20me%20back%20at%20your%20earliest.%20Thanks", "_blank");
  }
  openSideForm(){
    this.openForm = !this.openForm;
    this.sideopened = !this.sideopened;
    this.leadForm = !this.leadForm;
    $('body').toggleClass("overflow-hidden");
    // $('.main-content').toggleClass("overflow-hidden");
    window.scrollTo(0, 0);
    // $('#formName').focus();
  }

  initMap() {
    if (typeof google != "undefined") {
      // Hide Simple Map
      this.showSimpleMap = false;

      // Initialize Map if not already
      if(!this.prosMap) {
        // let position = new google.maps.LatLng(this.ListingsData.data[i].latitude, this.ListingsData.data[i].longitude);
        const mapProperties = {
             // center    : position,
             clickableIcons: false,
             zoom      : 14,
             mapTypeId : google.maps.MapTypeId.ROADMAP,
             styles : (AppSettings.mapStyles as any)
        };

        // console.log("this.mapElement: ",this.mapElement);
        this.prosMap = new google.maps.Map(this.mapElement.nativeElement, mapProperties);
        this.mapBounds   = new google.maps.LatLngBounds();

        // Initialize Info Window
        this.infowindo = new google.maps.InfoWindow();
      }
      // Add Listing Markers
      this.addMarkers();
    }
  }
  addMarkers() {
    // Empty Markers Array
    this.markersArray = [];

    for (let i = 0; i < this.agent.offices.length; i++) {
      if(this.agent.offices[i].latitude != null && this.agent.offices[i].longitude != null) {
        let position = new google.maps.LatLng(this.agent.offices[i].latitude, this.agent.offices[i].longitude);
         let price    = this.agent.offices[i].name + '';
         this.labelPrice = price;

        this.setMarker(position, this.agent.offices[i]);
        this.prosMap.setCenter(position);
        // this.markersArray.push(marker);
      }
    }
    // Set Bounds or Show Simple map incase no markers are available
    // this.prosMap.fitBounds(this.mapBounds);
    if(this.markersArray.length > 1)
      this.prosMap.fitBounds(this.mapBounds);
    else if(this.markersArray.length < 1)
      this.setMapWithoutMarkers();
    // if(this.markersArray.length > 0)

    // else
    //   this.setMapWithoutMarkers();
      // this.showSimpleMapFn();
  }
  setMarker(position, pro) {
    let self = this;
    // console.log("**** position: ",position);
    function HTMLMarker(lat, lng) {
        this.lat = lat;
        this.lng = lng;
        this.pos = new google.maps.LatLng(lat,lng);
    }

    HTMLMarker.prototype          = new google.maps.OverlayView();
    HTMLMarker.prototype.onRemove = function(){}

    //init your html element here
    HTMLMarker.prototype.onAdd = function() {
        // console.log("**** pro: ",pro);
        this.div = document.getElementById("mapsLabel-" + pro.id);
        // console.log("**** this.div: ",this.div);
        // this.div.className = "htmlMarker";
        this.div.style.position = 'absolute';
        // this.div.innerHTML = '<span>'+label+'</span>';
        var panes = this.getPanes();
        panes.overlayImage.appendChild(this.div);
    }

    HTMLMarker.prototype.draw = function() {
        var overlayProjection  = this.getProjection();
        var position           = overlayProjection.fromLatLngToDivPixel(this.pos);
        var panes              = this.getPanes();
        this.div.style.left    = position.x + 'px';
        this.div.style.top     = position.y + 'px';

        let pos = this.pos;
        let div = this.div;
        google.maps.event.addDomListener(this.div, "click", function(event) {
    			// // google.maps.event.trigger(self, "click");
          // // console.log("event: ", event);
          // // console.log("property: ", property);
          //  console.log("pos: ", pos);
          // console.log("self.prosMap: ", self.prosMap);
          // console.log("self.infowindo: ", self.infowindo);

          self.infoWindowProperty = pro;
          $('#infowindow-content').removeClass('af-none');
          $('.mapBox').removeClass('hideTri');
          self.infowindo.setContent(document.getElementById('infowindow-content'));
          self.infowindo.setPosition(pos);
          self.infowindo.open(self.prosMap);
    		});
    }

    //to use it
    let marker;
    marker = new HTMLMarker(position.lat(), position.lng());
    // console.log("position.lng()",position.lng())
    // console.log("this.prosMap",this.prosMap);
    marker.setMap(this.prosMap);

    // Extend Bounds
    this.mapBounds.extend(marker.pos);

    // Add to Markers Array
    this.markersArray.push(marker);
  }
  adjustMap(ofc_id, bool) {
    // console.log("ofc_id",ofc_id)
    if (bool) {
      $("#mapsLabel-" + ofc_id).addClass("hovered");
    }
    else{
      $("#mapsLabel-" + ofc_id).removeClass("hovered");
    }
  }
  adjustList(property_id, bool){
    if (bool) {
      $("#listLabel-" + property_id).addClass("hovered");
    }
    else{
      $("#listLabel-" + property_id).removeClass("hovered");
    }
  }
  setMapWithoutMarkers() {
    let address;
      address = this.agent.agency.city;
      // console.log("address",address)
    let self     = this;
    let geocoder = new google.maps.Geocoder();
    geocoder.geocode( { 'address': address}, function(results, status) {
      if (status.toString() == "OK") {
          self.Location_LatLng  = new google.maps.LatLng(results[0].geometry.location.lat(), results[0].geometry.location.lng());
          self.prosMap.setCenter(self.Location_LatLng);
      } else {
          console.error('Geocode was not successful for the following reason: ' + status);
      }
    });
  }
  closeInfoWindow(){
    $('#infowindow-content').addClass('af-none');
    $('.mapBox').addClass('hideTri');
  }
  // showSimpleMapFn() {
  //   // Show Simple Map if map has no markers
  //   this.showSimpleMap = true;
  //
  //   let address;
  //     address = this.searchParms.city.name;
  //
  //   this.simpleMapUrl = 'https://www.google.com/maps/embed/v1/place?q='+ address +'&key=' + AppSettings.GOOGLE_MAPS_KEY;
  // }

  agencyProfileRoute(agency) {
    let agent = { agency: agency };

    return this.helperService.agencyProfileRoute(agent);
    // return '/agent/' + this.replaceString(agency.city) + '_' + this.replaceString(agency.name) + '-' + agency.id;
  }

}

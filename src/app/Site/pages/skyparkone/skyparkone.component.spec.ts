import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SkyparkoneComponent } from './skyparkone.component';

describe('SkyparkoneComponent', () => {
  let component: SkyparkoneComponent;
  let fixture: ComponentFixture<SkyparkoneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SkyparkoneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SkyparkoneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit, ElementRef, ViewChild, HostListener } from '@angular/core';

import { Router, NavigationEnd } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';


// Import Local Services
import { ActiveNavService, HelperService, AppSettings } from '../../../services/_services';

import Swal from 'sweetalert2';

declare var $ , Pace;

@Component({
  selector: 'app-skyparkone',
  templateUrl: './skyparkone.component.html',
  styleUrls: ['./skyparkone.component.css']
})
export class SkyparkoneComponent implements OnInit {

    public displayTimerFooter     : any = false;
    public openForm               :any  = false;
    public leadForm               :any  = false;

    @ViewChild('myElem', {static: false}) MyProp: ElementRef;
    @ViewChild('section_af', {static: false}) section_af: ElementRef;
    // @ViewChild('section_Mob_btns', {static: false}) section_Mob_btns: ElementRef;

    @ViewChild('overview', { static: false })  overview: ElementRef;
    @ViewChild('overviewss', { static: false })  overviewss: ElementRef;
    @ViewChild('developer', { static: false })  developer: ElementRef;
    @ViewChild('features', { static: false })  features: ElementRef;
    @ViewChild('propertyType', { static: false })  propertyType: ElementRef;
    @ViewChild('location', { static: false })  location: ElementRef;
    @ViewChild('paymentPlan', { static: false })  paymentPlan: ElementRef;
    @ViewChild('bookingProcess', { static: false })  bookingProcess: ElementRef;
    @ViewChild('noc', { static: false })  noc: ElementRef;
    @ViewChild('contact', { static: false })  contact: ElementRef;
    @ViewChild('faq', { static: false })  faq: ElementRef;
    @ViewChild('leadFormView', { static: false })  leadFormView: ElementRef;
    @ViewChild('floorPlan', { static: false })  floorPlan: ElementRef;
    @ViewChild('constructionStatus', { static: false })  constructionStatus: ElementRef;


    public readmore         : any  = true;
    public activeMobileNav  : any;
    public helloFoot        : any = true;
    public sideopened       : any = true;

    public lead             : any = { name: '', phone: '',email: '', interest: '', paymentPlan:'', source : 'Website',project_id: 2};
    public LeadForm         : FormGroup;
    public submitted        : any = false;
    public ajaxloader       : any = false;
    public ShowAgentNum     : any = false;

    // public phnlead             : any = { name: '', phone: '', interest: '', paymentPlan:'', source : 'Website',project_id: 2};

    public phnlead           : any = { name: '', email: '', phone: '', interest: '' , source: 'Website'};
    public phnLeadForm         : FormGroup;
    public phnsubmitted        : any = false;
    public phnajaxloader       : any = false;

    public whatsapLead             : any = {project_id: 2, type: 'whatsapp'};

    public projectLead             : any = {project_id: 2, type: 'project'};

    public MobBtns          : any = false;

    public listDisplay       : any = true;
    public listDisplay2      : any = true;
    public listDisplay3      : any = true;
    public listDisplay4      : any = true;
    public stickNavbar       : any = false;
    public stickmobBtns      : any = false;
    public faqAccordiansMore : any = true;
    public developerMore     : any = true;
    public allReadMore       : any = true;
    public hasErrorPhn       : any = true;
    public phnNumber         : any;
    public leadEmail         : any;
    public revealForm        : any = false;
    public propertyAccordiansMore     :any = true;
    public floorAccordiansMore        :any = true;


    public spiedTags      = ['DIV'];
    public currentSection = '';
    public addPad         = false;

    public  screenOffsetTop : any = 0 ;

    public unitVal_Start                    :any = "";
    public unitVal_End                    :any = "";
    public unitName                   :any = "";

    public shopDetail               :any = true;
    public restaurantDetail        :any = true;
    public appartmentDetail        :any = true;

    public showFillForm       : any = false;

    public hoverEffectOver    : any = false;
    public owlNumberActive    : any;
    public totalNumberImages  : any;
    public owlOpen            : any;
    public owlChecker         : any = true;
    public BigowlChecker      : any = true;

    public reportedProp       : any = false;

  constructor(
      private router           : Router,
      private activeNavService : ActiveNavService,
      private helperService    : HelperService,
      private formBuilder      : FormBuilder,
     ) {

      this.router.events.subscribe((val) => {
          if(val instanceof NavigationEnd) {
             const tree = router.parseUrl(router.url);
             // console.log("tree", tree);
              if (tree.fragment) {
                // console.log("tree.fragment", tree.fragment);
                const element = document.querySelector("#" + tree.fragment + '-section');
                if (element) { element.scrollIntoView(true); window.scrollBy(0, -50); }
              }
           }
        });

  }

  @HostListener('window:scroll', ['$event']) track(event) {

        if(window.pageYOffset >= this.section_af.nativeElement.offsetTop + 100) {
          this.stickNavbar = true;
        } else {
          this.stickNavbar = false;
        }

        if (typeof window != "undefined") {
            if (window.innerWidth < 768){
                this.screenOffsetTop = 650;
            }
            else{
                this.screenOffsetTop = 880;
            }
        }

         if(window.pageYOffset <= this.overview.nativeElement.offsetTop + this.screenOffsetTop) {
           window.history.replaceState({}, '',`/skyparkone`);
           this.currentSection = "";
         }

        // console.log("this.overviewss.nativeElement", this.overviewss.nativeElement);
        // console.log("this.overviewss.nativeElement.offsetTop", this.overviewss.nativeElement.offsetTop);
      }


  ngOnInit() {

    $('.projMobile').owlCarousel({
      loop:true,
      margin:10,
      nav:false,
      dots:true,
      responsive:{
          0:{
              items:1
          },
          600:{
              items:1
          },
          1000:{
              items:1
          }
      }
    })

    if(typeof Pace != "undefined") {
      Pace.options.ajax.trackWebSockets = false;
      Pace.options.restartOnPushState = false;
    }

    function setInputFilter(textbox, inputFilter) {
  ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"].forEach(function(event) {
    textbox.addEventListener(event, function() {
      if (inputFilter(this.value)) {
        this.oldValue = this.value;
        this.oldSelectionStart = this.selectionStart;
        this.oldSelectionEnd = this.selectionEnd;
      } else if (this.hasOwnProperty("oldValue")) {
        this.value = this.oldValue;
        this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
      }
    });
  });
}

// Install input filters.
setInputFilter(document.getElementById("phoneNum"), function(value) {
  return /^[\d -]*$/.test(value); });

    let url  = AppSettings.API_ENDPOINT + 'project-stats/update';
    this.helperService.httpPostRequests(url, this.projectLead)
      .then(resp => {
        //console.log("resp: ",resp);
        this.ajaxloader = false;
        if (typeof resp != "undefined" && resp != null && typeof resp.error === "undefined") {
          this.projectLead = {project_id: 2, type: 'project'};

          //Swal.fire('Request Sent', 'Your message has been sent successfully. We will get back to you shortly.', 'success');
          // Set Submission false
        } else {
            console.error(resp);
        }
      })
      .catch(error => {
        console.log("error: ",error);
        this.ajaxloader = false;
        // Set Submission false
        this.submitted = false;
      });
    // Set FormGroup
    this.LeadForm = this.formBuilder.group({
      name     : [null, Validators.required],
      email    : [null, [Validators.required, Validators.email]],
      phone    : [null, Validators.required],
      interest : [null, Validators.required],
    });

    this.phnLeadForm = this.formBuilder.group({
      phone    : [null, Validators.required],
    });

     $(".image-caro-popup div:not(.cloned) .fancybox").fancybox({
          buttons: [
            "zoom",
            "share",
            "slideShow",
            "fullScreen",
            "download",
            "thumbs",
            "close"
          ],
      });

  }

  ngAfterViewInit(){
    $(document).ready(function() {
        var bigimage = $("#constructionBig");
        var thumbs = $("#constructionthumbs");
        //var totalslides = 10;
        var syncedSecondary = true;

        bigimage
          .owlCarousel({
          items: 1,
          slideSpeed: 5000,
          nav: false,
          autoplay: false,
          dots: false,
          loop: true,
          responsiveRefreshRate: 200,
          // navText: [
          //   '<i class="fa fa-arrow-left" aria-hidden="true"></i>',
          //   '<i class="fa fa-arrow-right" aria-hidden="true"></i>'
          // ]
        })
          .on("changed.owl.carousel", syncPosition);

        thumbs
          .on("initialized.owl.carousel", function() {
          thumbs
            .find(".owl-item")
            .eq(0)
            .addClass("current");
        })
          .owlCarousel({
          dots: false,
          nav: false,
          navText: [
            '<i class="fa fa-arrow-left" aria-hidden="true"></i>',
            '<i class="fa fa-arrow-right" aria-hidden="true"></i>'
          ],
          smartSpeed: 200,
          slideSpeed: 500,
          slideBy: 4,
          responsiveRefreshRate: 100,
          responsive: {
            0: {
              items: 4
            },
            600: {
              items: 6
            },
            1000: {
              items: 8
            }
          }
        })
          .on("changed.owl.carousel", syncPosition2);

        function syncPosition(el) {
          //if loop is set to false, then you have to uncomment the next line
          //var current = el.item.index;

          //to disable loop, comment this block
          var count = el.item.count - 1;
          var current = Math.round(el.item.index - el.item.count / 2 - 0.5);

          if (current < 0) {
            current = count;
          }
          if (current > count) {
            current = 0;
          }
          //to this
          thumbs
            .find(".owl-item")
            .removeClass("current")
            .eq(current)
            .addClass("current");
          var onscreen = thumbs.find(".owl-item.active").length - 1;
          var start = thumbs
          .find(".owl-item.active")
          .first()
          .index();
          var end = thumbs
          .find(".owl-item.active")
          .last()
          .index();

          if (current > end) {
            thumbs.data("owl.carousel").to(current, 100, true);
          }
          if (current < start) {
            thumbs.data("owl.carousel").to(current - onscreen, 100, true);
          }
        }

        function syncPosition2(el) {
          if (syncedSecondary) {
            var number = el.item.index;
            bigimage.data("owl.carousel").to(number, 100, true);
          }
        }

        thumbs.on("click", ".owl-item", function(e) {
          e.preventDefault();
          var number = $(this).index();
          bigimage.data("owl.carousel").to(number, 300, true);
        });
      });
      let self = this;
      $(document).ready(function() {
            var bigimage = $("#big");
            var thumbs = $("#thumbs");
            //var totalslides = 10;
            var syncedSecondary = true;

            bigimage
              .owlCarousel({
              items: 1,
              slideSpeed: 5000,
              nav: true,
              autoplay: false,
              dots: false,
              loop: false,
              responsiveRefreshRate: 200,
              // navText: [
              //   '<i class="fa fa-arrow-left" aria-hidden="true"></i>',
              //   '<i class="fa fa-arrow-right" aria-hidden="true"></i>'
              // ]
            })
              .on("changed.owl.carousel", syncPosition);

            thumbs
              .on("initialized.owl.carousel", function() {
              thumbs
                .find(".owl-item")
                .eq(0)
                .addClass("current");
            })
              .owlCarousel({
              dots: false,
              nav: false,
              navText: [
                '<i class="fa fa-arrow-left" aria-hidden="true"></i>',
                '<i class="fa fa-arrow-right" aria-hidden="true"></i>'
              ],
              smartSpeed: 200,
              slideSpeed: 500,
              slideBy: 4,
              responsiveRefreshRate: 100,
              responsive: {
                0: {
                  items: 4
                },
                767: {
                  items: 8
                },

                1000: {
                  items: 11
                },
                1200: {
                  items: 4
                }
              }
            })
              .on("changed.owl.carousel", syncPosition2);

            function syncPosition(el) {
              //if loop is set to false, then you have to uncomment the next line

              if (self.owlChecker == false) {
                  var current = self.owlOpen;
                  self.owlChecker = true;
              }
              else{
                var current = el.item.index;
              }
              // this.owlNumberActive = current + 1;
              self.owlNumberActive = current + 1;
              self.totalNumberImages = el.item.count;

              //to disable loop, comment this block
              // var count = el.item.count - 1;
              // var current = Math.round(el.item.index - el.item.count / 2 - 0.5);
              //
              // if (current < 0) {
              //   current = count;
              // }
              // if (current > count) {
              //   current = 0;
              // }
              //to this
              thumbs
                .find(".owl-item")
                .removeClass("current")
                .eq(current)
                .addClass("current");
              var onscreen = thumbs.find(".owl-item.active").length - 1;
              var start = thumbs
              .find(".owl-item.active")
              .first()
              .index();
              var end = thumbs
              .find(".owl-item.active")
              .last()
              .index();

              if (current > end) {
                thumbs.data("owl.carousel").to(current, 100, true);
              }
              if (current < start) {
                thumbs.data("owl.carousel").to(current - onscreen, 100, true);
              }
            }

            function syncPosition2(el) {
              if (syncedSecondary) {
                if (self.BigowlChecker == false) {
                  var number = self.owlOpen;
                  self.BigowlChecker = true;
                }
                else{
                  var number = el.item.index;
                }
                bigimage.data("owl.carousel").to(number, 100, true);
              }
            }

            thumbs.on("click", ".owl-item", function(e) {
              e.preventDefault();
              var number = $(this).index();
              bigimage.data("owl.carousel").to(number, 300, true);
            });
          });

  }

  ngOnDestroy() {
    if(typeof Pace != "undefined") {
      Pace.options.ajax.trackWebSockets = true;
      Pace.options.restartOnPushState   = true;
    }
  }

  changeOwlActivePosition(param){
    this.owlOpen = param;
    this.owlChecker = false;
    this.BigowlChecker = false;
  }
  @ViewChild('widgetsContent', { static: false }) public widgetsContent: ElementRef<any>;

  public scrollHorizontal(param): void {
    if (param == "left") {
      this.widgetsContent.nativeElement.scrollTo({ left: (this.widgetsContent.nativeElement.scrollLeft - 200), behavior: 'smooth' });
    }
    else{
      this.widgetsContent.nativeElement.scrollTo({ left: (this.widgetsContent.nativeElement.scrollLeft + 200), behavior: 'smooth' });
    }

  }


  onMobSectionChange(sectionId: string) {
     if (typeof sectionId != "undefined") {
        this.currentSection = sectionId.split('-')[0];
          // console.log("this.currentSection", this.currentSection);
        switch (this.currentSection) {
            case "overview":
              this.overview.nativeElement.scrollIntoView();
             window.history.replaceState({}, '',`/skyparkone#overview`);
              break;

            case "feature":
              this.features.nativeElement.scrollIntoView();
              window.history.replaceState({}, '',`/skyparkone#feature`);
              break;

            case "propertyType":
              this.propertyType.nativeElement.scrollIntoView();
              window.history.replaceState({}, '',`/skyparkone#propertyType`);
              break;

            case "paymentPlan":
              this.paymentPlan.nativeElement.scrollIntoView();
              window.history.replaceState({}, '',`/skyparkone#paymentPlan`);
              break;

            case "floorPlan":
              this.floorPlan.nativeElement.scrollIntoView();
            break;

            case "developer":
              this.developer.nativeElement.scrollIntoView();
              window.history.replaceState({}, '',`/skyparkone#developer`);
              break;

              case "constructionStatus":
              this.constructionStatus.nativeElement.scrollIntoView();
              break;

              case "faq":
              this.faq.nativeElement.scrollIntoView();
              window.history.replaceState({}, '',`/skyparkone#faq`);
              break;

            default:
              break;
          }
    }
  }


  viewReadmore(){
      this.readmore = !this.readmore;
      if (this.readmore) {
          this.scrollTo('myElem');
      }
  }


  scrollTo(section) {
    // console.log('$("#" + '+section+' )',  $("#" + section + '-section'))
    var x = $("#" + section + '-section').offset();
    window.scrollTo({ top: x.top - 50, behavior: 'smooth'});

  }

  closeHelloFoo(){
    this.helloFoot = !this.helloFoot ;
  }

  openSideForm(){
    this.openForm = !this.openForm;
    this.sideopened = !this.sideopened;
    this.leadForm = !this.leadForm;
    $('#contact_mobile').modal('hide');
    $('body').toggleClass("overflow-hidden");
    $('.main-content').toggleClass("overflow-hidden");
    window.scrollTo(0, 0);
    // $('#formName').focus();
  }

  textclick(){
    this.sideopened = !this.sideopened;
    $('#contact_mobile').modal('hide');
    $('#exclusiveOffer').modal('hide');
    // $('#formName').focus();
    this.leadForm = !this.leadForm;
    this.leadFormView.nativeElement.scrollIntoView();
    $('body').toggleClass("overflow-hidden");
  }

  closeOverlay(){
    this.openForm = false;
    this.leadForm = !this.leadForm;
    this.sideopened = !this.sideopened;
    $('body').removeClass("overflow-hidden");
  }
  closePopup(){
    this.revealForm = !this.revealForm;
    $('body').removeClass("overflow-hidden");
  }

   showPhoneNum(){
     this.ShowAgentNum = true;
   }

  // convenience getter for easy access to form fields

  telInputObject(obj) {
    // console.log("telInputObject" ,obj);
    obj.setCountry('pk');
  }
  onCountryChange($event){
    this.lead.phone ="";
    this.lead.countrycode = $event.dialcode;
    // console.log("onCountryChange", $event)
  }
  hasError($event){
    // console.log("hasError" ,$event);
    this.hasErrorPhn = $event;
  }
  getNumber($event){
    // console.log("getNumber" ,$event);
    this.phnNumber = $event;
  }

  get f() { return this.LeadForm.controls; }



  AddLead() {
    this.submitted        = true;

    // console.log("f.email.errors", this.LeadForm.controls.email.errors);
    // Stop here if form is invalid
    if (this.LeadForm.invalid) {
     return;
    }

    this.ajaxloader = true;
    let url  = 'https://portal.skyparkone.com/backend/' + 'email_leads.php?action=sendMail&name=' + this.lead.name + '&email=' + this.lead.email + '&phone=' + this.lead.phone + '&interest=' + this.lead.interest + '&source=' + this.lead.source;
    // this.lead.phone = this.lead.countrycode + this.lead.phone;
    // this.lead.phone = this.phnNumber;
    this.helperService.httpPostRequests(url, this.lead)
      .then(resp => {
        //console.log("resp: ",resp);
        this.ajaxloader = false;
        if (typeof resp != "undefined" && resp != null && typeof resp.error === "undefined") {
          this.lead = { name: '', phone: '',email: '', interest: '',paymentPlan:'', source : 'Website', project_id: 2};

          Swal.fire('Request Sent', 'Your message has been sent successfully. We will get back to you shortly.', 'success');

          this.leadForm = false;
          $('body').removeClass("overflow-hidden");

          // Set Submission false
          this.submitted = false;
        } else {
            console.error(resp);
        }
      })
      .catch(error => {
        console.log("error: ",error);
        this.ajaxloader = false;
        // Set Submission false
        this.submitted = false;
      });
  }

    get y() { return this.phnLeadForm.controls; }

  AddPhnLead() {
    this.phnsubmitted        = true;

    // console.log("f.email.errors", this.LeadForm.controls.email.errors);
    // Stop here if form is invalid
    if (this.phnLeadForm.invalid) {
     return;
    }

    this.phnajaxloader = true;
    let url  = 'https://portal.skyparkone.com/backend/' + 'email_leads.php?action=sendMail&name=' + this.phnlead.name + '&email=' + this.phnlead.email + '&phone=' + this.phnlead.phone + '&interest=' + this.phnlead.interest + '&source=' + this.phnlead.source;
    // this.lead.phone = this.lead.countrycode + this.lead.phone;
    // this.lead.phone = this.phnNumber;
    this.helperService.httpPostRequests(url, this.phnlead)
      .then(resp => {
        //console.log("resp: ",resp);
        this.phnajaxloader = false;
        if (typeof resp != "undefined" && resp != null && typeof resp.error === "undefined") {
          this.phnlead = { name: '', phone: '', interest: '',paymentPlan:'', source : 'Website'};

          // Swal.fire('Request Sent', 'Your message has been sent successfully. We will get back to you shortly.', 'success');
          this.ShowAgentNum = true;
          // this.revealForm = false;
          // $('body').removeClass("overflow-hidden");

          // Set Submission false
          this.phnsubmitted = false;
        } else {
            console.error(resp);
        }
      })
      .catch(error => {
        console.log("error: ",error);
        this.phnajaxloader = false;
        // Set Submission false
        this.phnsubmitted = false;
      });
  }

  openMoreFaq(){
    this.faqAccordiansMore = !this.faqAccordiansMore;
  }

  openMoredeveloper(){
    this.developerMore = !this.developerMore;
  }
  openMoreabout(){
    this.allReadMore = !this.allReadMore;
  }

  openmobileModal(){
    $('#contact_mobile').modal('hide');
    $('#exclusiveOffer').modal('hide');
    setTimeout(function() {
          $('#callus').modal();
      }, 200);
  }

  openNumberModal(){
    $('#exclusiveOffer').modal('hide');
    setTimeout(function() {
          $('#callus').modal();
      }, 200);
  }

  listDisplayFunc(param){
    switch (param) {
      case "mainFeature":
        this.listDisplay = !this.listDisplay;
        if (this.listDisplay) {
          this.scrollTo('featuresHead');
        }
      break;
      case "plotFeature":
        this.listDisplay2 = !this.listDisplay2;
      break;

      case "businessFeature":
        this.listDisplay3 = !this.listDisplay3;
      break;

    case "nearbyFeature":
      this.listDisplay4 = !this.listDisplay4;
    break;

      default:

      break;
    }
  }

  openMoreproperties(){
    this.propertyAccordiansMore = !this.propertyAccordiansMore;
  }

  openMoreFloor(){
    this.floorAccordiansMore = !this.floorAccordiansMore;
  }

  openPaymentDetails(param){
    if (param == "shops") {
      this.shopDetail = !this.shopDetail;
      this.restaurantDetail = true;
      this.appartmentDetail = true;
    }
    else if (param == "restuarants") {
      this.restaurantDetail = !this.restaurantDetail;
      this.shopDetail = true;
      this.appartmentDetail = true;
    }
    else{
      this.appartmentDetail = !this.appartmentDetail;
      this.shopDetail = true;
      this.restaurantDetail = true;
    }
  }

  updateBaseVal(unitVal_Start, unitVal_End){
    this.unitVal_Start  = unitVal_Start;
    this.unitVal_End  = unitVal_End;
  }

  areaConversion(start , unitName){
    if (start) {
      switch (unitName) {
        case "yard":
          return Math.round(this.unitVal_Start / 9 * 100) / 100;
          break;

          case "marla":
          return Math.round(this.unitVal_Start / 272.25 * 100) / 100;
          break;

          case "kanal":
          return Math.round(this.unitVal_Start / 5445 * 100) / 100
          break;

        default:
          // code...
          break;
      }
    }
    else{
      switch (unitName) {
        case "yard":
          return Math.round(this.unitVal_End / 9 * 100) / 100;
          break;

          case "marla":
          return Math.round(this.unitVal_End / 272.25 * 100) / 100;
          break;

          case "kanal":
          return Math.round(this.unitVal_End / 5445 * 100) / 100
          break;

        default:
          // code...
          break;
      }
    }
  }

  openWhatsapp() {
    if(typeof window != "undefined")
      window.open("https://api.whatsapp.com/send?phone=923415222111&text=Hi%2C%0AI%20would%20like%20to%20inquire%20about%20your%20project%20Skypark%20One.%20Please%20contact%20me%20back%20at%20your%20earliest.%0AThanks", "_blank");
  }
  whatsappLeadFunc(){
    let url  = AppSettings.API_ENDPOINT + 'project-stats/update';
    this.helperService.httpPostRequests(url, this.whatsapLead)
      .then(resp => {
        //console.log("resp: ",resp);
        this.ajaxloader = false;
        if (typeof resp != "undefined" && resp != null && typeof resp.error === "undefined") {
          this.whatsapLead = {project_id: 2, type: 'whatsapp'};

          //Swal.fire('Request Sent', 'Your message has been sent successfully. We will get back to you shortly.', 'success');
          // Set Submission false
        } else {
            console.error(resp);
        }
      })
      .catch(error => {
        console.log("error: ",error);
        this.ajaxloader = false;
        // Set Submission false
        this.submitted = false;
      });
  }
  fillForm(){
    this.showFillForm = !this.showFillForm;
  }
  hoverEffect(param){
    if(param == "in"){
      this.hoverEffectOver = true;
    }
    else{
      this.hoverEffectOver = false;
    }
  }

  ReportProject(){
    let url  = AppSettings.API_ENDPOINT + 'projects/report';
    let data = { project_id: this.lead.project_id };
    this.helperService.httpPostRequests(url, data).then(resp => {
      console.log("resp: ", resp);
      this.reportedProp = true;
    })
  }
}

import { Component, OnInit, ViewChild ,ElementRef, HostListener  } from '@angular/core';
import { Location } from '@angular/common';
import { Router, ActivatedRoute } from "@angular/router";
import { AppSettings, HelperService, SEOService, PurposeService, FavoriteService, HideService, SaveSearchService, EmailBucketService } from '../../../services/_services';
import { FormControl } from '@angular/forms';
import { Subject } from 'rxjs';

// Bootsteap Components
import { BsModalRef, BsModalService, ModalOptions } from 'ngx-bootstrap/modal';

// Modal Components
import { ContactDetailsComponent } from '../../entry-components/contact-details/contact-details.component';
import { InquiryModalComponent } from '../../entry-components/inquiry-modal/inquiry-modal.component';


@Component({
  selector: 'app-search-listings',
  templateUrl: './search-listings.component.html',
  styleUrls: ['./search-listings.component.css']
})
export class SearchListingsComponent implements OnInit {

  @ViewChild('section_af', {static: false}) section_af: ElementRef;
  @ViewChild('hide_sticky', {static: false}) hide_sticky: ElementRef;
  @ViewChild('propertyType', { static: false })  propertyType: ElementRef;

  // stickfilter Params
  public stickfilter    :any = false;
  public addPad              = false;

  // Search Params
  public cities          :any = AppSettings.cities;
  public areas           :any = [];
  public searchTypes     :any = [];
  public searchSubTypes  :any = AppSettings.searchTypes;
  public GOOGLE_MAPS_KEY :any = "";

  // Params
  public SelectedCity   = AppSettings.cities[0];
  public searchBeds     = AppSettings.searchBeds;
  public SelectedArea   = [];
  public SelectedType   = "House";

  // Ng-Slect Params
  public areasBuffer  = [];
  public bufferSize   = 10;
  public numberOfItemsFromEndBeforeFetchingMore = 5;
  public areasLoading = false;
  public areaQuery    = "";
  public areasError   = false;

  // Https Params
  protected ngUnsubscribe: Subject<void> = new Subject<void>();
  public httpLoading     = false;
  public corsHeaders     : any;

  public areasSelect_F = new FormControl('');


  public showFeatures :any = false;
  public searchParms  :any = {
                                type     : "",
                                sub_type : "all",
                                city: { id: null, name: "" },
                                area : [],
                                // area: {
                                //         city_id   : null,
                                //         id        : null,
                                //         name      : "",
                                //         p_loc     : [],
                                //         parent_id : ""
                                //       },
                                purpose      : "sale",
                                min_price    : 0,
                                max_price    : 0,
                                min_area     : 0,
                                max_area     : 0,
                                area_unit    : 'marla',
                                bedrooms     : 0,
                                sort_by      : "package",
                                sort_type    : "DESC",
                                current_page : 1
                              };

  public viewAllLocURL        :any = "";
  public modalChangeUnit      :any = 'marla';
  public parentLocs           :any = [];
  public Types_Prop_Count     :any = {};
  public subLocs              :any = [];
  public sortedSubLocs        :any = [];
  public locs_sorted          :any = [];
  public ListingsData         :any = [];
  public Listings_Available   :any = false;
  public pagination           :any = [];
  public locsByCity           :any = false;
  public loadingBreadCrumbs   :any = false;
  public toggleloadmore       :any = false;
  public toggleloadless       :any = true;
  public searchPrices         :any = AppSettings.searchPrices;
  public searchUnits          :any = AppSettings.searchUnits;
  public searchAreas          :any = AppSettings.searchAreas;
  public propPackages         :any = AppSettings.propPackages;
  public SortBy               :any = "package-DESC";
  public ImagesUrl            :any = AppSettings.IMG_ENDPOINT;
  public listView             :any = true;
  public hideLocs             :any = false;
  public hideEmailBucket      :any = true;
  public modalRef             :BsModalRef;
  public innerSideFilter      :any = false;
  public overFlow             :any = false;

  // Subscriptions
  public routerSubscription : any;


  constructor(
    private router                : Router,
    private activatedRoute        : ActivatedRoute,
    private helperService         : HelperService,
    private seoService            : SEOService,
    private purposeService        : PurposeService,
    private _location             : Location,
    private favoriteService       : FavoriteService,
    private hideService           : HideService,
    private saveSearchService     : SaveSearchService,
    private modalService          : BsModalService,
    private emailBucketService    : EmailBucketService,
    private elementRef            : ElementRef,
  ) {
    // Get Globals from AppSettings
    this.searchTypes     = Object.keys(AppSettings.searchTypes);
    // this.searchSubTypes  = AppSettings.searchTypes;
    // this.searchPrices    = AppSettings.searchPrices;
    // this.searchAreas     = AppSettings.searchAreas;
    // this.searchUnits     = AppSettings.searchUnits;
    // this.propPackages    = AppSettings.propPackages;
    this.GOOGLE_MAPS_KEY = AppSettings.GOOGLE_MAPS_KEY;

    // console.log("this.activatedRoute: ",this.activatedRoute);
    this.routerSubscription = this.activatedRoute.params.subscribe(routeParams => {
      let data = routeParams;

      // Get filter params from local storage
      if (typeof this.helperService.getSearchParms() != "undefined" && this.helperService.getSearchParms() != null) {
        if (typeof this.helperService.getSearchParms().minPrice != "undefined")
          this.searchParms.min_price = this.helperService.getSearchParms().minPrice;

        if (typeof this.helperService.getSearchParms().maxPrice != "undefined")
          this.searchParms.max_price = this.helperService.getSearchParms().maxPrice;

        if (typeof this.helperService.getSearchParms().minArea != "undefined")
          this.searchParms.min_area = this.helperService.getSearchParms().minArea;

        if (typeof this.helperService.getSearchParms().maxArea != "undefined")
          this.searchParms.max_area = this.helperService.getSearchParms().maxArea;

        if (typeof this.helperService.getSearchParms().area_unit != "undefined") {
          this.searchParms.area_unit = this.helperService.getSearchParms().area_unit;
          if(this.searchAreas[this.searchParms.area_unit] == 'undefined') {
            this.searchParms.area_unit = 'marla';
          }

          this.modalChangeUnit       = this.searchParms.area_unit;
        }

        if (typeof this.helperService.getSearchParms().bedrooms != "undefined")
          this.searchParms.bedrooms = this.helperService.getSearchParms().bedrooms;
      }

      // Set Search Type
      this.searchParms.type     = data.type.split("-")[0];

      if(typeof data.type.split("-")[1] != "undefined")
        this.searchParms.sub_type = data.type.split("-")[1].replace("_", " ");
      else
        this.searchParms.sub_type = 'all';

      // Set Filter SEO Tags
      let filterSEO = [];

      // Get Filter if isset
      if(typeof data.filter != "undefined") {
        filterSEO = data.filter.split("-");;
        this.searchParms.min_area  = parseInt(data.filter.split("-")[0]);
        this.searchParms.max_area  = parseInt(data.filter.split("-")[0]);
        this.searchParms.area_unit = data.filter.split("-")[1];
        this.modalChangeUnit       = this.searchParms.area_unit;
      }

      // Save Current Route
      this.saveSearchService.updateSearch_URL(this.activatedRoute['_routerState'].snapshot.url);

      // Set Search Purpose
      this.searchParms.purpose = this.activatedRoute.snapshot.url[0].path;

      // Reset View Loc URL Var
      this.viewAllLocURL = '';

      if (typeof data.slug != "undefined") {
        let slug = this.helperService.renderLocURL(data.slug);

        if(parseInt(slug.searchBy) == 1) {   // Search by City
            this.searchParms.city.id   = parseInt(slug.id);
            this.searchParms.city.name = slug.name;
            this.searchParms.area      = [];
            this.parentLocs            = [];

            this.getSubLocationsByCity(this.searchParms.city.id);
            this.locsByCity = true;

            // Get Search Areas
            this.getSearchAreas();

            // Get Property Listing
            this.getListings();

            let self = this;
            setTimeout(function() {
              // Set SEO Tags
              self.setSEO_Tags("City", filterSEO);
            }, 500);

            // Get View All Locs URL
            self.setViewAllLocURL();

        } else if(parseInt(slug.searchBy) == 2) { // Search by Area
            this.getParentLocations(parseInt(slug.id)).then(resp => {
              // Get Search Areas
              this.getSearchAreas();

              // Get Property Listing
              this.getListings();

              // Set SEO Tags
              this.setSEO_Tags("Area", filterSEO);
            })
        }
      }
    });
  }

  @HostListener('window:scroll', ['$event']) track(event) {
        // if(window.pageYOffset >= this.section_af.nativeElement.offsetTop - 10) {
        //   this.stickfilter = true;
        //   console.log("if this.stickfilter" ,this.stickfilter)
        // } else if (window.pageYOffset < this.section_af.nativeElement.offsetTop - 10){
        //   this.stickfilter = false;
        //   console.log("else this.stickfilter" ,this.stickfilter)
        // }

        if(window.pageYOffset >= 152) {
          this.stickfilter = true;
          this.addPad = true;
        } else if (window.pageYOffset < 152){
          this.stickfilter = false;
          this.addPad = false;
        }
    }



  ngOnInit() {
    // Bind jQuery for Dropdwons etc
    this.bindJQueryFuncs();

  }

  bindJQueryFuncs() {
    if(typeof $ != "undefined") {
      // Dropdown close prevention
      $('body').on('click', function(event) {
        $('.status1.bootstrap-select.open').removeClass('open');
      });
      $('#myTabs').on('click', '.nav-tabs a', function() {
          $(this).closest('.dropdown').addClass('dontClose');
      })

      $('#myDropDown').on('hide.bs.dropdown', function(e) {
          if ( $(this).hasClass('dontClose') ) {
              e.preventDefault();
          }
          $(this).removeClass('dontClose');
      });

      // Prevent dropdown close on Price selection
      $('#price_groups').on('click', 'li', function() {
          $('#priceDiv').addClass('dontClose');
      })

      $('#priceDiv').on('hide.bs.dropdown', function(e) {
          if ( $(this).hasClass('dontClose') ) {
              e.preventDefault();
          }
          $(this).removeClass('dontClose');
      });

      // Prevent dropdown close on Area selection
      $('#area_groups').on('click', 'li', function() {
          $('#prparea').addClass('dontClose');
      })

      $('#prparea').on('hide.bs.dropdown', function(e) {
          if ( $(this).hasClass('dontClose') ) {
              e.preventDefault();
          }
          $(this).removeClass('dontClose');
      });
    }
  }

  ngOnDestroy() {
    if (typeof this.routerSubscription != 'undefined') {
      //prevent memory leak when component destroyed
       this.routerSubscription.unsubscribe();
    }
  }

  setPropertyType(type) {
    // this.searchParms.type     = type;
    // this.searchParms.sub_type = "all";
    if(!this.loadingBreadCrumbs) {
      let routeLink = '';
      if (typeof this.searchParms.area.id != "undefined" && this.searchParms.area.id != null) {
          routeLink = this.helperService.getLocURL(this.searchParms.area, this.searchParms.city, false, this.searchParms.purpose, type, "all");
      } else {
          routeLink = this.helperService.getLocURL({}, this.searchParms.city, true, this.searchParms.purpose, type, "all");
      }
      return routeLink;
    }

    // // this.search();
    // this.setLocRoute();
  }

  setPropertySubType(type) {
    // this.searchParms.sub_type = type;

    if(!this.loadingBreadCrumbs) {
      let routeLink = '';
      if (typeof this.searchParms.area.id != "undefined" && this.searchParms.area.id != null) {
          routeLink = this.helperService.getLocURL(this.searchParms.area, this.searchParms.city, false, this.searchParms.purpose, this.searchParms.type, type);
      } else {
          routeLink = this.helperService.getLocURL({}, this.searchParms.city, true, this.searchParms.purpose, this.searchParms.type, type);
      }
      return routeLink;
    }

    // // this.search();
    // this.setLocRoute();
  }

  getFilterURL(filter) {
    if(!this.loadingBreadCrumbs) {
      let routeLink = '';
      if (typeof this.searchParms.area.id != "undefined" && this.searchParms.area.id != null) {
          routeLink = this.helperService.getLocURL_for_Filter(this.searchParms.area, this.searchParms.city, false, filter, this.searchParms.purpose, this.searchParms.type, this.searchParms.sub_type);
      } else {
          routeLink = this.helperService.getLocURL_for_Filter({}, this.searchParms.city, true, filter, this.searchParms.purpose, this.searchParms.type, this.searchParms.sub_type);
      }
      return routeLink;
    }
  }

  resetPropType() {
    this.setPropertyType(this.searchTypes[0]);
    // this.search();
    this.setLocRoute();
  }

  setPrice(type, price) {
    if(type == 'min') {
        this.searchParms.min_price = price;
    } else if(type == 'max') {
        this.searchParms.max_price = price;
    }
  }

  setMinPrices(price) {
    if (this.searchParms.max_price != 0) {
        if(price < this.searchParms.max_price) {
            return true;
        } else {
            return false;
        }
    } else {
        return true;
    }
  }

  resetPrice() {
    this.searchParms.min_price = 0;
    this.searchParms.max_price = 0;

    // this.search();
    this.setLocRoute();
  }

  setArea(type, area) {
    if(type == 'min') {
        this.searchParms.min_area = area;
    } else if(type == 'max') {
        this.searchParms.max_area = area;
    }
  }

  setMinAreas(area) {
    if (this.searchParms.max_area != 0) {
        if(area < this.searchParms.max_area) {
            return true;
        } else {
            return false;
        }
    } else {
        return true;
    }
  }

  resetArea() {
    this.searchParms.min_area = 0;
    this.searchParms.max_area = 0;

    // this.search();
    this.setLocRoute();
  }

  UpdateUnit() {
    this.searchParms.area_unit = this.modalChangeUnit;

    this.resetArea();
  }

  setSEO_Tags(args: string, filterSEO: any[]) {
    // Set Title & Metatag for property
    if (args == "City") {
        let title = '';
        if(filterSEO.length > 0) {
            if(this.searchParms.sub_type == '' || this.searchParms.sub_type == 'all')
              title = filterSEO[0] + ' ' + filterSEO[1].toUpperCase() + ' ' + this.searchParms.type + ' for '+ this.searchParms.purpose +' in '+ this.searchParms.city.name +' | Gharbaar.com';
            else
              title = filterSEO[0] + ' ' + filterSEO[1].toUpperCase() + ' ' + this.searchParms.sub_type + ' for '+ this.searchParms.purpose +' in '+ this.searchParms.city.name +' | Gharbaar.com';
        } else {
            title = 'Real Estate & Property for '+ this.searchParms.purpose +' in '+ this.searchParms.city.name +' | Gharbaar.com';
        }

        // Set Description
        let desc  = 'Find properties for '+ this.searchParms.purpose +' in '+this.searchParms.city.name +' on Gharbaar.com. Get complete details of properties and available amenities. Pakistan\'s No.1 Real Estate Portal. The most up-to-date listing information.'

        // Set Basic Meta Tags
        this.seoService.updateTitle(title);
        this.seoService.updateDescription(desc);

        // Set Og Meta tags
        this.seoService.updateOgTitle(title);
        this.seoService.updateOgDesc(desc);
        this.seoService.updateOgUrl();

        // Set Twitter Meta Tags
        this.seoService.updateTwitterTitle(title);
        this.seoService.updateTwitterDesc(desc);

        // Set Canonical Tag
        this.seoService.updateCanonicalUrl();

    } else {
        let title = '';
        if(filterSEO.length > 0) {
            if(this.searchParms.sub_type == '' || this.searchParms.sub_type == 'all')
              title = filterSEO[0] + ' ' + filterSEO[1].toUpperCase() + ' ' + this.searchParms.type + ' for '+ this.searchParms.purpose +' in '+ this.searchParms.area.name +' '+ this.searchParms.city.name +' | Gharbaar.com';
            else
              title = filterSEO[0] + ' ' + filterSEO[1].toUpperCase() + ' ' + this.searchParms.sub_type + ' for '+ this.searchParms.purpose +' in '+ this.searchParms.area.name +' '+ this.searchParms.city.name +' | Gharbaar.com';
        } else {
            title = 'Real Estate & Property for '+ this.searchParms.purpose +' in '+ this.searchParms.area.name +' '+ this.searchParms.city.name +' | Gharbaar.com';
        }

        // Set Description
        let desc  = 'Find properties for '+ this.searchParms.purpose +' in '+ this.searchParms.area.name +' '+this.searchParms.city.name +' on Gharbaar.com. Get complete details of properties and available amenities. Pakistan\'s No.1 Real Estate Portal. The most up-to-date listing information.'

        // Set Basic Meta Tags
        this.seoService.updateTitle(title);
        this.seoService.updateDescription(desc);

        // Set Og Meta tags
        this.seoService.updateOgTitle(title);
        this.seoService.updateOgDesc(desc);
        this.seoService.updateOgUrl();

        // Set Twitter Meta Tags
        this.seoService.updateTwitterTitle(title);
        this.seoService.updateTwitterDesc(desc);

        // Set Canonical Tag
        this.seoService.updateCanonicalUrl();
    }
  }

  getSearchAreas() {
    // Reset Area Selects
    this.areas        = [];
    this.areasBuffer  = [];

    const url  = AppSettings.API_ENDPOINT + 'locations/get-all-locations?city_id=' + this.searchParms.city.id;
    // console.log("url: ",url);
    this.helperService.httpGetRequests(url).then(resp => {
      if(typeof resp != "undefined") {
        // console.log("resp: ",resp);

        // Set Areas
        this.areas       = resp.locations;
        this.areasBuffer = this.areas.slice(0, this.bufferSize);

        // Hide Area Loader
        this.areasLoading = false;
      }
    });
  }

  selectCity(event) {
    // Reset Area Selects
    console.log("event :",  event)
    console.log("this.searchParms.city :",  this.searchParms.city)
    this.areas        = [];
    this.areasBuffer  = [];
    this.searchParms.area = {
                                city_id   : null,
                                id        : null,
                                name      : "",
                                p_loc     : [],
                                parent_id : ""
                              };

    // let selected = event.target.selectedOptions[0];
    // this.searchParms.city = { id: selected.value, name: selected.label}

    // Update Search
    // this.search();
    this.setLocRoute();
  }

  selectArea() {
    // console.log("this.SelectedArea:", this.SelectedArea);
    // console.log("this.searchParms.area:", this.searchParms.area);
    if(this.searchParms.area == null)
      this.searchParms.area = {
                                  city_id   : null,
                                  id        : null,
                                  name      : "",
                                  p_loc     : [],
                                  parent_id : ""
                                };

    console.log("this.searchParms: ",this.searchParms)
    // Update Search
    // this.search();
    this.setLocRoute();
  }

  InvertPurpose() {
    if (this.searchParms.purpose == 'sale') {
        // this.searchParms.purpose = 'rent';
        return this.helperService.getLocURL(this.searchParms.area, this.searchParms.city, this.locsByCity, 'rent', this.searchParms.type, this.searchParms.sub_type);
    } else {
        // this.searchParms.purpose = 'sale';
        return this.helperService.getLocURL(this.searchParms.area, this.searchParms.city, this.locsByCity, 'sale', this.searchParms.type, this.searchParms.sub_type);
    }

    this.setPurpose();
  }

  updatePurpose(purpose) {
    this.searchParms.purpose = purpose;
    this.setPurpose();
  }

  setPurpose() {
    this.purposeService.updatePurpose(this.searchParms.purpose);

    // this.search();
    this.setLocRoute();
  }

  setViewAllLocURL() {
    let purpose = 1; // 1 is for purpose 'sale'
    if(this.searchParms.purpose == 'rent')
        purpose = 2;

    let searchSeq = this.helperService.getSearchTypes_Seq();
    let typeSubType_ID = searchSeq[this.searchParms.type].id;
    if(this.searchParms.sub_type != 'all' && this.searchParms.sub_type != "")
      typeSubType_ID = searchSeq[this.searchParms.sub_type].id;

    this.viewAllLocURL = '/locations/' + this.searchParms.city.name.replace(/ /g, "_") + '-' + this.searchParms.city.id + '-' + purpose + '-' + typeSubType_ID;
  }

  getLocURL(location, byCity) {
    return this.helperService.getLocURL(location, this.searchParms.city, byCity, this.searchParms.purpose, this.searchParms.type, this.searchParms.sub_type);
  }

  setLocRoute() {
    let routeLink = '';

    if (typeof this.searchParms.area.id != "undefined" && this.searchParms.area.id != null) {
        routeLink = this.helperService.getLocURL(this.searchParms.area, this.searchParms.city, false, this.searchParms.purpose, this.searchParms.type, this.searchParms.sub_type);

        // Set Params locally
        this.setParams_locally(this.searchParms.area);
    } else {
        routeLink = this.helperService.getLocURL({}, this.searchParms.city, true, this.searchParms.purpose, this.searchParms.type, this.searchParms.sub_type);

        // Set Params locally
        this.setParams_locally([]);
    }

    // Navigate to new Route
    this.router.navigateByUrl('/', {skipLocationChange: true}).then(()=>
      this.router.navigate([routeLink]));
  }

  // search() {
  //   if(typeof $ != "undefined") {
  //     $("body").removeClass("overflow-hidden");
  //   }
  //   let type_sub_type = '';
  //   if(this.searchParms.sub_type != "all")
  //     type_sub_type = this.searchParms.type + '-' + this.searchParms.sub_type.replace(/ /g, "_");
  //   else
  //     type_sub_type = this.searchParms.type;
  //
  //   if (typeof this.searchParms.area.id != "undefined" && this.searchParms.area.id != null) {
  //     // Show Area Loader
  //     this.areasLoading = true;
  //
  //     let url = AppSettings.API_ENDPOINT + "locations/get-parent-locations-by-sublocation?sub_loc=" + this.searchParms.area.id;
  //     this.helperService.httpGetRequests(url).then(resp => {
  //       console.log("resp: ",resp);
  //
  //       // Hide Area Loader
  //       this.areasLoading = false;
  //
  //       if (typeof resp.p_loc != "undefined") {
  //           let routeLink = this.searchParms.purpose + '/' + type_sub_type + '/';
  //
  //           // Revese the location -- Parent To Child
  //           let locs      = resp.p_loc.slice().reverse();
  //
  //           // Create Params
  //           let params = this.searchParms.city.name.replace(/ /g, "_")  + '_';
  //
  //           // Loop to make Route
  //           for (let i = 0; i < locs.length; i++) {
  //               // routeLink += locs[i].name.replace(/ /g, "_").replace(/-/g, "_");
  //               params += locs[i].name.replace(/ /g, "_").replace(/-/g, "_").replace(/[/\\*]/g, "_");
  //               let len = locs.length - 1;
  //               if(i < len)
  //                 params += '_';
  //                 // routeLink += '_';
  //           }
  //
  //           params += "-" + this.searchParms.area.id + "-2"; // 2 in params specify its a sub location
  //
  //           // Add area Parents to SelectedArea
  //           this.searchParms.area.p_loc = locs;
  //
  //           // Set Params locally
  //           this.setParams_locally(this.searchParms.area);
  //
  //           // Navigate to Search Page
  //           this.navigateRoute(routeLink, params);
  //       }
  //     });
  //   } else {
  //     let routeLink = this.searchParms.purpose + '/' + type_sub_type + '/' + this.searchParms.city.name.replace(/ /g, "_") + "-" + this.searchParms.city.id + "-1"; // 1 in params specify its a City
  //     // console.log("*** By City routeLink:", routeLink);
  //
  //     // Set Params locally
  //     this.setParams_locally([]);
  //
  //     this.router.navigateByUrl('/', {skipLocationChange: true}).then(()=>
  //       this.router.navigate([routeLink]));
  //   }
  // }

  getParentLocations(id): Promise<any> {
    // Show BreadCrumb Loader
    this.loadingBreadCrumbs = true;

    const url = AppSettings.API_ENDPOINT + "locations/get-parent-locations-by-sublocation?sub_loc=" + id;
    return this.helperService.httpGetRequests(url).then(resp => {
      // Hide BreadCrumb Loader
      this.loadingBreadCrumbs = false;

      if (typeof resp.p_loc != "undefined") {
          let searchedArea = resp.p_loc.slice()[0];

          // if(typeof this.searchParms.area.p_loc != "undefined") {
          // }
          this.parentLocs  = [...resp.p_loc.slice().reverse()];


          this.searchParms.area = {
                                    city_id   : searchedArea.city_id,
                                    id        : searchedArea.id,
                                    name      : searchedArea.name,
                                    p_loc     : this.parentLocs,
                                    parent_id : searchedArea.parent_id,
                                    slug      : searchedArea.slug
                                  }
          // Set City param
          this.searchParms.city = { id: searchedArea.city_id, name: resp.city };

          // Remove the Last parent as its the same as searched area
          this.parentLocs.pop();

          // Get Searched Area Locations
          this.getSubLocationsByArea(searchedArea.id);
          this.locsByCity = false;

          return true;
      }
    })
  }

  getSubLocationsByArea(id) {
    const url  = AppSettings.API_ENDPOINT + 'locations/get-sub-locations-details?p_id=' + id  + "&type=" + this.searchParms.type + "&sub_type=" + this.searchParms.sub_type + "&purpose=" + this.searchParms.purpose;
    this.helperService.httpGetRequests(url).then(resp => {
      if (typeof resp.locations != "undefined") {
        this.subLocs        = resp.locations;
        this.subLocs.sort((a,b) => a.counts - b.counts);
        this.subLocs.reverse();

        this.sortedSubLocs  = [...resp.locations];
        this.sortedSubLocs.sort((a,b) => a.name.localeCompare(b.name));
      }

      // Set Prop Counts
      if(typeof resp.types != "undefined")
        this.Types_Prop_Count = resp.types;
    });
  }

  getSubLocationsByCity(id) {
    // Show BreadCrumb Loader
    this.loadingBreadCrumbs = true;

    const url  = AppSettings.API_ENDPOINT + 'locations/get-parent-locations-details?city_id=' + id + "&type=" + this.searchParms.type + "&sub_type=" + this.searchParms.sub_type + "&purpose=" + this.searchParms.purpose;
    this.helperService.httpGetRequests(url).then(resp => {
      // Hide BreadCrumb Loader
      this.loadingBreadCrumbs = false;

      if (typeof resp.locations != "undefined") {
        this.subLocs     = resp.locations;
        this.subLocs.sort((a,b) => a.counts - b.counts);
        this.subLocs.reverse();

        this.sortedSubLocs  = [...resp.locations];
        this.sortedSubLocs.sort((a,b) => a.name.localeCompare(b.name));
      }

      // Set Prop Counts
      if(typeof resp.types != "undefined")
        this.Types_Prop_Count = resp.types;
    });
  }

  toggleSort() {
    this.locs_sorted = !this.locs_sorted;
  }

  toggleLocsDiv() {
    this.hideLocs = !this.hideLocs;
  }

  SortListing() {
    console.log("this.SortBy: ",this.SortBy.split('-'));
    this.searchParms.sort_by   = this.SortBy.split('-')[0];
    this.searchParms.sort_type = this.SortBy.split('-')[1];

    // Sort Listing
    this.getListings();
  }

  getPageList(page, prev, next) {
    if (prev) {
        this.searchParms.current_page = page - 1;
        if(this.searchParms.current_page < 0)
          this.searchParms.current_page = 0;
    } else if (next) {
        this.searchParms.current_page = page + 1;
        if(this.searchParms.current_page > this.ListingsData.last_page)
          this.searchParms.current_page = this.searchParms.current_page - 1;
    } else {
        this.searchParms.current_page = page;
    }

    this.getListings();
  }

  getListings() {
    const url  = AppSettings.API_ENDPOINT + 'search';
    let data = {
                  purpose      : this.searchParms.purpose,
                  type         : this.searchParms.type,
                  sub_type     : this.searchParms.sub_type,
                  city_id      : this.searchParms.city.id,
                  location_id  : this.searchParms.area.id,
                  min_price    : this.searchParms.min_price,
                  max_price    : this.searchParms.max_price,
                  min_area     : this.searchParms.min_area,
                  max_area     : this.searchParms.max_area,
                  area_unit    : this.searchParms.area_unit,
                  bedrooms     : this.searchParms.bedrooms,
                  sort_by      : this.searchParms.sort_by,
                  sort_type    : this.searchParms.sort_type,
                  current_page : this.searchParms.current_page
                }
    // console.log("**********data: ",data);
    this.helperService.httpPostRequests(url, data).then(resp => {
      if (typeof resp != "undefined") {
        this.ListingsData = resp;

        // Save search data
        this.saveSearchService.updateSearch(resp.data);

        if (this.ListingsData.data.length > 0) {
            this.Listings_Available = true;
        } else {
            this.Listings_Available = false;
        }

        this.pagination = [];
        if (this.ListingsData.last_page > 1) {
          for (let i = 1; i <= this.ListingsData.last_page; i++) {
              this.pagination.push(i);
          }
        }
      }
    }).catch(error => {
        console.error("error: ",error);
    });
  }

  checkPropFavrt(id) {
    return this.favoriteService.getFavoritePropByIndex(id);
  }

  togglefavoriteProp(property) {
    property.id = property.p_id;
    if(this.checkPropFavrt(property.id))
      this.favoriteService.removefavoriteProp(property.id);
    else
      this.favoriteService.addfavoriteProp(property);
  }

  checkPropHidden(id) {
    return this.hideService.getHiddenPropByIndex(id);
  }

  toggleHiddenProp(property) {
    property.id = property.p_id;
    if(this.checkPropHidden(property.id))
      this.hideService.removeHiddenProp(property.id);
    else
      this.hideService.addHiddenProp(property);
  }

  propertyRoute(property) {
    return this.helperService.propertyRoute(property);
  }

  propertyURL(property) {
    return this._location['_platformLocation'].protocol + '//' + this._location['_platformLocation'].hostname + this.helperService.propertyRoute(property);
  }

  open_ContactDetails_Modal(property) {
    const initialState = { property: property };
    const config: ModalOptions = { class: 'modal-sm phone-number', initialState };
    this.modalRef = this.modalService.show(ContactDetailsComponent, config);
  }

  open_Inquiry_Modal(property) {
    const initialState = { property: property };
    const config: ModalOptions = { class: 'modal-md af-signup inquiry', initialState };
    this.modalRef = this.modalService.show(InquiryModalComponent, config);
  }

  checkBucketPropsByID(propID) {
    return this.emailBucketService.checkBucketPropsByID(propID);
  }

  toggleEmailBasket(property) {
    if (this.emailBucketService.checkBucketPropsByID(property.p_id)) {   // If already exisiting then remove it
        this.emailBucketService.removeBucketPropByID(property.p_id);
    } else {                                                                // Else add to Email Basket
        this.emailBucketService.addBucketProps(property);
    }
  }

  convertToInt(val) {
    return parseInt(val);
  }

  expandFeatures(property, index) {
    console.log("property: ",property);
    console.log("index: ",index);

    if (typeof this.ListingsData.data[index].expand == "undefined") {
        this.ListingsData.data[index].expand = false;
    }

    this.ListingsData.data[index].expand = !this.ListingsData.data[index].expand;
    // property.expand
  }

  setParams_locally(loc) {
    let data = {
      city       : this.searchParms.city,
      area       : loc,
      type       : this.searchParms.type,
      sub_type   : this.searchParms.sub_type,
      purpose    : this.searchParms.purpose,
      minPrice   : this.searchParms.min_price,
      maxPrice   : this.searchParms.max_price,
      minArea    : this.searchParms.min_area,
      maxArea    : this.searchParms.max_area,
      area_unit  : this.searchParms.area_unit,
      bedrooms   : this.searchParms.bedrooms,
    }

    // Set Search Params
    this.helperService.setSearchParms(data);
  }

  navigateRoute(url, params) {
    this.router.navigateByUrl('/', {skipLocationChange: true}).then(()=>
      this.router.navigate([url, params]));
  }

  toggleFeatures() {
    this.showFeatures = !this.showFeatures;
  }

  showListView(bool){
    this.listView = bool;
  }

  toggleEmailBucket(){
    this.hideEmailBucket = !this.hideEmailBucket ;
  }

  openSideInner(){
    this.innerSideFilter = !this.innerSideFilter ;
    $("body").toggleClass("overflow-hidden");

    if (typeof window != "undefined") {
        if (window.innerWidth < 768){
            window.scrollTo(0, 0);
        }
    }
  }

  addOverflow(){
    this.overFlow = !this.overFlow;
  }

  //////////////////////////////////////////
  /********* Areas Ng-Select Fns *********/
  ////////////////////////////////////////
  onScrollToEnd_Areas() {
      this.fetchMore_Areas();
  }

  onScroll_Areas({ end }) {
      if (this.areasLoading || this.areas.length <= this.areasBuffer.length) {
          return;
      }

      if (end + this.numberOfItemsFromEndBeforeFetchingMore >= this.areasBuffer.length) {
          this.fetchMore_Areas();
      }
  }

  private fetchMore_Areas() {
      const len = this.areasBuffer.length;
      const more = this.areas.slice(len, this.bufferSize + len);
      this.areasLoading = true;
      // using timeout here to simulate backend API delay
      setTimeout(() => {
          this.areasLoading = false;
          this.areasBuffer  = this.areasBuffer.concat(more);
      }, 200)
  }

}

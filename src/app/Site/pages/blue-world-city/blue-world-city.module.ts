import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// Plugins
import { ShareModule } from '@ngx-share/core';
import { OwlModule } from 'ngx-owl-carousel';
import { ShareButtonsModule } from '@ngx-share/buttons';
// import { ScrollSpyDirective } from '../../../services/_services/scroll-spy.directive';
// import { ScrollSpyDirective2 } from '../../../services/scroll-spy.directive2';

// import Scroll Spy
import { ScrollSpyModule } from '../../../services/_services/scroll-spy/scroll-spy.module';

// Site Includes
import { SiteIncludesModule } from '../../includes/site-includes.module';

// Pages
// import { BlueWorldCityComponent } from './blue-world-city.component';
import { BlueWorldCityRoutingModule } from './blue-world-city-routing.module';


@NgModule({
  declarations: [
    // BlueWorldCityComponent,
  ],
  imports: [
    CommonModule,
    BlueWorldCityRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    ShareModule,
    OwlModule,
    ShareButtonsModule,
    ScrollSpyModule,
    SiteIncludesModule
  ]
})
export class BlueWorldCityModule { }

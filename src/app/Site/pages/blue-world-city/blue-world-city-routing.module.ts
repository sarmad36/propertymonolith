import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Pages
import { BlueWorldCityComponent } from './blue-world-city.component';

const routes: Routes = [
  { path: '',                                  component: BlueWorldCityComponent },
  { path: 'success',                           component: BlueWorldCityComponent },
  // { path: 'blue-world-city/hot-offer',                        component: HotOfferComponent },
];

// Set Routes
// export const BlueWorldCityRoutingModule = routes;

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BlueWorldCityRoutingModule { }

import { Component, OnInit, ElementRef, ViewChild, HostListener, ViewChildren, QueryList, Renderer2, Host, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Location } from '@angular/common';
// Import Local Services
import { ActiveNavService, HelperService, AppSettings, SEOService } from '../../../services/_services';

// // TawkTo Components
// import { TawkToComponent } from '../../includes/tawk-to/tawk-to.component';

import Swal from 'sweetalert2';

declare var $, window: any;

@Component({
  selector: 'app-blue-world-city',
  templateUrl: './blue-world-city.component.html',
  styleUrls: ['./blue-world-city.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class BlueWorldCityComponent implements OnInit {

  @ViewChildren('appTawkTo', {read: ElementRef}) appTawkTo: QueryList<ElementRef>;
  public Tawk_To_ID             : any  = '5d9d99f2db28311764d8009b';
  public showTawkTo             : any  = false;
  public displayTimerFooter     : any  = false;
  public openForm               : any  = false;
  public leadForm               : any  = false;

  @ViewChild('myElem', {static: false}) MyProp: ElementRef;
  @ViewChild('section_af', {static: false}) section_af: ElementRef;
  // @ViewChild('section_Mob_btns', {static: false}) section_Mob_btns: ElementRef;

  @ViewChild('overview', { static: false })  overview: ElementRef;
  @ViewChild('overviewss', { static: false })  overviewss: ElementRef;
  @ViewChild('developer', { static: false })  developer: ElementRef;
  @ViewChild('features', { static: false })  features: ElementRef;
  @ViewChild('propertyType', { static: false })  propertyType: ElementRef;
  @ViewChild('location', { static: false })  location: ElementRef;
  @ViewChild('paymentPlan', { static: false })  paymentPlan: ElementRef;
  @ViewChild('bookingProcess', { static: false })  bookingProcess: ElementRef;
  @ViewChild('noc', { static: false })  noc: ElementRef;
  @ViewChild('contact', { static: false })  contact: ElementRef;
  @ViewChild('faq', { static: false })  faq: ElementRef;
  @ViewChild('leadFormView', { static: false })  leadFormView: ElementRef;


  public readmore         : any  = true;
  public activeMobileNav  : any;
  public sideopened       : any = true;

  public lead             : any = { name: '', phone: '',email: '', interest: '', paymentPlan:'', source : 'Website', project_id: 1, lead_type: 'email'};
  public LeadForm         : FormGroup;
  public submitted        : any = false;
  public ajaxloader       : any = false;
  public ShowAgentNum     : any = false;

  public whatsapLead             : any = {project_id: 1, type: 'whatsapp'};

  public projectLead             : any = {project_id: 1, type: 'project'};

  public phoneClicks             : any = {project_id: 1, type: 'phone'};

  public MobBtns          : any = false;

  public listDisplay       : any = true;
  public listDisplay2      : any = true;
  public listDisplay3      : any = true;
  public listDisplay4      : any = true;
  public stickNavbar       : any = false;
  public stickmobBtns      : any = false;
  public faqAccordiansMore : any = true;
  public developerMore     : any = true;
  public allReadMore       : any = true;
  public allReadAwami      : any = true;
  public hasErrorPhn       : any = true;
  public phnNumber         : any;
  public leadEmail         : any;

  public spiedTags      = ['DIV'];
  public currentSection = '';
  public addPad         = false;

  public monthsText :any = "0";
  public daysText   :any = "0";
  public hoursText  :any = "0";
  public minsText   :any = "0";
  public secText    :any = "0";

  public monthsTextPay :any = "0";
  public daysTextPay   :any = "0";
  public hoursTextPay  :any = "0";
  public minsTextPay   :any = "0";
  public secTextPay    :any = "0";

  public  screenOffsetTop : any = 0 ;

  public  contactReadMore : any = true ;
  public  faqReadMore : any = true;
  public  nocReadMore : any = true;
  public  bookingReadMore : any = true;
  public  developerReadMore : any = true;

  public showFillForm       : any = false;
  public hoverEffectOver    : any = false;
  public owlNumberActive    : any;
  public totalNumberImages  : any;
  public owlOpen            : any;
  public owlChecker         : any = true;
  public BigowlChecker      : any = true;

  public reportedProp       : any = false;

  public showCarouselModal  : any = false;

  // Subscriptions
  public routerSubscription : any;

  public projectViewOpen    : any = false;
  public dailyCounts        : any;

  public desktopbanner      : any = true;
  public farmhouses         : any = false;
  public overseasPayment    : any = false;
  public mapActive          : any = false;
  public accessActive       : any = false;
  public shareModal         : any = false;

  constructor(
      private router           : Router,
      private activeNavService : ActiveNavService,
      private helperService    : HelperService,
      private formBuilder      : FormBuilder,
      private renderer         : Renderer2,
      private host             : ElementRef,
      private seoService       : SEOService,
      private _location        : Location,
      private cdr              : ChangeDetectorRef
     ) {

      this.routerSubscription = this.router.events.subscribe((val) => {
          if(val instanceof NavigationEnd) {
             const tree = router.parseUrl(router.url);
             // console.log("tree", tree);
              if (tree.fragment) {
                // console.log("tree.fragment", tree.fragment);
                const element = document.querySelector("#" + tree.fragment + '-section');
                if (element) { element.scrollIntoView(true); window.scrollBy(0, -50); }
              }
           }
        });
        this.setSEO();
  }

  @HostListener('window:scroll', ['$event']) track(event) {

      if (window.pageYOffset >= 750) {
          this.projectViewOpen = true;
      }
      else{
        this.projectViewOpen = false;
      }

      if(window.pageYOffset >= this.section_af.nativeElement.offsetTop - 300) {
        this.stickNavbar = true;
      } else {
        this.stickNavbar = false;
      }

      if (typeof window != "undefined") {
          if (window.innerWidth < 768){
              this.screenOffsetTop = 450;
          }
          else if (window.innerWidth < 1300 && window.innerWidth > 768){
              this.screenOffsetTop = 580;
          }

          else{
              this.screenOffsetTop = 750;
          }
      }

       if(window.pageYOffset <=  this.screenOffsetTop) {
         window.history.replaceState({}, '',`/blue-world-city`);
         this.currentSection = "";
       }

      // console.log("this.overviewss.nativeElement", this.overviewss.nativeElement);
      // console.log("this.overviewss.nativeElement.offsetTop", this.overviewss.nativeElement.offsetTop);
  }


  ngOnInit() {

    // if(typeof Pace != "undefined") {
    //   Pace.options.ajax.trackWebSockets = false;
    //   Pace.options.restartOnPushState = false;
    // }

    // Set FormGroup
    this.LeadForm = this.formBuilder.group({
      name     : [null, Validators.required],
      email    : [null, [Validators.required, Validators.email]],
      phone    : [null, Validators.required],
      interest : [null, Validators.required],
    });

    if (window && window.FB) {
      if (typeof window.FB.XFBML != "undefined") {
          setTimeout(function(){
            window.FB.XFBML.parse();
        }, 100);
      }
    }
  }

  ngAfterViewInit() {
    if (typeof window != "undefined") {
        if (window.innerWidth < 768){
          this.desktopbanner =false;
          this.cdr.detectChanges();
          this.mobOwlCarousal();

          this.deferParseImages();
        }
        else{
          this.deferParseImages();
        }
    }
  }

  mobOwlCarousal(){
    if(typeof $ != "undefined") {
      $(".projMobile div:not(.cloned) .fancybox").fancybox({
           buttons: [
             "zoom",
             "share",
             "slideShow",
             "fullScreen",
             "download",
             "thumbs",
             "close"
           ],
           iframe : {
            preload : false
           }
       });
       $('.projMobile').owlCarousel({
         loop:true,
         margin:0,
         nav:true,
         dots:false,
         lazyLoad:true,
         responsive:{
             0:{
                 items:1
             },
             600:{
                 items:1
             },
             1000:{
                 items:1
             }
         }
     });
        var owl = $('.owl-carousel.projMobile');
     owl.owlCarousel();
     // Listen to owl events:
      owl.on('changed.owl.carousel', function(event) {

        if ( $('ul.owl-custom-dots li.active').is(':last-child')) {
            $('.owl-custom-dots li:last-child').removeClass('active');
            $('.owl-custom-dots li:first-child').addClass('active');
        }
        else{
          $('.owl-custom-dots li.active').addClass('prev');
          $('.owl-custom-dots li.active').next('li').addClass('active');

          $('.owl-custom-dots li.prev.active').remove();
          $("ul.owl-custom-dots").prepend('<li></li>');
        }
      });
    }
  }

  openProjectCarouselModal() {
    if(!this.showCarouselModal) {
        this.showCarouselModal = true;

        let self = this;
        setTimeout(function(){
          self.deferParseImages();

          // Initiate Owl Carousel
          self.initOwlCarousel();

          // Show CarouselModal
          $('#ProjectCarouselModal').modal('show');
        }, 100);
    } else {
        // Show CarouselModal
        $('#ProjectCarouselModal').modal('show');
    }
  }

  deferParseImages(){
    if (typeof document != "undefined") {
      var imgDefer = document.getElementsByTagName('img');
      for (var i=0; i<imgDefer.length; i++) {
        if(imgDefer[i].getAttribute('data-src')) {
          imgDefer[i].setAttribute('src',imgDefer[i].getAttribute('data-src'));
        }
      }
      var picDefer = document.getElementsByTagName('source');
      for (var i=0; i<picDefer.length; i++) {
        if(picDefer[i].getAttribute('data-src')) {
          picDefer[i].setAttribute('srcset',picDefer[i].getAttribute('data-src'));
        }
      }
    }
  }

  initOwlCarousel() {
    let self = this;

    if(typeof $ != "undefined") {

     // Set Owl Carousel
      $(document).ready(function() {
          var bigimage = $("#big");
          var thumbs = $("#thumbs");
          //var totalslides = 10;
          var syncedSecondary = true;

          bigimage
            .owlCarousel({
            items: 1,
            slideSpeed: 5000,
            nav: true,
            autoplay: false,
            dots: false,
            loop: false,
            responsiveRefreshRate: 200,
            // navText: [
            //   '<i class="fa fa-arrow-left" aria-hidden="true"></i>',
            //   '<i class="fa fa-arrow-right" aria-hidden="true"></i>'
            // ]
          })
            .on("changed.owl.carousel", syncPosition);

          thumbs
            .on("initialized.owl.carousel", function() {
            thumbs
              .find(".owl-item")
              .eq(0)
              .addClass("current");
          })
            .owlCarousel({
            dots: false,
            nav: false,
            navText: [
              '<i class="fa fa-arrow-left" aria-hidden="true"></i>',
              '<i class="fa fa-arrow-right" aria-hidden="true"></i>'
            ],
            smartSpeed: 200,
            slideSpeed: 500,
            slideBy: 4,
            responsiveRefreshRate: 100,
            responsive: {
              0: {
                items: 4
              },
              767: {
                items: 8
              },

              1000: {
                items: 11
              },
              1200: {
                items: 4
              }
            }
          })
            .on("changed.owl.carousel", syncPosition2);

          function syncPosition(el) {
            //if loop is set to false, then you have to uncomment the next line
            var current;
            if (self.owlChecker == false) {
                 current = self.owlOpen;
                self.owlChecker = true;

                self.cdr.detectChanges();
            }
            else{
               current = el.item.index;
            }
            // this.owlNumberActive = current + 1;
            self.owlNumberActive = current + 1;
            self.cdr.detectChanges();

            if (self.owlNumberActive == 1) {
                $('#bannerVideo').get(0).play();
                $('#bannerVideo1').get(0).pause();
                $('#bannerVideo2').get(0).pause();
            }
            else if(self.owlNumberActive == 4){
              // var vidDefer = document.getElementById('bannerVideo1');
                // var vidDefer = $('#bannerVideo1 source');
                //
                // if($(vidDefer).attr('data-src')) {
                //   $(vidDefer).attr('src', $(vidDefer).attr('data-src'));
                // }
                //
                // console.log("vidDefer: ",vidDefer);

                $('#bannerVideo').get(0).pause();
                $('#bannerVideo2').get(0).pause();
                $('#bannerVideo1').get(0).play();
            }
            else if(self.owlNumberActive == 5){
                $('#bannerVideo').get(0).pause();
                $('#bannerVideo1').get(0).pause();
                $('#bannerVideo2').get(0).play();
            }
            else{
              $('#bannerVideo').get(0).pause();
              $('#bannerVideo1').get(0).pause();
              $('#bannerVideo2').get(0).pause();
            }
            self.totalNumberImages = el.item.count;
            self.cdr.detectChanges();

            //to disable loop, comment this block
            // var count = el.item.count - 1;
            // var current = Math.round(el.item.index - el.item.count / 2 - 0.5);
            //
            // if (current < 0) {
            //   current = count;
            // }
            // if (current > count) {
            //   current = 0;
            // }
            //to this
            thumbs
              .find(".owl-item")
              .removeClass("current")
              .eq(current)
              .addClass("current");
            var onscreen = thumbs.find(".owl-item.active").length - 1;
            var start = thumbs
            .find(".owl-item.active")
            .first()
            .index();
            var end = thumbs
            .find(".owl-item.active")
            .last()
            .index();

            if (current > end) {
              thumbs.data("owl.carousel").to(current, 100, true);
            }
            if (current < start) {
              thumbs.data("owl.carousel").to(current - onscreen, 100, true);
            }
          }

          function syncPosition2(el) {
            if (syncedSecondary) {
              var number;
              if (self.BigowlChecker == false) {
                 number = self.owlOpen;
                self.BigowlChecker = true;
                self.cdr.detectChanges();
              }
              else{
                 number = el.item.index;
              }
              bigimage.data("owl.carousel").to(number, 100, true);
            }
          }

          thumbs.on("click", ".owl-item", function(e) {
            e.preventDefault();
            var number = $(this).index();
            bigimage.data("owl.carousel").to(number, 300, true);
          });
        });
    }
  }

  changeOwlActivePosition(param){
    this.owlOpen       = param;
    this.owlChecker    = false;
    this.BigowlChecker = false;
    this.cdr.detectChanges();
  }

  pauseVideo() {
      if (typeof $ != "undefined") {
          $('#bannerVideo').get(0).pause();
          $('#bannerVideo1').get(0).pause();
          $('#bannerVideo2').get(0).pause();
      }
  }

  ngAfterContentInit(): void {
    //Called after ngOnInit when the component's or directive's content has been initialized.
    //Add 'implements AfterContentInit' to the class.

    let self = this;
    setTimeout(function() {
      // Initialize Tawk-To
      self.showTawkTo = true;
      self.cdr.detectChanges();
    }, 3000);

    if(typeof $ != "undefined") {
      if (typeof window != "undefined") {
          if (window.innerWidth > 768){
            $(document).ready(function() {
               $(".main-table").clone(true).appendTo('#table-scroll').addClass('clone');
             });
             $(document).ready(function() {
              $(".main-table1").clone(true).appendTo('#table-scroll1').addClass('clone');
            });
          }
      }


      function setInputFilter(textbox, inputFilter) {
        ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"].forEach(function(event) {
          textbox.addEventListener(event, function() {
            if (inputFilter(this.value)) {
              this.oldValue = this.value;
              this.oldSelectionStart = this.selectionStart;
              this.oldSelectionEnd = this.selectionEnd;
              this.cdr.detectChanges();
            } else if (this.hasOwnProperty("oldValue")) {
              this.value = this.oldValue;
              this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
              this.cdr.detectChanges();
            }
          });
        });
      }

      // Install input filters.
      setInputFilter(document.getElementById("phoneNum"), function(value) {
        return /^[\d -]*$/.test(value);
      });
    }
    this.updateCounts();
  }

  ngOnDestroy() {
    // if(typeof Pace != "undefined") {
    //   Pace.options.ajax.trackWebSockets = true;
    //   Pace.options.restartOnPushState   = true;
    // }

    // // Remove Tawk-To
    // this.showTawkTo = false;
    // this.renderer.removeChild(this.host.nativeElement, this.appTawkTo.first.nativeElement);
    // console.log(this.appTawkTo);
    // console.log(this.appTawkTo.length);
    //
    // console.log("this.showTawkTo: ",this.showTawkTo)

    if (typeof this.routerSubscription != 'undefined') {
      //prevent memory leak when component destroyed
       this.routerSubscription.unsubscribe();
    }
  }
  updateCounts() {
    let url  = AppSettings.API_ENDPOINT + 'project-stats/update';
    this.helperService.httpPostRequests(url, this.projectLead).then(resp => {
        //console.log("resp: ",resp);
        if (typeof resp != "undefined" && resp != null && typeof resp.error === "undefined") {
          // this.projectLead = {project_id: 1, type: 'project'};
          // console.log("counts", resp);
          this.dailyCounts = resp.stats;
          //Swal.fire('Request Sent', 'Your message has been sent successfully. We will get back to you shortly.', 'success');
          // Set Submission false
        } else {
            console.error(resp);
        }

        this.ajaxloader = false;
        this.cdr.detectChanges();
    }).catch(error => {
        console.log("error: ",error);
        this.ajaxloader = false;
        // Set Submission false
        this.submitted = false;
        this.cdr.detectChanges();
    });
  }
@ViewChild('widgetsContent', { static: false }) public widgetsContent: ElementRef<any>;

public scrollHorizontal(param): void {
  if (param == "left") {
    this.widgetsContent.nativeElement.scrollTo({ left: (this.widgetsContent.nativeElement.scrollLeft - 200), behavior: 'smooth' });
  }
  else{
    this.widgetsContent.nativeElement.scrollTo({ left: (this.widgetsContent.nativeElement.scrollLeft + 200), behavior: 'smooth' });
  }

  this.cdr.detectChanges();
}


onMobSectionChange(sectionId: string) {
   if (typeof sectionId != "undefined") {
      this.currentSection = sectionId.split('-')[0];
         // console.log("this.currentSection", this.currentSection);
      switch (this.currentSection) {
          case "overview":
            this.overview.nativeElement.scrollIntoView();
           window.history.replaceState({}, '',`/blue-world-city#overview`);
            break;

          case "feature":
            this.features.nativeElement.scrollIntoView();
            window.history.replaceState({}, '',`/blue-world-city#feature`);
            break;

          case "location":
            this.location.nativeElement.scrollIntoView();
            window.history.replaceState({}, '',`/blue-world-city#location`);
            break;

          case "propertyType":
            this.propertyType.nativeElement.scrollIntoView();
            window.history.replaceState({}, '',`/blue-world-city#propertyType`);
            break;

          case "paymentPlan":
            this.paymentPlan.nativeElement.scrollIntoView();
            window.history.replaceState({}, '',`/blue-world-city#paymentPlan`);
            break;

          case "bookingProcess":
            this.bookingProcess.nativeElement.scrollIntoView();
            window.history.replaceState({}, '',`/blue-world-city#bookingProcess`);
            break;

          case "developer":
            this.developer.nativeElement.scrollIntoView();
            window.history.replaceState({}, '',`/blue-world-city#developer`);
            break;

          case "noc":
            this.noc.nativeElement.scrollIntoView();
            window.history.replaceState({}, '',`/blue-world-city#noc`);
            break;

            case "faq":
            this.faq.nativeElement.scrollIntoView();
            window.history.replaceState({}, '',`/blue-world-city#faq`);
            break;

          case "contact":
            this.contact.nativeElement.scrollIntoView();
            window.history.replaceState({}, '',`/blue-world-city#contact`);
            break;

          default:
            break;
        }
  }
}


viewReadmore(){
    this.readmore = !this.readmore;
    if (this.readmore) {
        this.scrollTo('myElem');
    }
}


scrollTo(section) {
  // console.log('$("#" + '+section+' )',  $("#" + section + '-section'))
  var x = $("#" + section + '-section').offset();
  window.scrollTo({ top: x.top - 50, behavior: 'smooth'});

}

openSideForm(){
  this.openForm = !this.openForm;
  this.sideopened = !this.sideopened;
  this.leadForm = !this.leadForm;
  $('body').toggleClass("overflow-hidden");
  // $('.main-content').toggleClass("overflow-hidden");
  window.scrollTo(0, 0);
  // $('#formName').focus();
  this.projectViewOpen = true;
}

textclick(){
  this.sideopened = !this.sideopened;
  // $('#formName').focus();
  this.leadForm = !this.leadForm;
  this.leadFormView.nativeElement.scrollIntoView();
  $('body').toggleClass("overflow-hidden");
}

closeOverlay(){
  this.openForm = false;
  this.leadForm = !this.leadForm;
  this.sideopened = !this.sideopened;
  $('body').removeClass("overflow-hidden");
}
closePopup(){
  $('body').removeClass("overflow-hidden");
}

 showPhoneNum(){
   this.ShowAgentNum = true;
 }
 phoneClick(){
   let url  = AppSettings.API_ENDPOINT + 'project-stats/update';
   this.helperService.httpPostRequests(url, this.phoneClicks)
     .then(resp => {
       this.ajaxloader = false;
       if (typeof resp != "undefined" && resp != null && typeof resp.error === "undefined") {
         this.phoneClicks = {project_id: 1, type: 'phone'};
         window.history.replaceState({}, '',`/blue-world-city/success`);
       } else {
           console.error(resp);
       }
     })
     .catch(error => {
       console.log("error: ",error);
       this.ajaxloader = false;
       // Set Submission false
       this.submitted = false;
     });
 }

get f() { return this.LeadForm.controls; }

AddLead() {
  this.submitted        = true;
  if (this.LeadForm.invalid) {
   return;
  }

  this.ajaxloader = true;
  let url  = AppSettings.API_ENDPOINT + 'projects/add-lead';
  // this.lead.phone = this.lead.countrycode + this.lead.phone;
  // this.lead.phone = this.phnNumber;
  this.helperService.httpPostRequests(url, this.lead)
    .then(resp => {
      //console.log("resp: ",resp);
      this.ajaxloader = false;
      if (typeof resp != "undefined" && resp != null && typeof resp.error === "undefined") {
        this.lead = { name: '', phone: '',email: '', interest: '',paymentPlan:'', source : 'Website', project_id: 1, lead_type: 'email'};
        // let routeLink = "blue-world-city/success";
        // this.router.navigate([routeLink]);
        Swal.fire('Request Sent', 'Your message has been sent successfully. We will get back to you shortly.', 'success');
        window.history.replaceState({}, '',`/blue-world-city/success`);
        this.leadForm = false;
        this.openForm = false;
        $('body').removeClass("overflow-hidden");

        // Set Submission false
        this.submitted = false;
      } else {
          console.error(resp);
      }
    })
    .catch(error => {
      console.log("error: ",error);
      this.ajaxloader = false;
      // Set Submission false
      this.submitted = false;
    });
}
openMoreFaq(){
  this.faqAccordiansMore = !this.faqAccordiansMore;
}
openMoredeveloper(){
  this.developerMore = !this.developerMore;
}
listDisplayFunc(param){
  switch (param) {
    case "mainFeature":
      this.listDisplay = !this.listDisplay;
      if (this.listDisplay) {
        this.scrollTo('featuresHead');
      }
    break;
    case "plotFeature":
      this.listDisplay2 = !this.listDisplay2;
    break;

    case "businessFeature":
      this.listDisplay3 = !this.listDisplay3;
    break;

  case "nearbyFeature":
    this.listDisplay4 = !this.listDisplay4;
  break;

    default:

    break;
  }
}
openMoreabout(param){
  switch (param) {
    case "AllAboutAwami":
    this.allReadAwami = !this.allReadAwami;
    break;
    case "AllAboutBwc":
      this.allReadMore = !this.allReadMore;
    break;
    case "contact":
    this.contactReadMore = !this.contactReadMore;
    break;
    case "faqs":
    this.faqReadMore = !this.faqReadMore;
    break;

    case "noc":
    this.nocReadMore = !this.nocReadMore;
    break;

    case "bookingProcess":
    this.bookingReadMore = !this.bookingReadMore;
    break;
    case "developer":
    this.developerReadMore = !this.developerReadMore;
    break;
    default:

    break;
  }
}
openWhatsapp() {
  if(typeof window != "undefined"){
    window.open("https://api.whatsapp.com/send?phone=923411100808&text=Hi%2C%20I%20would%20like%20to%20inquire%20about%20your%20project%20Blue%20World%20City.%20Please%20contact%20me%20back%20at%20your%20earliest.%20Thanks", "_blank");
  }
}
whatsappLeadFunc(){
  let url  = AppSettings.API_ENDPOINT + 'project-stats/update';
  this.helperService.httpPostRequests(url, this.whatsapLead)
    .then(resp => {
      //console.log("resp: ",resp);
      this.ajaxloader = false;
      if (typeof resp != "undefined" && resp != null && typeof resp.error === "undefined") {
        this.whatsapLead = {project_id: 1, type: 'whatsapp'};
        window.history.replaceState({}, '',`/blue-world-city/success`);
        //Swal.fire('Request Sent', 'Your message has been sent successfully. We will get back to you shortly.', 'success');
        // Set Submission false
      } else {
          console.error(resp);
      }
    })
    .catch(error => {
      console.log("error: ",error);
      this.ajaxloader = false;
      // Set Submission false
      this.submitted = false;
    });
}
contactHighlight(){
  this.leadFormView.nativeElement.scrollIntoView();
    $('body').toggleClass("overflow-hidden");
    this.leadForm = !this.leadForm;
}
hoverEffect(param){
  if(param == "in"){
    this.hoverEffectOver = true;
  }
  else{
    this.hoverEffectOver = false;
  }
}
ReportProject(){
  let url  = AppSettings.API_ENDPOINT + 'projects/report';
  let data = { project_id: this.lead.project_id };
  this.helperService.httpPostRequests(url, data).then(resp => {
    console.log("resp: ", resp);
    this.reportedProp = true;

    this.cdr.detectChanges();
  })
}

openPills(pillName){
    if (pillName == 'farm') {
        this.farmhouses = true;
    }
    else if(pillName == 'overseas'){
        this.overseasPayment = true;
    }
    else if(pillName == 'map'){
        this.mapActive = true;
    }
    else if(pillName == 'access'){
        this.accessActive = true;
    }
    else{
      this.farmhouses       = false;
      this.overseasPayment  = false;
      this.accessActive     = false;
      this.mapActive        = false;
    }
}
showShareModal(){
  this.shareModal = true;
}
setSEO() {

    let image = this._location['_platformLocation'].protocol + '//' + this._location['_platformLocation'].hostname + "/assets/site/images/blue_world_city_logo.jpg";
    // console.log("image",image)
    // Set Title & Metatag for property

    // let title =   'Blue World City Islamabad | Plots For Sale | Gharbaar.com';
    let title =   'Blue World City | Plots For Sale | Gharbaar.com';

    let desc  = "All about Blue World City. Project Details, NOC, Location, Map, Prices and more at Pakistan's best property portal | Gharbaar.com";
    // let desc  = "All about Blue World City Islamabad. Project Details, NOC, Location, Map, Prices and more at Pakistan's best property portal | Gharbaar.com";

    // Set description lengt6h
    if(desc.length > 160)
      desc = desc.substring(0, 160);

    // Set Basic Meta Tags
      this.seoService.updateTitle(title);
      this.seoService.updateDescription(desc);

    // Set Og Meta tags
      this.seoService.updateOgTitle(title);
      this.seoService.updateOgDesc(desc);
      this.seoService.updateOgUrl();
      this.seoService.updateOgImage(image);

    // Set Twitter Meta Tags
      this.seoService.updateTwitterTitle(title);
      this.seoService.updateTwitterDesc(desc);

    // Set Canonical Tag
      this.seoService.updateCanonicalUrl();
  }

}

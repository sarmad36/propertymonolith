import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BlueWorldCityComponent } from './blue-world-city.component';

describe('BlueWorldCityComponent', () => {
  let component: BlueWorldCityComponent;
  let fixture: ComponentFixture<BlueWorldCityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BlueWorldCityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlueWorldCityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

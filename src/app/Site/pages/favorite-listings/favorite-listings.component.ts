import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { AppSettings, HelperService, SEOService, PurposeService, FavoriteService, HideService } from '../../../services/_services';
import { BsModalRef, BsModalService, ModalOptions } from 'ngx-bootstrap/modal';
import { ContactDetailsComponent } from '../../entry-components/contact-details/contact-details.component';
import { InquiryModalComponent } from '../../entry-components/inquiry-modal/inquiry-modal.component';

@Component({
  selector: 'app-favorite-listings',
  templateUrl: './favorite-listings.component.html',
  styleUrls: ['./favorite-listings.component.css']
})
export class FavoriteListingsComponent implements OnInit {

  public ListingsData         :any = [];
  public ImagesUrl            :any = AppSettings.IMG_ENDPOINT;
  public listView             :any = true;
  public propPackages         :any = {};
  public searchAreas          :any = {};
  public searchUnits          :any = {};
  public favoriteSub          :any;
  public modalRef             :BsModalRef;
  constructor(
    private helperService         : HelperService,
    private seoService            : SEOService,
    private _location             : Location,
    private favoriteService       : FavoriteService,
    private hideService           : HideService,
    private modalService          : BsModalService,
  ) {
      // Get Globals from AppSettings
      this.searchAreas     = AppSettings.searchAreas;
      this.searchUnits     = AppSettings.searchUnits;
      this.propPackages    = AppSettings.propPackages;

      // Get Favorited Listings
      this.favoriteSub = this.favoriteService.favoritesS$.subscribe(resp => {
        this.ListingsData = this.favoriteService.getfavorites_array();
        console.log("this.ListingsData: ",this.ListingsData)
      });
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    if (typeof this.favoriteSub != 'undefined') {
      //prevent memory leak when component destroyed
      this.favoriteSub.unsubscribe();
    }
  }

  convertToInt(val) {
    return parseInt(val);
  }

  showContactText(property) {
    if(typeof property.contacts != 'undefined')
      return property.contacts[0].phone_number;
    else
      return "View Number";
  }

  checkNumbers(property) {
    let check = false;
    if(typeof property.contacts != 'undefined' && property.contacts.length > 1)
      check = true;
    else
      check = false;

    return check;
  }

  toggle2ndNum(property, index) {
    if(typeof this.ListingsData.data[index].contacts[1].showNum == 'undefined')
      this.ListingsData.data[index].contacts[1].showNum = false;

    // Toggle View of 2nd Number
    this.ListingsData.data[index].contacts[1].showNum = !this.ListingsData.data[index].contacts[1].showNum;
  }

  setListView(bool){
    this.listView = bool;
  }

  propertyRoute(property) {
    return this.helperService.propertyRoute(property);
  }

  checkPropFavrt(id) {
    return this.favoriteService.getFavoritePropByIndex(id);
  }

  togglefavoriteProp(property) {
    property.id = property.p_id;
    if(this.checkPropFavrt(property.id))
      this.favoriteService.removefavoriteProp(property.id);
    else
      this.favoriteService.addfavoriteProp(property);
  }

  checkPropHidden(id) {
    return this.hideService.getHiddenPropByIndex(id);
  }

  toggleHiddenProp(property) {
    property.id = property.p_id;
    if(this.checkPropHidden(property.id))
      this.hideService.removeHiddenProp(property.id);
    else
      this.hideService.addHiddenProp(property);
  }
  open_ContactDetails_Modal(property) {
    const initialState = { property: property };
    const config: ModalOptions = { class: 'modal-sm phone-number', initialState };
    this.modalRef = this.modalService.show(ContactDetailsComponent, config);
  }
  open_Inquiry_Modal(property) {
    const initialState = { property: property };
    const config: ModalOptions = { class: 'modal-md af-signup inquiry', initialState };
    this.modalRef = this.modalService.show(InquiryModalComponent, config);
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FavoriteListingsComponent } from './favorite-listings.component';

describe('FavoriteListingsComponent', () => {
  let component: FavoriteListingsComponent;
  let fixture: ComponentFixture<FavoriteListingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FavoriteListingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FavoriteListingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HiddenListingsComponent } from './hidden-listings.component';

describe('HiddenListingsComponent', () => {
  let component: HiddenListingsComponent;
  let fixture: ComponentFixture<HiddenListingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HiddenListingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HiddenListingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

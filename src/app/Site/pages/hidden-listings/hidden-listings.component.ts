import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { AppSettings, HelperService, SEOService, PurposeService, FavoriteService, HideService } from '../../../services/_services';

@Component({
  selector: 'app-hidden-listings',
  templateUrl: './hidden-listings.component.html',
  styleUrls: ['./hidden-listings.component.css']
})
export class HiddenListingsComponent implements OnInit {

  public ListingsData         :any = [];
  public ImagesUrl            :any = AppSettings.IMG_ENDPOINT;
  public listView             :any = true;
  public propPackages         :any = {};
  public searchAreas          :any = {};
  public searchUnits          :any = {};
  public hiddenSub            :any;

  constructor(
    private helperService         : HelperService,
    private seoService            : SEOService,
    private _location             : Location,
    private favoriteService       : FavoriteService,
    private hideService           : HideService,
  ) {
      // Get Globals from AppSettings
      this.searchAreas     = AppSettings.searchAreas;
      this.searchUnits     = AppSettings.searchUnits;
      this.propPackages    = AppSettings.propPackages;

      // Get Favorited Listings
      this.hiddenSub = this.hideService.hiddenS$.subscribe(resp => {
        this.ListingsData = this.hideService.getHiddenProps_array();
        console.log("this.ListingsData: ",this.ListingsData)
      });
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    if (typeof this.hiddenSub != 'undefined') {
      //prevent memory leak when component destroyed
      this.hiddenSub.unsubscribe();
    }
  }

  convertToInt(val) {
    return parseInt(val);
  }

  showContactText(property) {
    if(typeof property.contacts != 'undefined')
      return property.contacts[0].phone_number;
    else
      return "View Number";
  }

  checkNumbers(property) {
    let check = false;
    if(typeof property.contacts != 'undefined' && property.contacts.length > 1)
      check = true;
    else
      check = false;

    return check;
  }

  toggle2ndNum(property, index) {
    if(typeof this.ListingsData.data[index].contacts[1].showNum == 'undefined')
      this.ListingsData.data[index].contacts[1].showNum = false;

    // Toggle View of 2nd Number
    this.ListingsData.data[index].contacts[1].showNum = !this.ListingsData.data[index].contacts[1].showNum;
  }

  setListView(bool){
    this.listView = bool;
  }
  
  propertyRoute(property) {
    return this.helperService.propertyRoute(property);
  }

  checkPropFavrt(id) {
    return this.favoriteService.getFavoritePropByIndex(id);
  }

  togglefavoriteProp(property) {
    property.id = property.p_id;
    if(this.checkPropFavrt(property.id))
      this.favoriteService.removefavoriteProp(property.id);
    else
      this.favoriteService.addfavoriteProp(property);
  }

  checkPropHidden(id) {
    return this.hideService.getHiddenPropByIndex(id);
  }

  toggleHiddenProp(property) {
    property.id = property.p_id;
    if(this.checkPropHidden(property.id))
      this.hideService.removeHiddenProp(property.id);
    else
      this.hideService.addHiddenProp(property);
  }

}

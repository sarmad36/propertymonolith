import { Component, OnInit, ViewChild ,ElementRef, HostListener  } from '@angular/core';
import { Location } from '@angular/common';
import { Router, ActivatedRoute } from "@angular/router";
import { AppSettings, HelperService, SEOService, PurposeService, FavoriteService, HideService, SaveSearchService, EmailBucketService } from '../../../services/_services';
import { FormControl } from '@angular/forms';
import { Subject } from 'rxjs';

// Bootsteap Components
import { BsModalRef, BsModalService, ModalOptions } from 'ngx-bootstrap/modal';

// Modal Components
import { ContactDetailsComponent } from '../../entry-components/contact-details/contact-details.component';
import { InquiryModalComponent } from '../../entry-components/inquiry-modal/inquiry-modal.component';

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.css']
})
export class ProjectsComponent implements OnInit {
@ViewChild('section_af', {static: false}) section_af: ElementRef;
  @ViewChild('hide_sticky', {static: false}) hide_sticky: ElementRef;

  // stickfilter Params
  public stickfilter    :any = false;
  public addPad         :any = false;

  // City and Areas List
  public cities          :any = AppSettings.cities;
  public areas           :any = [];
  public projects        :any = [];

  // Params
  public SelectedCity     = AppSettings.cities[0];
  public SelectedArea     = [];
  public SelectedProject  = [];
  public SelectedAgencies = [];
  public SelectedType     = AppSettings.agencyTypes[0].value;
  public parentLocs       = [];

  // Ng-Slect Params
  public areasBuffer  = [];
  public bufferSize   = 10;
  public numberOfItemsFromEndBeforeFetchingMore = 5;
  public areasLoading = false;
  public areaQuery    = "";
  public areasError   = false;

  // Agency Params
  public agencyNames     :any = [];
  public agencyLoading   :any = false;

  // Agent Types
  public types           :any = AppSettings.agencyTypes;

  // Https Params
  protected ngUnsubscribe: Subject<void> = new Subject<void>();
  public httpLoading     = false;
  public corsHeaders     : any;

  public areasSelect_F = new FormControl('');


  public showFeatures :any = false;
  public searchParms  :any = {
                                city         : { id: null, name: "" },
                                area         : [],
                                project      : [],
                                type         : "",
                                agencies     : [],

                                sort_by      : "type",
                                sort_type    : "DESC",
                                current_page : 1
                              };


  public searchAreas          :any = [];
  public AgenciesData         :any = [];
  public ListingsData         :any = [];
  public Listings_Available   :any = false;
  public pagination           :any = [];
  public locsByCity           :any = false;
  public loadingBreadCrumbs   :any = false;

  public SortBy               :any = "type-DESC";
  public ImagesUrl            :any = AppSettings.IMG_ENDPOINT;
  public listView             :any = false;

  public hideEmailBucket      :any = true;
  public modalRef             :BsModalRef;
  public innerSideFilter      :any = false;

  // Investment Projects Params
  public projectsLocs : any = AppSettings.projectsLocs;

  public ProjectCategories  : any = AppSettings.ProjectCategories;
  public housing            : any = true;
  public commercial         : any = true;
  public categoryValue      : any = 'all';

  constructor(
    private router                : Router,
    private activatedRoute        : ActivatedRoute,
    private helperService         : HelperService,
    private seoService            : SEOService,
    private purposeService        : PurposeService,
    private _location             : Location,
    private favoriteService       : FavoriteService,
    private hideService           : HideService,
    private emailBucketService    : EmailBucketService,
    private modalService          : BsModalService,

  ) {
    // Push All in Cities
    this.cities = [{"id": 0, "name" : "All"}, ...this.cities]

    this.activatedRoute.params.subscribe(routeParams => {
      let data = routeParams;

      // Get filter params from local storage
      if (typeof this.helperService.getAgencySearchParms() != "undefined" && this.helperService.getAgencySearchParms() != null) {
        if (typeof this.helperService.getAgencySearchParms().project != "undefined")
          this.SelectedProject = this.helperService.getAgencySearchParms().project;
      }

      if (typeof data.slug != "undefined") {
        let slug = data.slug.split("-");

        if(parseInt(slug[2]) == 1) {   // Search by City
            this.searchParms.city.id   = parseInt(slug[1]);
            this.searchParms.city.name = slug[0];
            this.searchParms.area      = [];
            this.projects              = AppSettings.projects[this.searchParms.city.id];

            // this.getSubLocationsByCity(this.searchParms.city.id);
            this.locsByCity = true;

            // // Get Search Areas
            // this.getSearchProjects();

            // // Get Agency Names
            // this.getAgencyNames();

            // // Get Property Listing
            // this.getListings();

            let self = this;
            setTimeout(function() {
              // Set SEO Tags
              self.setSEO_Tags("City");
            }, 500);

        } else if(parseInt(slug[2]) == 2) { // Search by Area
            // this.getParentLocations(parseInt(slug[1])).then(resp => {
            //   // Get Search Areas
            //   this.getSearchAreas();
            //
            //   // Get Agency Names
            //   this.getAgencyNames();
            //
            //   // Get Property Listing
            //   this.getListings();
            //
            //   // Set SEO Tags
            //   this.setSEO_Tags("Area");
            // })
        }
      }
    });
  }

  @HostListener('window:scroll', ['$event']) track(event) {
        if(window.pageYOffset >= 152) {
          this.stickfilter = true;
        }
        else if (window.pageYOffset < 153){
          this.stickfilter = false;
        }
        this.addPad = false;
    }

  ngOnInit() {
    // this.bindJQueryFuncs();
  }
  // bindJQueryFuncs() {
  //   if (typeof $ != "undefined") {
  //     $('.filterBtns .dropdown > button').on('click', function (event) {
  //         $('.filterBtns .dropdown').removeClass('open');
  //         $(this).parent().toggleClass('open');
  //     });
  //     $('body').on('click', function (e) {
  //         if (!$('.filterBtns .dropdown').is(e.target)
  //             && $('.filterBtns .dropdown').has(e.target).length === 0
  //             && $('.open').has(e.target).length === 0
  //         ) {
  //             $('.filterBtns .dropdown').removeClass('open');
  //         }
  //     });
  //   }
  // }
  setSEO_Tags(args: string) {
    // Set Title & Metatag for property
    if (args == "City") {
        let title = 'Find '+ this.searchParms.city.name +' Property Projects and Real Estate developments | Gharbaar.com';
        let desc = 'Find '+ this.searchParms.city.name +' Property Projects and Real Estate developments | Gharbaar.com';
        // let desc  = 'Find projects in '+this.searchParms.city.name +' on Gharbaar.com, Pakistan\'s No.1 Real Estate Portal. Get complete details of properties and available amenities.';

        // Set Basic Meta Tags
        this.seoService.updateTitle(title);
        this.seoService.updateDescription(desc);

        // Set Og Meta tags
        this.seoService.updateOgTitle(title);
        this.seoService.updateOgDesc(desc);
        this.seoService.updateOgUrl();

        // Set Twitter Meta Tags
        this.seoService.updateTwitterTitle(title);
        this.seoService.updateTwitterDesc(desc);

        // Set Canonical Tag
        this.seoService.updateCanonicalUrl();

    } else {
        let title = 'Projects in '+ this.searchParms.area.name +' '+ this.searchParms.city.name +' - Gharbaar.com';
        let desc  = 'Find projects in '+ this.searchParms.area.name +' '+this.searchParms.city.name +' on Gharbaar.com, Pakistan\'s No.1 Real Estate Portal. Get complete details of properties and available amenities.';

        // Set Basic Meta Tags
        this.seoService.updateTitle(title);
        this.seoService.updateDescription(desc);

        // Set Og Meta tags
        this.seoService.updateOgTitle(title);
        this.seoService.updateOgDesc(desc);
        this.seoService.updateOgUrl();

        // Set Twitter Meta Tags
        this.seoService.updateTwitterTitle(title);
        this.seoService.updateTwitterDesc(desc);

        // Set Canonical Tag
        this.seoService.updateCanonicalUrl();
    }
  }

  getSearchProjects() {
    let url  = AppSettings.API_ENDPOINT + 'projects/get-projects';
    this.helperService.httpGetRequests(url).then(resp => {
      //Set Projects
      this.projects       = resp;

      // Hide Area Loader
      this.areasLoading = false;

    }).catch(error => {
      console.error("error: ",error);
    });
  }

  selectCity(city) {
    // Reset Area Selects

    this.searchParms.city = city;
    this.areas        = [];
    this.areasBuffer  = [];
    this.searchParms.area = {
                                city_id   : null,
                                id        : null,
                                name      : "",
                                p_loc     : [],
                                parent_id : ""
                              };

    // Empty selected Agencies
    this.searchParms.agencies = [];

    // let selected = event.target.selectedOptions[0];
    // this.searchParms.city = { id: selected.value, name: selected.label}

    // Update Search
    // this.search();

    let routeLink =  this.helperService.investRoute(this.searchParms.city);
    this.router.navigate([routeLink]);
  }

  categoryFilter(category){

      if (category.value == '1') {
          this.housing = false;
          this.commercial = true;
          this.categoryValue = '1';
      }
      else if (category.value == '2') {
          this.housing = true;
          this.commercial = false;
          this.categoryValue = '2';
      }
      else{
        this.housing = true;
        this.commercial = true;
        this.categoryValue = 'all';
      }
  }

  openSideInner(){
    this.innerSideFilter = !this.innerSideFilter ;
    $("body").toggleClass("overflow-hidden");

    if (typeof window != "undefined") {
        if (window.innerWidth < 768){
            window.scrollTo(0, 0);
        }
    }
  }

  //////////////////////////////////////////
  /********* Areas Ng-Select Fns *********/
  ////////////////////////////////////////
  onScrollToEnd_Areas() {
      this.fetchMore_Areas();
  }

  onScroll_Areas({ end }) {
      if (this.areasLoading || this.areas.length <= this.areasBuffer.length) {
          return;
      }

      if (end + this.numberOfItemsFromEndBeforeFetchingMore >= this.areasBuffer.length) {
          this.fetchMore_Areas();
      }
  }

  private fetchMore_Areas() {
      const len = this.areasBuffer.length;
      const more = this.areas.slice(len, this.bufferSize + len);
      this.areasLoading = true;
      // using timeout here to simulate backend API delay
      setTimeout(() => {
          this.areasLoading = false;
          this.areasBuffer  = this.areasBuffer.concat(more);
      }, 200)
  }
}

import { Component, OnInit, ViewChild ,ElementRef, HostListener  } from '@angular/core';
import { Location } from '@angular/common';
import { Router, ActivatedRoute, NavigationEnd } from "@angular/router";
import { AppSettings, HelperService, SEOService, PurposeService, FavoriteService, HideService, SaveSearchService, EmailBucketService } from '../../../services/_services';
import { FormControl } from '@angular/forms';
import { Subject } from 'rxjs';

// Bootstrap Components
import { BsModalService, BsModalRef, ModalOptions } from 'ngx-bootstrap/modal';
import { ProContactDetailsComponent } from '../../entry-components/pro-contact-details/pro-contact-details.component';


@Component({
  selector: 'app-find-pro-results',
  templateUrl: './find-pro-results.component.html',
  styleUrls: ['./find-pro-results.component.css']
})
export class FindProResultsComponent implements OnInit {

  @ViewChild('section_af', {static: false}) section_af: ElementRef;
  @ViewChild('hide_sticky', {static: false}) hide_sticky: ElementRef;
  @ViewChild('section_map', {static: false}) section_map: ElementRef;
  @ViewChild('hide_map', {static: false}) hide_map: ElementRef;

  @ViewChild('propMap', {static: false}) mapElement: ElementRef;

  // Map Objects
  public prosMap          : any;
  public mapBounds        : any;
  public simpleMapUrl     : any = '';
  public showSimpleMap    : any = false;
  public infowindo        : any;
  public listingMarker    : any;
  public Location_LatLng  : any = [];
  public markerlatitudes  : any;
  public markersArray     : any = [];
  public infoWindowProperty : any = [];
  public labelPrice      :any = "";

  public maptoggler      : any = true;
  public stickmap        : any = false;
  public footeroffSet    : any = 0;
  public openLoc        : any = false;

  // Modal ref
  public modalRef      : BsModalRef;

  // stickfilter Params
  public stickfilter    :any = false;
  public addPad         :any = false;

  // City and Areas List
  public cities          :any = AppSettings.allCities;
  public areas           :any = [];
  public ProCategories   :any = AppSettings.ProCategories;

  // Params
  public SelectedCity     = AppSettings.allCities[0];
  public SelectedArea     = [];
  public SelectedAgencies = [];
  public SelectedType     = AppSettings.agencyTypes[0].value;
  public parentLocs       = [];

  // Ng-Slect Params
  public areasBuffer  = [];
  public bufferSize   = 10;
  public numberOfItemsFromEndBeforeFetchingMore = 5;
  public areasLoading = false;
  public areaQuery    = "";
  public areasError   = false;

  // Agency Params
  public agencyNames     :any = [];
  public agencyLoading   :any = false;

  // Agent Types
  public types           :any = AppSettings.agencyTypes;

  // Https Params
  protected ngUnsubscribe: Subject<void> = new Subject<void>();
  public httpLoading     = false;
  public corsHeaders     : any;

  public areasSelect_F = new FormControl('');


  public showFeatures :any = false;
  public searchParms  :any = {
                                city         : AppSettings.cities[1],
                                area         : [],
                                category     : '',
                                categoryName : '',
                                categorySlug : '',
                                type         : "",
                                agencies     : [],

                                sort_by      : "created_at",
                                sort_type    : "DESC",
                                current_page : 1
                              };


  public AgenciesData         :any = [];
  public ListingsData         :any = [];
  public Listings_Available   :any = false;
  public pagination           :any = [];
  public locsByCity           :any = false;
  public loadingBreadCrumbs   :any = false;

  public SortBy               :any = "type-DESC";
  public ImagesUrl            :any = AppSettings.IMG_ENDPOINT;
  public listView             :any = false;

  public hideEmailBucket      :any = true;
  public innerSideFilter      :any = false;

  public monthsText :any = "0";
  public daysText   :any = "0";
  public hoursText  :any = "0";
  public minsText   :any = "0";
  public secText    :any = "0";
  public agentRouteInfo  : any;
  // Subscriptions
  public routerSubscription : any;
  public categoryActive : any;
  public ShowAllCity  : any;
  constructor(
    private router                : Router,
    private activatedRoute        : ActivatedRoute,
    private helperService         : HelperService,
    private seoService            : SEOService,
    private purposeService        : PurposeService,
    private _location             : Location,
    private favoriteService       : FavoriteService,
    private hideService           : HideService,
    private emailBucketService    : EmailBucketService,
    private modalService           : BsModalService,
  ) {
    // console.log("this.activatedRoute: ",this.activatedRoute);
    this.routerSubscription = this.activatedRoute.params.subscribe(routeParams => {
      let data = routeParams;

      // Get filter params from local storage
      if (typeof this.helperService.getAgencySearchParms() != "undefined" && this.helperService.getAgencySearchParms() != null) {
        if (typeof this.helperService.getAgencySearchParms().type != "undefined")
          this.searchParms.type = this.helperService.getAgencySearchParms().type;

        if (typeof this.helperService.getAgencySearchParms().agencies != "undefined")
          this.searchParms.agencies = this.helperService.getAgencySearchParms().agencies;
      }

      // Set Category for search
      this.searchParms.categorySlug = data.category_slug;

      this.searchParms.category     = '';
      this.searchParms.categoryName = '';
      for (let i = 0; i < this.ProCategories.length; i++) {
          if(this.ProCategories[i].slug == this.searchParms.categorySlug) {
            this.searchParms.category     = this.ProCategories[i].value;
            this.searchParms.categoryName = this.ProCategories[i].name;

            if(this.searchParms.categoryName == 'Any')
              this.searchParms.categoryName = 'Professionals';
            break;
          }
      }

      if (typeof data.slug != "undefined") {
        let slug = this.helperService.render_FindPro_URL(data.slug);

        if(parseInt(slug.searchBy) == 1) {   // Search by City
            this.searchParms.city.id   = parseInt(slug.id);
            this.searchParms.city.name = slug.name;

            // this.searchParms.area      = [];

            // this.getSubLocationsByCity(this.searchParms.city.id);
            this.locsByCity = true;

            // Get Search Areas
            this.getSearchAreas();

            // // Get Agency Names
            // this.getAgencyNames();

            // Get Listing
            this.getListings();

            let self = this;
            setTimeout(function() {
              // Set SEO Tags
              self.setSEO_Tags("City");
            }, 200);

        } else if(parseInt(slug.searchBy) == 2) { // Search by Area
            this.getParentLocations(parseInt(slug.id)).then(resp => {
              // Get Search Areas
              this.getSearchAreas();

              // // Get Agency Names
              // this.getAgencyNames();

              // Get Listing
              this.getListings();

              // Set SEO Tags
              this.setSEO_Tags("Area");
            })
        }
        if (this.searchParms.city.name == 'All') {
            this.ShowAllCity = 'Islamabad & Rawalpindi'
        }
        else{
          this.ShowAllCity = this.searchParms.city.name;
        }
      }
    });
  }

  @HostListener('window:scroll', ['$event']) track(event) {

    if (typeof window != "undefined") {

      if (window.innerWidth < 1280 && window.innerWidth > 991){
          this.footeroffSet = 0;
      }
      else{
          this.footeroffSet = 0;
      }
      // console.log(window.pageYOffset);
      // console.log(this.section_map.nativeElement.offsetTop);
      // console.log(this.hide_map.nativeElement.offsetTop);
      if(window.pageYOffset >= this.section_map.nativeElement.offsetTop + 17 && window.pageYOffset < this.hide_map.nativeElement.offsetTop - this.footeroffSet) {
        this.stickmap = true;
        // console.log("if this.stickmap" ,this.stickmap)
      } else{
        this.stickmap = false;
        // console.log("else this.stickmap" ,this.stickmap)
      }

        if(window.pageYOffset >= 138) {
          this.stickfilter = true;
        }
        else if (window.pageYOffset < 138){
          this.stickfilter = false;
        }

        this.addPad = false;
    }
}

  ngOnInit() {
    // this.bindJQueryFuncs();
  }
  // bindJQueryFuncs() {
  //   if (typeof $ != "undefined") {
  //     $('.filterBtns .dropdown > button').on('click', function (event) {
  //         $('.filterBtns .dropdown').removeClass('open');
  //         $(this).parent().toggleClass('open');
  //     });
  //     $('body').on('click', function (e) {
  //         if (!$('.filterBtns .dropdown').is(e.target)
  //             && $('.filterBtns .dropdown').has(e.target).length === 0
  //             && $('.open').has(e.target).length === 0
  //         ) {
  //             $('.filterBtns .dropdown').removeClass('open');
  //         }
  //     });
  //   }
  // }
  ngAfterViewInit() {
    //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
    //Add 'implements AfterViewInit' to the class.
    // if (typeof window != "undefined") {
    //     if (window.innerWidth < 768){
    //         this.maptoggler = false;
    //     }
    // }
  }
  ngOnDestroy() {
    if (typeof this.routerSubscription != 'undefined') {
      //prevent memory leak when component destroyed
       this.routerSubscription.unsubscribe();
    }
  }

  setSEO_Tags(args: string) {
    // Set Title & Metatag for property
    if (args == "City") {
        let title = 'Search '+ this.searchParms.categoryName +' in '+ this.searchParms.city.name +' | Gharbaar.com';
        // let desc  = 'Find agents in '+this.searchParms.city.name +' on Gharbaar.com, Pakistan\'s No.1 Real Estate Portal. Get complete details of properties and available amenities.';
        let desc  = 'Gharbaar.com, the leading real estate resource for property buyers, sellers, landlords, tenants and real estate agents in Pakistan. Real-time residential, commercial, and investment listing updates. Gharbaar.com is the easy-to-use online portal that takes the hassle and uncertainty out of buying, selling, renting, or owning.';
        // Set Basic Meta Tags
        this.seoService.updateTitle(title);
        this.seoService.updateDescription(desc);

        // Set Og Meta tags
        this.seoService.updateOgTitle(title);
        this.seoService.updateOgDesc(desc);
        this.seoService.updateOgUrl();

        // Set Twitter Meta Tags
        this.seoService.updateTwitterTitle(title);
        this.seoService.updateTwitterDesc(desc);

        // Set Canonical Tag
        this.seoService.updateCanonicalUrl();

    } else {
        let title = 'Search '+ this.searchParms.categoryName +' in '+ this.searchParms.area.name +' '+ this.searchParms.city.name +' - Gharbaar.com';
        // let desc  = 'Find agents in '+ this.searchParms.area.name +' '+this.searchParms.city.name +' on Gharbaar.com, Pakistan\'s No.1 Real Estate Portal. Get complete details of properties and available amenities.';
        let desc  = 'Gharbaar.com, the leading real estate resource for property buyers, sellers, landlords, tenants and real estate agents in Pakistan. Real-time residential, commercial, and investment listing updates. Gharbaar.com is the easy-to-use online portal that takes the hassle and uncertainty out of buying, selling, renting, or owning.';
        // Set Basic Meta Tags
        this.seoService.updateTitle(title);
        this.seoService.updateDescription(desc);

        // Set Og Meta tags
        this.seoService.updateOgTitle(title);
        this.seoService.updateOgDesc(desc);
        this.seoService.updateOgUrl();

        // Set Twitter Meta Tags
        this.seoService.updateTwitterTitle(title);
        this.seoService.updateTwitterDesc(desc);

        // Set Canonical Tag
        this.seoService.updateCanonicalUrl();
    }
  }

  getSearchAreas() {
    // Reset Area Selects
    this.areas        = [];
    this.areasBuffer  = [];

    const url  = AppSettings.API_ENDPOINT + 'locations/get-all-locations?city_id=' + this.searchParms.city.id;
    // console.log("url: ",url);
    this.helperService.httpGetRequests(url).then(resp => {
      if(typeof resp != "undefined") {
        // Set Areas
        this.areas       = resp.locations;
        this.areasBuffer = this.areas.slice(0, this.bufferSize);

        // Hide Area Loader
        this.areasLoading = false;
      }
    });
  }

  selectCity(city) {
    // Reset Area Selects
    this.searchParms.city = city;
    // console.log("this.searchParms.city :",  this.searchParms.city)
    this.areas        = [];
    this.areasBuffer  = [];
    this.searchParms.area = {
                                city_id   : null,
                                id        : null,
                                name      : "",
                                p_loc     : [],
                                parent_id : "",
                                slug      : ""
                              };

    // Empty selected Agencies
    this.searchParms.agencies = [];

    // let selected = event.target.selectedOptions[0];
    // this.searchParms.city = { id: selected.value, name: selected.label}

    // Update Search
    this.setFindProRoute();
  }

  selectArea() {
    if(this.searchParms.area == null)
      this.searchParms.area = {
                                  city_id   : null,
                                  id        : null,
                                  name      : "",
                                  p_loc     : [],
                                  parent_id : "",
                                  slug      : '',
                                };

    // Empty selected Agencies
    this.searchParms.agencies = [];

    // Update Search
    this.setFindProRoute();
  }

  selectAgencyType() {
    // Empty selected Agencies
    this.searchParms.agencies = [];

    // Update Search
    this.setFindProRoute();
  }

  selectAgency() {
    // console.log("this.searchParms.agencies:", this.searchParms.agencies);
    if(this.searchParms.agencies == null)
      this.searchParms.agencies = [];

    // Update Search
    this.setFindProRoute();
  }

  getLocURL(location, byCity) {
    return this.helperService.get_FindPro_URL(location, this.searchParms.city, byCity, this.searchParms.category);
  }

  setFindProRoute() {
    let routeLink = '';

    // Set Params locally
    this.setParams_locally();

    if (typeof this.searchParms.area.id != "undefined" && this.searchParms.area.id != null) {
        routeLink = this.helperService.get_FindPro_URL(this.searchParms.area, this.searchParms.city, false, this.searchParms.categorySlug);
    } else {
        routeLink = this.helperService.get_FindPro_URL({}, this.searchParms.city, true, this.searchParms.categorySlug);
    }

    // Navigate to new Route
    this.router.navigateByUrl('/', {skipLocationChange: true}).then(()=>
      this.router.navigate([routeLink]));
  }

  setCategoryRoute(categorySlug) {
    let routeLink = '';

    if (typeof this.searchParms.area.id != "undefined" && this.searchParms.area.id != null) {
        routeLink = '/' + this.helperService.get_FindPro_URL(this.searchParms.area, this.searchParms.city, false, categorySlug);
    } else {
        routeLink = '/' + this.helperService.get_FindPro_URL({}, this.searchParms.city, true, categorySlug);
    }

    return routeLink;
  }

  getParentLocations(id): Promise<any> {
    // Show BreadCrumb Loader
    this.loadingBreadCrumbs = true;

    const url = AppSettings.API_ENDPOINT + "locations/get-parent-locations-by-sublocation?sub_loc=" + id;
    return this.helperService.httpGetRequests(url).then(resp => {
      // Hide BreadCrumb Loader
      this.loadingBreadCrumbs = false;

      if (typeof resp.p_loc != "undefined") {
          let searchedArea = resp.p_loc.slice()[0];

          // Set Parent locs for Breadcrumbs
          this.parentLocs  = [...resp.p_loc.slice().reverse()];


          this.searchParms.area = {
                                    city_id   : searchedArea.city_id,
                                    id        : searchedArea.id,
                                    name      : searchedArea.name,
                                    p_loc     : this.parentLocs,
                                    parent_id : searchedArea.parent_id,
                                    slug      : searchedArea.slug
                                  }

          this.searchParms.city = { id: searchedArea.city_id, name: resp.city };

          // Remove the Last parent as its the same as searched area
          this.parentLocs.pop();

          // Set Locs by City False
          this.locsByCity = false;

          return true;
      }
    })
  }

  getAgencyNames() {
    // // Empty Previous Agency Names
    // this.searchParms.agencies = [];

    let url  = AppSettings.API_ENDPOINT + 'get-agency-name';

    let data = {};
    if(this.searchParms.area && this.searchParms.area['id'])
      data = { city_id: this.searchParms.city.id,  location_id: this.searchParms.area['id'], type: this.searchParms.type };
    else
      data = { city_id: this.searchParms.city.id, type: this.searchParms.type };

    this.helperService.httpPostRequests(url, data).then(resp => {
      if(typeof resp != "undefined") {
        // Set Areas
        this.agencyNames     = resp;

        // Hide Area Loader
        this.agencyLoading = false;
      }
    }).catch(error => {
        console.error("error: ",error);

        // Hide Area Loader
        this.agencyLoading = false;
    });
  }

  SortListing() {
    console.log("this.SortBy: ",this.SortBy.split('-'));
    this.searchParms.sort_by   = this.SortBy.split('-')[0];
    this.searchParms.sort_type = this.SortBy.split('-')[1];

    // Sort Listing
    this.getListings();
  }

  getPageList(page, prev, next) {
    if (prev) {
        this.searchParms.current_page = page - 1;
        if(this.searchParms.current_page < 0)
          this.searchParms.current_page = 0;
    } else if (next) {
        this.searchParms.current_page = page + 1;
        if(this.searchParms.current_page > this.ListingsData.last_page)
          this.searchParms.current_page = this.searchParms.current_page - 1;
    } else {
        this.searchParms.current_page = page;
    }

    // console.log("this.searchParms.current_page: ",this.searchParms.current_page);
    this.getListings();
  }

  getListings() {
    // Empty Previous data
    this.AgenciesData = [];

    let url  = AppSettings.API_ENDPOINT + 'search/pros';
    let data = {
                  city_id      : this.searchParms.city.id,
                  location_id  : this.searchParms.area.id,
                  category     : this.searchParms.category,
                  //agencies     : this.searchParms.agencies,
                  sort_by      : this.searchParms.sort_by,
                  sort_type    : this.searchParms.sort_type,
                  current_page : this.searchParms.current_page,
                }
     // console.log("**********data: ",data);
    this.helperService.httpPostRequests(url, data).then(resp => {
      if (typeof resp != "undefined" && resp) {
        this.AgenciesData = resp;

        // Initialize Map
        let self = this;
        setTimeout(function() {
          self.initMap();
        }, 300);

        // console.log("**********this.AgenciesData: ", this.AgenciesData);
        // // Save search data
        // this.saveSearchService.updateSearch(resp.data);

        this.pagination = [];
        if (this.AgenciesData.last_page > 1) {
          for (let i = 1; i <= this.AgenciesData.last_page; i++) {
              this.pagination.push(i);
          }
        }
      }
    }).catch(error => {
        console.error("error: ",error);
    });
  }

  setProProfileRoute(pro) {
    return this.helperService.proProfileRoute(pro);
  }

  agencyProfileRoute(agency) {
    console.log("agency",agency)
    let agent = { agency: agency };
    return this.helperService.agencyProfileRoute(agent);
    // return '/agent/' + this.replaceString(agency.city) + '_' + this.replaceString(agency.name) + '-' + agency.id;
  }

  open_ContactDetails_Modal(pro) {
    // console.log("pro",pro);
    const initialState = { proID: pro.id, proName: pro.user_name};
    const config: ModalOptions = { class: 'modal-sm phone-number', initialState };
    this.modalRef = this.modalService.show(ProContactDetailsComponent, config);
  }

  convertToInt(val) {
    return parseInt(val);
  }

  expandFeatures(property, index) {
    console.log("property: ",property);
    console.log("index: ",index);

    if (typeof this.ListingsData.data[index].expand == "undefined") {
        this.ListingsData.data[index].expand = false;
    }

    this.ListingsData.data[index].expand = !this.ListingsData.data[index].expand;
    // property.expand
  }

  setParams_locally() {
    let data = {
      city         : this.searchParms.city,
      area         : this.searchParms.area,
      category     : this.searchParms.category,
      type         : this.searchParms.type,
      agencies     : this.searchParms.agencies
    }

    // Set Search Params
    this.helperService.setAgencySearchParms(data);
  }

  navigateRoute(url, params) {
    this.router.navigateByUrl('/', {skipLocationChange: true}).then(()=>
      this.router.navigate([url, params]));
  }

  toggleFeatures() {
    this.showFeatures = !this.showFeatures;
  }

  showListView(bool){
    this.listView = bool;
  }

  toggleEmailBucket(){
    this.hideEmailBucket = !this.hideEmailBucket ;
  }

  openSideInner(){
    this.innerSideFilter = !this.innerSideFilter ;
    $("body").toggleClass("overflow-hidden");

    if (typeof window != "undefined") {
        if (window.innerWidth < 768){
            window.scrollTo(0, 0);
        }
    }
  }

  //////////////////////////////////////////
  /********* Areas Ng-Select Fns *********/
  ////////////////////////////////////////
  onScrollToEnd_Areas() {
      this.fetchMore_Areas();
  }

  onScroll_Areas({ end }) {
      if (this.areasLoading || this.areas.length <= this.areasBuffer.length) {
          return;
      }

      if (end + this.numberOfItemsFromEndBeforeFetchingMore >= this.areasBuffer.length) {
          this.fetchMore_Areas();
      }
  }

  private fetchMore_Areas() {
      const len = this.areasBuffer.length;
      const more = this.areas.slice(len, this.bufferSize + len);
      this.areasLoading = true;
      // using timeout here to simulate backend API delay
      setTimeout(() => {
          this.areasLoading = false;
          this.areasBuffer  = this.areasBuffer.concat(more);
      }, 200)
  }

  toggleMap(){
    this.maptoggler = !this.maptoggler;

  }
  toggleLocations(){
    this.openLoc = !this.openLoc;
  }

  initMap() {
    if (typeof google != "undefined") {
      // Hide Simple Map
      this.showSimpleMap = false;

      // Initialize Map if not already
      if(!this.prosMap) {
        // let position = new google.maps.LatLng(this.ListingsData.data[i].latitude, this.ListingsData.data[i].longitude);
        const mapProperties = {
             // center    : position,
             clickableIcons: false,
             zoom      : 15,
             mapTypeId : google.maps.MapTypeId.ROADMAP,
             panControlOptions: {
              position: google.maps.ControlPosition.RIGHT_TOP
            },
            streetViewControlOptions: {
              position: google.maps.ControlPosition.RIGHT_TOP
            },
            zoomControlOptions: {
                position: google.maps.ControlPosition.RIGHT_TOP
            },
            styles : (AppSettings.mapStyles as any)
        };
        this.prosMap = new google.maps.Map(this.mapElement.nativeElement, mapProperties);
        this.mapBounds   = new google.maps.LatLngBounds();

        // Initialize Info Window
        this.infowindo = new google.maps.InfoWindow();
      }
      // Add Listing Markers
      this.addMarkers();
    }
  }
  addMarkers() {
    // Empty Markers Array
    this.markersArray = [];

    for (let i = 0; i < this.AgenciesData.data.length; i++) {
      if(this.AgenciesData.data[i].latitude != null && this.AgenciesData.data[i].longitude != null) {
        let position = new google.maps.LatLng(this.AgenciesData.data[i].latitude, this.AgenciesData.data[i].longitude);
         let price    = this.AgenciesData.data[i].name + '';
         this.labelPrice = price;

        this.setMarker(position, this.AgenciesData.data[i]);
        this.prosMap.setCenter(position);
        // this.markersArray.push(marker);
      }
    }
    // Set Bounds or Show Simple map incase no markers are available
    if(this.markersArray.length > 1)
      this.prosMap.fitBounds(this.mapBounds);
    else if(this.markersArray.length < 1)
      this.setMapWithoutMarkers();
      // this.showSimpleMapFn();
  }


  setMarker(position, pro) {
    let self = this;
     // console.log("**** position: ",position);
    function HTMLMarker(lat, lng) {
        this.lat = lat;
        this.lng = lng;
        this.pos = new google.maps.LatLng(lat,lng);
    }

    HTMLMarker.prototype          = new google.maps.OverlayView();
    HTMLMarker.prototype.onRemove = function(){}

    //init your html element here
    HTMLMarker.prototype.onAdd = function() {
       // console.log("**** pro: ",pro);
        this.div = document.getElementById("mapsLabel-" + pro.pro_id);
        // console.log("**** this.div: ",this.div);
        // this.div.className = "htmlMarker";
        this.div.style.position = 'absolute';
        // this.div.innerHTML = '<span>'+label+'</span>';
        var panes = this.getPanes();
        panes.overlayImage.appendChild(this.div);
    }

    HTMLMarker.prototype.draw = function() {
        var overlayProjection  = this.getProjection();
        var position           = overlayProjection.fromLatLngToDivPixel(this.pos);
        var panes              = this.getPanes();
        this.div.style.left    = position.x + 'px';
        this.div.style.top     = position.y + 'px';

        let pos = this.pos;
        let div = this.div;
        google.maps.event.addDomListener(this.div, "click", function(event) {
    			// // google.maps.event.trigger(self, "click");
          // // console.log("event: ", event);
          // // console.log("property: ", property);
          //  console.log("pos: ", pos);
          // console.log("self.prosMap: ", self.prosMap);
          // console.log("self.infowindo: ", self.infowindo);

          self.infoWindowProperty = pro;
          self.agentRouteInfo = self.helperService.proProfileRoute(self.infoWindowProperty);
          $('#infowindow-content').removeClass('af-none');
          $('.mapBox').removeClass('hideTri');
          self.infowindo.setContent(document.getElementById('infowindow-content'));
          self.infowindo.setPosition(pos);
          self.infowindo.open(self.prosMap);
    		});
    }

    //to use it
    let marker;
    marker = new HTMLMarker(position.lat(), position.lng());
    // console.log("position.lng()",position.lng())
    // console.log("this.prosMap",this.prosMap);
    marker.setMap(this.prosMap);

    // Extend Bounds
    this.mapBounds.extend(marker.pos);

    // Add to Markers Array
    this.markersArray.push(marker);
  }

  adjustMap(pro_id, bool) {
    if (bool) {
      $("#mapsLabel-" + pro_id).addClass("hovered");
    }
    else{
      $("#mapsLabel-" + pro_id).removeClass("hovered");
    }
  }
  adjustList(property_id, bool){
    if (bool) {
      $("#listLabel-" + property_id).addClass("hovered");
    }
    else{
      $("#listLabel-" + property_id).removeClass("hovered");
    }
  }
  setMapWithoutMarkers() {
    let address;
      address = this.searchParms.city.name;

    // console.log("********** address: ",address);
    let self     = this;
    let geocoder = new google.maps.Geocoder();
    geocoder.geocode( { 'address': address}, function(results, status) {
      if (status.toString() == "OK") {
          self.Location_LatLng  = new google.maps.LatLng(results[0].geometry.location.lat(), results[0].geometry.location.lng());
          self.prosMap.setCenter(self.Location_LatLng);
      } else {
          // alert('Geocode was not successful for the following reason: ' + status);
          console.error('Geocode was not successful for the following reason: ' + status);
      }
    });
  }
  closeInfoWindow(){
    $('#infowindow-content').addClass('af-none');
    $('.mapBox').addClass('hideTri');
  }
  showSimpleMapFn() {
    // Show Simple Map if map has no markers
    this.showSimpleMap = true;

    let address;
      address = this.searchParms.city.name;

    this.simpleMapUrl = 'https://www.google.com/maps/embed/v1/place?q='+ address +'&key=' + AppSettings.GOOGLE_MAPS_KEY;
  }
  openWhatsapp(pro){

    let url  = AppSettings.API_ENDPOINT + 'pro/get-contact-details';
    let data = { user_id: pro.id };

    this.helperService.httpPostRequests(url, data).then(resp => {
        window.open("https://api.whatsapp.com/send?phone="+ resp.data.whatsapp_number_code + resp.data.whatsapp_number +"&text=Hi%2C%20I%20would%20like%20to%20inquire%20about%20your%20agency%20at%20Gharbaar.com.%20Please%20contact%20me%20back%20at%20your%20earliest.%20Thanks", "_blank");
    }).catch(error => {
        console.error("error: ",error);
    })
  //   if(typeof window != "undefined")

  }

}

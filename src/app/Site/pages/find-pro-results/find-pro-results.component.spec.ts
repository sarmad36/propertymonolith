import { async, ComponentFixture, TestBed } from '@angular/core/testing';

// import { SarchListingsAgentsComponent } from './sarch-listings-agents.component';
import { FindProResultsComponent } from './find-pro-results.component';

describe('FindProResultsComponent', () => {
  let component: FindProResultsComponent;
  let fixture: ComponentFixture<FindProResultsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FindProResultsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FindProResultsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

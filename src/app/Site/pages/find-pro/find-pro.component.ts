import { Component, OnInit, ChangeDetectorRef, Optional } from '@angular/core';
import { Router } from "@angular/router";
import { HttpClient } from '@angular/common/http';
import { AppSettings, HelperService, PurposeService } from '../../../services/_services';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-find-pro',
  templateUrl: './find-pro.component.html',
  styleUrls: ['./find-pro.component.css']
})
export class FindProComponent implements OnInit {

  // City and Areas List
  public cities          :any = AppSettings.cities;
  public areas           :any = [];
  public ProCategories   :any = AppSettings.ProCategories;

  // Params
  public SelectedCity     :any = AppSettings.cities[1];
  public SelectedArea     = [];
  public SelectedCategory : any = 'any';
  public SelectedAgencies = [];
  public SelectedType     = AppSettings.agencyTypes[0].value;


  // Ng-Slect Params
  public areasBuffer  = [];
  public bufferSize   = 10;
  public numberOfItemsFromEndBeforeFetchingMore = 5;
  public areasLoading = false;
  public areaQuery    = "";
  public areasError   = false;

  // Agency Params
  public agencyNames     :any = [];
  public agencyLoading   :any = false;

  public OpenSideAg      :any = false;

  // Agent Types
  public types           :any = AppSettings.agencyTypes;

  // Https Params
  protected ngUnsubscribe: Subject<void> = new Subject<void>();
  public httpLoading     = false;
  public corsHeaders     : any;

  public areasSelect_F = new FormControl('');

  // Subscriptions
  public propuseSub :any;


  constructor(
    private http: HttpClient,
    private router: Router,
    private helperService: HelperService,
    private purposeService: PurposeService,
    private formBuilder      : FormBuilder,
    @Optional() private cdr  : ChangeDetectorRef
  ) {}

  ngOnInit() {

    // // Get Locations on Init
    this.selectCity();

  }
  ngAfterViewInit(){
    $(document).ready(function() {
        ($('.owl-carousel') as any).owlCarousel({
        loop:true,
        margin:0,
        nav:false,
        dots:false,
        autoplay:true,
        autoplaySpeed: 3000,
        mouseDrag:false,
        animateOut: 'fadeOut',
        animateIn: 'fadeIn',
        responsive:{
            0:{
                items:1
            },
            600:{
                items:1
            },
            1000:{
                items:1
            }
        }
      });
    });
  }

  ngAfterContentInit() {
    //Called after ngOnInit when the component's or directive's content has been initialized.
    //Add 'implements AfterContentInit' to the class.
    if (typeof document != "undefined") {
      var imgDefer = document.getElementsByTagName('img');
      for (var i=0; i<imgDefer.length; i++) {
        if(imgDefer[i].getAttribute('data-src')) {
          imgDefer[i].setAttribute('src',imgDefer[i].getAttribute('data-src'));
        }
      }
      var picDefer = document.getElementsByTagName('source');
      for (var i=0; i<picDefer.length; i++) {
        if(picDefer[i].getAttribute('data-src')) {
          picDefer[i].setAttribute('srcset',picDefer[i].getAttribute('data-src'));
        }
      }
    }
  }

  selectCity() {
    // Reset Area Selects
    this.areas        = [];
    this.areasBuffer  = [];
    // console.log("this.areasSelect_F: ",this.areasSelect_F);
    // this.areasSelect.active = [];
    this.SelectedArea = [];

    if (typeof this.SelectedCity.name != 'undefined') {
        let index = this.cities.findIndex(x => x.name === this.SelectedCity.name);
        this.SelectedCity = {"id": this.cities[index].id, "name": this.cities[index].name};

        // Detect Changes
        this.cdr.detectChanges();
        // Show Area Loader
        // this.areasLoading = true;

        const url  = AppSettings.API_ENDPOINT + 'locations/get-all-locations?city_id=' + this.SelectedCity.id;
        this.helperService.httpGetRequests(url).then(resp => {
          if(typeof resp.locations != "undefined") {
            // Set Areas
            this.areas       = resp.locations;
            this.areasBuffer = this.areas.slice(0, this.bufferSize);

            // Hide Area Loader
            this.areasLoading = false;
          }
        }).catch(error => {
            console.error("error: ",error);

            // Hide Area Loader
            this.areasLoading = false;
        });

        // Get Agency Names
        //this.getAgencyNames();
    }
  }

  selectArea() {
    console.log("this.SelectedArea:", this.SelectedArea);
    if(this.SelectedArea == null)
      this.SelectedArea = [];

    // Get Agency Names
    this.getAgencyNames();
  }

  selectCategory(){
    console.log("this.SelectedCategory:", this.SelectedCategory);

    if(this.SelectedCategory == null)
      this.SelectedCategory = [];

  }

  getAgencyNames() {
    // Empty Previous Agency Names
    this.SelectedAgencies = [];

    let url  = AppSettings.API_ENDPOINT + 'get-agency-name';

    let data = {};
    if(this.SelectedArea && this.SelectedArea['id'])
      data = { city_id: this.SelectedCity.id,  location_id: this.SelectedArea['id'], type: this.SelectedType };
    else
      data = { city_id: this.SelectedCity.id, type: this.SelectedType };

    this.helperService.httpPostRequests(url, data).then(resp => {
      if(typeof resp != "undefined") {

        // Empty Previous Agency Names
        this.SelectedAgencies = [];

        // Set Areas
        this.agencyNames     = resp;

        // Hide Area Loader
        this.agencyLoading = false;
      }
    }).catch(error => {
        console.error("error: ",error);

        // Hide Area Loader
        this.agencyLoading = false;
    });
  }

  selectAgency() {
    console.log("this.SelectedAgencies:", this.SelectedAgencies);
    if(this.SelectedAgencies == null)
      this.SelectedAgencies = [];
  }

  setFindProRoute() {
    // Set Params locally
    this.setParams_locally();
    let route = '';
    if (typeof this.SelectedArea['id'] != "undefined") {
        return this.helperService.get_FindPro_URL(this.SelectedArea, this.SelectedCity, false, this.SelectedCategory);
    } else {
        return this.helperService.get_FindPro_URL({}, this.SelectedCity, true, this.SelectedCategory);
    }
  }

  // search() {
  //   // Save Params Locally
  //   this.setParams_locally();
  //
  //   // Set Router Link
  //   let routeLink =  '/agents/';
  //
  //   if (typeof this.SelectedArea['id'] != "undefined") {
  //     // Show Area Loader
  //     this.areasLoading = true;
  //
  //     let url = AppSettings.API_ENDPOINT + "locations/get-parent-locations-by-sublocation?sub_loc=" + this.SelectedArea['id'];
  //     this.helperService.httpGetRequests(url).then(resp => {
  //       console.log("resp: ",resp);
  //
  //       // Hide Area Loader
  //       this.areasLoading = false;
  //
  //       if (typeof resp.p_loc != "undefined") {
  //
  //           // Revese the location -- Parent To Child
  //           let locs      = resp.p_loc.slice().reverse();
  //
  //           // Create Params
  //           routeLink += this.SelectedCity.name.replace(/ /g, "_")  + '_';
  //
  //           // Loop to make Route
  //           for (let i = 0; i < locs.length; i++) {
  //               // routeLink += locs[i].name.replace(/ /g, "_").replace(/-/g, "_");
  //               routeLink += locs[i].name.replace(/ /g, "_").replace(/-/g, "_").replace(/[/\\*]/g, "_");
  //               let len = locs.length - 1;
  //               if(i < len)
  //                 routeLink += '_';
  //                 // routeLink += '_';
  //           }
  //
  //           routeLink += "-" + this.SelectedArea['id'] + "-2"; // 2 in params specify its a sub location
  //
  //           // Naviagate to Search Page
  //           this.router.navigate([routeLink]);
  //       }
  //     }).catch(error => {
  //         console.error("error: ",error);
  //     });
  //   } else {
  //       routeLink += this.SelectedCity.name.replace(/ /g, "_") + "-" + this.SelectedCity.id + "-1"; // 1 in params specify its a City
  //
  //       // Naviagate to Search Page
  //       this.router.navigate([routeLink]);
  //   }
  // }

  setParams_locally() {
    let data = {
      city         : this.SelectedCity,
      area         : this.SelectedArea,
      category     : this.SelectedCategory,
      //type         : this.SelectedType,
      //agencies     : this.SelectedAgencies
    }

    // Set Search Params
    this.helperService.setAgencySearchParms(data);
  }

  //////////////////////////////////////////
  /********* Areas Ng-Select Fns *********/
  ////////////////////////////////////////
  onScrollToEnd_Areas() {
      this.fetchMore_Areas();
  }

  onScroll_Areas({ end }) {
      if (this.areasLoading || this.areas.length <= this.areasBuffer.length) {
          return;
      }

      if (end + this.numberOfItemsFromEndBeforeFetchingMore >= this.areasBuffer.length) {
          this.fetchMore_Areas();
      }
  }

  private fetchMore_Areas() {
      const len = this.areasBuffer.length;
      const more = this.areas.slice(len, this.bufferSize + len);
      this.areasLoading = true;
      // using timeout here to simulate backend API delay
      setTimeout(() => {
          this.areasLoading = false;
          this.areasBuffer  = this.areasBuffer.concat(more);
      }, 200)
  }

  openSideAgents(){
    this.OpenSideAg = !this.OpenSideAg;
  }

}

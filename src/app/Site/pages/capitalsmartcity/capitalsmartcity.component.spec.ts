import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CapitalsmartcityComponent } from './capitalsmartcity.component';

describe('CapitalsmartcityComponent', () => {
  let component: CapitalsmartcityComponent;
  let fixture: ComponentFixture<CapitalsmartcityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CapitalsmartcityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CapitalsmartcityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit, Injectable, TemplateRef, ViewChild } from '@angular/core';
import { AlertService, AuthenticationService, UserService } from '../../../services/_services';
import { Router } from "@angular/router";
import { HttpClient } from '@angular/common/http';

// Social Login
import { AuthService } from "angularx-social-login";
import { FacebookLoginProvider, GoogleLoginProvider } from "angularx-social-login";


// Bootsteap Components
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';

// Modal Components
import { LoginComponent } from '../../entry-components/login/login.component';
import { SiginUpOptsComponent } from '../../entry-components/sigin-up-opts/sigin-up-opts.component';
// import { SiginUpComponent } from '../../entry-components/sigin-up/sigin-up.component';

@Component({
  selector: 'app-sigin-up',
  templateUrl: './sigin-up.component.html',
  styleUrls: ['./sigin-up.component.css']
})
export class SiginUpComponent implements OnInit {

  public authSub            : any;
  public socail_AuthSub     : any;

  public modalRef: BsModalRef;
  public isUserLoggedIn     : any = false;
  public loading            : any = false;
  public showPass           : any = false;
  public user :any = {
    'first_name'            : '',
    'last_name'             : '',
    'email'                 : '',
    'password'              : '',
    'mobile_number'         : '',
    'dob'                   : null,
  }

  public fieldsError :any = {
    'first_name'            : false,
    'last_name'             : false,
    'email'                 : false,
    'emailErrMsg'           : "",
    'inValidCreds'          : false,
    'inValidCredsMsg'       : "",
    'password'              : false,
    'mobile_number'         : false,
    'dob'                   : false,
  }

  public notValidEmail      : any = false;

  public month :any = "";
  public day   :any = "";
  public year  :any = "";

  constructor(
    private modalService: BsModalService,
    private bsModalRef: BsModalRef,
    private authService: AuthService,
    private http: HttpClient,
    private userService: UserService,
    private authenticationService: AuthenticationService,
    private alertService: AlertService,
    private router: Router,
  ) {
    console.log("Sign-up this.bsModalRef: ",this.bsModalRef);
  }

  ngOnInit() {
    this.socail_AuthSub = this.authService.authState.subscribe((user) => {
      // console.log("**** user: ",user);

      if (user !== null) {
        // Set User
        this.user = {
          'first_name'            : user.firstName,
          'last_name'             : user.lastName,
          'email'                 : user.email,
        };

        // Social Login or Signup
        this.userService.socialLoginRegister(user)
           .then(resp => {
    			      // console.log("resp: ",resp);
                this.authenticationService.setCurrentUserValue(resp);
                // this.router.navigate(['/']);

                // Set User
                this.user           = resp.user;
                this.isUserLoggedIn = true;

                // Hide Modal
                this.bsModalRef.hide();
    		    },
            error => {
                // alert(JSON.stringify(error));
     			      console.error("error: ",error);
     		    });
      }
    });
  }
  ngAfterViewInit() {
    //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
    //Add 'implements AfterViewInit' to the class.
    function setInputFilter(textbox, inputFilter) {
      ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"].forEach(function(event) {
        textbox.addEventListener(event, function() {
          if (inputFilter(this.value)) {
            this.oldValue = this.value;
            this.oldSelectionStart = this.selectionStart;
            this.oldSelectionEnd = this.selectionEnd;
          } else if (this.hasOwnProperty("oldValue")) {
            this.value = this.oldValue;
            this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
          }
        });
      });
    }

    // Install input filters.
    setInputFilter(document.getElementById("phone"), function(value) {
      return /^[\d -]*$/.test(value);
    });

  }
  ngOnDestroy() {
    if (typeof this.socail_AuthSub != 'undefined') {
      //prevent memory leak when component destroyed
      this.socail_AuthSub.unsubscribe();
    }
  }

  togglePass(){
    this.showPass = !this.showPass;
  }

  signInWithGoogle(): void {
    if (typeof this.authService != 'undefined') {
      console.log("***** signInWithGoogle ****");
      this.authService.signIn(GoogleLoginProvider.PROVIDER_ID);
    }
  }

  signInWithFB(): void {
    if (typeof this.authService != 'undefined') {
      console.log("***** signInWithFB ****");
      this.authService.signIn(FacebookLoginProvider.PROVIDER_ID);
    }
  }

  signOut(): void {
    this.authService.signOut();
  }

  open_Login_Modal() {
    // Hide Sign-Up with Options Modal
    this.bsModalRef.hide();

    const initialState = {};
    this.modalRef = this.modalService.show(LoginComponent, {initialState});
  }

  clearErrors() {
    this.fieldsError = {
      'first_name'            : false,
      'last_name'             : false,
      'email'                 : false,
      'emailErrMsg'           : "",
      'inValidCreds'          : false,
      'inValidCredsMsg'       : "",
      'password'              : false,
      'mobile_number'          : false,
      'dob'                   : false,
    };
  }

  checkErrors(arg) {
    let returnCheck = true;

    if(this.user.first_name.trim() === "" && arg != 'Login') {
        this.fieldsError.first_name = true;
        returnCheck = false;
    }
    if(this.user.last_name.trim() === "" && arg != 'Login') {
        this.fieldsError.last_name = true;
        returnCheck = false;
    }
    if (this.user.email.trim() === "") {
        this.fieldsError.email = true;
        this.fieldsError.emailErrMsg = "Email is required";
        returnCheck = false;
    }
    if(this.user.password.trim() === "") {
        this.fieldsError.password = true;
        returnCheck = false;
    }

    return returnCheck;
  }

  register() {
    if(this.year != "" && this.month != "" && this.day != "")
      this.user.dob = this.year + '/' + this.month + '/' + this.day;
    console.log("this.user: ",this.user);

    this.clearErrors();

    if (this.checkErrors("Signup")) {
      // Show Loader
      this.loading = true;

      this.userService.register(this.user)
         .then(user => {
  			      console.log("user: ",user);
              this.authenticationService.setCurrentUserValue(user);
              // this.router.navigate(['/']);

              // Set User
              this.user           = user.user;
              this.isUserLoggedIn = true;

              // Hide Loader
              this.loading = false;

              // Hide Modal
              this.bsModalRef.hide();
  		    },
          error => {
              // Hide Loader
              this.loading = false;

              console.log("error: ",error);
              if(typeof error.errors.email != "undefined") {
                  this.fieldsError.email       = true;
                  this.fieldsError.emailErrMsg = error.errors.email[0];
              } else {
                  alert(JSON.stringify(error));
              }
   		    });
    }
  }

}

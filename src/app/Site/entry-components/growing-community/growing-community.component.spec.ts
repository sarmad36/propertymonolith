import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GrowingCommunityComponent } from './growing-community.component';

describe('GrowingCommunityComponent', () => {
  let component: GrowingCommunityComponent;
  let fixture: ComponentFixture<GrowingCommunityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GrowingCommunityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GrowingCommunityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

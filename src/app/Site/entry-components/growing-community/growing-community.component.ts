import { Component, OnInit } from '@angular/core';
import { AppSettings, HelperService, SEOService, PurposeService, FavoriteService, HideService, SaveSearchService, EmailBucketService } from '../../../services/_services';

@Component({
  selector: 'app-growing-community',
  templateUrl: './growing-community.component.html',
  styleUrls: ['./growing-community.component.css']
})
export class GrowingCommunityComponent implements OnInit {

  public getCommunity         : any = { gb_subscribers: '',youtube_subscribers: '',twitter_followers: '',instagram_followers: '' };

  constructor(
    private helperService         : HelperService,
  ) {
    this.getCommunityDetails();
   }

  ngOnInit() {
  }
  getCommunityDetails() {
    let url = AppSettings.API_ENDPOINT + 'join-our-community';
    this.helperService.httpGetRequests(url).then(resp => {
      if (typeof resp != "undefined") {
        this.getCommunity = resp;
        //
         console.log("getCommunityDetails",this.getCommunity);
        // console.log("this.getCommunity.gb_subscribers",this.getCommunity.gb_subscribers);
      }
    }).catch(error => {
        console.error("error: ",error);
    });
  }
}

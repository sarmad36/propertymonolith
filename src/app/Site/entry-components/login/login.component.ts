import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { AuthenticationService, UserService } from '../../../services/_services';

// Social Login
import { AuthService } from "angularx-social-login";
import { FacebookLoginProvider, GoogleLoginProvider } from "angularx-social-login";

import { SiginUpOptsComponent } from '../../entry-components/sigin-up-opts/sigin-up-opts.component';

// Bootsteap Components
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  @ViewChild('signup', {static: true})
  public signup: TemplateRef<any>;

  public authSub            : any;
  public socail_AuthSub     : any;

  public modalRef: BsModalRef;
  public isUserLoggedIn     : any = false;
  public loading            : any = false;
  public showPass           : any = false;
  public user :any = {
    'first_name'            : '',
    'last_name'             : '',
    'email'                 : '',
    'password'              : '',
    'phone_number'          : '',
    'dob'                   : null,
  }

  public fieldsError :any = {
    'first_name'            : false,
    'last_name'             : false,
    'email'                 : false,
    'emailErrMsg'           : "",
    'inValidCreds'          : false,
    'inValidCredsMsg'       : "",
    'password'              : false,
    'phone_number'          : false,
    'dob'                   : false,
  }

  public notValidEmail      : any = false;

  public month :any = "";
  public day   :any = "";
  public year  :any = "";


  constructor(
    private modalService: BsModalService,
    private bsModalRef: BsModalRef,
    private authService: AuthService,
    private userService: UserService,
    private authenticationService: AuthenticationService
  ) {
    console.log("Login this.bsModalRef: ",this.bsModalRef);
  }

  ngOnInit() {
    this.socail_AuthSub = this.authService.authState.subscribe((user) => {
      console.log("**** user: ",user);

      if (user !== null) {
        // Set User
        this.user = {
          'first_name'            : user.firstName,
          'last_name'             : user.lastName,
          'email'                 : user.email,
        };

        // Social Login or Signup
        this.userService.socialLoginRegister(user)
           .then(resp => {
                // console.log("social this.authSub: ",this.authSub);
    			      // console.log("social resp: ",resp);
                this.authenticationService.setCurrentUserValue(resp);
                // this.router.navigate(['/']);

                // Set User
                this.user           = resp.user;
                this.isUserLoggedIn = true;

                // Hide Modal
                this.bsModalRef.hide();
    		    },
            error => {
                alert(JSON.stringify(error));
     			      console.log("error: ",error);
     		    });
      }
    });
  }

  ngOnDestroy() {
    if (typeof this.socail_AuthSub != 'undefined') {
      //prevent memory leak when component destroyed
      this.socail_AuthSub.unsubscribe();
    }
  }

  togglePass(){
    this.showPass = !this.showPass;
  }

  public openModal(template: TemplateRef<any>) {
    this.clearErrors();

    // Hide Modal
    if(typeof this.modalRef != "undefined")
      this.modalRef.hide();

    this.modalRef = this.modalService.show(template, {class: 'modal-sm'});
  }

  signInWithGoogle(): void {
    if (typeof this.authService != 'undefined') {
      console.log("***** signInWithGoogle ****");
      this.authService.signIn(GoogleLoginProvider.PROVIDER_ID);
    }
  }

  signInWithFB(): void {
    if (typeof this.authService != 'undefined') {
      console.log("***** signInWithFB ****");
      this.authService.signIn(FacebookLoginProvider.PROVIDER_ID);
    }
  }

  signOut(): void {
    this.authService.signOut();
  }

  clearErrors() {
    this.fieldsError = {
      'first_name'            : false,
      'last_name'             : false,
      'email'                 : false,
      'emailErrMsg'           : "",
      'inValidCreds'          : false,
      'inValidCredsMsg'       : "",
      'password'              : false,
      'phone_number'          : false,
      'dob'                   : false,
    };
  }

  checkErrors(arg) {
    let returnCheck = true;

    if(this.user.first_name === "" && arg != 'Login') {
        this.fieldsError.first_name = true;
        returnCheck = false;
    }
    if(this.user.last_name === "" && arg != 'Login') {
        this.fieldsError.last_name = true;
        returnCheck = false;
    }
    if (this.user.email === "") {
        this.fieldsError.email = true;
        this.fieldsError.emailErrMsg = "Email is required";
        returnCheck = false;
    }
    if(this.user.password === "") {
        this.fieldsError.password = true;
        returnCheck = false;
    }

    return returnCheck;
  }

  login() {
    // console.log("this.user: ",this.user);

    // Clear Previous Errors
    this.clearErrors();

    console.log("this.checkErrors('Login'): ",this.checkErrors('Login'));

    if (this.checkErrors('Login')) {
      // Show Loader
      this.loading = true;

      this.userService.login(this.user)
         .then(user => {
              console.log("user: ",user);
              this.authenticationService.setCurrentUserValue(user);
              // this.router.navigate(['/']);

              // Set User
              this.user           = user.user;
              this.isUserLoggedIn = true;

              // Hide Loader
              this.loading = false;

              // Hide Modal
              // this.modalRef.hide();
              this.bsModalRef.hide();
          },
          error => {
              console.log("error: ",error);
              if(typeof error.message != "undefined") {
                  this.fieldsError.inValidCreds     = true;
                  this.fieldsError.inValidCredsMsg  = error.message;
              } else {
                  alert(JSON.stringify(error));
              }

              // Hide Loader
              this.loading = false;
          });
    }
  }

  open_SignUpOpts_Modal() {
    const initialState = {};
    this.modalRef = this.modalService.show(SiginUpOptsComponent, {initialState});
    this.bsModalRef.hide();
  }
}

import { Component, OnInit } from '@angular/core';
import { AuthenticationService, UserService } from '../../../services/_services';

// Social Login
import { AuthService } from "angularx-social-login";
import { FacebookLoginProvider, GoogleLoginProvider } from "angularx-social-login";

// Bootsteap Components
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';

// Modal Components
import { LoginComponent } from '../../entry-components/login/login.component';
import { SiginUpComponent } from '../../entry-components/sigin-up/sigin-up.component';


@Component({
  selector: 'app-sigin-up-opts',
  templateUrl: './sigin-up-opts.component.html',
  styleUrls: ['./sigin-up-opts.component.css']
})
export class SiginUpOptsComponent implements OnInit {
  public authSub            : any;
  public socail_AuthSub     : any;

  public modalRef: BsModalRef;
  public isUserLoggedIn     : any = false;
  public loading            : any = false;
  public user :any = {
    'first_name'            : '',
    'last_name'             : '',
    'email'                 : '',
    'password'              : '',
    'phone_number'          : '',
    'dob'                   : null,
  }

  constructor(
    private modalService: BsModalService,
    private bsModalRef: BsModalRef,
    private authService: AuthService,
    private userService: UserService,
    private authenticationService: AuthenticationService
  ) {
    console.log("Sign-up OPTs this.bsModalRef: ",this.bsModalRef);
  }

  ngOnInit() {
    this.socail_AuthSub = this.authService.authState.subscribe((user) => {
      // console.log("**** user: ",user);

      if (user !== null) {
        // Set User
        this.user = {
          'first_name'            : user.firstName,
          'last_name'             : user.lastName,
          'email'                 : user.email,
        };

        // Social Login or Signup
        this.userService.socialLoginRegister(user)
           .then(resp => {
    			      // console.log("resp: ",resp);
                this.authenticationService.setCurrentUserValue(resp);
                // this.router.navigate(['/']);

                // Set User
                this.user           = resp.user;
                this.isUserLoggedIn = true;

                // Hide Modal
                this.bsModalRef.hide();
    		    },
            error => {
                alert(JSON.stringify(error));
     			      console.log("error: ",error);
     		    });
      }
    });
  }

  ngOnDestroy() {
    if (typeof this.socail_AuthSub != 'undefined') {
      //prevent memory leak when component destroyed
      this.socail_AuthSub.unsubscribe();
    }
  }

  signInWithGoogle(): void {
    if (typeof this.authService != 'undefined') {
      console.log("***** signInWithGoogle ****");
      this.authService.signIn(GoogleLoginProvider.PROVIDER_ID);
    }
  }

  signInWithFB(): void {
    if (typeof this.authService != 'undefined') {
      console.log("***** signInWithFB ****");
      this.authService.signIn(FacebookLoginProvider.PROVIDER_ID);
    }
  }

  signOut(): void {
    this.authService.signOut();
  }

  open_SignUp_Modal() {
    // Hide Sign-Up with Options Modal
    this.bsModalRef.hide();

    const initialState = {};
    this.modalRef = this.modalService.show(SiginUpComponent, {initialState});
    // this.modalRef.content.closeBtnName = 'Close';
  }

  open_Login_Modal() {
    // Hide Sign-Up with Options Modal
    this.bsModalRef.hide();

    const initialState = {};
    this.modalRef = this.modalService.show(LoginComponent, {initialState});
  }

}

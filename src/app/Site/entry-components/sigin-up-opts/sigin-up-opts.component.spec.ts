import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SiginUpOptsComponent } from './sigin-up-opts.component';

describe('SiginUpOptsComponent', () => {
  let component: SiginUpOptsComponent;
  let fixture: ComponentFixture<SiginUpOptsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SiginUpOptsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SiginUpOptsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthenticationService, AppSettings, HelperService } from '../../../services/_services';
import { BsModalService, BsModalRef, ModalOptions } from 'ngx-bootstrap/modal';

// Sweet Alerts
import Swal from 'sweetalert2';

// Modal Components
import { ContactDetailsComponent } from '../contact-details/contact-details.component';

@Component({
  selector: 'app-inquiry-modal',
  templateUrl: './inquiry-modal.component.html',
  styleUrls: ['./inquiry-modal.component.css']
})
export class InquiryModalComponent implements OnInit {

  public property   : any;
  public propID     : any;
  public userName   : any;
  public contacts   : any = [];
  public modalRef   : BsModalRef;
  public authSubs   : any;

  // Request Info Form
  public lead      : any = { name: '', email: '', phone: '', message: '' };
  public LeadForm  : FormGroup;
  public submitted : any = false;

  constructor(
    public authenticationService : AuthenticationService,
    public bsModalRef            : BsModalRef,
    public modalService          : BsModalService,
    public formBuilder           : FormBuilder,
    public helperService         : HelperService,
  ) { }

  ngOnInit() {
    this.authSubs = this.authenticationService.currentUserS$.subscribe(user => {
      if(user) {
        this.lead.name  = user.user.first_name + ' ' + user.user.last_name;
        this.lead.email = user.user.email;
        this.lead.phone = user.user.phone_number;
      }
    });

    // Set FormGroup
    this.LeadForm = this.formBuilder.group({
      name    : [null, Validators.required],
      email   : [null, [Validators.required, Validators.email]],
      phone   : [null, Validators.required],
      message : [null, Validators.required]
    });

    // Set propertyID
    if(typeof this.property.id != "undefined")
      this.propID = this.property.id;
    else
     this.propID = this.property.p_id;

    // Set Username
    if(typeof this.property.user != "undefined")
      this.userName = this.property.user.first_name + ' ' + this.property.user.last_name;
    else
      this.userName = this.property.first_name + ' ' + this.property.last_name;

    // Set Default Msg
    this.lead.message = 'I would like to inquire about your property Gharbaar - ID-'+ this.propID +'. Please contact me at your earliest convenience.';
  }

  ngOnDestroy() {
    if (typeof this.authSubs != 'undefined') {
      //prevent memory leak when component destroyed
      this.authSubs.unsubscribe();
    }
  }

  // convenience getter for easy access to form fields
  get f() { return this.LeadForm.controls; }

  AddLead(leadData) {
    leadData.type         = 'request_info';
    leadData.property     = [];

    let prop = { user_id: this.property.user_id, property_id: this.propID }
    leadData.property.push(prop);
    this.submitted        = true;

    // Stop here if form is invalid
    if (this.LeadForm.invalid) {
     console.log(this.LeadForm.controls);
     return;
    }

    let user = this.authenticationService.get_currentUserValue();
    if (user) {
      leadData['requested_by'] = user.user.id;
    }

    const url  = AppSettings.API_ENDPOINT + 'lead/add';
    this.helperService.httpPostRequests(url, leadData)
      .then(resp => {
        if (typeof resp != "undefined" && resp != null && typeof resp.error === "undefined" ) {
          this.lead = {};

          // Hide this Modal
          this.closeModal();

          Swal.fire('Request Sent', 'Your message has been sent successfully. You will receive a reply directly at your email address.', 'success')

          // Set Submission false
          this.submitted = false;
        } else {
            console.error(resp);
        }
      })
      .catch(error => {
        console.log("error: ",error);

        // Set Submission false
        this.submitted = false;
      });
  }

  open_ContactDetails_Modal() {
    this.closeModal();

    const initialState = { property: this.property };
    const config: ModalOptions = { class: 'modal-sm phone-number', initialState };
    this.modalRef = this.modalService.show(ContactDetailsComponent, config);
  }

  closeModal() {
    this.bsModalRef.hide();
  }

}

import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthenticationService, AppSettings, HelperService, EmailBucketService } from '../../../services/_services';
import { BsModalService, BsModalRef, ModalOptions } from 'ngx-bootstrap/modal';

// Sweet Alerts
import Swal from 'sweetalert2';

@Component({
  selector: 'app-bulk-inquiry-modal',
  templateUrl: './bulk-inquiry-modal.component.html',
  styleUrls: ['./bulk-inquiry-modal.component.css']
})
export class BulkInquiryModalComponent implements OnInit {

  public properties         :any = [];
  public searchParms        :any = {};
  public ImagesUrl          :any = AppSettings.IMG_ENDPOINT;
  public authSubs           :any;
  public bucketSubs         :any;

  // Request Info Form
  public lead      : any = { name: '', email: '', phone: '', message: 'I would like to inquire about your properties Gharbaar. Please contact me at your earliest convenience.' };
  public LeadForm  : FormGroup;
  public submitted : any = false;

  constructor(
    public emailBucketService    : EmailBucketService,
    public authenticationService : AuthenticationService,
    public bsModalRef            : BsModalRef,
    public modalService          : BsModalService,
    public formBuilder           : FormBuilder,
    public helperService         : HelperService,
  ) { }

  ngOnInit() {
    this.authSubs = this.authenticationService.currentUserS$.subscribe(user => {
      if(user) {
        this.lead.name  = user.user.first_name + ' ' + user.user.last_name;
        this.lead.email = user.user.email;
        this.lead.phone = user.user.phone_number;
      }
    });

    // Set FormGroup
    this.LeadForm = this.formBuilder.group({
      name    : [null, Validators.required],
      email   : [null, [Validators.required, Validators.email]],
      phone   : [null, Validators.required],
      message : [null, Validators.required]
    });

    this.bucketSubs = this.emailBucketService.propertiesS$.subscribe(props => {
      this.properties = props;
      if(this.properties.length <= 0)
        this.closeModal();

      this.searchParms = this.helperService.getSearchParms();
    });
  }

  ngOnDestroy() {
    if (typeof this.bucketSubs != 'undefined') {
      //prevent memory leak when component destroyed
      this.bucketSubs.unsubscribe();
    }

    if (typeof this.authSubs != 'undefined') {
      //prevent memory leak when component destroyed
      this.authSubs.unsubscribe();
    }
  }

  convertToInt(val) {
    return parseInt(val);
  }

  propertyRoute(property) {
    return this.helperService.propertyRoute(property);
  }

  removeProp(propID) {
    this.emailBucketService.removeBucketPropByID(propID);
  }

  removeAllProps() {
    this.emailBucketService.removeAllBucketProps();
  }

  // convenience getter for easy access to form fields
  get f() { return this.LeadForm.controls; }

  AddLead(leadData) {
    leadData.type         = 'request_info';
    leadData.property     = [];

    // Set array of bulk properties
    for (let i = 0; i < this.properties.length; i++) {
        this.properties[i];
        let prop = { user_id: this.properties[i].user_id, property_id: this.properties[i].id }
        leadData.property.push(prop);
    }

    this.submitted  = true;

    // Stop here if form is invalid
    if (this.LeadForm.invalid) {
     console.log(this.LeadForm.controls);
     return;
    }

    let user = this.authenticationService.get_currentUserValue();
    if (user) {
      leadData['requested_by'] = user.user.id;
    }

    const url  = AppSettings.API_ENDPOINT + 'lead/add';
    this.helperService.httpPostRequests(url, leadData)
      .then(resp => {
        if (typeof resp != "undefined" && resp != null && typeof resp.error === "undefined" ) {
          this.lead = {};

          // Hide this Modal
          this.closeModal();

          // Remove all props from Email Bucket
          this.removeAllProps();

          Swal.fire('Request Sent', 'Your message has been sent successfully. You will receive a reply directly at your email address.', 'success');

          // Set Submission false
          this.submitted = false;
        } else {
            console.error(resp);
        }
      })
      .catch(error => {
        console.log("error: ",error);

        // Set Submission false
        this.submitted = false;
      });
  }

  closeModal() {
    this.bsModalRef.hide();
  }

}

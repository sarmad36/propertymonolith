import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BulkInquiryModalComponent } from './bulk-inquiry-modal.component';

describe('BulkInquiryModalComponent', () => {
  let component: BulkInquiryModalComponent;
  let fixture: ComponentFixture<BulkInquiryModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BulkInquiryModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BulkInquiryModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit, Input } from '@angular/core';
import { AuthenticationService, AppSettings, HelperService } from '../../../services/_services';
import { OwlCarousel } from 'ngx-owl-carousel';
@Component({
  selector: 'app-related-properties',
  templateUrl: './related-properties.component.html',
  styleUrls: ['./related-properties.component.css']
})
export class RelatedPropertiesComponent implements OnInit {

  @Input() location_id :any = '';
  @Input() property_id :any = '';
  public properties   : any = [];
  public ImagesUrl    : any = AppSettings.IMG_ENDPOINT;
  public mySlideProperties = [];
  public mySlideOptions    = {  dots: false, nav: true, thumbs:false,stagePadding: 10,margin:10,
                                  responsive:{
                                      0:{
                                          items:1
                                      },
                                      650:{
                                          items:2
                                      },
                                      991:{
                                          items:3
                                      },
                                      1200:{
                                          items:4
                                      }
                                  }};

  constructor(
    private helperService: HelperService,
  ) {
  }

  ngOnInit() {
    this.get_RelatedProperties();
  }

  get_RelatedProperties() {
    let data = { property_id: this.property_id, location_id: this.location_id };
    const url  = AppSettings.API_ENDPOINT + 'property/related';

    this.helperService.httpPostRequests(url, data).then(resp => {
      if (typeof resp != "undefined") {
        this.properties = resp;
      }
    })
  }

  convertToInt(val) {
    return parseInt(val);
  }

  propertyRoute(property) {
    return this.helperService.propertyRoute(property);
  }

}

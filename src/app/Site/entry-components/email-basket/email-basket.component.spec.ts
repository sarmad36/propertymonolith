import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmailBasketComponent } from './email-basket.component';

describe('EmailBasketComponent', () => {
  let component: EmailBasketComponent;
  let fixture: ComponentFixture<EmailBasketComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmailBasketComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmailBasketComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

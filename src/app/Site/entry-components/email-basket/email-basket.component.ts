import { Component, OnInit } from '@angular/core';
import { AppSettings, HelperService, EmailBucketService } from '../../../services/_services';

// Bootsteap Components
import { BsModalRef, BsModalService, ModalOptions } from 'ngx-bootstrap/modal';

// Modal Components
import { BulkInquiryModalComponent } from '../bulk-inquiry-modal/bulk-inquiry-modal.component';

@Component({
  selector: 'app-email-basket',
  templateUrl: './email-basket.component.html',
  styleUrls: ['./email-basket.component.css']
})
export class EmailBasketComponent implements OnInit {

  public properties         : any = [];
  public searchParms        : any = {};
  public hideEmailBucket    : any;
  public ImagesUrl          : any = AppSettings.IMG_ENDPOINT;
  public bucketSubs         : any;
  public modalRef           : BsModalRef;

  constructor(
    private emailBucketService    : EmailBucketService,
    private helperService         : HelperService,
    private modalService          : BsModalService,
  ) {
      // Get Email Bucket Toggler
      this.hideEmailBucket = this.helperService.getHideEmailBucket();
  }

  ngOnInit() {
    this.bucketSubs = this.emailBucketService.propertiesS$.subscribe(props => {
      this.properties  = props;
      this.searchParms = this.helperService.getSearchParms();
    });
  }

  ngOnDestroy() {
    if (typeof this.bucketSubs != 'undefined') {
      //prevent memory leak when component destroyed
      this.bucketSubs.unsubscribe();
    }
  }

  toggleEmailBucket(){
    this.hideEmailBucket = !this.hideEmailBucket;

    // Set it Globally for other routes
    this.helperService.setHideEmailBucket(this.hideEmailBucket);
  }

  convertToInt(val) {
    return parseInt(val);
  }

  propertyRoute(property) {
    return this.helperService.propertyRoute(property);
  }

  removeProp(propID) {
    this.emailBucketService.removeBucketPropByID(propID);
  }

  removeAllProps() {
    this.emailBucketService.removeAllBucketProps();
  }

  openBulkEmail() {
    const initialState = {};
    const config: ModalOptions = { class: 'modal-lg af-signup inquiry', initialState };
    this.modalRef = this.modalService.show(BulkInquiryModalComponent, config);
  }

}

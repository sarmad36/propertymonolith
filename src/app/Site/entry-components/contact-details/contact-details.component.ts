import { Component, OnInit } from '@angular/core';
import { AppSettings, HelperService } from '../../../services/_services';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-contact-details',
  templateUrl: './contact-details.component.html',
  styleUrls: ['./contact-details.component.css']
})
export class ContactDetailsComponent implements OnInit {

  public property   : any;
  public propID     : any;
  public userName   : any;
  public contacts   : any = [];

  constructor(
    public bsModalRef    : BsModalRef,
    public helperService : HelperService,
  ) {
  }

  ngOnInit() {
    // Set propertyID
    if(typeof this.property.id != "undefined")
      this.propID = this.property.id;
    else
     this.propID = this.property.p_id;

    // Set Username
    if(typeof this.property.user != "undefined")
      this.userName = this.property.user.first_name + ' ' + this.property.user.last_name;
    else
      this.userName = this.property.first_name + ' ' + this.property.last_name;

    this.getContactDetails();
  }

  getContactDetails() {
    // Call the Api if Numbers aren't available first
    const url  = AppSettings.API_ENDPOINT + 'view-contact-details/' + this.propID;
    this.helperService.httpPostRequests(url, {}).then(resp => {
      this.contacts = resp.contact_details;
    }).catch(error => {
      console.error("getNumbers error:", error);
    });
  }

}

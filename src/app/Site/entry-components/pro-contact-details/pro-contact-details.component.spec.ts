import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProContactDetailsComponent } from './pro-contact-details.component';

describe('ProContactDetailsComponent', () => {
  let component: ProContactDetailsComponent;
  let fixture: ComponentFixture<ProContactDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProContactDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProContactDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

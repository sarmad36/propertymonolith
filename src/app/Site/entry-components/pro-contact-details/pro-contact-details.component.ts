import { Component, OnInit } from '@angular/core';
import { AppSettings, HelperService } from '../../../services/_services';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-pro-contact-details',
  templateUrl: './pro-contact-details.component.html',
  styleUrls: ['./pro-contact-details.component.css']
})
export class ProContactDetailsComponent implements OnInit {

  public pro        : any;
  public proID      : any;
  public proName    : any;

  public phone_number         : any;
  public whatsapp_number      : any;
  public whatsapp_number_code : any;
  public simiAgencies : any;
  public staff : any;

  public contacts   : any = [];

  constructor(
    public bsModalRef    : BsModalRef,
    public helperService : HelperService,
  ) { }

  ngOnInit() {
    // // Set proID
    // if(typeof this.pro.id != "undefined")
    //   this.proID = this.pro.id;
    //
    // console.log("this.pro: ",this.pro);
    // // Set Username
    // this.userName = this.pro.agent_name;

    if (!this.staff && !this.simiAgencies) {
        this.getContactDetails();
    }
    else{
      this.getInnerProfileDetails();
    }
  }

  getContactDetails() {
    let url  = AppSettings.API_ENDPOINT + 'pro/get-contact-details';
    let data = { user_id: this.proID };

    this.helperService.httpPostRequests(url, data).then(resp => {
        if (resp.data) {
            this.contacts = resp.data;
        }
    }).catch(error => {
        console.error("error: ",error);
    })
  }
  getInnerProfileDetails() {
    const data = { proName: this.proName ,phone_number: this.phone_number, whatsapp_number: this.whatsapp_number, whatsapp_number_code: this.whatsapp_number_code};
    console.log("data",data)
    this.contacts = data;
  }
}

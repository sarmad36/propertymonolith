import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { DatePipe } from '@angular/common';
import { DOCUMENT } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app.routing';

// import { FooterComponent } from './Site/includes/footer/footer.component';
// import { HeaderComponent } from './Site/includes/header/header.component';
// Site Includes
import { SiteIncludesModule } from './Site/includes/site-includes.module';

import { PanelNavbarComponent } from './UserPanel/includes/panel-navbar/panel-navbar.component';
import { CommonModule } from '@angular/common';

// Import Layouts
import { SiteLayoutComponent } from './Layouts/site-layout/site-layout.component';
import { UserDashboardLayoutComponent } from './Layouts/user-dashboard-layout/user-dashboard-layout.component';
// import { UserLayoutComponent } from './Layouts/user-layout/user-layout.component';

// Modal Components
import { LoginComponent } from './Site/entry-components/login/login.component';
import { SiginUpOptsComponent } from './Site/entry-components/sigin-up-opts/sigin-up-opts.component';
import { SiginUpComponent } from './Site/entry-components/sigin-up/sigin-up.component';
import { ContactDetailsComponent } from './Site/entry-components/contact-details/contact-details.component';
import { ProContactDetailsComponent } from './Site/entry-components/pro-contact-details/pro-contact-details.component';
import { InquiryModalComponent } from './Site/entry-components/inquiry-modal/inquiry-modal.component';

// App Services
import { AlertComponent } from './services/_components';

// Bootsteap Components
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { ModalModule } from 'ngx-bootstrap/modal';
import { PopoverModule } from 'ngx-bootstrap/popover';

// Social Login Pakages
import { SocialLoginModule, AuthServiceConfig } from "angularx-social-login";
import { GoogleLoginProvider, FacebookLoginProvider, LoginOpt } from "angularx-social-login";


// Custom Pipes
import { ApplicationPipesModule } from './services/pipes/application-pipes.module';

// Import Pages
import { BlueWorldCityComponent } from './Site/pages/blue-world-city/blue-world-city.component';
// import { SkyparkoneComponent } from './Site/pages/skyparkone/skyparkone.component';
// import { HotOfferComponent } from './Site/pages/hot-offer/hot-offer.component';
// import { OverseasBlockComponent } from './Site/pages/overseas-block/overseas-block.component';
// import { PaymentPlanComponent } from './Site/pages/payment-plan/payment-plan.component';
// import { CapitalsmartcityComponent } from './Site/pages/capitalsmartcity/capitalsmartcity.component';
import { PageNotFoundComponent } from './Site/pages/page-not-found/page-not-found.component';


// Social Config Keys
const googleLoginOptions: LoginOpt = {
  scope: 'profile email'
};

let config = new AuthServiceConfig([
  {
    id: GoogleLoginProvider.PROVIDER_ID,
    // provider: new GoogleLoginProvider("462686534378-1bhln6l87a71ulphjlp4qc436ncir1vg.apps.googleusercontent.com")
    provider: new GoogleLoginProvider("1009868765014-nijok3a5dpp24ldnpcbd81tekpp3pjkr.apps.googleusercontent.com", googleLoginOptions) // For Localhost
  },
  {
    id: FacebookLoginProvider.PROVIDER_ID,
    // provider: new FacebookLoginProvider("2348682845447060")
    provider: new FacebookLoginProvider("626742448065864")
  }
]);

export function provideConfig() {
  return config;
}

// import Scroll Spy
import { ScrollSpyModule } from './services/_services/scroll-spy/scroll-spy.module';

@NgModule({
  declarations: [
    AlertComponent,
    AppComponent,
    // FooterComponent,
    // HeaderComponent,
    PanelNavbarComponent,
    SiteLayoutComponent,
    UserDashboardLayoutComponent,
    // UserLayoutComponent,
    LoginComponent,
    SiginUpOptsComponent,
    SiginUpComponent,
    ContactDetailsComponent,
    ProContactDetailsComponent,
    InquiryModalComponent,
    PageNotFoundComponent,

    // Pages
    BlueWorldCityComponent,
    // SkyparkoneComponent,
    // HotOfferComponent,
    // OverseasBlockComponent,
    // PaymentPlanComponent,
    // CapitalsmartcityComponent,
  ],
  imports: [
    BrowserAnimationsModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgbModule,
    RouterModule,
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    AppRoutingModule,
    NgbModule,
    SocialLoginModule,

    // Site Includes
    SiteIncludesModule,

    // Bootsteap Components
    // BsDropdownModule.forRoot(),
    // TooltipModule.forRoot(),
    ModalModule.forRoot(),
    // PopoverModule,
    //
    // Custom Pipes Modules
    ApplicationPipesModule,
    ScrollSpyModule
  ],
  providers: [
    {
      provide: AuthServiceConfig,
      useFactory: provideConfig
    },
    DatePipe,
  ],
  bootstrap: [AppComponent],
  entryComponents: [
    LoginComponent,
    SiginUpOptsComponent,
    SiginUpComponent,
    ContactDetailsComponent,
    ProContactDetailsComponent,
    InquiryModalComponent
  ]
})
export class AppModule { }

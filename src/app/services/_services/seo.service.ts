import { Injectable, Inject  } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';
import { Location } from '@angular/common';
import { DOCUMENT } from '@angular/common';

// import { WINDOW } from '@ng-toolkit/universal'

@Injectable({ providedIn: 'root' })
export class SEOService  {

    constructor(
      private title: Title,
      private meta: Meta,
      private _location  : Location,
      // @Inject(WINDOW) private window: Window,
      @Inject(DOCUMENT) private doc
    ) {
        // By Default set no index
        this.setNoIndex(false);
    }

    updateTitle(title: string) {
      this.title.setTitle(title);
    }

    updateDescription(desc: string) {
      this.meta.updateTag({ name: 'description', content: desc })
    }

    updateOgTitle(title: string) {
      this.meta.updateTag({ property: 'og:title', content: title })
    }

    updateOgDesc(desc: string) {
      this.meta.updateTag({ property: 'og:description', content: desc })
    }

    updateOgUrl(currentHref = "https://gharbaar.com" + this._location['_platformLocation'].pathname) {

      // let  currentHref = this._location['_platformLocation'].protocol + '//' + this._location['_platformLocation'].hostname + this._location['_platformLocation'].pathname;
      // let  currentHref = "https://gharbaar.com" + this._location['_platformLocation'].pathname;
      this.meta.updateTag({ property: 'og:url', content: currentHref })
    }

    updateOgImage(url: string) {
      this.meta.updateTag({ property: 'og:image', content: url })
    }

    updateTwitterTitle(title: string) {
      this.meta.updateTag({ property: 'twitter:title', content: title })
    }

    updateTwitterDesc(desc: string) {
      this.meta.updateTag({ property: 'twitter:description', content: desc })
    }

    updateCanonicalUrl(currentHref = "https://gharbaar.com" + this._location['_platformLocation'].pathname) {
      // console.log("currentHref", currentHref)
      // let  currentHref = this._location['_platformLocation'].protocol + '//' + this._location['_platformLocation'].hostname + this._location['_platformLocation'].pathname;
      // let currentHref =  "https://gharbaar.com" + this._location['_platformLocation'].pathname;
      // console.log("updateCanonicalUrl url: ",url)
      if(typeof $ != "undefined")
        $('link[rel="canonical"]').attr('href', currentHref);
      // // let link: HTMLLinkElement = this.doc.createElement('link');
      // // link.setAttribute('rel', 'canonical');
      // // this.doc.head.appendChild(link);
      // // link.setAttribute('href', url);

      // // Uncomment this for Universal
      // console.log("this.doc: ",this.doc)
      // console.log("this.doc: ",this.doc.getElementsByTagName("link"))
      // console.log("this.doc: ",this.doc.getElementById("canonicalLink"))
      // // let link: HTMLLinkElement = this.doc.createElement('link');
      let link: HTMLLinkElement = this.doc.getElementById('canonicalLink');
      // link.setAttribute('rel', 'canonical');
      // this.doc.head.appendChild(link);
      link.setAttribute('href', currentHref);
    }

    setNoIndex(bool) {
      if(bool)
        this.meta.addTag({ name: 'robots', content: 'noindex' });
      else
        this.meta.removeTag('name="robots"');
    }

}

import { Directive, Injectable, Input, EventEmitter, Output, ElementRef, HostListener } from '@angular/core';

@Directive({
    selector: '[scrollSpy2]'
})
export class ScrollSpyDirective2 {
    @Input() public spiedTags = [];
    @Output() public sectionChange = new EventEmitter<string>();
    private currentSection: string;
    public  screenOffsetSolver : any = 0 ;

    constructor(private _el: ElementRef) {

        if (typeof window != "undefined") {
            if (window.innerWidth < 768){
                this.screenOffsetSolver = 450;
            }
            else if (window.innerWidth < 1300 && window.innerWidth > 768){
                this.screenOffsetSolver = 580;
            }
            else{
                this.screenOffsetSolver = 750;
            }
        }
    }

    @HostListener('window:scroll', ['$event'])
    onScroll(event: any) {
        let currentSection: string;
        const children = this._el.nativeElement.children;
        const scrollTop = window.pageYOffset;
        // const parentOffset = event.target.offsetTop;
        const parentOffset = 100;


        for (let i = 0; i < children.length; i++) {
            const element = children[i];
            if (this.spiedTags.some(spiedTag => spiedTag === element.tagName)) {
                if ((element.offsetTop - parentOffset + this.screenOffsetSolver) <= scrollTop) {
                    // console.log("element: ",element)
                    // console.log("element.offsetTop - parentOffset: ",element.offsetTop - parentOffset)
                    // console.log("screenOffsetSolver ",this.screenOffsetSolver)
                    // console.log("window scrollTop: ",scrollTop)

                    currentSection = element.id;
                    // console.log("currentSection: ",currentSection)
                }
            }
        }
        // console.log("**** this.currentSection: ", this.currentSection);
        // console.log("**** currentSection: ", currentSection);
        if (currentSection !== this.currentSection) {
            this.currentSection = currentSection;
            this.sectionChange.emit(this.currentSection);
        }
    }

}

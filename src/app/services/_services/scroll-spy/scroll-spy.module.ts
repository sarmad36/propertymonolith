import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ScrollSpyDirective2 } from './scroll-spy.directive2';


@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    ScrollSpyDirective2
  ],
  exports: [
    ScrollSpyDirective2
  ]
})
export class ScrollSpyModule { }

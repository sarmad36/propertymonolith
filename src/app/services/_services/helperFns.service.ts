import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthenticationService } from './authentication.service';
import { AppSettings } from './app.setting';
import { BehaviorSubject, Observable, throwError, Subject } from 'rxjs';
import { catchError, retry, finalize, tap, map, takeUntil } from 'rxjs/operators';


@Injectable({ providedIn: 'root' })
export class HelperService {
    public corsHeaders: any = new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept': 'application/json',
    });;

    public searchParms       : any;
    public searchTypes_Seq   : any;
    public agencySearchParms : any;
    public investSearchParms : any;

    // Global For hiding email bucket
    public hideEmailBucket : any = true;

    // Https Params
    protected ngUnsubscribe: Subject<void> = new Subject<void>();
    public httpLoading     = false;

    constructor(
      private http: HttpClient,
      private authenticationService: AuthenticationService,
    ) {
        // Set Params OnLoad
        if(typeof localStorage != "undefined") {
          this.searchParms       = JSON.parse(localStorage.getItem('searchParms'));
          this.agencySearchParms = JSON.parse(localStorage.getItem('agencySearchParms'));
          this.investSearchParms = JSON.parse(localStorage.getItem('investSearchParms'));
        }

        let subscription = this.authenticationService.currentUserS$.subscribe(user => {
          if (user) {
              this.corsHeaders = {
                    headers: new HttpHeaders()
                      .set('Content-Type',  'application/json')
                      .set('Accept',  'application/json')
                      .set('Authorization',  `Bearer ${user.access_token}`)
                  }
          } else {
              this.corsHeaders = new HttpHeaders({
                                        'Content-Type': 'application/json',
                                        'Accept': 'application/json',
                                      });;
          }
        });
    }

    public getIPAddress(): Promise<any> {
      // return this.http.get("");

      // Set loader true
      this.httpLoading = true;

      return this.http.get('https://api.ipify.org/?format=json')
          .pipe( takeUntil(this.ngUnsubscribe) )
          .toPromise()
          .then( resp => {
              // Set loader false
              this.httpLoading = false;

              return resp;
              // console.log("resp: ",resp);
          })
          .catch(error => {
              // Set loader false
              this.httpLoading = false;
              console.log("helperFunc error: ",error);

              // Show Error Msg
              if(typeof error.error != "undefined") {
                  if(error.error.message == "Unauthenticated.") {
                    this.authenticationService.logout();
                  }

                  throw error;
              } else {
                  throw "Something went wrong. Please try again.";
              }
          });
    }

    public redirectTo404() {
      window.location.replace(AppSettings.notFoundUrl);
    }

    public setSearchTypes_Seq(seq) {
      this.searchTypes_Seq = seq;
    }

    public getSearchTypes_Seq() {
      return this.searchTypes_Seq;
    }

    public setHideEmailBucket(bool: any) {
      this.hideEmailBucket = bool;
    }

    public getHideEmailBucket() {
      return this.hideEmailBucket;
    }

    public setSearchParms(params) {
      if(typeof localStorage != "undefined") {
        localStorage.setItem('searchParms', JSON.stringify(params));
        this.searchParms = params;
      }
    }

    public getSearchParms() {
      return this.searchParms;
    }

    public setAgencySearchParms(params) {
      if(typeof localStorage != "undefined") {
        localStorage.setItem('agencySearchParms', JSON.stringify(params));
        this.agencySearchParms = params;
      }
    }

    public getAgencySearchParms() {
      return this.agencySearchParms;
    }

    public setInvestSearchParms(params) {
      if(typeof localStorage != "undefined") {
        localStorage.setItem('investSearchParms', JSON.stringify(params));
        this.investSearchParms = params;
      }
    }

    public getInvestSearchParms() {
      return this.investSearchParms;
    }

    getLocURL(location, city,  byCity, purpose = "sale", type = Object.keys(AppSettings.searchTypes)[0], sub_type = '') {
      // Mrege if sub type is also set
      let type_sub_type = type;
      if(sub_type != "all" && sub_type != "")
        type_sub_type += '-' + sub_type.replace(/ /g, "_");

      if(byCity) {
          let url =  '/' + purpose + '/' + type_sub_type + '/' + city.name.replace(/ /g, "-")  + "-" + city.id + "-1"; // 1 in params specify its a City
          return url;
      } else {
          // let url = '/' + purpose + '/' + type_sub_type + '/' + location.slug.replace(/ /g, "_").replace(/-/g, "_").replace(/[/\\*]/g, "_") + "-" + location.id + "-2"; // 2 in params specify its a sub location;
          let url = '/' + purpose + '/' + type_sub_type + '/' + location.slug.replace(/ /g, "-").replace(/_/g, "-").replace(/[/\\*]/g, "-") + "-" + location.id + "-2"; // 2 in params specify its a sub location;
          return url;
      }
    }

    getLocURL_for_Filter(location, city,  byCity, filter, purpose = "sale", type = Object.keys(AppSettings.searchTypes)[0], sub_type = '') {
      // Mrege if sub type is also set
      let type_sub_type = type;
      if(sub_type != "all" && sub_type != "")
        type_sub_type += '-' + sub_type.replace(/ /g, "_");

      if(byCity) {
          let url =  '/' + purpose + '/' + filter + '/' + type_sub_type + '/' + city.name.replace(/ /g, "-")  + "-" + city.id + "-1"; // 1 in params specify its a City
          return url;
      } else {
          let url = '/' + purpose + '/' + filter + '/' + type_sub_type + '/' + location.slug.replace(/ /g, "-").replace(/_/g, "-").replace(/[/\\*]/g, "-") + "-" + location.id + "-2"; // 2 in params specify its a sub location;
          return url;
      }
    }

    renderLocURL(slug) {
      slug = slug.split("-");
      return { id: slug[slug.length - 2], name: slug[0], searchBy: slug[slug.length - 1] };
    }

    get_FindPro_URL(location, city,  byCity, category) {
      if(byCity) {
          let url =  '/find-pro/' + category + '/' + city.name.replace(/ /g, "-")  + "-" + city.id + "-1"; // 1 in params specify its a City
          return url;
      } else {
          let url = '/find-pro/' + category + '/' + location.slug.replace(/ /g, "-").replace(/_/g, "-").replace(/[/\\*]/g, "-") + "-" + location.id + "-2"; // 2 in params specify its a sub location;
          return url;
      }
    }

    render_FindPro_URL(slug) {
      slug = slug.split("-");
      return { id: slug[slug.length - 2], name: slug[0], searchBy: slug[slug.length - 1] };
    }

    getParentLocations(id): Promise<any> {
      const url = AppSettings.API_ENDPOINT + "locations/get-parent-locations-by-sublocation?sub_loc=" + id;
      return this.httpGetRequests(url).then(resp => {

        if (typeof resp.p_loc != "undefined") {
            let searchedArea = resp.p_loc.slice()[0];

            let parentLocs  = [...resp.p_loc.slice().reverse()];

            // Remove the Last parent as its the same as searched area
            parentLocs.pop();

            return {searchedArea: searchedArea, parentLocs: parentLocs};
        }
      }).catch(error => {
          console.log("error: ",error);
      })
    }

    getSublocalities(geocodeResults): Promise<any> {
      let sublocalities = [];
      let cityFound     = false;

      console.log("geocode: ", geocodeResults);

      for (let i = 0; i < geocodeResults.length; i++) {
        let route   = geocodeResults[i];
        let element = geocodeResults[i];
        let resAC   = geocodeResults[i].address_components;

        if(element.types.indexOf('sublocality') != -1) {
          for (let j = 0; j < resAC.length; j++) {
            let component = resAC[j];
            if(component.types.indexOf('sublocality') != -1) {

              // Remove If area already exits
              sublocalities = this.checkIfLocationAlreadyExists(sublocalities, component.long_name);

              let loc = {
                          name     : component.long_name,
                          // name     : element.formatted_address.split(',')[0],
                          place_id : element.place_id,
                          lat      : element.geometry.location.lat(),
                          lng      : element.geometry.location.lng(),
                          city     : 0
                        }
              sublocalities.push(loc);
              break;
            }
          }
        }
        else if(element.types.indexOf('neighborhood') != -1) {
          for (let j = 0; j < resAC.length; j++) {
            let component = resAC[j];
            if(component.types.indexOf('sublocality') != -1) {

              // Remove If area already exits
              sublocalities = this.checkIfLocationAlreadyExists(sublocalities, component.long_name);

              let loc = {
                          name     : component.long_name,
                          // name     : element.formatted_address.split(',')[0],
                          place_id : element.place_id,
                          lat      : element.geometry.location.lat(),
                          lng      : element.geometry.location.lng(),
                          city     : 0
                        }
              sublocalities.push(loc);
              break;
            }
          }
        }
        // else if(element.types.indexOf('locality') != -1) {
        else if(route.types.indexOf("administrative_area_level_3") != -1 && !cityFound) {
          let component = resAC[0];

          // Remove If area already exits
          sublocalities = this.checkIfLocationAlreadyExists(sublocalities, component.long_name);

          let loc = {
                      name     : component.long_name,
                      // name     : element.formatted_address.split(',')[0],
                      place_id : element.place_id,
                      lat      : element.geometry.location.lat(),
                      lng      : element.geometry.location.lng(),
                      city     : 1
                    }
          sublocalities.push(loc);

          // Set City Found
          cityFound = true;
        }
      }

      // Incase no location was found use 1st route address_components
      if(sublocalities.length <= 1) {
        sublocalities = [];
        cityFound     = false;

        let address_components = geocodeResults[0].address_components
        for (let i = 0; i < address_components.length; i++) {
          let route   = address_components[i];
          let element = geocodeResults[0];

          if(route.types.indexOf('sublocality') != -1) {

            // Remove If area already exits
            sublocalities = this.checkIfLocationAlreadyExists(sublocalities, route.long_name);

            let loc = {
                        name     : route.long_name,
                        // name     : element.formatted_address.split(',')[0],
                        place_id : element.place_id,
                        lat      : element.geometry.location.lat(),
                        lng      : element.geometry.location.lng(),
                        city     : 0
                      }
            sublocalities.push(loc);
          }
          // else if(route.types.indexOf('locality') != -1) {
          else if(route.types.indexOf("administrative_area_level_3") != -1 && !cityFound) {

            // Remove If area already exits
            sublocalities = this.checkIfLocationAlreadyExists(sublocalities, route.long_name);

            let loc = {
                        name     : route.long_name,
                        // name     : element.formatted_address.split(',')[0],
                        place_id : element.place_id,
                        lat      : element.geometry.location.lat(),
                        lng      : element.geometry.location.lng(),
                        city     : 1
                      }
            sublocalities.push(loc);

            // Set City Found
            cityFound = true;
          }
        }
      }

      // Incase no location was found use 1st route
      if(sublocalities.length <= 1) {
        sublocalities = [];
        cityFound     = false;

        // Remove If area already exits
        sublocalities = this.checkIfLocationAlreadyExists(sublocalities, geocodeResults[0].address_components[0].long_name);

        let loc = {
                    name     : geocodeResults[0].address_components[0].long_name,
                    // name     : element.formatted_address.split(',')[0],
                    place_id : geocodeResults[0].place_id,
                    lat      : geocodeResults[0].geometry.location.lat(),
                    lng      : geocodeResults[0].geometry.location.lng(),
                    city     : 0
                  }
        sublocalities.push(loc);

        for (let i = 0; i < geocodeResults[0].address_components.length; i++) {
          let element = geocodeResults[0].address_components[i];
          if(element.types.indexOf('sublocality') != -1) {

            // Remove If area already exits
            sublocalities = this.checkIfLocationAlreadyExists(sublocalities, element.long_name);

            let loc = {
                        name     : element.long_name,
                        place_id : geocodeResults[0].place_id,
                        lat      : geocodeResults[0].geometry.location.lat(),
                        lng      : geocodeResults[0].geometry.location.lng(),
                        city     : 0
                      }
            sublocalities.push(loc);
          } else if(element.types.indexOf('neighborhood') != -1) {

            // Remove If area already exits
            sublocalities = this.checkIfLocationAlreadyExists(sublocalities, element.long_name);

            let loc = {
                        name     : element.long_name,
                        place_id : geocodeResults[0].place_id,
                        lat      : geocodeResults[0].geometry.location.lat(),
                        lng      : geocodeResults[0].geometry.location.lng(),
                        city     : 0
                      }
            sublocalities.push(loc);
          }
          // else if(element.types.indexOf('locality') != -1) {
          else if(element.types.indexOf("administrative_area_level_3") != -1 && !cityFound) {

            // Remove If area already exits
            sublocalities = this.checkIfLocationAlreadyExists(sublocalities, element.long_name);

            let loc = {
                        name     : element.long_name,
                        // name     : element.formatted_address.split(',')[0],
                        // place_id : element.place_id,
                        // lat      : element.geometry.location.lat(),
                        // lng      : element.geometry.location.lng(),
                        city     : 1
                      }
            sublocalities.push(loc);

            // Set City Found
            cityFound = true;
          }
        }
      }

      // Request Server to get exact Area
      let url  = AppSettings.API_ENDPOINT + 'locations/get-all-locations-with-city';
      let data = { locations: sublocalities };
      return this.httpPostRequests(url, data).then(resp => {
          return resp;
      }).catch(error => {
          return false;
      })
    }

    checkIfLocationAlreadyExists(sublocalities, locationName) {
      for (let index = 0; index < sublocalities.length; index++) {
        let loc = sublocalities[index];
        if(loc.name == locationName) {
          sublocalities.splice(index, 1);
        }
      }
      return sublocalities;
    }

    replaceString(s) {
      return s.toLowerCase().replace(/ /g, '-').replace(/_/g, '-').replace(/[/\\*]/g, "-").replace(/---/g, '-');
    }

    propertyRoute(property) {
      let propID;
      if(typeof property.p_id != "undefined")
        propID = property.p_id;
      else
        propID = property.id;
      return '/property/' + this.replaceString(property.location_name) + '-' + this.replaceString(property.title) + '-' + propID;
    }

    renderPropertyURL(slug) {
      slug = slug.split("-");
      return { id: slug[slug.length - 1], name: slug[0] };
    }

    propertyEditRoute(property) {
      let propID;
      if(typeof property.p_id != "undefined")
        propID = property.p_id;
      else
        propID = property.id;

      return '/dashboard/edit-property/' + this.replaceString(property.location_name) + '-' + this.replaceString(property.title) + '-' + propID;
    }

    renderEditPropertyURL(slug) {
      slug = slug.split("-");
      return { id: slug[slug.length - 1], name: slug[0] };
    }

    investRoute(city) {
      let routeLink =  '/projects/' + city.name.replace(/ /g, "_") + "-" + city.id + "-1"; // 1 in params specify its a City

      return routeLink;
    }

    renderInvestURL(slug) {
      slug = slug.split("-");
      return { id: slug[slug.length - 1], name: slug[0] };
    }

    agencyProfileRoute(agent) {
      let agentID;
      if(typeof agent.id != "undefined")
        agentID = agent.id;
      else
        agentID = agent.agency.id;
      return '/agent/' + this.replaceString(agent.agency.city) + '-' + this.replaceString(agent.agency.name) + '-' + agentID;
    }


    renderAgencyProfileURL(slug) {
      slug = slug.split("-");
      return { id: slug[slug.length - 1], name: slug[0] };
    }

    proProfileRoute(pro) {
      let proID;
      if(typeof pro.id != "undefined")
        proID = pro.id;

      let route = '/' + AppSettings.ProCategoriesRoutes[pro.category].route + '/' + this.replaceString(pro.city) + '-' + this.replaceString(pro.name) + '-' + proID;
      return route;
    }

    renderProProfileURL(slug) {
      let proCategory = slug.split("/")[1];
      let categoryID;
      for(var key in AppSettings.ProCategoriesRoutes) {
        if(AppSettings.ProCategoriesRoutes[key].route === proCategory) {
          categoryID = key;
        }
      }

      // Split slug to get pro id
      slug = slug.split("-");
      return { id: slug[slug.length - 1], category: categoryID };
    }

    get_blog_URL(blog) {
      return '/blog/' + blog.title.replace(/ /g, "-").replace(/---/g, "-");
      // return '/blog/' + blog.title.replace(/ /g, "-").replace(/---/g, "-") + "-" + blog.id;
    }

    render_blog_URL(slug) {
      slug = slug.split("-");
      return { id: parseInt(slug[slug.length - 1]) };
    }

    get_author_URL(blogger) {
      return '/author/' + blogger.blogger.name.replace(/ /g, "-") + "-" + blogger.blogger_id;
    }

    render_author_URL(slug) {
      slug = slug.split("-");
      return { id: parseInt(slug[slug.length - 1]) };
    }

    get_category_URL(category) {
    return '/category/' + category.name.replace(/ /g, "-") + "-" + category.id;
    }

    render_category_URL(slug) {
    slug = slug.split("-");
    return { id: parseInt(slug[slug.length - 1]) };
    }

    getCORS_Headers() {
      return this.corsHeaders;
    }

    //////////////////////////////////////////
    /********* HTTP Requests Fns ***********/
    ////////////////////////////////////////
    httpGetRequests(url): Promise<any> {
        // Set loader true
        this.httpLoading = true;

        return this.http.get(url, this.corsHeaders)
            .pipe( takeUntil(this.ngUnsubscribe) )
            .toPromise()
            .then( resp => {
                // Set loader false
                this.httpLoading = false;

                return resp;
                // console.log("resp: ",resp);
            })
            .catch(error => {
                // Set loader false
                this.httpLoading = false;
                console.log("helperFunc error: ",error);

                // Show Error Msg
                if(typeof error.error != "undefined") {
                    if(error.error.message == "Unauthenticated.") {
                      this.authenticationService.logout();
                    }

                    throw error;
                } else {
                    throw "Something went wrong. Please try again.";
                }
            });
      }

      httpPostRequests(url, data): Promise<any> {
          // Set loader true
          this.httpLoading = true;

          return this.http.post(url, data, this.corsHeaders)
              .pipe( takeUntil(this.ngUnsubscribe) )
              .toPromise()
              .then( resp => {
                  // Set loader false
                  this.httpLoading = false;
                  return resp;
              })
              .catch(error => {
                  // Set loader false
                  this.httpLoading = false;
                  console.log("error: ",error);

                  // Show Error Msg
                  if(typeof error.error != "undefined") {
                      if(error.error.message == "Unauthenticated.") {
                        this.authenticationService.logout();
                      }

                      throw error;
                  } else {
                      throw "Something went wrong. Please try again.";
                  }
              });
        }

        httpPutRequests(url, data): Promise<any> {
            // Set loader true
            this.httpLoading = true;

            return this.http.put(url, data, this.corsHeaders)
                .pipe( takeUntil(this.ngUnsubscribe) )
                .toPromise()
                .then( resp => {
                    // Set loader false
                    this.httpLoading = false;
                    return resp;
                })
                .catch(error => {
                    // Set loader false
                    this.httpLoading = false;
                    console.log("error: ",error);

                    // Show Error Msg
                    if(typeof error.error != "undefined") {
                        if(error.error.message == "Unauthenticated.") {
                          this.authenticationService.logout();
                        }

                        throw error;
                    } else {
                        throw "Something went wrong. Please try again.";
                    }
                });
          }

      httpDeleteRequests(url): Promise<any> {
          // Set loader true
          this.httpLoading = true;

          return this.http.delete(url, this.corsHeaders)
              .pipe( takeUntil(this.ngUnsubscribe) )
              .toPromise()
              .then( resp => {
                  // Set loader false
                  this.httpLoading = false;

                  console.log("resp: ",resp);
                  return resp;
              })
              .catch(error => {
                  // Set loader false
                  this.httpLoading = false;

                  console.log("error: ",error);

                  // Show Error Msg
                  if(typeof error.error != "undefined") {
                      if(error.error.message == "Unauthenticated.") {
                        this.authenticationService.logout();
                      }

                      throw error;
                  } else {
                      throw "Something went wrong. Please try again.";
                  }
              });
        }
}

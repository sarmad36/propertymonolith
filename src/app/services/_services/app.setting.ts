export class AppSettings {
  // public static API_ENDPOINT    = 'http://192.168.18.30:8000/api/';
  // public static TEMP_IMAGES     = 'http://192.168.18.30:8000/uploads/temp/';
  public static API_ENDPOINT    = 'https://api-live.gharbaar.com/api/';
  public static TEMP_IMAGES     = 'https://api-live.gharbaar.com/uploads/temp/';
  // public static IMG_ENDPOINT    = 'https://dxrixtte8m5bi.cloudfront.net/';
  public static IMG_ENDPOINT    = 'https://gharbaar-uploads.s3.ap-south-1.amazonaws.com/';
  // public static GOOGLE_MAPS_KEY = 'AIzaSyDkG81UvqVdgrc5IhMuq4YkMOW2utyD9rQ';
  public static GOOGLE_MAPS_KEY = 'AIzaSyDkwqsIbqWQR3ZH3n9WEfJ8YDdgCFDLZsg';
  public static AWS_ASSETS      = 'https://gharbaar-assets.s3.amazonaws.com/';

  public static notFoundUrl     = "404";

  public static purpose  = [
    { name : "Buy",   value: 'sale'},
    { name : "Rent",  value: 'rent'}
  ];

  public static cities  = [
    {"id": 11, "name" : "Islamabad"},
    {"id": 5, "name" : "Rawalpindi"}
  ];
  public static allCities  = [
    {"id": 0, "name" : "All"},
    {"id": 11, "name" : "Islamabad"},
    {"id": 5, "name" : "Rawalpindi"}
  ];
  // public static projects  = [
  //   {"id": 1, "name" : "Blue World City", city_id: 5 },
  //   {"id": 2, "name" : "SkyParkOne", city_id: 11 },
  //   {"id": 3, "name" : "Capital Smart City", city_id: 5 }
  // ];

  public static projects  = {
    0: [
      {"id": 1, "name" : "Blue World City",    route: '/blue-world-city',    city_id: 5 },
      {"id": 2, "name" : "SkyParkOne",         route: '/skyparkone',         city_id: 11 },
      {"id": 3, "name" : "Capital Smart City", route: '/capital-smart-city', city_id: 5 }
    ],
    5: [
      {"id": 1, "name" : "Blue World City",    route: '/blue-world-city'},
      {"id": 3, "name" : "Capital Smart City", route: '/capital-smart-city' }
    ],
    11: [
      {"id": 2, "name" : "SkyParkOne",  route: '/skyparkone' },
    ]
  };

  public static projectsLocs = {
                                  11 : [ 'Gulberg Greens' ],
                                  5  : [ 'Chakri Road' ]
                                };

  public static propPackages  = {
    1: "Standard",
    2: "Silver",
    3: "Gold",
  };

  public static searchBeds  = [
    { value: 0, name: 'All' },
    { value: 1, name: 1 },
    { value: 2, name: 2 },
    { value: 3, name: 3 },
    { value: 4, name: 4 },
    { value: 5, name: 5 },
    { value: 6, name: 6 },
    { value: 7, name: 7 },
    { value: 8, name: 8 },
    { value: 9, name: 9 },
    { value: 10, name: 10 },
  ];

  public static searchTypes  = {
    Homes: [
      // Homes Types
      { name: 'House',              value: 'House',           type: 'Homes' },
      { name: 'Flat',               value: 'Flat',            type: 'Homes' },
      { name: 'Upper Portion',      value: 'Upper Portion',   type: 'Homes' },
      { name: 'Lower Portion',      value: 'Lower Portion',   type: 'Homes' },
      { name: 'Farm House',         value: 'Farm House',      type: 'Homes' },
      { name: 'Room',               value: 'Room',            type: 'Homes' },
      { name: 'Penthouse',          value: 'Penthouse',       type: 'Homes' },
    ],

    Plots: [
      // Plot Types
      { name: 'Residential Plot',   value: 'Residential Plot',   type: 'Plots' },
      { name: 'Commercial Plot',    value: 'Commercial Plot',    type: 'Plots' },
      { name: 'Agricultural Land',  value: 'Agricultural Land',  type: 'Plots' },
      { name: 'Industrial Land',    value: 'Industrial Land',    type: 'Plots' },
      { name: 'Plot File',          value: 'Plot File',          type: 'Plots' },
      { name: 'Plot Form',          value: 'Plot Form',          type: 'Plots' },
    ],

    Commercial: [
      // Commercial Types
      { name: 'Office',             value: 'Office',       type: 'Commercial' },
      { name: 'Shop',               value: 'Shop',         type: 'Commercial' },
      { name: 'Warehouse',          value: 'Warehouse',    type: 'Commercial' },
      { name: 'Factory',            value: 'Factory',      type: 'Commercial' },
      { name: 'Building',           value: 'Building',     type: 'Commercial' },
      { name: 'Other',              value: 'Other',        type: 'Commercial' }
    ]
  };

  public static searchPrices    = [ 500000, 1000000, 2000000, 3500000, 5000000, 6500000, 8000000, 10000000, 12500000, 15000000,
    17500000, 20000000, 25000000, 30000000, 40000000, 50000000, 75000000, 100000000,
    250000000, 500000000, 1000000000, 5000000000
  ];

  public static searchUnits  = [
    { unit: 'ftsq',  name: 'Ft²' },
    // { unit: 'yard',  name: 'Yd²' },
    // { unit: 'meter', name: 'M²' },
    { unit: 'marla', name: 'Marla' },
    { unit: 'kanal', name: 'Kanal' },
  ];

  public static searchAreas  = {
    ftsq  : [ 450, 675, 1125, 1800, 2250, 3375, 4500, 6750, 9000, 11250 ],
    yard  : [ 50, 60, 70, 80, 100, 120, 150, 200, 250, 300, 350, 400, 450, 500, 1000, 2000, 4000 ],
    meter : [ 50, 80, 130, 200, 250, 380, 510, 760, 1000, 1300, 1900, 2500, 3800, 5100, 6300, 13000, 19000, 25000, 51000 ],
    marla : [ 2, 3, 5, 8, 10, 15, 20, 30, 40, 50 ],
    kanal : [ 1, 2, 3, 4, 5, 6, 7, 8, 10, 12, 15, 20, 30, 50, 100 ]
  };

  public static agencyTypes  = [
    { name: 'All',       value: 'all', },
    { name: 'Titanium',  value: 3, },
    { name: 'Featured',  value: 2, },
    { name: 'Normal',    value: 1, },
  ];

  public static ProCategories  = [
    { name: 'Any',                    value: 'any' , slug:'any' },
    { name: 'Agencies',               value: '1'   , slug:'agency'  },
    { name: 'Interior Designers',     value: '2'   , slug:'interior-designers' },
    { name: 'Architects',             value: '3'   , slug:'architects'  },
    { name: 'Contractors & Builders', value: '4'   , slug:'contractors-builders'  },
    { name: '3D Animators',           value: '5'   , slug:'3d-animators'  },
    { name: 'Home Insurance',         value: '6'   , slug:'home-insurance'   },
    { name: 'Home Automation',        value: '7'   , slug:'home-automation'  },
    { name: 'Home Security',          value: '8'   , slug:'home-security'  },
  ];

  public static ProCategoriesRoutes  = {
    1: { route: 'agent',               name: 'Agency' },
    2: { route: 'interior-designer',   name: 'Interior Designer' },
    3: { route: 'architect',           name: 'Architect' },
    4: { route: 'contractors-builder', name: 'Contractor & Builder' },
    5: { route: '3d-animator',         name: '3D Animator' },
    6: { route: 'home-insurance',      name: 'Home Insurance' },
    7: { route: 'home-automation',     name: 'Home Automation' },
    8: { route: 'home-security',       name: 'Home Security' },
  };

  public static ProjectCategories = [
    { name: 'All',                    value: 'all' , slug:'All' },
    { name: 'Commercial projects',    value: '1'   , slug:'commercial-projects' },
    { name: 'Housing projects',       value: '2'   , slug:'housing-projects'  },
  ];

  // public static ProCategories  = [
  //   { name: 'Any',                    value: 'any' },
  //   { name: 'Agencies',               value: 'agency'},
  //   { name: 'Interior Designers',     value: 'interior-designers' },
  //   { name: 'Architects',             value: 'architects' },
  //   { name: 'Contractors & Builders', value: 'contractors-builders' },
  //   { name: '3D Animators',           value: '3d-animators' },
  //   { name: 'Home Insurance',         value: 'home-insurance' },
  //   { name: 'Home Automation',        value: 'home-automation' },
  //   { name: 'Home Security',          value: 'home-security' },
  // ];

  public static citiesDropDown = [
    'Karachi','Lahore','Faisalabad','Serai','Rawalpindi','Multan','Gujranwala','Hyderabad City','Peshawar','Abbottabad','Islamabad','Quetta',
    'Bannu','Bahawalpur','Sargodha','Sialkot City','Sukkur','Larkana','Sheikhupura','Mirpur Khas','Rahamyar Khan','Kohat','Jhang Sadr','Gujrat',
    'Bardar','Kasar','Dera Ghazi Khan','Masawala','Nawabshah','Okara','Gilgit','Chiniot','Sadiqabad','Turbat','Dera Ismail Khan','Chaman','Zhob',
    'Mehra','Parachinar','Gwadar','Kundian','Shahdad Kot','Harapur','Matiari','Dera Allahyar','Lodhran','Batgram','Thatta','Bagh','Badan','Mansehra',
    'Ziarat','Muzaffargarh','Tando Allahyar','Dera Murad Jamali','Karak','Mardan','Uthal','Nankana Sahib','Barkhan','Hafizabad','Kotli','Loralai','Dera Bugti',
    'Jhang City','Sahawal','Sanghar','Pakpattan','Chakwal','Khushab','Ghotki','Kohlu','Khuzdar','Awaran','Nowshera','Charsadda','Qila Abdullah','Bahawalnagar',
    'Dadu','Alaabad','Lakki Marwat','Chilas','Pishin','Tank','Chitral','Qila Saifullah','Shikarpur','Panjgar','Mastung','Kalat','Gandava','Khanewal','Narowal',
    'Khairpur','Malakand','Vihari','Saidu Sharif','Jhelum','Mandi Bahauddan','Bhakkar','Toba Tek Singh','Jamshoro','Kharan','Umarkot','Hangu','Timargara','Gakuch',
    'Jacobabad','Alparai','Mianwali','Masa Khel Bazar','Naushahro Faroz','New Mirpur','Daggar','Eidgah','Sibi','Dalbandan','Rajanpur','Leiah','Upper Dir',
    'Tando Muhammad Khan','Attock City','Rawala Kot','Swabi','Kandhkot','Dasu','Athmuqam'
  ];

  public static country_list = ["Pakistan","Afghanistan","Albania","Algeria","Andorra","Angola","Anguilla","Antigua and Barbuda","Argentina","Armenia","Aruba","Australia","Austria","Azerbaijan","Bahamas","Bahrain","Bangladesh","Barbados","Belarus","Belgium","Belize","Benin","Bermuda","Bhutan","Bolivia","Bosnia and Herzegovina","Botswana","Brazil","British Virgin Islands","Brunei","Bulgaria","Burkina Faso","Burundi","Cambodia","Cameroon","Cape Verde","Cayman Islands","Chad","Chile","China","Colombia","Congo","Cook Islands","Costa Rica","Cote D Ivoire","Croatia","Cruise Ship","Cuba","Cyprus","Czech Republic","Denmark","Djibouti","Dominica","Dominican Republic","Ecuador","Egypt","El Salvador","Equatorial Guinea","Estonia","Ethiopia","Falkland Islands","Faroe Islands","Fiji","Finland","France","French Polynesia","French West Indies","Gabon","Gambia","Georgia","Germany","Ghana","Gibraltar","Greece","Greenland","Grenada","Guam","Guatemala","Guernsey","Guinea","Guinea Bissau","Guyana","Haiti","Honduras","Hong Kong","Hungary","Iceland","India","Indonesia","Iran","Iraq","Ireland","Isle of Man","Israel","Italy","Jamaica","Japan","Jersey","Jordan","Kazakhstan","Kenya","Kuwait","Kyrgyz Republic","Laos","Latvia","Lebanon","Lesotho","Liberia","Libya","Liechtenstein","Lithuania","Luxembourg","Macau","Macedonia","Madagascar","Malawi","Malaysia","Maldives","Mali","Malta","Mauritania","Mauritius","Mexico","Moldova","Monaco","Mongolia","Montenegro","Montserrat","Morocco","Mozambique","Namibia","Nepal","Netherlands","Netherlands Antilles","New Caledonia","New Zealand","Nicaragua","Niger","Nigeria","Norway","Oman","Palestine","Panama","Papua New Guinea","Paraguay","Peru","Philippines","Poland","Portugal","Puerto Rico","Qatar","Reunion","Romania","Russia","Rwanda","Saint Pierre and Miquelon","Samoa","San Marino","Satellite","Saudi Arabia","Senegal","Serbia","Seychelles","Sierra Leone","Singapore","Slovakia","Slovenia","South Africa","South Korea","Spain","Sri Lanka","St Kitts and Nevis","St Lucia","St Vincent","St. Lucia","Sudan","Suriname","Swaziland","Sweden","Switzerland","Syria","Taiwan","Tajikistan","Tanzania","Thailand","Timor L'Este","Togo","Tonga","Trinidad and Tobago","Tunisia","Turkey","Turkmenistan","Turks and Caicos","Uganda","Ukraine","United Arab Emirates","United Kingdom","Uruguay","Uzbekistan","Venezuela","Vietnam","Virgin Islands (US)","Yemen","Zambia","Zimbabwe"];

  public static mapStyles = [
  {
    "featureType": "administrative",
    "elementType": "geometry",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "poi",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "road",
    "elementType": "labels.icon",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "transit",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  }
];

}

import { Injectable, EventEmitter } from '@angular/core';

/**
 * this class stubs out ng2-dragula's DragulaService for loading server-side
 * you don't need any of this functionality on the server
 */
@Injectable()
export class DragulaService {
  private drakeFactory;
  private dispatch$;
  private elContainerSource;
  cancel: EventEmitter<any> = new EventEmitter();
  cloned: EventEmitter<any> = new EventEmitter();
  drag: EventEmitter<any> = new EventEmitter();
  dragend: EventEmitter<any> = new EventEmitter();
  drop: EventEmitter<any> = new EventEmitter();
  out: EventEmitter<any> = new EventEmitter();
  over: EventEmitter<any> = new EventEmitter();
  remove: EventEmitter<any> = new EventEmitter();
  shadow: EventEmitter<any> = new EventEmitter();
  dropModel: EventEmitter<any> = new EventEmitter();
  removeModel: EventEmitter<any> = new EventEmitter();
  private groups;
  
  add(name: string, drake: any): any {}
  find(name: string): any {}
  destroy(name: string): void {}
  setOptions(name: string, options: any): void {}
  createGroup(name: string, options: any): void {}
  // handleModels(name: string, options: any): void {}
  handleModels({}): void {};
  // handleModels({name, drake, options});

  /** Public mainly for testing purposes. Prefer `createGroup()`. */
  /**
   * Creates a group with the specified name and options.
   *
   * Note: formerly known as `setOptions`
   */
  // createGroup<T = any>(name: string, options: DragulaOptions<T>): Group;
  private setupEvents(group): void {};
  private domIndexOf(child, parent): void {};
}

import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'round2Dec'
})
export class Round2DecPipe implements PipeTransform {

  transform(value: any): any {
    return Math.round(value * 100) / 100;
  }

}

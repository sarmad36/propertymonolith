import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'mapCurrency'
})
export class MapCurrencyPipe implements PipeTransform {

  transform(price: any): any {
    price = Math.round(price);
    if (price >= 1000 && price < 100000) {
        price = price / 1000;
        price = Math.round(price * 100) / 100
        return price + " K";
    }
    else if (price >= 100000 && price < 10000000) {
        price = price / 100000;
        price = Math.round(price * 100) / 100
        return price + " L";
    } else if(price >= 10000000) {
        price = price / 10000000;
        price = Math.round(price * 100) / 100
        return price + " C";
    }
    return price;
  }

}

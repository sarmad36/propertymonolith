import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Custom Pipes
import { SafeUrlPipe } from './safe-url/safe-url.pipe';
import { CapitalizePipe } from './capitalize/capitalize.pipe';
import { ConvertCurrencyPipe } from './convertCurrency/convert-currency.pipe';
import { Round2DecPipe } from './round/round-2-dec.pipe';
import { MapCurrencyPipe } from './mapCurrency/map-currency.pipe';
import { SafeStylePipe } from './safe-style/safe-style.pipe';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    SafeUrlPipe,
    CapitalizePipe,
    ConvertCurrencyPipe,
    Round2DecPipe,
    MapCurrencyPipe,
    SafeStylePipe
  ],
  exports: [
    SafeUrlPipe,
    CapitalizePipe,
    ConvertCurrencyPipe,
    Round2DecPipe,
    MapCurrencyPipe,
    SafeStylePipe
  ]
})
export class ApplicationPipesModule { }

import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'capitalize'
})
export class CapitalizePipe implements PipeTransform {

  transform(s: any): any {
    if (typeof s !== 'string') return ''

    s = s.split('_');
    if (typeof s[1] !== 'undefined')
      return s[0].charAt(0).toUpperCase() + s[0].slice(1) + " " + s[1].charAt(0).toUpperCase() + s[1].slice(1);
    else
      return s[0].charAt(0).toUpperCase() + s[0].slice(1);
  }

}

import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'convertCurrency'
})
export class ConvertCurrencyPipe implements PipeTransform {

  transform(price: any): any {
    price = Math.round(price);
    if (price >= 100000 && price < 10000000) {
        price = price / 100000;
        price = Math.round(price * 100) / 100
        return price + " lac";
    } else if(price >= 10000000) {
        price = price / 10000000;
        price = Math.round(price * 100) / 100
        return price + " crore";
    }
    return price;
  }

}

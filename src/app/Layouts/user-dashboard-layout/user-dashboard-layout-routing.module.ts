import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AppSettings } from '../../services/_services';
import { AuthGuard } from '../../services/_helpers';

// User Panel Pages
import { UserDashboardComponent } from '../../UserPanel/pages/user-dashboard/user-dashboard.component';
import { UserListingComponent } from '../../UserPanel/pages/user-listing/user-listing.component';
import { UserLeadsComponent } from '../../UserPanel/pages/user-leads/user-leads.component';
import { UserCreditsComponent } from '../../UserPanel/pages/user-credits/user-credits.component';
import { UserLikesComponent } from '../../UserPanel/pages/user-likes/user-likes.component';
import { UserReportsComponent } from '../../UserPanel/pages/user-reports/user-reports.component';
import { UserAccountComponent } from '../../UserPanel/pages/user-account/user-account.component';
import { UserComplaintsComponent } from '../../UserPanel/pages/user-complaints/user-complaints.component';
import { EditPropertyComponent } from '../../UserPanel/pages/edit-property/edit-property.component';

let routes : Routes = [
  { path: '',                      component: UserDashboardComponent },
  { path: 'listing',               component: UserListingComponent },
  { path: 'leads',                 component: UserLeadsComponent },
  { path: 'credits',               component: UserCreditsComponent },
  { path: 'likes',                 component: UserLikesComponent },
  { path: 'reports',               component: UserReportsComponent },
  { path: 'account',               component: UserAccountComponent },
  { path: 'compliants',            component: UserComplaintsComponent },
  { path: 'edit-property/:slug',   component: EditPropertyComponent },
];

// Set Routes
export const UserDashboardLayoutRoutingModule = routes;

// @NgModule({
//   imports: [RouterModule.forChild(routes)],
//   exports: [RouterModule]
// })
// export class UserDashboardLayoutRoutingModule { }

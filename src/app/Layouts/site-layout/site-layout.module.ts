import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
// import { SiteLayoutComponent } from './site-layout.component';

// Plugins
import { NgSelectModule } from '@ng-select/ng-select';
import { ShareModule } from '@ngx-share/core';
import { OwlModule } from 'ngx-owl-carousel';
import { ShareButtonsModule } from '@ngx-share/buttons';
import { DragulaModule } from 'ng2-dragula';

// import Scroll Spy
import { ScrollSpyModule } from '../../services/_services/scroll-spy/scroll-spy.module';

// import filepond module
import { FilePondModule, registerPlugin } from 'ngx-filepond';

// import and register filepond file type validation plugin
import FilePondPluginFileValidateType from 'filepond-plugin-file-validate-type';
import FilePondPluginFileValidateSize from 'filepond-plugin-file-validate-size';
import FilePondPluginImagePreview from 'filepond-plugin-image-preview';
registerPlugin(FilePondPluginFileValidateType);
registerPlugin(FilePondPluginFileValidateSize);
registerPlugin(FilePondPluginImagePreview);

// Pages
import { SiteLayoutRoutes } from './site-layout.routing';
import { HomeComponent } from '../../Site/pages/home/home.component';
import { AddPropertyComponent } from '../../Site/pages/add-property/add-property.component';
import { SearchListingsComponent } from '../../Site/pages/search-listings/search-listings.component';
import { ListingDetailComponent } from '../../Site/pages/listing-detail/listing-detail.component';
import { AgentProfileComponent } from '../../Site/pages/agent-profile/agent-profile.component';
import { ViewAllLocationsComponent } from '../../Site/pages/view-all-locations/view-all-locations.component';
import { FavoriteListingsComponent } from '../../Site/pages/favorite-listings/favorite-listings.component';
import { HiddenListingsComponent } from '../../Site/pages/hidden-listings/hidden-listings.component';
import { FindProComponent } from '../../Site/pages/find-pro/find-pro.component';
import { FindProResultsComponent } from '../../Site/pages/find-pro-results/find-pro-results.component';
import { ProjectsComponent } from '../../Site/pages/projects/projects.component';
import { BlueWorldCityComponent } from '../../Site/pages/blue-world-city/blue-world-city.component';
import { SkyparkoneComponent } from '../../Site/pages/skyparkone/skyparkone.component';
import { HotOfferComponent } from '../../Site/pages/hot-offer/hot-offer.component';
import { OverseasBlockComponent } from '../../Site/pages/overseas-block/overseas-block.component';
import { AboutUsComponent } from '../../Site/pages/about-us/about-us.component';
import { PaymentPlanComponent } from '../../Site/pages/payment-plan/payment-plan.component';
import { AgentRegistrationPageComponent } from '../../Site/pages/agent-registration-page/agent-registration-page.component';
import { BlogsComponent } from '../../Site/pages/blogs/blogs.component';
import { BlogsDetailComponent } from '../../Site/pages/blogs-detail/blogs-detail.component';
import { AcademyComponent } from '../../Site/pages/academy/academy.component';
import { ManageMyPropertyComponent } from '../../Site/pages/manage-my-property/manage-my-property.component';
import { ContactComponent } from '../../Site/pages/contact/contact.component';
import { AuthorComponent } from '../../Site/pages/author/author.component';
import { BlogCategoryComponent } from '../../Site/pages/blog-category/blog-category.component';
import { WantedComponent } from '../../Site/pages/wanted/wanted.component';
import { ShortTermRentalsComponent } from '../../Site/pages/short-term-rentals/short-term-rentals.component';
import { SearchListingNewDesignComponent } from '../../Site/pages/search-listing-new-design/search-listing-new-design.component';
import { CapitalsmartcityComponent } from '../../Site/pages/capitalsmartcity/capitalsmartcity.component';
import { FindProDetailsComponent } from '../../Site/pages/find-pro-details/find-pro-details.component';
// import { PageNotFoundComponent } from '../../Site/pages/page-not-found/page-not-found.component';

// Injectable Components
import { RelatedPropertiesComponent } from '../../Site/entry-components/related-properties/related-properties.component';
import { EmailBasketComponent } from '../../Site/entry-components/email-basket/email-basket.component';
import { GMapsComponent } from '../../Site/includes/g-maps/g-maps.component';
import { GrowingCommunityComponent } from '../../Site/entry-components/growing-community/growing-community.component';

// Site Includes
import { SiteIncludesModule } from '../../Site/includes/site-includes.module';

// Custom Pipes
import { ApplicationPipesModule } from '../../services/pipes/application-pipes.module';

// Modal Components
import { BulkInquiryModalComponent } from '../../Site/entry-components/bulk-inquiry-modal/bulk-inquiry-modal.component';

// Bootsteap Components
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { ModalModule } from 'ngx-bootstrap/modal';
import { PopoverModule } from 'ngx-bootstrap/popover';

// // User Panel Pages
// import { UserPanelPagesModule } from '../../UserPanel/pages/userpanel-pages.module';

@NgModule({
  declarations: [
    HomeComponent,
    AddPropertyComponent,
    SearchListingsComponent,
    ListingDetailComponent,
    RelatedPropertiesComponent,
    EmailBasketComponent,
    GrowingCommunityComponent,
    GMapsComponent,
    AgentProfileComponent,
    ViewAllLocationsComponent,
    FavoriteListingsComponent,
    HiddenListingsComponent,
    // ScrollSpyDirective2,
    BulkInquiryModalComponent,
    FindProComponent,
    FindProResultsComponent,
    ProjectsComponent,

    // BlueWorldCityComponent,
    SkyparkoneComponent,
    CapitalsmartcityComponent,
    HotOfferComponent,
    OverseasBlockComponent,
    PaymentPlanComponent,
    // TawkToComponent,

    AboutUsComponent,
    AgentRegistrationPageComponent,
    BlogsDetailComponent,
    BlogsComponent,
    AcademyComponent,
    ManageMyPropertyComponent,
    ContactComponent,
    // PageNotFoundComponent,
    AuthorComponent,
    BlogCategoryComponent,
    WantedComponent,
    ShortTermRentalsComponent,
    SearchListingNewDesignComponent,
    FindProDetailsComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(SiteLayoutRoutes),
    FormsModule,
    ReactiveFormsModule,
    NgSelectModule,
    FilePondModule,
    ShareModule,
    OwlModule,
    ShareButtonsModule,

    // Custom Pipes Modules
    ApplicationPipesModule,

    ScrollSpyModule,

    // // User Panel Pages
    // UserPanelPagesModule,
    SiteIncludesModule,

    // Bootsteap Components
    BsDropdownModule.forRoot(),
    TooltipModule.forRoot(),
    ModalModule.forRoot(),
    PopoverModule.forRoot(),
    DragulaModule.forRoot()
  ],
  entryComponents: [
    BulkInquiryModalComponent
  ]
})
export class SiteLayoutModule { }

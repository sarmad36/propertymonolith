import { Routes } from '@angular/router';
import { AppSettings } from '../../services/_services';
import { AuthGuard } from '../../services/_helpers';

// import { UserLayoutComponent } from '../user-layout/user-layout.component';

import { HomeComponent } from '../../Site/pages/home/home.component';
import { AddPropertyComponent } from '../../Site/pages/add-property/add-property.component';
import { SearchListingsComponent } from '../../Site/pages/search-listings/search-listings.component';
import { ListingDetailComponent } from '../../Site/pages/listing-detail/listing-detail.component';
import { AgentProfileComponent } from '../../Site/pages/agent-profile/agent-profile.component';
import { ViewAllLocationsComponent } from '../../Site/pages/view-all-locations/view-all-locations.component';
import { FavoriteListingsComponent } from '../../Site/pages/favorite-listings/favorite-listings.component';
import { HiddenListingsComponent } from '../../Site/pages/hidden-listings/hidden-listings.component';
import { FindProComponent } from '../../Site/pages/find-pro/find-pro.component';
import { FindProResultsComponent } from '../../Site/pages/find-pro-results/find-pro-results.component';
import { ProjectsComponent } from '../../Site/pages/projects/projects.component';
import { BlueWorldCityComponent } from '../../Site/pages/blue-world-city/blue-world-city.component';
import { SkyparkoneComponent } from '../../Site/pages/skyparkone/skyparkone.component';
import { HotOfferComponent } from '../../Site/pages/hot-offer/hot-offer.component';
import { OverseasBlockComponent } from '../../Site/pages/overseas-block/overseas-block.component';
import { AboutUsComponent } from '../../Site/pages/about-us/about-us.component';
import { PaymentPlanComponent } from '../../Site/pages/payment-plan/payment-plan.component';
import { AgentRegistrationPageComponent } from '../../Site/pages/agent-registration-page/agent-registration-page.component';
import { BlogsComponent } from '../../Site/pages/blogs/blogs.component';
import { BlogsDetailComponent } from '../../Site/pages/blogs-detail/blogs-detail.component';
import { AcademyComponent } from '../../Site/pages/academy/academy.component';
import { ManageMyPropertyComponent } from '../../Site/pages/manage-my-property/manage-my-property.component';
import { ContactComponent } from '../../Site/pages/contact/contact.component';
import { AuthorComponent } from '../../Site/pages/author/author.component';
import { BlogCategoryComponent } from '../../Site/pages/blog-category/blog-category.component';
// import { PageNotFoundComponent } from '../../Site/pages/page-not-found/page-not-found.component';
import { WantedComponent } from '../../Site/pages/wanted/wanted.component';
import { ShortTermRentalsComponent } from '../../Site/pages/short-term-rentals/short-term-rentals.component';
import { SearchListingNewDesignComponent } from '../../Site/pages/search-listing-new-design/search-listing-new-design.component';
import { CapitalsmartcityComponent } from '../../Site/pages/capitalsmartcity/capitalsmartcity.component';
import { FindProDetailsComponent } from '../../Site/pages/find-pro-details/find-pro-details.component';

let routes : Routes = [
    // { path: '',                                              component: UserDashboardComponent },
    { path: '',                                                 component: HomeComponent },
    { path: 'add',                                              component: AddPropertyComponent },
    { path: 'search',                                           component: SearchListingsComponent },
    { path: 'search/:type',                                     component: SearchListingsComponent },
    { path: 'search/:type/:slug',                               component: SearchListingsComponent },
    { path: 'search/:type/:city/:slug1',                        component: SearchListingsComponent },
    { path: 'search/:type/:city/:slug1/:slug2/:slug3',          component: SearchListingsComponent },
    { path: 'search/:type/:city/:slug1/:slug2/:slug3/:slug4',   component: SearchListingsComponent },

    // Property Detail page
    { path: 'property',                                         component: ListingDetailComponent },
    { path: 'property/:slug',                                   component: ListingDetailComponent },
    // { path: 'property',                                      component: ListingDetail2Component },
    // { path: 'property/:slug',                                component: ListingDetail2Component },

    // Routes for Sale
    // { path: 'sale/:type',                                       component: SearchListingsComponent },
    // { path: 'sale/:type/:slug',                                 component: SearchListingsComponent },
    // { path: 'sale/:filter/:type/:slug',                         component: SearchListingsComponent },

    { path: 'sale/:type',                                       component: SearchListingNewDesignComponent },
    { path: 'sale/:type/:slug',                                 component: SearchListingNewDesignComponent },
    { path: 'sale/:filter/:type/:slug',                         component: SearchListingNewDesignComponent },

    // Routes for Rent
    // { path: 'rent/:slug',                                       component: SearchListingsComponent },
    // { path: 'rent/:type/:slug',                                 component: SearchListingsComponent },
    // { path: 'rent/:filter/:type/:slug',                         component: SearchListingsComponent },

    { path: 'rent/:slug',                                       component: SearchListingNewDesignComponent },
    { path: 'rent/:type/:slug',                                 component: SearchListingNewDesignComponent },
    { path: 'rent/:filter/:type/:slug',                         component: SearchListingNewDesignComponent },

    // // Routes for Investment Oppertunity / Projects
    { path: 'projects/:slug',                                   component: ProjectsComponent },
    // { path: 'blue-world-city',                                  component: BlueWorldCityComponent },
    // { path: 'blue-world-city/success',                          component: BlueWorldCityComponent },
    { path: 'blue-world-city/hot-offer',                        component: HotOfferComponent },
    // { path: 'blueworldcity/hot_offer',                          redirectTo: 'blue-world-city/hot-offer' },
    { path: 'blue-world-city/overseas-block',                   component: OverseasBlockComponent},
    // { path: 'blue_world_city/overseas_block',                   redirectTo: 'blue-world-city/overseas-block' },
    // { path: 'blueworldcity/overseas_block',                     redirectTo: 'blue-world-city/overseas-block' },
    // { path: 'blueworldcity',                                    redirectTo: 'blue-world-city' },
    // { path: 'blue_world_city',                                  redirectTo: 'blue-world-city' },
    { path: 'skyparkone',                                       component: SkyparkoneComponent },
    { path: 'skyparkone/payment-plan',                          component: PaymentPlanComponent },

    { path: 'capital-smart-city',                               component: CapitalsmartcityComponent },
    { path: 'capital-smart-city/success',                       component: CapitalsmartcityComponent },

    { path: 'locations/:city',                                  component: ViewAllLocationsComponent },
    { path: 'favorites',                                        component: FavoriteListingsComponent },
    { path: 'hidden',                                           component: HiddenListingsComponent },

    // Find-a-pro Routes
    { path: 'find-pro',                                         component: FindProComponent },
    { path: 'find-pro/:category_slug/:slug',                    component: FindProResultsComponent },

    // Routes for Agents / Agency Profile
    { path: 'agent',                                            component: AgentProfileComponent },
    { path: 'agent/:slug',                                      component: AgentProfileComponent },
    { path: 'agents',                                           component: FindProResultsComponent },
    { path: 'agents/:slug',                                     component: FindProResultsComponent },

    { path: 'interior-designer/:slug',                          component: FindProDetailsComponent },
    { path: 'architect/:slug',                                  component: FindProDetailsComponent },
    { path: 'contractors-builder/:slug',                        component: FindProDetailsComponent },
    { path: '3d-animator/:slug',                                component: FindProDetailsComponent },
    { path: 'home-insurance/:slug',                             component: FindProDetailsComponent },
    { path: 'home-automation/:slug',                            component: FindProDetailsComponent },
    { path: 'home-security/:slug',                              component: FindProDetailsComponent },

    { path: 'about-us',                                         component: AboutUsComponent },
    { path: 'agent-registration',                               component: AgentRegistrationPageComponent },

    // Blogging routes
    { path: 'blog',                                             component: BlogsComponent },
    { path: 'blog/:slug',                                       component: BlogsDetailComponent },

    { path: 'manage-my-property',                               component: ManageMyPropertyComponent },
    { path: 'academy',                                          component: AcademyComponent },
    { path: 'contact',                                          component: ContactComponent },
    { path: 'author/:slug',                                     component: AuthorComponent },
    { path: 'category/:slug',                                   component: BlogCategoryComponent },
    // { path: '**',                                            component: PageNotFoundComponent },

    // { path: 'wanted',                                        component: WantedComponent },
    // { path: 'short-term-rentals',                            component: ShortTermRentalsComponent },

    // // User Panel Routes
    // {
    //   path: 'dashboard',
    //   canActivate: [AuthGuard],
    //   children: [
    //     { path: '',                      component: UserDashboardComponent },
    //     { path: 'listing',               component: UserListingComponent },
    //     { path: 'leads',                 component: UserLeadsComponent },
    //     { path: 'credits',               component: UserCreditsComponent },
    //     { path: 'likes',                 component: UserLikesComponent },
    //     { path: 'reports',               component: UserReportsComponent },
    //     { path: 'account',               component: UserAccountComponent },
    //     { path: 'compliants',            component: UserComplaintsComponent },
    //     { path: 'edit-property/:slug',   component: EditPropertyComponent },
    //   ]
    // },
];

// Set Routes
export const SiteLayoutRoutes = routes;

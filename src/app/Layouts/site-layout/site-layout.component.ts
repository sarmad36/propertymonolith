import { Component, OnInit, ViewContainerRef, ChangeDetectorRef, AfterContentChecked } from '@angular/core';
import { AppSettings, HelperService } from '../../services/_services';
import { Router, NavigationEnd } from '@angular/router';

declare var Pace;

@Component({
  selector: 'app-site-layout',
  templateUrl: './site-layout.component.html',
  styleUrls: ['./site-layout.component.css']
})
export class SiteLayoutComponent implements OnInit, AfterContentChecked {

  // Subscriptions
  public routerSubscription : any;
  public showDashboard_Nav  : any = false;
  public map_togfooter      : any = false;
  public hideloader         : any = false;

  constructor(
    private cdr           : ChangeDetectorRef,
    private helperService : HelperService,
    public router         : Router,
  ){
      let searchTypes_Seq = {};
      let types           = Object.keys(AppSettings.searchTypes);
      let sub_types       = AppSettings.searchTypes;

      let id = 1;
      for (let i = 0; i < types.length; i++) {
          searchTypes_Seq[types[i]] = {id : id, type: 'type'};
          let subs = sub_types[types[i]];
          for (let j = 0; j < subs.length; j++) {
              id++;
              searchTypes_Seq[subs[j].value] = {id : id, type: 'sub_type', parent: types[i]};
          }
          id++;
      }

      this.helperService.setSearchTypes_Seq(searchTypes_Seq);

      this.routerSubscription = this.router.events.subscribe((val) => {
        if(val instanceof NavigationEnd) {
          if(val.url.indexOf('dashboard') !== -1)
            this.showDashboard_Nav = true;
          else
            this.showDashboard_Nav = false;
          console.log(val.url);
          if((val.url.indexOf('sale') === 1 || val.url.indexOf('rent') === 1 || (val.url.indexOf('find-pro') === 1 && val.url !== '/find-pro')) && (window.innerWidth > 768))
          {
            this.map_togfooter=true;
          }
          else{
            this.map_togfooter=false;
          }

   			}
	    });
  }

  ngAfterContentChecked() {
      // console.log("---- Detecting changes ----");
      // this.cdr.detectChanges();
  }

  ngOnInit() {
    // let self = this;
    // if(typeof Pace != "undefined") {
    //   console.log("Pace: ",Pace)
    //   Pace.on('start', function() {
    //       self.hideloader = false;
    //   });
    //
    //   Pace.on('stop', function() {
    //   });
    //
    //   Pace.on('restart', function() {
    //       self.hideloader = false;
    //   });
    //
    //   Pace.on('done', function() {
    //       self.hideloader = true;
    //   });
    // }
  }

  ngOnDestroy() {
    if (typeof this.routerSubscription != 'undefined') {
      //prevent memory leak when component destroyed
       this.routerSubscription.unsubscribe();
    }
  }

}

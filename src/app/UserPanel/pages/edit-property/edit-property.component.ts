import { Component, OnInit, ViewChild, TemplateRef, ElementRef, AfterViewInit  } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AlertService, AuthenticationService, AppSettings, UserService, HelperService } from '../../../services/_services';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { Router, ActivatedRoute } from "@angular/router";

// Alert Plugin
import Swal from 'sweetalert2';

// Social Login
import { AuthService } from "angularx-social-login";

// Bootsteap Components
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';


@Component({
  selector: 'app-edit-property',
  templateUrl: './edit-property.component.html',
  styleUrls: ['./edit-property.component.css']
})
export class EditPropertyComponent implements OnInit {

  public modalRef: BsModalRef;
  public isUserLoggedIn     : any = false;
  public loading            : any = false;
  public user :any = {
    'first_name'            : '',
    'last_name'             : '',
    'email'                 : '',
    'password'              : '',
    'phone_number'          : '',
    'dob'                   : null,
  }

  public propID         : any;
  public propPrev_Title : any = '';

  // Add Property ngModel
  public saveData : any = {
                            purpose            : "",
                            title              : null,
                            ip_address         : '',
                            type               : "",
                            sub_type           : "",
                            city               : "",
                            city_id            : null,
                            location_id        : "",
                            location_name      : "",
                            address            : "",
                            display_address    : false,
                            size               : "",
                            unit               : "",
                            price              : "",
                            price_text         : "",
                            description        : "",
                            property_feature   : {
                                                    year_built          : "",
                                                    bathrooms           : 0,
                                                    BathsText           : "No. of Bathrooms",
                                                    bedrooms            : 0,
                                                    BedsText            : "No. of Bedrooms",
                                                    parking_spaces      : 0,
                                                    ParkingText         : "Total Parking Space",
                                                    floors              : 0,
                                                    floorsText          : "No. of Floors",
                                                    down_payment        : "",
                                                    down_paymentText    : "",

                                                    security_deposit        : "",
                                                    security_depositText    : "",

                                                    maintenance_charges        : "",
                                                    maintenance_chargesText    : "",

                                                    central_heating     : false,
                                                    central_cooling     : false,
                                                    lift                : false,
                                                    public_parking      : false,
                                                    underground_parking : false,
                                                    internet            : false,
                                                    cctv_camera         : false,
                                                    backup_generator    : false,
                                                    electricity         : false,
                                                    gas                 : false,
                                                    maintenance         : false,
                                                    water               : false,
                                                    tv_lounge           : false,
                                                    dining_room         : false,
                                                    drawing_room        : false,
                                                    kitchen             : false,
                                                    store_room          : false,
                                                    lawn                : false,
                                                    swimming_pool       : false,
                                                    furnished           : false,
                                                    wifi                : false,
                                                    balcony             : false,
                                                    laundry_room        : false,
                                                    servant_quarter     : false,
                                                    dirty_kitchen       : false,
                                                    posession           : false,
                                                    corner              : false,
                                                    park_facing         : false,
                                                    boundary_wall       : false,
                                                    sewerage            : false,
                                                    north               : false,
                                                    north_east          : false,
                                                    south_west          : false,
                                                    east                : false,
                                                    north_west          : false,
                                                    south               : false,
                                                    south_east          : false,
                                                    west                : false,
                                                  },
                            images             : [],
                            contact            : [],
                          };
  // Contact Params
  public contact1     : any = "";
  public contact2     : any = "";
  public showContact2 : any = false;


  public additionalFeaturesSet : any = false;

  // FilePond Params
  @ViewChild('myPond', {static: false}) myPond: any;

  pondOptions = {
    class: 'my-filepond',
    multiple: true,
    labelIdle: '<span _ngcontent-uhm-c6="" class="fake-btn"><i _ngcontent-uhm-c6="" class="fa fa-plus"></i> Add Images here</span>',
    acceptedFileTypes: 'image/jpeg, image/png',
    imagePreviewTransparencyIndicator: 'grid',
    imagePreviewHeight: '150px',
    // maxFileSize: '5MB',
    imagePreviewMaxFileSize: '15MB',
    // stylePanelLayout: 'compact circle',
    server: {
        url: AppSettings.API_ENDPOINT,
        timeout: 7000,
        process: {
            url: './temp-images',
            method: 'POST',
            // headers: {
            //     'x-customheader': 'Hello World'
            // },
            withCredentials: false,
            onload: (response) => {
                let image = JSON.parse(response).temp_image;

                // Add image id to property image array
                this.propertyImages.push(image.id);

                return image.id;
            },
            onerror: (response) => {
                return response.key;
            },
            ondata: (formData) => {
                // formData.append('Hello', 'World');
                return formData;
            }
        },
        revert: (uniqueFileId, load, error) => {
            const url  = AppSettings.API_ENDPOINT + 'temp-images/' + uniqueFileId;
            this.httpDeleteRequests(url);

            // Remove image id from property image array
            var index = this.propertyImages.indexOf(uniqueFileId);
            if (index > -1) {
              this.propertyImages.splice(index, 1);
            }

            // Clear Image
            load();
        },
        load: (source, load, error, progress, abort, headers) => {
          var myRequest = new Request(source);
          // console.log("myRequest: ",myRequest);
          fetch(myRequest).then(function(response) {
            // console.log("response: ",response);
            response.blob().then(function(myBlob) {
              load(myBlob)
            });
          });
        },
        restore: null,
        fetch: null
    }
  }

  public pondFiles      = [];
  public propertyImages = [];

  public showPondPicker     = false;

  // Custum Params
  public showCustomTitle = false;
  public showResdAddOn   = false;
  public showPlotAddOn   = false;
  public showCommAddOn   = false;
  public submitted       = false;
  public disableArea     = true;

  // Https Params
  protected ngUnsubscribe: Subject<void> = new Subject<void>();
  public httpLoading     = false;
  public corsHeaders     : any;

  // City and Areas List
  public cities          :any = AppSettings.cities;
  public areas           :any = [];
  public searchTypes     :any = [];
  public searchSubTypes  :any = [];
  public SelectedArea_Main = [];

  public addForm : FormGroup;

  // Params
  public SelectedCity   = {"id": "", "name": ""};


  // Ng-Slect Params
  public areasBuffer  = [];
  public bufferSize   = 10;
  public numberOfItemsFromEndBeforeFetchingMore = 5;
  public areasLoading = false;
  public SelectedArea  :any;
  public areaQuery    = "";
  public areasError   = false;

  // Select Areas Tiers 1 params
  public areasTier1         = [];
  public areasTier1_buffer  = [];
  public SelectedArea_Tier1 = [];
  public areasTier1Loading  = false;
  public areasTier1Error    = false;

  // Select Areas Tiers 2 params
  public showAreasTier2     = false;
  public areasTier2         = [];
  public SelectedArea_Tier2 = [];
  public areasTier2Loading  = false;
  public areasTier2Error    = false;

  // Select Areas Tiers 3 params
  public showAreasTier3     = false;
  public areasTier3         = [];
  public SelectedArea_Tier3 = [];
  public areasTier3Loading  = false;
  public areasTier3Error    = false;

  // Select Areas Tiers 4 params
  public showAreasTier4     = false;
  public areasTier4         = [];
  public SelectedArea_Tier4 = [];
  public areasTier4Loading  = false;
  public areasTier4Error    = false;


  public showSuccess        = false;
  public showFailure        = false;
  public showFailureMsg     = "";

  // Subscriptions
  public userSubscription   : any;
  public routerSubscription : any;

  public searchUnits        :any = [];


  constructor(
      private http                  : HttpClient,
      private authenticationService : AuthenticationService,
      private fb                    : FormBuilder,
      private activatedRoute        : ActivatedRoute,
      private elementRef            : ElementRef,
      private helperService         : HelperService,
      private router                : Router,
  ) {
      // Set Cors Header with token if User Logged In
      this.userSubscription = this.authenticationService.currentUserS$.subscribe(user => {
        if (user) {
            var corsHeaders = {
                  headers: new HttpHeaders()
                    .set('Content-Type',  'application/json')
                    .set('Accept',  'application/json')
                    .set('Authorization',  `Bearer ${user.access_token}`)
                }

              // Set User
              this.user = user.user;
              this.isUserLoggedIn = true;

            // Submit Data After User Logged-In
            if (this.submitted)
              this.SaveData();

        } else {
          var corsHeaders = {
                headers: new HttpHeaders()
                  .set('Content-Type',  'application/json')
                  .set('Accept',  'application/json')
              }

            this.isUserLoggedIn = false;
        }
        this.corsHeaders = corsHeaders;
      });

      // Get Client IP
      this.saveData.ip_address = this.helperService.getIPAddress().then(resp => {
        this.saveData.ip_address = resp.ip;
      });

      // Get Globals from AppSettings
      this.searchTypes     = Object.keys(AppSettings.searchTypes);
      this.searchSubTypes  = AppSettings.searchTypes;
      this.searchUnits     = AppSettings.searchUnits;
  }

  ngOnInit() {
    // Set FormGroup
    this.addForm = this.fb.group({
            title: [null],
            type: [null, Validators.required],
            sub_type: [null, Validators.required],
            city: [null, Validators.required],
            area: [null],
            areasTier1: [null],
            areasTier2: [null],
            areasTier3: [null],
            areasTier4: [null],
            size: [null, Validators.required],
            unit: [null, Validators.required],
            price: [null, [Validators.required, Validators.max(999999999)]],
            contact1: [null, Validators.required],
        });

    // Disable Area Select
    this.addForm.controls.area.disable();
    this.addForm.controls.areasTier1.disable();

    // Subscribe to Routes
    this.routerSubscription = this.activatedRoute.params.subscribe(routeParams => {
      let slug = this.helperService.renderEditPropertyURL(routeParams.slug);

      // Set Property ID
      this.propID = parseInt(slug.id);

      // Get Property Details
      this.getPropertyDetails();
  	});
  }

  getPropertyDetails() {
    let url = AppSettings.API_ENDPOINT + 'property/' + this.propID;
    // console.log("this.propID : ", this.propID);
    this.helperService.httpGetRequests(url)
      .then(resp => {
        if (typeof resp != "undefined" && resp != null && typeof resp.error === "undefined") {
           this.saveData  = resp;

           // Set Prev Title
           this.propPrev_Title = this.saveData.title;
           // this.saveData.title = '';

           // Set selected City
           this.SelectedCity = {id: this.saveData.city_id, name: this.saveData.city};
           this.selectCity();

           // Set selected Area
           let search = { term: this.saveData.location_name };
           this.searchAreas(search);

           this.SelectedArea = { id: this.saveData.location_id, name: this.saveData.location_name };
           this.selectArea();

           // Set prop Size
           this.saveData.size = Math.round(this.saveData['size_in_' + this.saveData.unit] * 100) / 100;;

           // Set prop features
           if(this.saveData.feature)
            this.saveData.property_feature = this.saveData.feature;
           else
            this.resetPropertyFeatures();

            let unit = this.saveData.unit;
           // Switch type for Additional info tab
           this.switchType(this.saveData.type);

           // Reset Org Unit
           this.saveData.unit = unit;

           // Set Prop Images
           if(this.saveData.all_images && this.saveData.all_images != '') {
             this.saveData.all_images = this.saveData.all_images.split(',');

             // this.pondClicked();
             this.showPondPicker = true;

             for (let i = 0; i < this.saveData.all_images.length; i++) {
                 let img = AppSettings.IMG_ENDPOINT + 'uploads/property-images/thumbnails/' + this.saveData.all_images[i];
                 this.myPond.addFile(img, { type: 'local' });
             }

             // Remove irrelevent key
             delete this.saveData.all_images;
           }


           // Set Additional Features
           // $(".panel-collapse").slideToggle();

           // Set Text
           this.num_of_Baths(0);
           this.num_of_Beds(0);
           this.num_of_parking(0);
           this.num_of_floor(0);

           // Set Contacts
           if(this.saveData.contacts[0])
             this.contact1 = this.saveData.contacts[0]['phone_number']

           if(this.saveData.contacts[1]) {
             this.contact2 = this.saveData.contacts[1]['phone_number']
             this.showContact2 = true;
           }

        } else {
            console.error(resp);
        }
      })
      .catch(error => {
        console.log("error: ",error);
        // Set Submission false
        this.submitted = false;
      });
  }

  ngOnDestroy() {
    if (typeof this.userSubscription != 'undefined') {
      //prevent memory leak when component destroyed
       this.userSubscription.unsubscribe();
    }

    if (typeof this.routerSubscription != 'undefined') {
      //prevent memory leak when component destroyed
       this.routerSubscription.unsubscribe();
    }
  }

  setPurpose(arg) {
    this.saveData.purpose = arg;

    console.log("this.saveData.purpose: ",this.saveData.purpose)
  }

  convertPrice(FOR = "DP") {
    if (FOR == "price") {
        let price = this.saveData.price;
        if (price > 1000) {
          this.saveData.price_text = price;
        }
    } else if (FOR == "SD") {
        let price = this.saveData.property_feature.security_deposit;
        if (price > 1000) {
            this.saveData.property_feature.security_depositText = price;
        }
    } else if (FOR == "MC") {
        let price = this.saveData.property_feature.maintenance_charges;
        if (price > 1000) {
            this.saveData.property_feature.maintenance_chargesText = price;
        }
    } else {
        let price = this.saveData.property_feature.down_payment;
        if (price > 1000) {
            this.saveData.property_feature.down_paymentText = price;
        }
    }
  }

  toggleTitle(toggleShow: boolean) {
    this.showCustomTitle = toggleShow;
    this.saveData.title  = null;
  }

  toggleContact2(toggleContact2: boolean) {
    this.showContact2 = toggleContact2;
    this.contact2     = "";
  }

  resetPropertyFeatures() {
    this.saveData.property_feature = {
                            year_built          : "",
                            bathrooms           : 0,
                            BathsText           : "No. of Bathrooms",
                            bedrooms            : 0,
                            BedsText            : "No. of Bedrooms",
                            parking_spaces      : 0,
                            ParkingText         : "Total Parking Space",
                            floors              : 0,
                            floorsText          : "No. of Floors",
                            down_payment        : "",
                            down_paymentText    : "",
                            central_heating     : false,
                            central_cooling     : false,
                            lift                : false,
                            public_parking      : false,
                            underground_parking : false,
                            internet            : false,
                            cctv_camera         : false,
                            backup_generator    : false,
                            electricity         : false,
                            gas                 : false,
                            maintenance         : false,
                            water               : false,
                            tv_lounge           : false,
                            dining_room         : false,
                            drawing_room        : false,
                            kitchen             : false,
                            store_room          : false,
                            lawn                : false,
                            swimming_pool       : false,
                            furnished           : false,
                            wifi                : false,
                            balcony             : false,
                            laundry_room        : false,
                            servant_quarter     : false,
                            dirty_kitchen       : false,
                            posession           : false,
                            corner              : false,
                            park_facing         : false,
                            boundary_wall       : false,
                            sewerage            : false,
                            north               : false,
                            north_east          : false,
                            south_west          : false,
                            east                : false,
                            north_west          : false,
                            south               : false,
                            south_east          : false,
                            west                : false,
                          };
  }

  openAdditionalDetails(type) {
    // Reset Property Features on Type Change
    this.resetPropertyFeatures();

    // Switch type for Additional info tab
    this.switchType(type);

    this.saveData.sub_type = "";
  }

  switchType(type) {
    switch(type) {
      case "Homes":
        this.showResdAddOn = true;
        this.showPlotAddOn = false;
        this.showCommAddOn = false;
        this.saveData.type = type;
        this.saveData.unit = "marla";
        break;
      case "Plots":
        this.showResdAddOn = false;
        this.showPlotAddOn = true;
        this.showCommAddOn = false;
        this.saveData.type = type;
        this.saveData.unit = "marla";
        break;
      case "Commercial":
        this.showResdAddOn = false;
        this.showPlotAddOn = false;
        this.showCommAddOn = true;
        this.saveData.type = type;
        this.saveData.unit = "kanal";
        // this.saveData.unit = "feet";
        break;
      default:
        // text = "No value found";
    }
  }

  selectCity() {
    // Reset Area Selects
    this.addForm.get('area').patchValue([]);
    this.addForm.get('areasTier1').patchValue([]);
    this.areas       = [];
    this.areasBuffer = [];
    this.areasTier1  = [];

    if (this.SelectedCity.name != "") {
        let index = this.cities.findIndex(x => x.name === this.SelectedCity.name);
        this.SelectedCity = {"id": this.cities[index].id, "name": this.cities[index].name};

        this.saveData.city    = this.SelectedCity.name;
        this.saveData.city_id = this.SelectedCity.id;

        // Enable Area Search Select
        this.addForm.controls.area.enable();

        // Show Area Teir1 Loader
        this.areasTier1Loading = true;

        const url  = AppSettings.API_ENDPOINT + 'locations/get-parent-locations?city_id=' + this.SelectedCity.id;
        this.httpGetRequests(url).then(resp => {
          if(typeof resp != "undefined") {

            // Set Areas
            this.areasTier1 = resp.locations;

            // // Enable Area Tier1 Select
            // this.addForm.controls.areasTier1.enable();

            // Hide Area Teir1 Loader
            this.areasTier1Loading = false;
          }
        });
    } else {
        // Disable Area Selects
        this.addForm.controls.area.disable();
        this.addForm.controls.areasTier1.disable();

        // Remove from main model
        this.saveData.city = "";
    }
  }

  searchAreas(event: any) {
    // This aborts all HTTP requests.
    this.ngUnsubscribe.next();

    // Show Area Loader
    this.areasLoading = true;

    // // This completes the subject properlly.
    // this.ngUnsubscribe.complete();

    const url  = AppSettings.API_ENDPOINT + 'locations/get-all-locations?city_id=' + this.SelectedCity.id + "&search=" + event.term;
    this.httpGetRequests_AreaSearch(url).then(resp => {
      if(typeof resp != "undefined") {

        // Set Areas
        this.areas       = resp.locations;
        this.areasBuffer = this.areas.slice(0, this.bufferSize);

        // Hide Area Loader
        this.areasLoading = false;
      }
    });
  }

  selectArea() {
    if (this.SelectedArea != null) {
      // Set mian selected area for al Tiers
      this.SelectedArea_Main = this.SelectedArea;

      // Disable Area Tier1 Select
      this.addForm.controls.areasTier1.disable();

      // Remove Error Css
      if(this.submitted) {
          this.areasError = false;
          $(".af-area .ng-select-container").css("border-color", "");
      }
    } else {
      // Enable Area Tier1 Select
      this.addForm.controls.areasTier1.enable();

      // Clear mian selected area for al Tiers
      this.SelectedArea_Main = [];
    }
  }

  selectAreaTier1() {
    // Reset Lower Tiers Area Selection
    this.showAreasTier2 = false;
    this.showAreasTier3 = false;
    this.showAreasTier4 = false;
    this.addForm.get('areasTier2').patchValue([]);
    this.addForm.get('areasTier3').patchValue([]);
    this.addForm.get('areasTier4').patchValue([]);

    // Clear mian selected area for al Tiers
    this.SelectedArea_Main = [];

    if (this.SelectedArea_Tier1 != null) {
      // this.addForm.controls.area.enable();

      // Show Area Teir1 Loader
      this.areasTier1Loading = true;

      const url  = AppSettings.API_ENDPOINT + 'locations/get-sub-locations?p_id=' + this.SelectedArea_Tier1['id'];
      this.httpGetRequests(url).then(resp => {
        if(typeof resp != "undefined") {

          if (resp.locations.length > 0) {
            // Set Areas
            this.areasTier2  = resp.locations;

            // Show Tier 2 Area Selection
            this.showAreasTier2 = true;
          }
          else {
              // Set mian selected area for al Tiers
              this.SelectedArea_Main = this.SelectedArea_Tier1;
          }

          // Hide Area Teir1 Loader
          this.areasTier1Loading = false;

          // Remove Error Css
          if(this.submitted) {
              this.areasTier1Error = false;
              $(".areasTier1 .ng-select-container").css("border-color", "");

              // Remove Area Search Select Errors
              this.areasError = false;
              $(".af-area .ng-select-container").css("border-color", "");
          }
        }
      });

      // Disable Area Search Select
      this.addForm.controls.area.disable();
    } else {
      // Enable Area Search Select
      this.addForm.controls.area.enable();
    }
  }

  selectAreaTier2() {
    // Reset Lower Tiers Area Selection
    this.showAreasTier3 = false;
    this.showAreasTier4 = false;
    this.addForm.get('areasTier3').patchValue([]);
    this.addForm.get('areasTier4').patchValue([]);

    // Clear mian selected area for al Tiers
    this.SelectedArea_Main = [];

    if (this.SelectedArea_Tier2 != null) {
      // Show Area Teir2 Loader
      this.areasTier2Loading = true;

      const url  = AppSettings.API_ENDPOINT + 'locations/get-sub-locations?p_id=' + this.SelectedArea_Tier2['id'];
      this.httpGetRequests(url).then(resp => {
        if(typeof resp != "undefined") {
          if (resp.locations.length > 0) {
            // Set Areas
            this.areasTier3  = resp.locations;

            // Show Tier 3 Area Selection
            this.showAreasTier3 = true;
          } else {
              // Set mian selected area for al Tiers
              this.SelectedArea_Main = this.SelectedArea_Tier2;
          }

          // Hide Area Teir2 Loader
          this.areasTier2Loading = false;

          // Remove Error Css
          if(this.submitted) {
              this.areasTier2Error = false;
              $(".areasTier2 .ng-select-container").css("border-color", "");
          }
        }
      });
    }
  }

  selectAreaTier3() {
    // Reset Tier 4 Area Selection
    this.showAreasTier4 = false;
    this.addForm.get('areasTier4').patchValue([]);

    // Clear mian selected area for al Tiers
    this.SelectedArea_Main = [];

    if (this.SelectedArea_Tier3 != null) {
      // Show Area Teir3 Loader
      this.areasTier3Loading = true;

      const url  = AppSettings.API_ENDPOINT + 'locations/get-sub-locations?p_id=' + this.SelectedArea_Tier3['id'];
      this.httpGetRequests(url).then(resp => {
        if(typeof resp != "undefined") {

          if (resp.locations.length > 0) {
            // Set Areas
            this.areasTier4  = resp.locations;

            // Show Tier 3 Area Selection
            this.showAreasTier4 = true;
          } else {
              // Set mian selected area for al Tiers
              this.SelectedArea_Main = this.SelectedArea_Tier3;
          }

          // Hide Area Teir3 Loader
          this.areasTier3Loading = false;

          // Remove Error Css
          if(this.submitted) {
              this.areasTier3Error = false;
              $(".areasTier3 .ng-select-container").css("border-color", "");
          }
        }
      });
    }
  }

  selectAreaTier4() {
    // // Reset Tier 4 Area Selection
    // this.showAreasTier4 = false;
    // this.addForm.get('areasTier4').patchValue([]);

    // Clear mian selected area for al Tiers
    this.SelectedArea_Main = [];

    if (this.SelectedArea_Tier4 != null) {
      // Set mian selected area for al Tiers
      this.SelectedArea_Main = this.SelectedArea_Tier4;

      // Remove Error Css
      if(this.submitted) {
          this.areasTier4Error = false;
          $(".areasTier4 .ng-select-container").css("border-color", "");
      }
    }
  }


  //////////////////////////////////////////
  /********* HTTP Requests Fns ***********/
  ////////////////////////////////////////
  httpGetRequests_AreaSearch(url): Promise<any> {
      // Set loader true
      this.httpLoading = true;

      return this.http.get(url, this.corsHeaders)
          .pipe( takeUntil(this.ngUnsubscribe) )
          .toPromise()
          .then( resp => {
              // Set loader false
              this.httpLoading = false;

              return resp;
          })
          .catch(error => {
              // Set loader false
              this.httpLoading = false;
          });
    }

  httpGetRequests(url): Promise<any> {
      // Set loader true
      this.httpLoading = true;

      return this.http.get(url, this.corsHeaders)
          .toPromise()
          .then( resp => {
              // Set loader false
              this.httpLoading = false;

              return resp;
          })
          .catch(error => {
              // Set loader false
              this.httpLoading = false;
          });
    }

    httpPostRequests(url): Promise<any> {
        // Set loader true
        this.httpLoading = true;

        return this.http.post(url, this.saveData, this.corsHeaders)
            .pipe( takeUntil(this.ngUnsubscribe) )
            .toPromise()
            .then( resp => {
                // Set loader false
                this.httpLoading = false;

                return resp;
            })
            .catch(error => {
                // Set loader false
                this.httpLoading = false;

                // Show Error Msg
                if(typeof error.error != "undefined") {
                    this.showFailureMsg = error.error.message;
                } else {
                    this.showFailureMsg = "Something went wrong. Please try again.";
                }
                this.showFailure    = true;
            });
      }

    httpDeleteRequests(url): Promise<any> {
        // Set loader true
        this.httpLoading = true;

        return this.http.delete(url, this.corsHeaders)
            .pipe( takeUntil(this.ngUnsubscribe) )
            .toPromise()
            .then( resp => {
                // Set loader false
                this.httpLoading = false;

                return resp;
            })
            .catch(error => {
                // Set loader false
                this.httpLoading = false;
            });
      }

    //////////////////////////////////////////
    /********* Areas Ng-Select Fns *********/
    ////////////////////////////////////////
    onScrollToEnd_Areas() {
        this.fetchMore_Areas();
    }

    onScroll_Areas({ end }) {
        if (this.areasLoading || this.areas.length <= this.areasBuffer.length) {
            return;
        }

        if (end + this.numberOfItemsFromEndBeforeFetchingMore >= this.areasBuffer.length) {
            this.fetchMore_Areas();
        }
    }

    private fetchMore_Areas() {
        const len = this.areasBuffer.length;
        const more = this.areas.slice(len, this.bufferSize + len);
        this.areasLoading = true;
        // using timeout here to simulate backend API delay
        setTimeout(() => {
            this.areasLoading = false;
            this.areasBuffer  = this.areasBuffer.concat(more);
        }, 200)
    }

    //////////////////////////////////////////
    /********** FilePond Handler ***********/
    ////////////////////////////////////////
    pondHandleInit() {
    }

    pondHandleAddFile(event: any) {
      this.showPondPicker = true;
    }

    pondClicked() {
      this.myPond.browse();
    }

    deletePropImage(event: any) {
      console.log("Delete Image: ",event);
      console.log("Delete Image: ",event.file['filename']);

      let url = AppSettings.API_ENDPOINT + 'property/delete-image/' + event.file['filename'] + '/' + this.propID;
      this.helperService.httpDeleteRequests(url).then(resp => {
          console.log("resp: ",resp);
      }).catch(error => {
          console.log("error: ",error);
      })
    }

    //////////////////////////////////////////
    /******* Additional Features Fns *******/
    ////////////////////////////////////////
    num_of_Baths(args) {
      this.saveData.property_feature.bathrooms = this.saveData.property_feature.bathrooms + args;

      if (this.saveData.property_feature.bathrooms < 0) {
          this.saveData.property_feature.bathrooms = 0;
      }

      switch(this.saveData.property_feature.bathrooms) {
        case 0:
          this.saveData.property_feature.BathsText = "No. of Bathrooms";
          break;
        case 1:
          this.saveData.property_feature.BathsText = "1 Bathroom";
          break;
        default:
          this.saveData.property_feature.BathsText = this.saveData.property_feature.bathrooms + " Bathrooms";
          break;
      }
    }

    num_of_Beds(args) {
      this.saveData.property_feature.bedrooms = this.saveData.property_feature.bedrooms + args;

      if (this.saveData.property_feature.bedrooms < 0) {
          this.saveData.property_feature.bedrooms = 0;
      }

      switch(this.saveData.property_feature.bedrooms) {
        case 0:
          this.saveData.property_feature.BedsText = "No. of Bedrooms";
          break;
        case 1:
          this.saveData.property_feature.BedsText = "1 Bedroom";
          break;
        default:
          this.saveData.property_feature.BedsText = this.saveData.property_feature.bedrooms + " Bedrooms";
          break;
      }
    }

    num_of_parking(args) {
      this.saveData.property_feature.parking_spaces = this.saveData.property_feature.parking_spaces + args;

      if (this.saveData.property_feature.parking_spaces < 0) {
          this.saveData.property_feature.parking_spaces = 0;
      }

      switch(this.saveData.property_feature.parking_spaces) {
        case 0:
          this.saveData.property_feature.ParkingText = "Total Parking Space";
          break;
        case 1:
          this.saveData.property_feature.ParkingText = "1 Parking Space";
          break;
        default:
          this.saveData.property_feature.ParkingText = this.saveData.property_feature.parking_spaces + " Parking Spaces";
          break;
      }
    }

    num_of_floor(args) {
      this.saveData.property_feature.floors = this.saveData.property_feature.floors + args;

      if (this.saveData.property_feature.floors < 0) {
          this.saveData.property_feature.floors = 0;
      }

      switch(this.saveData.property_feature.floors) {
        case 0:
          this.saveData.property_feature.floorsText = "No. of Floors";
          break;
        case 1:
          this.saveData.property_feature.floorsText = "1 Floor";
          break;
        default:
          this.saveData.property_feature.floorsText = this.saveData.property_feature.floors + " Floors";
          break;
      }
    }


    //////////////////////////////////////////
    /************** Save Data **************/
    ////////////////////////////////////////

    // convenience getter for easy access to form fields
    get f() { return this.addForm.controls; }

    checkLocationErrors() {
      if (this.SelectedArea_Main.length == 0) {
        if (this.addForm.controls.area.enabled) {
            this.areasError = true;
            $(".af-area .ng-select-container").css("border-color", "#dc3545");
        } else if(this.showAreasTier4) {
            this.areasTier4Error = true;
            $(".areasTier4 .ng-select-container").css("border-color", "#dc3545");
        } else if(this.showAreasTier3) {
            this.areasTier3Error = true;
            $(".areasTier3 .ng-select-container").css("border-color", "#dc3545");
        } else if(this.showAreasTier2) {
            this.areasTier2Error = true;
            $(".areasTier2 .ng-select-container").css("border-color", "#dc3545");
        } else if(this.addForm.controls.areasTier1.enabled) {
            this.areasTier1Error = true;
            $(".areasTier1 .ng-select-container").css("border-color", "#dc3545");
        }

          return true;
      } else {
          return false;
      }
    }

    setPropertData() {
      // Set Location Details
      this.saveData.location_id   = this.SelectedArea_Main['id'];
      this.saveData.location_name = this.SelectedArea_Main['name'];

      // Set Contact Details
      this.saveData.contact = [];
      this.saveData.contact.push(this.contact1);
      if (this.contact2 != "") {
          this.saveData.contact.push(this.contact2);
      }

      // Set Images
      this.saveData.images = this.propertyImages;

      // if (this.saveData.property_feature.year_built == "" && this.saveData.property_feature.bathrooms == 0 && this.saveData.property_feature.bedrooms == 0 && this.saveData.property_feature.parking_spaces == 0 && this.saveData.property_feature.floors == 0 && this.saveData.property_feature.down_payment == ""
      //     && !this.saveData.property_feature.central_heating && !this.saveData.property_feature.central_cooling && !this.saveData.property_feature.lift && !this.saveData.property_feature.public_parking && !this.saveData.property_feature.underground_parking
      //     && !this.saveData.property_feature.internet && !this.saveData.property_feature.cctv_camera && !this.saveData.property_feature.backup_generator && !this.saveData.property_feature.electricity && !this.saveData.property_feature.gas && !this.saveData.property_feature.maintenance
      //     && !this.saveData.property_feature.water && !this.saveData.property_feature.tv_lounge && !this.saveData.property_feature.dining_room && !this.saveData.property_feature.drawing_room && !this.saveData.property_feature.kitchen && !this.saveData.property_feature.store_room
      //     && !this.saveData.property_feature.lawn && !this.saveData.property_feature.swimming_pool && !this.saveData.property_feature.furnished && !this.saveData.property_feature.wifi && !this.saveData.property_feature.balcony && !this.saveData.property_feature.laundry_room
      //     && !this.saveData.property_feature.servant_quarter && !this.saveData.property_feature.dirty_kitchen && !this.saveData.property_feature.posession && !this.saveData.property_feature.corner && !this.saveData.property_feature.park_facing && !this.saveData.property_feature.boundary_wall
      //     && !this.saveData.property_feature.sewerage && !this.saveData.property_feature.north && !this.saveData.property_feature.north_east && !this.saveData.property_feature.south_west && !this.saveData.property_feature.east && !this.saveData.property_feature.north_west
      //     && !this.saveData.property_feature.south && !this.saveData.property_feature.south_east && !this.saveData.property_feature.west )
      //     {
      //       // Remove Property Features Key
      //       delete this.saveData.property_feature;
      //     }

      // Set Property Features
      delete this.saveData.property_feature.BathsText;
      delete this.saveData.property_feature.BedsText;
      delete this.saveData.property_feature.ParkingText;
      delete this.saveData.property_feature.floorsText;
      delete this.saveData.property_feature.down_paymentText;
      delete this.saveData.property_feature.security_depositText;
      delete this.saveData.property_feature.maintenance_chargesText;
    }

    gotoTop() {
      let el = this.elementRef.nativeElement.querySelector('.radio-toolbar');
      el.scrollIntoView({behavior: "smooth", block: "start", inline: "nearest"});
    }

    SaveData() {
      // Hide previous failure messages
      this.hideFailureMsg();

      // Set Submission True
      this.submitted = true;
      let locErrors = this.checkLocationErrors();

      // Stop here if form is invalid and scroll to Top
      if (this.addForm.invalid || locErrors) {
          this.gotoTop();
          return;
      }

      // Set MAin Data if no Errors Occured
      this.setPropertData();

      // Show Loader
      this.loading = true;

      // Update Property Listing
      let url  = AppSettings.API_ENDPOINT + 'auth/property/' + this.propID;
      this.helperService.httpPutRequests(url, this.saveData).then(resp => {
          if (typeof resp != "undefined" && resp != null && typeof resp.error === "undefined" ) {

           Swal.fire('Property Updated', 'Property has been updated successfully.', 'success')
           .then((result) => {
              // Reload Page
              this.navigateRoute(this.propertyEditRoute(resp));
            });

           // Hide Loader
           this.loading = false;
         } else {
             console.error(resp);

             // Hide Loader
             this.loading = false;
         }
      }).catch(error => {
          console.log("error: ",error);

          // Set Text
          this.num_of_Baths(0);
          this.num_of_Beds(0);
          this.num_of_parking(0);
          this.num_of_floor(0);

          // Hide Loader
          this.loading = false;
      });

      // // Reset Property Features
      // if(typeof this.saveData.property_feature == "undefined")
      //   this.resetPropertyFeatures();
    }

    hideFailureMsg() {
      this.showFailure = false;
    }

    propertyEditRoute(property) {
      return this.helperService.propertyEditRoute(property);
    }

    navigateRoute(url: any) {
      this.router.navigateByUrl('/', {skipLocationChange: true}).then(()=>
        this.router.navigate([url]));
    }
}

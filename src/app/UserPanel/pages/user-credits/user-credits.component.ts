import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-user-credits',
  templateUrl: './user-credits.component.html',
  styleUrls: ['./user-credits.component.css']
})
export class UserCreditsComponent implements OnInit {
  public standardCredit     :any = 0;
  public silverCredit         :any = 0;
  public goldCredit         :any = 0;
  public platinumCredit     :any = 0;
  public refreshCredit      :any = 0;
  public allCredits         :any = 0;

  public credit             : any = { standard: '', gold: '',platinum: '', refresh: ''};
  constructor() { }

  ngOnInit() {

  }
  credits(param){
    console.log("this.standardCredit",this.standardCredit);
    switch(param){
      case "standard":
      this.standardCredit = this.credit.standard * 120;
      break;
      case "silver":
      this.silverCredit = this.credit.silver * 500;
      break;
      case "gold":
      this.goldCredit = this.credit.gold * 3000;
      break;
      case "platinum":
      this.platinumCredit = this.credit.platinum * 3000;
      break;
      case "refresh":
      this.refreshCredit = this.credit.refresh * 50;
      break;
      default:
      break;
    }
    // this.allCredits = this.standardCredit + this.goldCredit + this.platinumCredit + this.refreshCredit;
    this.allCredits = this.standardCredit + this.silverCredit + this.goldCredit + this.refreshCredit;
  }
}

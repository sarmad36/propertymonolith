import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

// Custom Pipes
import { ApplicationPipesModule } from '../../services/pipes/application-pipes.module';

// Plugins
import { DataTablesModule } from 'angular-datatables';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgxGalleryModule } from 'ngx-gallery';
import { ChartsModule } from 'ng2-charts';
import { BsDatepickerModule } from 'ngx-bootstrap';
import { Ng2TelInputModule } from 'ng2-tel-input';

// import filepond module
import { FilePondModule, registerPlugin } from 'ngx-filepond';

// import and register filepond file type validation plugin
import FilePondPluginFileValidateType from 'filepond-plugin-file-validate-type';
import FilePondPluginFileValidateSize from 'filepond-plugin-file-validate-size';
import FilePondPluginImagePreview from 'filepond-plugin-image-preview';
registerPlugin(FilePondPluginFileValidateType);
registerPlugin(FilePondPluginFileValidateSize);
registerPlugin(FilePondPluginImagePreview);

// User Panel Pages
import { UserDashboardComponent } from './user-dashboard/user-dashboard.component';
import { UserListingComponent } from './user-listing/user-listing.component';
import { UserLeadsComponent } from './user-leads/user-leads.component';
import { UserCreditsComponent } from './user-credits/user-credits.component';
import { UserLikesComponent } from './user-likes/user-likes.component';
import { UserReportsComponent } from './user-reports/user-reports.component';
import { UserAccountComponent } from './user-account/user-account.component';
import { UserComplaintsComponent } from './user-complaints/user-complaints.component';
import { EditPropertyComponent } from './edit-property/edit-property.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    ApplicationPipesModule,

    // Plugins
    FilePondModule,
    BsDatepickerModule.forRoot(),
    Ng2TelInputModule,
    NgSelectModule,
    DataTablesModule,
    ChartsModule,
  ],
  declarations: [
    UserDashboardComponent,
    UserListingComponent,
    UserLeadsComponent,
    UserCreditsComponent,
    UserLikesComponent,
    UserReportsComponent,
    UserAccountComponent,
    UserComplaintsComponent,
    EditPropertyComponent,
  ],
  exports: [
    UserDashboardComponent,
    UserListingComponent,
    UserLeadsComponent,
    UserCreditsComponent,
    UserLikesComponent,
    UserReportsComponent,
    UserAccountComponent,
    UserComplaintsComponent,
    EditPropertyComponent,
  ]
})
export class UserPanelPagesModule { }

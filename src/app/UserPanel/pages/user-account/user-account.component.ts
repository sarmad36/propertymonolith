import { Component, OnInit, ViewChild } from '@angular/core';
import { AuthenticationService, UserService, SEOService, AppSettings, HelperService, PurposeService, FavoriteService, HideService } from '../../../services/_services';
import { Ng2TelInputModule} from 'ng2-tel-input';

// Alert Plugin
import Swal from 'sweetalert2';

@Component({
  selector: 'app-user-account',
  templateUrl: './user-account.component.html',
  styleUrls: ['./user-account.component.css']
})
export class UserAccountComponent implements OnInit {

  public user       :any = [];
  public PakCity    :any = [];
  public country    :any = [];
  public authSub    :any;

  public hideCity   :any = true;
  public loading    :any = false;

  @ViewChild('agentImg', {static: false}) agentImg: any;

  public pond_Options = {
        acceptedFileTypes: 'image/jpeg, image/png',
        // labelIdle: `Drag & Drop your picture or <span class="filepond--label-action">Browse</span>`,
        labelIdle: `Upload profile picture`,
        imagePreviewHeight: 170,
        imageCropAspectRatio: '1:1',
        imageResizeTargetWidth: 100,
        imageResizeTargetHeight: 100,
        stylePanelLayout: 'compact circle',
        styleLoadIndicatorPosition: 'center bottom',
        styleButtonRemoveItemPosition: 'center bottom',

        styleProgressIndicatorPosition: 'center bottom',
        styleButtonProcessItemPosition: 'center bottom',

        server: {
            url: AppSettings.API_ENDPOINT,
            timeout: 7000,
            chunkUploads: true,
            process: {
                url: './temp-images',
                method: 'POST',
                withCredentials: false,
                onload: (response) => {
                    let image = JSON.parse(response).temp_image;

                    // Add image id to property image array
                    this.user.image = image.id;

                    return image.id;
                },
                onerror: (response) => {
                    console.log("response: ",response);
                    return response.key;
                },
                ondata: (formData) => {
                    formData.append('Hello', 'World');
                    return formData;
                }
            },
            revert: (uniqueFileId, load, error) => {
                const url  = AppSettings.API_ENDPOINT + 'temp-images/' + uniqueFileId;
                this.helperService.httpDeleteRequests(url);

                // Remove image id from property image array
                this.user.image = '';

                // Clear Image
                load();
            },
            restore: null,
            load: (source, load, error, progress, abort, headers) => {
              var myRequest = new Request(source);
              fetch(myRequest).then(function(response) {
                response.blob().then(function(myBlob) {
                  load(myBlob)
                });
              });
            },
            fetch: null
        }
      }

  public pondFiles = []

  constructor(
    private authenticationService: AuthenticationService,
    private helperService        : HelperService,
  ) {
    this.PakCity  = AppSettings.citiesDropDown;
    this.country  = AppSettings.country_list;
  }

  ngOnInit() {
    // Subscribe To User Local Login
    this.authSub = this.authenticationService.currentUserS$.subscribe(user => {
      if (user) {
          this.user = Object.assign({}, user.user)

          // Make links for local images
          if(this.user.image != null) {
            if (this.user.image.indexOf("https://") == -1 && this.user.image.indexOf("http://") == -1) {
                this.user.image = AppSettings.IMG_ENDPOINT + 'uploads/users/thumbnails/' + this.user.image;
            }

            // Load profile image
            this.pondFiles.push({
                                  source: this.user.image,
                                  options:{
                                    type: "local"
                                  }
                                });

          }

          console.log("this.user: ", this.user);
      }
    });
  }

  ngOnDestroy() {
    if (typeof this.authSub != 'undefined') {
      //prevent memory leak when component destroyed
      this.authSub.unsubscribe();
    }
  }

  pondHandleInit() {
    // console.log('FilePond has initialised agentImg', this.agentImg);
    // console.log('FilePond has initialised', this.myPond);
  }

  pondHandleAddFile(event: any) {
    // console.log('A file was added', event);
  }

  addImage() {
    $(".filepond--browser").click();
  }

  telInputObject(obj) {
    if(this.user.phone_country_code && this.user.phone_country_code != '')
      obj.setNumber(this.user.phone_country_code);
    // obj.setCountry('+92');
  }

  onCountryChange($event) {
    this.user.phone_country_code = $event.dialCode;
    let country = $event.name.split('(')[0];
    this.user.country = this.cleanString(country);

    this.selectCountry();
  }

  hasError($event){
    // console.log("hasError" ,$event);
    // this.hasErrorPhn = $event;
  }

  getNumber($event){
    console.log("getNumber" ,$event);
    // this.phnNumber = $event;
  }

  cleanString (str) {
    return str.trim(str.replace(/[^0-9a-z-A-Z ]/g, "").replace(/ +/, " "));
  }

  selectCountry(){
    if (this.user.country === "Pakistan")
      this.hideCity = true;
    else
      this.hideCity = false;

    this.user.city = null;
  }

  updateProfile() {
    // Show loader
    this.loading = true;

    let url  = AppSettings.API_ENDPOINT + 'auth/update-profile';
    let data = {
                  id                 : this.user.id,
                  first_name         : this.user.first_name,
                  last_name          : this.user.last_name,
                  email              : this.user.email,
                  phone_number       : this.user.phone_number,
                  phone_country_code : this.user.phone_country_code,
                  mobile_number      : this.user.mobile_number,
                  address            : this.user.address,
                  city               : this.user.city,
                  country            : this.user.country,
                  temp_id            : this.user.image
                }

    this.helperService.httpPostRequests(url, data).then(resp => {
      if (typeof resp != "undefined") {
          this.user = resp;

          console.log("this.user: ",this.user);

          // Update user local object
          let data  = this.authenticationService.get_currentUserValue();
          data.user = this.user;

          // Update User Data in Local Storage Session
          this.authenticationService.setCurrentUserValue(data);

          // Load profile image
          this.pondFiles = [];
          this.pondFiles.push({
                                source: this.user.image,
                                options:{
                                  type: "local"
                                }
                              });

          Swal.fire('Suucess', 'Profile updated successfully', 'success');
      }

      // Hide loader
      this.loading = false;
    }).catch(error => {
        // if(error.status === 404)
        //   Swal.fire('Error', '404 User not found', 'error');

        // Hide loader
        this.loading = false;
        console.error("error: ",error);
    })
  }

}

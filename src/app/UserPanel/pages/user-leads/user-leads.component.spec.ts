import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserLeadsComponent } from './user-leads.component';

describe('UserLeadsComponent', () => {
  let component: UserLeadsComponent;
  let fixture: ComponentFixture<UserLeadsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserLeadsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserLeadsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit, ViewChild } from '@angular/core';
import { AuthenticationService, AppSettings, HelperService } from '../../../services/_services';
import { DatePipe } from '@angular/common';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import { BaseChartDirective } from 'ng2-charts';
// import * as pluginDataLabels from 'chartjs-plugin-datalabels';
import { Label } from 'ng2-charts';

import { BsDaterangepickerDirective, BsDatepickerConfig } from 'ngx-bootstrap/datepicker';

import { Router, Route, ActivatedRoute } from "@angular/router";

@Component({
  selector: 'app-user-dashboard',
  templateUrl: './user-dashboard.component.html',
  styleUrls: ['./user-dashboard.component.css']
})
export class UserDashboardComponent implements OnInit {
  @ViewChild('dp_EndDate', { static: false }) dp_EndDate: BsDaterangepickerDirective;
  @ViewChild(BaseChartDirective, {static: false})
    public chart: BaseChartDirective;

  public showCharts : boolean = false;
  public ChartOptions = {
    scaleShowVerticalLines: false,
    responsive: true,
    scales: {
        xAxes: [{
            stacked: true
        }],
        yAxes: [{
            stacked: true,
            ticks: {
                beginAtZero: true
            }
        }]
    }
  };

  public ChartLabels  :any = [];
  public ChartType    :any = 'bar';
  public SelectedTab  :any = 'listings';
  public ChartLegend  :any = true;
  public ChartData    :any = [];
  public ChartColors  :any = [];

  public user  :any = {};
  public stats :any = {};

  public bsConfig   : Partial<BsDatepickerConfig>;
  public maxDate    : Date;
  public dateRange  : Date[];
  public tempInit   : any = 0;
  public startDate  : Date;
  public endDate    : Date = new Date();

  constructor(
    private authenticationService: AuthenticationService,
    private helperService: HelperService,
    private router: Router,
    private datePipe: DatePipe,
  ) {

  }

  ngOnInit() {
    if(typeof this.authenticationService.get_currentUserValue() != "undefined") {
      this.user = this.authenticationService.get_currentUserValue().user;
      // console.log("this.user: ",this.user);

      this.bsConfig = Object.assign({}, { containerClass: "theme-dark-blue", isAnimated: true, rangeInputFormat: 'YYYY-MM-DD', showWeekNumbers: false });
      this.maxDate  = new Date();

      let startData = new Date();
      startData.setDate(this.maxDate.getDate() - 7);

      this.startDate = startData;

      this.dateRange = [startData, this.maxDate];

      // Get user props listings
      this.get_User_Properties();
      //
      this.get_Dashboard_Stats(startData, this.maxDate);
    }
  }

  get_User_Properties() {
    const url  = AppSettings.API_ENDPOINT + 'auth/get-properties';
    this.helperService.httpPostRequests(url, {}).then(resp => {
      if (typeof resp.user != "undefined") {
          this.user = resp.user;

          let data  = this.authenticationService.get_currentUserValue();
          data.user = this.user;

          // Update User Data in Local Storage Session
          this.authenticationService.setCurrentUserValue(data);
      }
    }).catch(error => {
        // if(error.status === 404)
        //   Swal.fire('Error', '404 User not found', 'error');

        console.error("error: ",error);
    })
  }

  onDateChange(value, from) {
    if (value && value != "Invalid Date" && this.tempInit > 3) {
      if (from === "start") {
          this.startDate = value;

          // Get Stats
          this.get_Dashboard_Stats(value, this.endDate);

          let self = this;
          setTimeout(() => {
            self.dp_EndDate.show();
          }, 100);
      } else {
          // this.endDate = value;

          // Get Stats
          this.get_Dashboard_Stats(this.startDate, value);
      }
    } else {
        this.tempInit++; // Temp check for multiple date value change on start
    }

    // if (value) {
    //   console.log("value: ",value[0]);
    //   console.log("value: ",value[1]);
    //
    //   // // Update Dashboard Stats
    //   // this.get_Dashboard_Stats(value[0], value[1]);
    // }

  }

  get_Dashboard_Stats(start_date = this.startDate, end_date = this.endDate) {
    const url  = AppSettings.API_ENDPOINT + 'auth/get-stats';
    const data = { start_date: this.datePipe.transform(start_date, 'yyyy-MM-dd'), end_date: this.datePipe.transform(end_date, 'yyyy-MM-dd')}

    this.helperService.httpPostRequests(url, data).then(resp => {
      console.log("resp: ",resp);

      if (typeof resp != "undefined") {
          this.stats = resp;

          // Show Chart
          this.setChart_View();
      }
    })
  }

  set_SelectedTab(tab) {
    this.SelectedTab = tab;

    // Show Chart
    this.setChart_View();
  }

  set_ChartType(type) {
    this.ChartType = type;

    // Show Chart
    this.setChart_View();
  }

  setChart_View() {
    let type = this.SelectedTab;

    // Hide the Chart
    this.showCharts = false;

    this.ChartLabels = [];
    this.ChartData   = [];

    let backgroundColor = [];
    let borderColor     = [];

    // Set Rental Category Data
    let chartData_Rent = { data: [], label: 'Rent', backgroundColor: [], borderColor: [], hoverBackgroundColor: 'rgba(75, 192, 192, 1)', hoverBorderColor: 'rgba(75, 192, 192, 1)', pointBackgroundColor: "rgba(75, 192, 192, 1)", pointHoverBorderColor: "rgba(75, 192, 192, 0.8)", borderWidth: 1};

    let rentData = this.stats[type].rent;
    for (let i = 0; i < rentData.length; i++) {
        this.ChartLabels.push(rentData[i].date);

        // Push data in a Temp Object
        chartData_Rent.data.push(rentData[i].count);

        // Set Background color for easch label
        if (this.ChartType == 'bar') {
            chartData_Rent.backgroundColor.push('rgba(75, 192, 192, 1)');
        } else {
            chartData_Rent.backgroundColor.push('rgba(75, 192, 192, 0)');
        }

        // Set Border color for easch label
        chartData_Rent.borderColor.push('rgba(75, 192, 192, 1)');
    }

    // Set Sale Category Data
    let chartData_Sale = { data: [], label: 'Sale', backgroundColor: [], borderColor: [], hoverBackgroundColor:'rgba(239, 83, 80, 1)', hoverBorderColor: 'rgba(239, 83, 80, 1)', pointBackgroundColor: "rgba(239, 83, 80, 1)", pointHoverBorderColor: "rgba(239, 83, 80, 0.8)", borderWidth: 1};

    let saleData = this.stats[type].sale;
    for (let i = 0; i < saleData.length; i++) {
        // Push data in a Temp Object
        chartData_Sale.data.push(saleData[i].count);

        // Set Background color for easch label
        if (this.ChartType == 'bar') {
            chartData_Sale.backgroundColor.push('rgba(239, 83, 80, 1)');
        } else {
            chartData_Sale.backgroundColor.push('rgba(239, 83, 80, 0)');
        }

        // Set Border color for easch label
        chartData_Sale.borderColor.push('rgba(239, 83, 80, 1)');
    }

    // Set Rental and Sale Category Data in Chart
    this.ChartData.push(chartData_Rent);
    this.ChartData.push(chartData_Sale);

    // console.log("this.ChartData: ",this.ChartData);


    // this.ChartColors = [
    // {
    //   backgroundColor : backgroundColor,
    //   borderColor     : borderColor,
    //   // hoverBackgroundColor: ['rgba(63, 191, 127, 0.6)', 'rgba(63, 191, 191, 0.6)'],
    //   borderWidth: 1
    // }];
    // console.log("this.ChartColors: ",this.ChartColors);

    if(this.ChartData[0].data.length > 0) {
      // Show the hidden Chart
      this.showCharts = true;
    }

    // console.log("this.chart: ",this.chart);
    // setTimeout(() => {
    //       this.chart.chart.update()
    //   }, 1000);
  }

  propertyRoute(property) {
    return this.helperService.propertyRoute(property);
  }

  navigateRoute(url: any, params: any) {
    this.router.navigateByUrl('/', {skipLocationChange: true}).then(()=>
      this.router.navigate([url, params]));
  }

}

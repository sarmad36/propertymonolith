import { Component, OnInit } from '@angular/core';
import { AuthenticationService, AppSettings, HelperService } from '../../../services/_services';

@Component({
  selector: 'app-user-likes',
  templateUrl: './user-likes.component.html',
  styleUrls: ['./user-likes.component.css']
})
export class UserLikesComponent implements OnInit {

  public activeTab     : any = 'Listings';
  public loading       : any = false;
  public properties    : any = { data: [] };
  public ImagesUrl     :any = AppSettings.IMG_ENDPOINT;

  constructor(
    private helperService  : HelperService,
  ) {
    // Get User liked Property Listings
    this.getListing();
  }

  ngOnInit() {
  }

  setActiveTab(tab) {
    this.activeTab = tab;

    // Reset Listing for new tab
    this.getListing();
  }

  getListing() {
    // Show loader
    this.loading = true;

    // Clear previous properties
    this.properties = { data: [] };

    const url = AppSettings.API_ENDPOINT + "auth/get-user-likes";
    // let data  = { status: this.activeTab }
    this.helperService.httpPostRequests(url, {}).then(resp => {
        // console.log("resp ",resp);
        this.properties = resp;

        // Hide loader
        this.loading = false;
    }).catch(error => {
        console.error("error: ",error);

        // Hide loader
        this.loading = false;
    });
  }

  propertyRoute(property) {
    return this.helperService.propertyRoute(property);
  }

}

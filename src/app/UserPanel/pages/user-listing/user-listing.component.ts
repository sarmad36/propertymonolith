import { Component, OnInit, ViewChild } from '@angular/core';
import { AuthenticationService, AppSettings, HelperService } from '../../../services/_services';
import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';

// Alert Plugin
import Swal from 'sweetalert2';

@Component({
  selector: 'app-user-listing',
  templateUrl: './user-listing.component.html',
  styleUrls: ['./user-listing.component.css']
})
export class UserListingComponent implements OnInit {


  public Tabs :any = [
    { name: "Active Listings",       title: "Active Listings",   status: 'approved',   active: true,  img: 'acti-listings.png', sub_tab1_Title: 'Sale', sub_tab2_Title: 'Rent'   },
    { name: "Waiting Approval",      title: "Waiting Approval",  status: 'pending',    active: false, img: 'waiting-appr.png',  sub_tab1_Title: 'Sale', sub_tab2_Title: 'Rent'   },
    { name: "Expired or Deleted",    title: "Closed Listings",   status: 'expired',    active: false, img: 'expire-del.png',    sub_tab1_Title: 'Sale', sub_tab2_Title: 'Rent'   },
    { name: "Sold or Rented",        title: "Sold/Rent",         status: 'sold',       active: false, img: 'sold-rent.png',     sub_tab1_Title: 'Sold', sub_tab2_Title: 'Rented' },
  ]

  public activeTab     : any = this.Tabs[0];
  public sub_activeTab : any = 'sale';

  public loading       : any = false;
  public properties    : any = { data: [] };
  public query_ID      : any = '';

  @ViewChild(DataTableDirective, {static: false})
  public dtElement          : DataTableDirective;
  public dtTrigger          : any = new Subject();
  public dtOptions          : DataTables.Settings = {};

  public corsHeaders: any = new HttpHeaders({
    'Content-Type': 'application/json',
    'Accept': 'application/json',
  });;

  constructor(
    private helperService         : HelperService,
    private http                  : HttpClient,
    private authenticationService : AuthenticationService,
  ) {
      var lang = {
          "decimal":        "",
          "emptyTable":     "No data available in table",
          "info":           "Showing _START_ to _END_ of _TOTAL_ entries",
          "infoEmpty":      "Showing 0 to 0 of 0 entries",
          "infoFiltered":   "(filtered from _MAX_ total entries)",
          "infoPostFix":    "",
          "thousands":      ",",
          "lengthMenu":     "Show _MENU_ entries",
          "loadingRecords": "Loading...",
          "processing":     "Processing...",
          "search":         "Search:",
          "zeroRecords":    "No matching records found",
          "paginate": {
              "first":      "<<",
              "last":       ">>",
              "next":       ">",
              "previous":   "<"
          },
          "aria": {
              "sortAscending":  ": activate to sort column ascending",
              "sortDescending": ": activate to sort column descending"
          }
      }

      this.dtOptions = {
        pagingType: 'full_numbers',
        pageLength: 10,
        serverSide: true,
        processing: true,
        searching: false,
        lengthChange: false,
        'language': lang,
        "columnDefs": [
          // { "orderable": false,                       "targets": 8 },
          // { "orderable": false, "targets": 0 },
          // { "orderable": false,                       "targets": 7 },
          // { "orderable": false,                       "targets": 8 },
          { "orderable": false,                       "targets": 8 },
          // { "orderable": false, "visible"  : isAdmin, "targets": 11 },
        ],
        "order": [[ 0, "desc" ]],
        ajax: (dataTablesParameters: any, callback) => {
          // Show Loader
          this.loading = true;

          let url = AppSettings.API_ENDPOINT + "auth/get-user-properties";

          // Table Params
          dataTablesParameters.status      = this.activeTab.status;
          dataTablesParameters.purpose     = this.sub_activeTab;
          dataTablesParameters.property_id = this.query_ID;


          this.http.post(url, dataTablesParameters, this.helperService.getCORS_Headers()).toPromise()
            .then( resp => {
                this.properties = resp['data'];

                // Hide Loader
                this.loading = false;

                callback({
                  recordsTotal: resp['total'],
                  recordsFiltered: resp['total'],
                  // recordsFiltered: resp['recordsFiltered'],
                  // recordsFiltered: 1000,
                  data: []
                });

                // // $(".paginate_button .current").css("color", "white");
                // // console.log('$(".paginate_button .current"): ',$(".paginate_button .current"))
                $('div.dataTables_length select').addClass('dt-form-control');
                $('div.dataTables_filter input').addClass('dt-form-control');
            })
            .catch(error => {
                // Set loader false
                this.loading = false;

                // Rerender DT in case of failure
                // this.rerender();

                console.log("error: ",error);
            });
        },
        columns: [
          { data: 'id' },
          { data: 'title' },
          { data: 'location_name' },
          { data: 'price' },
          { data: 'property_view_count' },
          { data: 'search_view_count' },
          { data: 'all_images' },
          { data: 'created_at' },
          { data: 'action' },
        ],
        // Use this attribute to enable the responsive extension
        responsive: true
      }
  }

  ngOnInit() {
  }

  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      // Call the dtTrigger to rerender again
      this.dtTrigger.next();
    });
  }

  setActiveTab(index) {
    this.activeTab = this.Tabs[index];

    // Reset Listing
    // this.getListing();
    this.rerender();
  }

  set_Sub_ActiveTab(sub_tab) {
    this.sub_activeTab = sub_tab;

    // Reset Listing
    // this.getListing();
    this.rerender();
  }

  getListing() {
    // Show loader
    this.loading = true;

    // Clear previous properties
    this.properties = { data: [] };

    const url = AppSettings.API_ENDPOINT + "auth/get-user-properties";
    let data  = { status: this.activeTab.status, purpose: this.sub_activeTab, property_id: this.query_ID }
    this.helperService.httpPostRequests(url, data).then(resp => {
        // console.log("resp ",resp);
        this.properties = resp;

        // Hide loader
        this.loading = false;
    }).catch(error => {
        console.error("error: ",error);

        // Hide loader
        this.loading = false;
    });
  }

  serachByID() {
    // console.log("query_ID: ",this.query_ID);
    if (this.query_ID == "") {
        // this.getListing();

        this.rerender();
    }
  }

  propertyRoute(property) {
    return this.helperService.propertyRoute(property);
  }

  propertyEditRoute(property) {
    return this.helperService.propertyEditRoute(property);
  }

  setSoldRent(property) {
    Swal.fire({
      title: 'Are you sure?',
      text: 'Set property status to Sold/Rented',
      type: 'info',
      showCancelButton: true,
      confirmButtonText: 'Yes',
      cancelButtonText: 'Cancel'
    }).then((result) => {
      if (result.value) {
            //Show Loader
            this.loading = true;

           console.log("Pro Sold");

           let url  = AppSettings.API_ENDPOINT + 'auth/update-property-status';
           let data = { id: property.id, status: 'sold'};

           this.helperService.httpPostRequests(url, data)
             .then(resp => {
               if (typeof resp != "undefined" && resp != null && typeof resp.error === "undefined" ) {
                // Rerender DT for changes
                this.rerender();

                Swal.fire('Status Updated', 'Status has been sent successfully.', 'success');

                // Hide Loader
                this.loading = false;
              } else {
                  console.error(resp);

                  // Hide Loader
                  this.loading = false;
              }
            })
            .catch(error => {
              console.log("error: ",error);

              // Hide Loader
              this.loading = false;
            });
      } else if (result.dismiss === Swal.DismissReason.cancel) {
      }
    })
  }

  deleteProp(property) {
    Swal.fire({
      title: 'Are you sure?',
      text: 'Delete ' + property.title,
      type: 'error',
      showCancelButton: true,
      confirmButtonText: 'Yes',
      cancelButtonText: 'Cancel'
    }).then((result) => {
      if (result.value) {
           //Show Loader
           this.loading = true;

           let url  = AppSettings.API_ENDPOINT + 'auth/update-property-status';
           let data = { id: property.id, status: 'deleted'};

           this.helperService.httpPostRequests(url, data)
             .then(resp => {
               if (typeof resp != "undefined" && resp != null && typeof resp.error === "undefined" ) {
                // Rerender DT for changes
                this.rerender();

                Swal.fire('Status Updated', 'Status has been sent successfully.', 'success');

                // Hide Loader
                this.loading = false;
              } else {
                  console.error(resp);

                  // Hide Loader
                  this.loading = false;
              }
            })
            .catch(error => {
              console.log("error: ",error);

              // Hide Loader
              this.loading = false;
            });
      }
    })
  }

}

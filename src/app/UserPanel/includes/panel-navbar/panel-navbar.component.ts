import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { SEOService } from '../../../services/_services';

@Component({
  selector: 'app-panel-navbar',
  templateUrl: './panel-navbar.component.html',
  styleUrls: ['./panel-navbar.component.css']
})
export class PanelNavbarComponent implements OnInit {
  public menus :any = [
    { name: "Stats",           route: "/dashboard",                active: true  },
    { name: "Listings",            route: "/dashboard/listing",        active: false  },
    // { name: "Leads",               route: "/dashboard/leads",          active: false  },
    { name: "Saved Listings",               route: "/dashboard/likes",          active: false  },
    // { name: "Purchase Credits",         route: "/dashboard/credits",        active: false  },
    // { name: "Reports",             route: "/dashboard/reports",        active: false  },
    { name: "Profile",    route: "/dashboard/account",        active: false  },
    // { name: "Compliants",          route: "/dashboard/compliants",     active: false  },
  ]

  // Subscriptions
  public routerSubscription : any;

  constructor(
  		public router           : Router,
      private seoService      : SEOService,
      private activatedRoute  : ActivatedRoute,
  ) {
    this.routerSubscription = this.router.events.subscribe((val) => {
      if(val instanceof NavigationEnd) {
        this.setActiveMenu(val.url);
      }
    });
  }

  ngOnInit() {
    // Initial Set Route
    this.setActiveMenu(this.activatedRoute.snapshot['_routerState'].url);
  }

  ngOnDestroy() {
    if (typeof this.routerSubscription != 'undefined') {
      //prevent memory leak when component destroyed
       this.routerSubscription.unsubscribe();
    }
  }

  setActiveMenu(currentRoute) {
    for (let i = 0; i < this.menus.length; i++) {
        if(this.menus[i].route == currentRoute) {
          this.menus[i].active = true;

          // Set SEO Tags
          this.setSEO_Tags('User Panel', 'Manage User ' + this.menus[i].name);
        }
        else
          this.menus[i].active = false;
    }
  }

  setSEO_Tags(title: string, desc: string) {
      let self = this;
      setTimeout(function() {
        // Set Basic Meta Tags
        self.seoService.updateTitle(title);
        self.seoService.updateDescription(desc);

        // Set Og Meta tags
        self.seoService.updateOgTitle(title);
        self.seoService.updateOgDesc(desc);
        self.seoService.updateOgUrl();

        // Set Twitter Meta Tags
        self.seoService.updateTwitterTitle(title);
        self.seoService.updateTwitterDesc(desc);

        // Set Canonical Tag
        self.seoService.updateCanonicalUrl();
      }, 500);
    }

}
